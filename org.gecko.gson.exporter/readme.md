# Gson Generator for Ecore

This is a Acceleo based Gson Generator for ecore models.
It generates Google Gson compatible Gson entities and serializers from an ecore.

## Bnd Exporter

The bnd exporter has the name 'EcoreGson.exporter'. There are two mendatory attributes:

* **model.path** - The path to the Ecore model file
* **target.path** - The path to the folder where the code will be generated

Usage:
```
-pluginpath:\
	${.}/generated/org.gecko.gson.exporter.jar
-plugin.export:\
	org.gecko.gson.exporter.GsonExporter
	
-exporttype: EcoreGson.exporter;\
	model.path=test.ecore;\
	target.path=../MyGsonProject/src
	
```

