/**
 * 
 */
package org.gecko.gson.exporter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.common.util.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import aQute.bnd.build.Project;
import aQute.bnd.osgi.Resource;
import aQute.bnd.service.export.Exporter;
import gson.emtl.GenerateGson;

/**
 * @author mark
 *
 */
public class GsonExporter implements Exporter {
	
	public static final String EXPORTER_TYPE = "EcoreGson.exporter";
	public static final String MODEL_PATH = "model.path";
	public static final String TARGET_PATH = "target.path";
	private final static Logger logger = LoggerFactory.getLogger(GsonExporter.class);

	@Override
	public String[] getTypes() {
		return new String[] { EXPORTER_TYPE};
	}

	@Override
	public Entry<String, Resource> export(String type, Project project, Map<String, String> options) throws Exception {
		if (!EXPORTER_TYPE.equals(type)) {
			logger.error("The exporter type '{}’ does not match!", type);
			return null;
		}
		String modelPath = options.get(MODEL_PATH);
		if (modelPath == null) {
			logger.error("Please provide a 'model.path' where to load the ecore!");
			return null;
		}
		URI modelURI = URI.createFileURI(modelPath);
		
		String targetPath = options.get(TARGET_PATH);
		if (targetPath == null) {
			logger.error("Please provide a 'target.path', where to generate the Gson code!");
			return null;
		}
        File folder = new File(targetPath);
        List<String> arguments = new ArrayList<String>();
        GenerateGson generator = new GenerateGson(modelURI, folder, arguments);
        generator.doGenerate(new BasicMonitor());
		return null;
	}

}
