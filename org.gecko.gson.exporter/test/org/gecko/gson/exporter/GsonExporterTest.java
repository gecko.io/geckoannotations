package org.gecko.gson.exporter;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GsonExporterTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test()
	public void testExport_NoOptions() {
		GsonExporter exporter = new GsonExporter();
		try {
			assertNull(exporter.export(GsonExporter.EXPORTER_TYPE, null, Collections.emptyMap()));
		} catch (Exception e) {
			fail("Unexpected error " + e.getMessage());
		}
	}

	@Test()
	public void testExport_Options() throws Exception {
		GsonExporter exporter = new GsonExporter();
		Map<String, String> options = new HashMap<String, String>();
		options.put("model.path", "test.ecore");
		options.put("target.path", "generated/tmp");
		assertNull(exporter.export(GsonExporter.EXPORTER_TYPE, null, options));
		String test = "generated/tmp/src/test";
		Files.list(Paths.get(test)).forEach(p->{
			assertTrue(p.toFile().exists());
			if (!p.toFile().isDirectory()) {
				assertTrue(p.toString().endsWith("java"));
				try {
					testFile(p);
				} catch (IOException e) {
					fail("File cannot be tested");
				}
			}
		});

	}

	private void testFile(Path filePath) throws IOException {
		byte[] readAllBytes = Files.readAllBytes(filePath);
		String fileContent = new String(readAllBytes);
		assertTrue(fileContent.contains("package test"));
		assertTrue(fileContent.contains("public "));
		assertTrue(fileContent.contains("private"));
		assertTrue(Files.deleteIfExists(filePath));
	}

}
