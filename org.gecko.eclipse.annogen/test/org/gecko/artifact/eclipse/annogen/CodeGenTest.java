/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.eclipse.annogen;

import static org.junit.Assert.*;

import org.gecko.eclipse.annogen.FeatureAnnotations;
import org.junit.Test;

public class CodeGenTest {

	@Test
	public void testSimple() {
		String p = "org.eclipse.core.runtime";
		FeatureAnnotations fa = new FeatureAnnotations();
		assertEquals("org.eclipse.core.runtime.annotation", fa.getPackageSegments(p));
		assertEquals("CoreRuntimeAnnotation", fa.getFeatureName(p));
		assertEquals("org.eclipse.core.runtime.annotation.CoreRuntimeAnnotation", fa.getImportSegments(p));
	}
	
	@Test
	public void testDash() {
		String p = "org.eclipse.core.runtime-test";
		FeatureAnnotations fa = new FeatureAnnotations();
		assertEquals("org.eclipse.core.runtime.test.annotation", fa.getPackageSegments(p));
		assertEquals("CoreRuntimeTestAnnotation", fa.getFeatureName(p));
		assertEquals("org.eclipse.core.runtime.test.annotation.CoreRuntimeTestAnnotation", fa.getImportSegments(p));
	}
	
	@Test
	public void testPackage() {
		String p = "org.eclipse.core.package";
		FeatureAnnotations fa = new FeatureAnnotations();
		assertEquals("org.eclipse.core.p1ckage.annotation", fa.getPackageSegments(p));
		assertEquals("CoreP1ckageAnnotation", fa.getFeatureName(p));
		assertEquals("org.eclipse.core.p1ckage.annotation.CoreP1ckageAnnotation", fa.getImportSegments(p));
	}

}
