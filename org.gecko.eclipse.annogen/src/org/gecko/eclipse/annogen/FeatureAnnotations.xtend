/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.eclipse.annogen

import org.gecko.artifact.api.ArtifactConstants
import org.gecko.artifacts.EclipseFeature
import org.gecko.artifacts.ReferenceType
import java.util.Arrays
import java.util.stream.Collectors

/**
 * Creates a 
 * @author mark
 * @since 25.02.2020
 */
class FeatureAnnotations {
	
	def CharSequence convertFeature(EclipseFeature feature) {
		feature.toAnnotation
	}
	
	//def CharSequence convertFeature(Feature feature, EclipseResourceFileSystemAccess2 fsa) {
		
		
	//}
	
	def String getPackageSegments(String name) {
		var n = name.replace("-", ".").replace("package", "p1ckage");
		n += ".annotation";
		n;
	}
	
	def String getImportSegments(String name) {
		val n = name.packageSegments;
		return n + "." + name.featureName;
	}
	
	def String getFeatureName(String name) {
		val n = name.packageSegments;
		var String[] segs = n.split("\\.");
		if (segs.length > 2) {
			segs = Arrays.copyOfRange(segs, 2, segs.length);
		}
		segs.stream.map(t|t.toFirstUpper).collect(Collectors.joining(""));
	}
	
	def toAnnotation(EclipseFeature feature) {
		val incReq = feature.references.filter[r|ReferenceType.REQUIRE_ARTIFACT.equals(r.type) || ReferenceType.INCLUDE_ARTIFACT.equals(r.type)].filter[r|!r.name.endsWith("source")];
		val includes = incReq.filter[r|ArtifactConstants.P2_OSGI_BUNDLE.equals(r.filter)];
		val requires = incReq.filter[r|ArtifactConstants.P2_ECLIPSE_FEATURE.equals(r.filter)];
'''
package «feature.name.packageSegments»;

import static java.lang.annotation.ElementType.PACKAGE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.osgi.annotation.bundle.Requirement;

 «FOR require : requires»
import «require.name.importSegments»;
 «ENDFOR»

/**
 * Description:
 * «feature.label»
 *
 */
 «FOR require : requires»
 @«require.name.featureName»
 «ENDFOR»
 «FOR include : includes»
 @Requirement(namespace="osgi.identity", name="«include.name»")
 «ENDFOR»
 @Documented
 @Retention(CLASS)
 @Target({ TYPE, PACKAGE })
 public @interface «feature.name.featureName» {
 
 }
'''
}

	
}