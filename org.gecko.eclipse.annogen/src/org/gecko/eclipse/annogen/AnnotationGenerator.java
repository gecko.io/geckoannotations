/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.eclipse.annogen;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import org.gecko.artifact.api.ArtifactLoader;
import org.gecko.artifacts.EclipseFeature;
import org.gecko.artifacts.P2Repository;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.util.promise.PromiseFactory;

@Component(immediate = true)
public class AnnotationGenerator {
	
	private final static Logger logger = Logger.getLogger(AnnotationGenerator.class.getName());
	@Reference(target = "(artifactRepositoryType=cached)")
	private ArtifactLoader loader;
	private String projectPath = "/opt/git/geckoAnnotations/org.gecko.eclipse.feature/src-gen/";
	
	@Activate
	public void activate() {
		PromiseFactory pf = new PromiseFactory(Executors.newCachedThreadPool());
		try {
			pf.resolved(null).delay(100).onResolve(()->{
				try {
					String repoUri = "https://devel.data-in-motion.biz/repository/eclipse-2019-09";
//					String repoUri = "http://download.eclipse.org/releases/2019-12";
//					String repoUri = "https://devel.data-in-motion.biz/public/repository/gecko/release/geckoEMF.eclipse.tooling";
					logger.info("Loading Repo : '" + repoUri + "'");
					P2Repository repository = (P2Repository) loader.loadRepository(repoUri);
					logger.info("Repo : " + repository);
					logger.info("Repo parent: " + repository.getParent());
					logger.info("Repo size sub-repos: " + repository.getRepositories().size());
					logger.info("Repo Artifacts size: " + repository.getArtifacts().size());
					repository.getRepositories().forEach(r->logger.info("  Sub-Repo Artifacts size: " + r.getArtifacts().size()));
					logger.info("Repo All-Artifacts size: " + repository.getAllArtifacts().size());
					List<EclipseFeature> features = repository.getEclipseFeatures();
					logger.info("Repo Features size: " + features.size());
					features.stream().filter(f->!f.getName().endsWith("source")).forEach(this::generateFeatureAnnotation);
					logger.info("Repo Features generated annotation: " + features.size() + " to " + projectPath);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void generateFeatureAnnotation(EclipseFeature feature) {
		Path base = Paths.get(URI.create("file:" + projectPath));
		File baseFolder = base.toFile();
		FeatureAnnotations fa = new FeatureAnnotations();
		String segments = fa.getPackageSegments(feature.getName());
		String fileName = fa.getFeatureName(feature.getName()) + ".java";
		String path = segments.replace(".", "/");
		File sourceFolder = new File(baseFolder, path);
		if (!sourceFolder.exists()) {
			sourceFolder.mkdirs();
		}
		File javaFile = new File(sourceFolder, fileName);
		try (FileOutputStream fos = new FileOutputStream(javaFile)) {
			String featureAnnotation = fa.convertFeature(feature).toString();
			fos.write(featureAnnotation.getBytes());
			fos.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
