/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.p2.components;

import java.net.URI;
import java.util.List;
import java.util.logging.Logger;

import org.gecko.artifact.api.ArtifactLoader;
import org.gecko.artifact.api.ResolvedArtifactLoader;
import org.gecko.artifact.p2.P2RepositoryLoader;
import org.gecko.artifact.p2.helper.P2LoaderHelper;
import org.gecko.artifacts.ArtifactRepository;
import org.gecko.artifacts.EclipseFeature;
import org.gecko.artifacts.P2Repository;
import org.osgi.util.promise.Promise;

//@Component(scope = ServiceScope.PROTOTYPE, property = {"artifactRepositoryType=P2"})
public class P2ArtifactLoaderImpl implements ArtifactLoader, ResolvedArtifactLoader {

	private final static Logger logger = Logger.getLogger(P2ArtifactLoaderImpl.class.getName());
//	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED, target = "(repo_id=artifacts.artifacts)")
//	private EMFRepository repository;
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.artifact.api.ArtifactLoader#loadRepository(java.lang.String)
	 */
	@Override
	public ArtifactRepository loadRepository(String baseUri) {
		try {
			return loadRepositoryAsync(baseUri).getValue();
		} catch (Exception e) {
			if (e instanceof IllegalStateException) {
				throw (IllegalStateException)e;
			} else {
				throw new IllegalStateException("Error loading repository: ", e);
			}
		}
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.artifact.api.ArtifactLoader#loadRepositoryAsync(java.lang.String)
	 */
	@Override
	public Promise<ArtifactRepository> loadRepositoryAsync(String baseUri) {
		return loadRepository(baseUri, false);
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.artifact.api.ArtifactLoader#loadFeatures(java.lang.String)
	 */
	@Override
	public List<EclipseFeature> loadFeatures(String baseUri) {
		try {
			P2Repository repository = (P2Repository) loadRepository(baseUri);
			return repository.getEclipseFeatures();
		} catch (Exception e) {
			throw new IllegalStateException("Error loading all features: ", e);
		}
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.artifact.api.ResolvedArtifactLoader#loadResolvedRepository(java.lang.String)
	 */
	@Override
	public ArtifactRepository loadResolvedRepository(String baseUri) {
		try {
			return loadResolvedRepositoryAsync(baseUri).getValue();
		} catch (Exception e) {
			if (e instanceof IllegalStateException) {
				throw (IllegalStateException)e;
			} else {
				throw new IllegalStateException("Error loading and resolving srepository: ", e);
			}
		}
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.artifact.api.ResolvedArtifactLoader#loadResolvedRepositoryAsync(java.lang.String)
	 */
	@Override
	public Promise<ArtifactRepository> loadResolvedRepositoryAsync(String baseUri) {
		return loadRepository(baseUri, true);
	}
	
	/**
	 * Loads the repository for the given URI. If resolve is set to <code>true</code>,
	 * the data are fully resolved, by loading features and bundles
	 * @param baseUri the repository URI
	 * @param resolve set to <code>true</code>, to fetch all data
	 * @return the {@link Promise} for the {@link ArtifactRepository}
	 */
	private Promise<ArtifactRepository> loadRepository(String baseUri, boolean resolve) {
		try {
			P2RepositoryLoader loader = new P2RepositoryLoader(URI.create(baseUri));
//			loader.setSaveConsumer(this::saveEObject);
			return loader.getP2Repository()
					.map(r->(ArtifactRepository)r)
					.map(r->{
						if (resolve) {
							resolveFeatures(r, loader);
						}
						return r;
					})
					.map(loader::resolveReferences);
		} catch (Exception e) {
			throw new IllegalStateException("Error loading repository: ", e);
		}
	}
	
	/**
	 * Resolves all {@link EclipseFeature}
	 * @param repository the repository, that contains the features to be resolved
	 * @param loader the loader that resolves
	 */
	private void resolveFeatures(ArtifactRepository repository, P2RepositoryLoader loader) {
		try {
			repository.getAllArtifacts().stream()
					.filter(a->a instanceof EclipseFeature)
					.map(a->(EclipseFeature)a)
					.filter(f->!f.getName().endsWith("source"))
					.map(loader::resolveEclipseFeature)
					.map(P2LoaderHelper::safeGetValue)
					.forEach(f->logger.info("Feature: " + f.getName() + " (" + f.getLabel() + ")"));
		} catch (Exception e) {
			throw new IllegalStateException("Error loading all features: ", e);
		}
	}
	
//	private void saveEObject(EObject object) {
//		if (object == null) {
//			return;
//		}
//		Map<String, Object> saveProperties = new HashMap<String, Object>();
//		if (object instanceof ArtifactRepository) {
//			saveProperties.put(Options.OPTION_COLLECTION_NAME, ArtifactsPackage.Literals.ARTIFACT_REPOSITORY.getName());
//			repository.save(object, saveProperties);
//		} else if (object instanceof Artifact) {
//			saveProperties.put(Options.OPTION_COLLECTION_NAME, ArtifactsPackage.Literals.ARTIFACT.getName());
//			repository.save(object, saveProperties);
//		} else if (object instanceof ArtifactReference) {
//			saveProperties.put(Options.OPTION_COLLECTION_NAME, ArtifactsPackage.Literals.ARTIFACT_REFERENCE.getName());
//			repository.save(object, saveProperties);
//		}
//	}

	// TODO: class provided by template

}
