/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.p2.helper;

import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.gecko.artifact.api.ArtifactConstants;
import org.gecko.artifacts.Artifact;
import org.gecko.artifacts.ArtifactReference;
import org.gecko.artifacts.ArtifactsFactory;
import org.gecko.artifacts.BinaryArtifact;
import org.gecko.artifacts.BundleArtifact;
import org.gecko.artifacts.EclipseFeature;
import org.gecko.artifacts.FilterRule;
import org.gecko.artifacts.P2Repository;
import org.gecko.artifacts.Property;
import org.gecko.artifacts.ReferenceType;
import org.gecko.eclipse.EclipsePackage;
import org.gecko.eclipse.Feature;
import org.gecko.eclipse.Import;
import org.gecko.eclipse.Include;
import org.gecko.eclipse.Plugin;
import org.gecko.eclipse.Repository;
import org.gecko.eclipse.Rule;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.util.promise.Promise;

import aQute.bnd.util.dto.DTO;

/**
 * Helper class
 * @author Mark Hoffmann
 * @since 02.03.2020
 */
public class P2LoaderHelper implements ArtifactConstants {

	private final static Logger logger = Logger.getLogger(P2LoaderHelper.class.getName());

	/**
	 * @see aQute.p2.provider.P2Impl#P2Index
	 */
	public interface P2Index {
		public long	getModified();
		public void	setModified(long modified);
		public List<URI> getContent();
		public List<URI> getArtifacts();
	}

	/**
	 * @see aQute.p2.provider.P2Impl#P2Index
	 */
	private static class P2IndexImpl extends DTO implements P2Index {
		public long			modified;
		public List<URI>	content		= new ArrayList<>();
		public List<URI>	artifacts	= new ArrayList<>();

		/* 
		 * (non-Javadoc)
		 * @see org.gecko.artifact.impl.p2.P2Helper.P2Index#getModified()
		 */
		@Override
		public long getModified() {
			return modified;
		}

		/* 
		 * (non-Javadoc)
		 * @see org.gecko.artifact.impl.p2.P2Helper.P2Index#setModified(long)
		 */
		@Override
		public void setModified(long modified) {
			this.modified = modified;
		}

		/* 
		 * (non-Javadoc)
		 * @see org.gecko.artifact.impl.p2.P2Helper.P2Index#getArtifacts()
		 */
		@Override
		public List<URI> getArtifacts() {
			return artifacts;
		}

		/* 
		 * (non-Javadoc)
		 * @see org.gecko.artifact.impl.p2.P2Helper.P2Index#getContent()
		 */
		@Override
		public List<URI> getContent() {
			return content;
		}
	}

	/**
	 * Creates a new instance.
	 */
	public P2LoaderHelper() {
	}

	/**
	 * Resolves the slash at the end ot the URI problem
	 * @param base the URI to normalize
	 * @return the normalized URI
	 * @throws Exception
	 */
	public static URI normalize(URI base) throws Exception {
		if (base == null) {
			throw new IllegalArgumentException("Expected argument URI must not be null");
		}
		String path = base.getPath();
		if (path == null || 
				path.endsWith("/") || 
				path.endsWith(".xml") || 
				path.endsWith(".xml.xz") || 
				path.endsWith("p2.index"))
			return base;

		return new URI(base.toString() + "/");
	}

	public static void canonicalize(List<URI> artifacts) throws URISyntaxException {
		if (artifacts.size() < 2)
			return;
		for (URI uri : new ArrayList<>(artifacts)) {
			if (uri.getPath().endsWith(".xml")) {
				artifacts.remove(new URI(uri.toString() + ".xz"));
			}
		}
	}

	/**
	 * A exception safe {@link Promise#getValue()} call 
	 * @param <T> the type of the promise
	 * @param promise the promise
	 * @return the type instance or <code>null</code>, in case of an error
	 */
	public static <T> T safeGetValue(Promise<T> promise) {
		try {
			return promise.getValue();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error in safe getValue for promise. Returning null!", e);
			return null;
		}
	}

	/**
	 * Creates an {@link Filter} without throwing an exception
	 * @param filterString the filter string to create filter from
	 * @return the {@link Filter} instance or <code>null</code>
	 */
	public static Filter safeCreateFilter(String filterString) {
		try {
			return FrameworkUtil.createFilter(filterString);
		} catch (InvalidSyntaxException e) {
			logger.log(Level.SEVERE, String.format("Error creating Filter for invalid rule '%s'", filterString), e);
			return null;
		}
	}

	/**
	 * Creates a map with filters and the rule values
	 * @param rules the filter rule instances
	 * @param an additional filter to filter certain {@link FilterRule} filter-values
	 * @return the {@link Map} with {@link Filter} and rule value 
	 */
	public static Map<Filter, String> getFilterRule(List<FilterRule> rules, String filter) {
		Map<Filter, String> filters = rules.stream()
				.filter(r->filter == null || r.getFilter().contains(filter))
				.filter(r->safeCreateFilter(r.getFilter()) != null)
				.collect(Collectors.toMap(r->safeCreateFilter(r.getFilter()), FilterRule::getRule));
		return filters;
	}

	public static String getP2ArtifactType(Artifact artifact) {
		if (artifact == null) {
			throw new IllegalArgumentException("Artifact argument must not be null");
		}
		if (artifact instanceof EclipseFeature) {
			return P2_ECLIPSE_FEATURE;
		} else if (artifact instanceof BundleArtifact) {
			return P2_OSGI_BUNDLE;
		} else if (artifact instanceof BinaryArtifact) {
			return P2_BINARY;
		} else {
			throw new IllegalArgumentException("Unknown P2 artifact type " + artifact);
		}
	}

	/**
	 * Returns the filtered artifact path
	 * @param repository the {@link P2Repository} the artifact belongs to
	 * @param artifact the artifact proxy
	 * @param filterProperties the filter properties to check against
	 * @return the path as {@link String} or <code>null</code>
	 */
	public String getFilteredArtifactPath(Map<Filter, String> filterMap, URI repoBase, Artifact artifact, Dictionary<String, Object> filterProperties) {
		String path = filterMap.entrySet()
				.stream()
				.map(e->{
					if (e.getKey().match(filterProperties)) {
						return e.getValue();
					} else {
						return null;
					}
				})
				.filter(p->p != null)
				.findFirst()
				.map(p->p.replace("${repoUrl}/", repoBase.toString()))
				.map(p->p.replace("${id}", artifact.getId()))
				.map(p->p.replace("${version}", artifact.getVersion()))
				.orElse(null);
		return path;
	}

	/**
	 * Returns the filtered artifact path
	 * @param repository the {@link P2Repository} the artifact belongs to
	 * @param artifact the artifact proxy
	 * @param filterProperties the filter properties to check against
	 * @return the path as {@link String} or <code>null</code>
	 */
	public String getFilteredArtifactPath(P2Repository repository, Artifact artifact, Dictionary<String, Object> filterProperties) {
		String artifactType = getP2ArtifactType(artifact);
		Map<Filter, String> filters = P2LoaderHelper.getFilterRule(repository.getRules(), artifactType);
		URI repositoryBase = repository.getLocationUri();
		String path = filters.entrySet()
				.stream()
				.map(e->{
					if (e.getKey().match(filterProperties)) {
						return e.getValue();
					} else {
						return null;
					}
				})
				.filter(p->p != null)
				.findFirst()
				.map(p->p.replace("${repoUrl}/", repositoryBase.toString()))
				.map(p->p.replace("${id}", artifact.getId()))
				.map(p->p.replace("${version}", artifact.getVersion()))
				.orElse(null);
		if (path != null) {
			artifact.setSourceLocation(path);
		}
		return path;
	}

	public static P2Index createIndex() {
		return new P2IndexImpl();
	}

	public P2Index getDefaultIndex(URI base) {
		P2IndexImpl index = new P2IndexImpl();
		index.artifacts.add(base.resolve("compositeArtifacts.xml"));
		index.artifacts.add(base.resolve("artifacts.xml"));
		index.content.add(base.resolve("compositeContent.xml"));
		index.content.add(base.resolve("content.xml"));
		return index;
	}

	public Set<URI> setDefaults(P2Index index, Set<URI> defaults) {
		if (index == null || defaults == null) {
			return Collections.emptySet();
		}
		defaults.addAll(index.getArtifacts());
		defaults.addAll(index.getContent());
		return defaults;
	}

	/**
	 * Maps Eclipse model property into an artifact property
	 * @param prop Eclipse model property
	 * @return the artifact property
	 */
	Property mapProperty(org.gecko.eclipse.Property prop) {
		Property p = ArtifactsFactory.eINSTANCE.createProperty();
		p.setKey(prop.getName());
		p.setValue(prop.getValue());
		return p;
	}

	/**
	 * Maps Eclipse artifact repository mapping rule into anartifact {@link FilterRule}
	 * @param rule the mapping rule
	 * @return the {@link FilterRule} instance
	 */
	FilterRule mapRule(Rule rule) {
		FilterRule r = ArtifactsFactory.eINSTANCE.createFilterRule();
		r.setFilter(rule.getFilter());
		r.setRule(rule.getOutput());
		return r;
	}

	/**
	 * Updates the information of the Eclipse {@link Repository} into the {@link P2Repository} 
	 * @param repository the Eclipse repository
	 * @param p2repository the {@link P2Repository}
	 */
	public void updateP2Repository(Repository repository, P2Repository p2repository) {
		p2repository.setName(repository.getName());
		p2repository.setP2RepositoryType(repository.getType());
		p2repository.setVersion(repository.getVersion());
		if (repository.getProperties() != null) {
			repository.getProperties().getProperty()
			.stream()
			.map(this::mapProperty)
			.forEach(p2repository.getRepositoryProperties()::add);
		}
		if (repository.getMappings() != null) {
			repository.getMappings().getRule()
			.stream()
			.map(this::mapRule)
			.forEach(p2repository.getRules()::add);
		}
	}

	/**
	 * Appends an artifact from an Eclipse-ArtifactRepository entry
	 * @param repository the {@link P2Repository} to append the artifact to
	 * @param artifact the Eclipse-Repository artifact to transform an append
	 */
	public void appendArtifact(P2Repository repository, org.gecko.eclipse.Artifact artifact, Consumer<EObject> saveCallback) {
		Artifact a;
		String artifactType = null;
		switch (artifact.getClassifier()) {
		case P2_OSGI_BUNDLE:
			a = ArtifactsFactory.eINSTANCE.createBundleArtifact();
			artifactType = P2_OSGI_BUNDLE;
			break;
		case P2_ECLIPSE_FEATURE:
			a = ArtifactsFactory.eINSTANCE.createEclipseFeature();
			artifactType = P2_ECLIPSE_FEATURE;
			break;
		case P2_BINARY:
			a = ArtifactsFactory.eINSTANCE.createBinaryArtifact();
			artifactType = P2_BINARY;
			break;
		default:
			a = ArtifactsFactory.eINSTANCE.createArtifact();
			break;
		}
		Dictionary<String, Object> filterProperties = new Hashtable<String, Object>();
		if (artifactType != null) {
			filterProperties.put("classifier", artifactType);
		}
		// map Eclipse artifact into generic artifact
		mapArtifact(artifact, a);
		// Need to transform the artifacts properties first, otherwise the property is not in the map
		updateArtifactsSizeAndHash(a);
		// update feature specific information
		updateFeature(a, filterProperties);
		//updateBundle(a); NOT IMPLEMENTED YET
		//updateBinary(a); NOT IMPLEMENTED YET
		// calculate source path for the artifacts
		calculateArtifactSourcePath(repository, a, artifactType, filterProperties);
		if (saveCallback != null) {
			saveCallback.accept(a);
		}
		repository.getArtifacts().add(a);
	}

	/**
	 * Loads {@link Properties} from an input stream an sets it to the given object
	 * @param propertyStream the property stream
	 * @param object the object to set
	 * @param propertyFeature the attribute to be used
	 */
	public void loadAndSetProperties(InputStream propertyStream, EObject object, EAttribute propertyFeature) {
		if (propertyFeature == null || propertyStream == null || object == null) {
			logger.log(Level.WARNING, "Cannot load and set proeprties without mandatory parameters");
			return;
		}
		Properties p = new Properties();
		try {
			p.load(propertyStream);
			object.eSet(propertyFeature, p);
		} catch (Exception e) {
			logger.log(Level.SEVERE, String.format("Error loading properties for object %s and attribute %s", object, propertyFeature.getName()), e);
		}
	}

	/**
	 * Checks all {@link EAttribute} with type {@link String} or similar and tries to replace the current values
	 * by the ones from the properties file, if exists
	 * @param eObject the object to replace
	 * @param propertiesFeature the feature where to find the properties
	 * @return the replaced instance
	 */
	public EObject replaceProperties(EObject eObject, EAttribute propertiesFeature) {
		if (propertiesFeature == null) {
			logger.log(Level.WARNING, "Cannot replace values without properties");
			return eObject;
		}
		if (eObject == null) {
			logger.log(Level.WARNING, "Cannot replace values without EObject");
			return null;
		}
		Properties properties = (Properties) eObject.eGet(propertiesFeature);
		if (properties == null) {
			logger.log(Level.FINE, String.format("No properties found for feature %s", propertiesFeature.getName()));
			return eObject;

		}
		List<EAttribute> stringAttributes = eObject.eClass()
				.getEAllAttributes()
				.stream()
				.filter(a->a.getEType().getName().equals("String") || a.getEType().getName().equals("EString"))
				.collect(Collectors.toList());
		stringAttributes.stream().forEach(a->{
			Object o = eObject.eGet(a);
			if (o != null && o.toString().startsWith("%")) {
				String key = o.toString().replace("%", "");
				String value = properties.getProperty(key);
				eObject.eSet(a, value);
			}
		});
		return eObject;
	}

	/**
	 * Calculates the artifacts source path using the repository rule mapping in P2
	 * @param repository the {@link P2Repository}
	 * @param artifact the artifact to update the location for
	 * @param artifactType the {@link Artifact} type
	 * @param properties filter properties
	 */
	private void calculateArtifactSourcePath(P2Repository repository, Artifact artifact, String artifactType, Dictionary<String, Object> properties) {
		if (repository == null || artifact == null || properties == null) {
			logger.log(Level.WARNING, "Cannot create a path without mandatory parameters");
			return;
		}
		if (artifactType == null) {
			artifactType = getP2ArtifactType(artifact);
		}
		URI repositoryBase = repository.getLocationUri();
		// read the artifact repositoryProperty and take it, if available
		String path = artifact.getProperties().get(P2_PROP_ARTIFACT_REFERENCE);
		// map the path using the rules
		if (path == null) {
			Map<Filter, String> filterMap = P2LoaderHelper.getFilterRule(repository.getRules(), artifactType);
			path = filterMap.entrySet()
					.stream()
					.map(e->{
						if (e.getKey().match(properties)) {
							return e.getValue();
						} else {
							return null;
						}
					})
					.filter(p->p != null)
					.findFirst()
					.map(p->p.replace("${repoUrl}/", repositoryBase.toString() + "/"))
					.map(p->p.replace("${id}", artifact.getName()))
					.map(p->p.replace("${version}", artifact.getVersion()))
					.orElse(null);
		}
		if (path != null) {
			updateSourcePathLocation(artifact, path);
		} else {
			throw new IllegalStateException(String.format("Cannot create a path for %s, because repository has no rule mappings for features", artifact.getName()));
		}
	}

	/**
	 * Updates the source location for the artifact. For {@link EclipseFeature} the additional path for the 
	 * property file will be set as well
	 * @param artifact the artifact to update the source location for
	 * @param path the calculated path for the artifacts 
	 */
	private void updateSourcePathLocation(Artifact artifact, String path) {
		if (artifact == null || path == null) {
			logger.log(Level.WARNING, "Cannot update artifact source path without mandatory parameters");
			return;
		}
		if (artifact instanceof EclipseFeature && !((EclipseFeature)artifact).isJar()) {
			EclipseFeature feature = (EclipseFeature) artifact;
			String propertyPath = "feature.properties";
			if (!feature.isJar() && !feature.isPacked()) {
				path = path.replace(".jar", "/feature.xml");
				propertyPath = path.replace("feature.xml", "feature.properties");
			}
			feature.setPropertyLocation(propertyPath);
		}
		artifact.setSourceLocation(path);
	}

	/**
	 * Update basic information from the eclipse artifact into the generic artifact
	 * @param eclipseArtifact the eclipse artifact
	 * @param artifact the generic artifact
	 */
	private void mapArtifact(org.gecko.eclipse.Artifact eclipseArtifact, Artifact artifact) {
		artifact.setName(eclipseArtifact.getId());
		artifact.setVersion(eclipseArtifact.getVersion());
		if (eclipseArtifact.getProperties() != null) {
			eclipseArtifact.getProperties().getProperty()
			.stream()
			.map(this::mapProperty)
			.forEach(artifact.getArtifactProperties()::add);
		}
		if (eclipseArtifact.getRepositoryProperties() != null) {
			eclipseArtifact.getRepositoryProperties().getProperty()
			.stream()
			.map(this::mapProperty)
			.forEach(artifact.getArtifactProperties()::add);
		}
	}

	/**
	 * Update basic information from the eclipse artifact into the generic artifact
	 * @param eclipseArtifact the eclipse artifact
	 * @param artifact the generic artifact
	 */
	public void mapFeature(org.gecko.eclipse.Feature feature, EclipseFeature eclipseFeature, Consumer<EObject> saveCallback) {
		if (feature.getCopyright() != null) {
			String copyright = feature.getCopyright().getUrl();
			if (copyright != null && !copyright.isEmpty()) {
				copyright += System.lineSeparator();
				copyright += feature.getCopyright().getValue(); 
			} else {
				copyright = feature.getCopyright().getValue(); 
			}
			eclipseFeature.setCopyright(copyright);
		}
		if (feature.getDescription() != null) {
			String description = feature.getDescription().getUrl();
			if (description != null && !description.isEmpty()) {
				description += System.lineSeparator();
				description += feature.getDescription().getValue(); 
			} else {
				description = feature.getDescription().getValue(); 
			}
			eclipseFeature.setDescription(description);
		}
		addFeatureProperty(eclipseFeature, feature, EclipsePackage.Literals.FEATURE__ARCH);
		addFeatureProperty(eclipseFeature, feature, EclipsePackage.Literals.FEATURE__WS);
		addFeatureProperty(eclipseFeature, feature, EclipsePackage.Literals.FEATURE__OS);
		addFeatureProperty(eclipseFeature, feature, EclipsePackage.Literals.FEATURE__NL);
		addFeatureProperty(eclipseFeature, feature, EclipsePackage.Literals.FEATURE__NL);
		eclipseFeature.setProvider(feature.getProviderName());
		eclipseFeature.setImage(feature.getImage());
		eclipseFeature.setLabel(feature.getLabel());
		eclipseFeature.setName(feature.getId());
		eclipseFeature.setVersion(feature.getVersion());

		if (feature.getRequires() != null) {
			feature.getRequires().getImport().stream()
			.map(this::mapRequires)
			.map(r->attachSourceAndTarget(r, eclipseFeature, null))
			.map(r->interceptReferenceSave(r, saveCallback))
			.collect(Collectors.toCollection(()-> eclipseFeature.getReferences()));
		}
		feature.getIncludes().stream()
		.map(this::mapFeatureInclude)
		.map(r->attachSourceAndTarget(r, eclipseFeature, null))
		.map(r->interceptReferenceSave(r, saveCallback))
		.collect(Collectors.toCollection(()-> eclipseFeature.getReferences()));
		feature.getPlugin().stream()
		.map(this::mapBundleInclude)
		.map(r->attachSourceAndTarget(r, eclipseFeature, null))
		.map(r->interceptReferenceSave(r, saveCallback))
		.collect(Collectors.toCollection(()-> eclipseFeature.getReferences()));
		if (saveCallback != null) {
			saveCallback.accept(eclipseFeature);
		}
	}

	private ArtifactReference interceptReferenceSave(ArtifactReference reference, Consumer<EObject> consumer) {
		if (consumer != null) {
			consumer.accept(reference);
		}
		return reference;
	}

	/**
	 * Sets the source and or target artifact to the reference
	 * @param reference the reference to attach target or source
	 * @param source the source, can be <code>null</code>
	 * @param target the target, can be <code>null</code>
	 * @return the reference or <code>null</code>
	 */
	private ArtifactReference attachSourceAndTarget(ArtifactReference reference, Artifact source, Artifact target) {
		if (reference == null) {
			return null;
		}
		if (source != null) {
			reference.setSource(source);
			reference.setSourceName(source.getName());
			reference.setSourceVersion(source.getVersion());
		}
		if (target != null) {
			reference.getTarget().add(target);
		}
		return reference;
	}

	/**
	 * Maps an include feature into a {@link ArtifactReference}
	 * @param dependency the feature include
	 * @return the mapped reference
	 */
	private ArtifactReference mapRequires(Import dependency) {
		ArtifactReference reference = ArtifactsFactory.eINSTANCE.createArtifactReference();
		reference.setType(ReferenceType.REQUIRE_ARTIFACT);
		String name = dependency.getFeature();
		String filter = P2_ECLIPSE_FEATURE;
		if (dependency.getPlugin() != null) {
			name = dependency.getPlugin();
			filter = P2_OSGI_BUNDLE;
		}
		reference.setName(name);
		reference.setVersion(dependency.getVersion());
		reference.setFilter(filter);
		reference.setTargetName(name);
		reference.setTargetVersion(dependency.getVersion());
		addReferenceProperty(reference, dependency, EclipsePackage.Literals.IMPORT__MATCH, PROP_TARGET_VERSION_HINT);
		return reference;
	}

	/**
	 * Maps an include feature into a {@link ArtifactReference}
	 * @param include the feature include
	 * @return the mapped reference
	 */
	private ArtifactReference mapFeatureInclude(Include include) {
		ArtifactReference reference = ArtifactsFactory.eINSTANCE.createArtifactReference();
		reference.setType(ReferenceType.INCLUDE_ARTIFACT);
		reference.setFilter(P2_ECLIPSE_FEATURE);
		reference.setName(include.getId());
		reference.setLabel(include.getName());
		reference.setVersion(include.getVersion());
		reference.setLocation(include.getSearchLocation());
		addReferenceProperty(reference, include, EclipsePackage.Literals.INCLUDE__ARCH);
		addReferenceProperty(reference, include, EclipsePackage.Literals.INCLUDE__OS);
		addReferenceProperty(reference, include, EclipsePackage.Literals.INCLUDE__WS);
		addReferenceProperty(reference, include, EclipsePackage.Literals.INCLUDE__NL);
		return reference;
	}

	/**
	 * Maps an include bundle into a {@link ArtifactReference}
	 * @param plugin the bundle include
	 * @return the mapped reference
	 */
	private ArtifactReference mapBundleInclude(Plugin plugin) {
		ArtifactReference reference = ArtifactsFactory.eINSTANCE.createArtifactReference();
		reference.setType(ReferenceType.INCLUDE_ARTIFACT);
		reference.setFilter(P2_OSGI_BUNDLE);
		reference.setName(plugin.getId());
		reference.setVersion(plugin.getVersion());
		addReferenceProperty(reference, plugin, EclipsePackage.Literals.PLUGIN__FRAGMENT);
		addReferenceProperty(reference, plugin, EclipsePackage.Literals.PLUGIN__UNPACK);
		addReferenceProperty(reference, plugin, EclipsePackage.Literals.PLUGIN__INSTALL_SIZE);
		addReferenceProperty(reference, plugin, EclipsePackage.Literals.PLUGIN__DOWNLOAD_SIZE);
		addReferenceProperty(reference, plugin, EclipsePackage.Literals.PLUGIN__ARCH);
		addReferenceProperty(reference, plugin, EclipsePackage.Literals.PLUGIN__OS);
		addReferenceProperty(reference, plugin, EclipsePackage.Literals.PLUGIN__WS);
		addReferenceProperty(reference, plugin, EclipsePackage.Literals.PLUGIN__NL);
		return reference;
	}

	private void addFeatureProperty(EclipseFeature eclipseFeature, Feature feature, EAttribute featureToRead) {
		if (feature == null || eclipseFeature == null || featureToRead == null) {
			logger.log(Level.WARNING, "Cannot add property to feature without mandatory parameters");
			return;
		}
		Object o = feature.eGet(featureToRead);
		if (o != null) {
			eclipseFeature.putProperty(featureToRead.getName(), o.toString());
		}
	}

	private void addReferenceProperty(ArtifactReference reference, EObject object, EAttribute featureToRead) {
		addReferenceProperty(reference, object, featureToRead, featureToRead.getName());
	}

	private void addReferenceProperty(ArtifactReference reference, EObject object, EAttribute featureToRead, String keyName) {
		if (object == null || reference == null || featureToRead == null) {
			logger.log(Level.WARNING, "Cannot add property to reference without mandatory parameters");
			return;
		}
		keyName = (keyName == null) ? featureToRead.getName() : keyName;
		Object o = object.eGet(featureToRead);
		if (o != null) {
			reference.putProperty(keyName, o.toString());
		}
	}

	/**
	 * Update {@link EclipseFeature} specific information
	 * @param artifact the artifact instance, expected to be an {@link EclipseFeature}
	 * @param properties the properties
	 */
	private void updateFeature(Artifact artifact, Dictionary<String, Object> properties) {
		if (artifact == null) {
			logger.log(Level.WARNING, "Artifact is null");
			return;
		}
		if (!(artifact instanceof EclipseFeature)) {
			logger.log(Level.FINEST, String.format("Artifact %s is no EclipseFeature", artifact.getName()));
			return;
		}
		EclipseFeature feature = (EclipseFeature) artifact;
		String contentType = feature.getProperties().get(P2_PROP_ARTIFACT_CONTENT_TYPE);
		if (contentType != null) {
			if (!APPLICATION_ZIP.equals(contentType)) {
				if (properties != null) {
					properties.put("format", "packed");
				}
				feature.setPacked(true);
			} else {
				feature.setJar(true);
			}
		}
	}

	/**
	 * Updates the download size if available
	 * @param artifact the artifact to update
	 */
	void updateArtifactsSizeAndHash(org.gecko.artifacts.Artifact artifact) {
		if (artifact == null) {
			logger.log(Level.SEVERE, "Error parsing size property for a null artifacts parameter");
			return;
		}
		String sizeString = artifact.getProperties().get(P2_PROP_ARTIFACT_SIZE);
		if (sizeString != null) {
			try {
				int size = Integer.valueOf(sizeString);
				artifact.setSize(size);
			} catch (Exception e) {
				logger.log(Level.SEVERE, String.format("[%s] Error parsing size property '%s' into int", artifact.getName(), sizeString), e);
			}
		}
		String hashString = artifact.getProperties().get(P2_PROP_DOWNLOAD_MD5);
		if (hashString != null) {
			artifact.setHash(hashString);
		}
	}

}
