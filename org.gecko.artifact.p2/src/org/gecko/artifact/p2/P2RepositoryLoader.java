/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.p2;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.gecko.artifact.api.ArtifactConstants;
import org.gecko.artifact.helper.ArtifactsHelper;
import org.gecko.artifact.helper.EclipseModelHelper;
import org.gecko.artifact.p2.helper.P2LoaderHelper;
import org.gecko.artifact.p2.helper.P2LoaderHelper.P2Index;
import org.gecko.artifacts.Artifact;
import org.gecko.artifacts.ArtifactReference;
import org.gecko.artifacts.ArtifactRepository;
import org.gecko.artifacts.ArtifactsFactory;
import org.gecko.artifacts.ArtifactsPackage;
import org.gecko.artifacts.EclipseFeature;
import org.gecko.artifacts.P2Repository;
import org.gecko.artifacts.Property;
import org.gecko.artifacts.ReferenceType;
import org.gecko.eclipse.Artifacts;
import org.gecko.eclipse.ChildrenType;
import org.gecko.eclipse.DocumentRoot;
import org.gecko.eclipse.Feature;
import org.gecko.eclipse.Repository;
import org.gecko.eclipse.RepositoryRef;
import org.osgi.util.promise.Deferred;
import org.osgi.util.promise.Promise;
import org.osgi.util.promise.PromiseFactory;
import org.tukaani.xz.XZInputStream;

import aQute.bnd.header.Parameters;
import aQute.bnd.http.HttpClient;
import aQute.bnd.http.HttpRequest;
import aQute.bnd.osgi.Jar;
import aQute.bnd.osgi.Resource;
import aQute.lib.io.IO;
import aQute.lib.strings.Strings;

/**
 * Loader that loads P2 repository data
 * @author Mark Hoffmann
 * @since 02.03.2020
 */
public class P2RepositoryLoader implements ArtifactConstants {

	private final static Logger logger = Logger.getLogger(P2RepositoryLoader.class.getName());
	private PromiseFactory promiseFactory = new PromiseFactory(Executors.newCachedThreadPool());
	private final Set<URI> defaults	= Collections.newSetFromMap(new ConcurrentHashMap<URI, Boolean>());
	private final HttpClient client;
	private final P2Repository baseRepository;
	private final URI base;
	private final P2LoaderHelper helper;
	private final ResourceSet resourceSet;
	private boolean resolveReferences = true;
	private Consumer<EObject> saveCallback;

	public P2RepositoryLoader(URI base) throws Exception {
		this.base = P2LoaderHelper.normalize(base);
		this.baseRepository  = createRepository(this.base);
		client = new HttpClient();
		helper = new P2LoaderHelper();
		resourceSet = EclipseModelHelper.createResourceSet();
	}
	
	/**
	 * Sets the saveCallback.
	 * @param saveCallback the saveCallback to set
	 */
	public void setSaveConsumer(Consumer<EObject> saveCallback) {
		this.saveCallback = saveCallback;
	}
	
	/**
	 * Sets the resolveReferences.
	 * @param resolveReferences the resolveReferences to set
	 */
	public void setResolveReferences(boolean resolveReferences) {
		this.resolveReferences = resolveReferences;
	}
	
	/**
	 * Returns the resolveReferences.
	 * @return the resolveReferences
	 */
	public boolean isResolveReferences() {
		return resolveReferences;
	}

	/**
	 * Returns the {@link P2Repository} promise
	 * @return the {@link P2Repository} promise
	 */
	public Promise<P2Repository> getP2Repository() {
		Set<URI> cycles = Collections.newSetFromMap(new ConcurrentHashMap<URI, Boolean>());
		return getP2Repository(cycles, base, baseRepository);
	}
	
	/**
	 * Loads an {@link EclipseFeature} out of an {@link Artifact}. Usually the artifact parameter is a given "un-resolved" instance.
	 * When loading repositories, such proxy artifacts will be created. This method acts like resolving these instances. 
	 * @param artifact the un-resolved artifact
	 * @return an {@link EclipseFeature} {@link Promise}
	 * @throws MalformedURLException
	 */
	public Promise<EclipseFeature> resolveEclipseFeature(Artifact artifact) {
		if (artifact == null) {
			logger.log(Level.WARNING, "Cannot load a feature without artifact");
			throw new IllegalStateException("Cannot load a feature without artifact");
		}
		if (!(artifact instanceof EclipseFeature)) {
			logger.log(Level.WARNING, "Cannot load an artifact, that isn't already an Eclipse Feature");
			throw new IllegalStateException("Cannot load an artifact, that isn't already an Eclipse Feature");
		}
		EclipseFeature feature = (EclipseFeature) artifact;
		P2Repository repository = (P2Repository) feature.getRepository();
		if (repository == null) {
			logger.log(Level.WARNING, "Cannot load a feature with an invalid artifact that has not repository assignment");
			throw new IllegalStateException("Cannot load a feature with an invalid artifact that has not repository assignment");
		}
		final URI featureUri = URI.create(feature.getSourceLocation());
		String contentType = feature.getProperties().get(P2_PROP_ARTIFACT_CONTENT_TYPE);
		logger.log(Level.SEVERE, String.format("[%s] Content type feature jared: %s, packed: %s", feature.getName(), feature.isJar(), feature.isPacked()));
		
		HttpRequest<File> request = createRequest(contentType);
		try {
			Promise<File> file = request.async(featureUri.toURL());
			return file
					.map(f->{
						if (f == null) 
							throw new IllegalStateException("File is null");
						return f;
					})
					.recoverWith((p)->{
						/*
						 * Obviously the calculated URI from the artifacts.xml was wrong, We give it another try. this can happen,
						 * if a content type for the feature was not provided or wrong.
						 */
						if (featureUri.toString().endsWith("/feature.xml")) {
							logger.log(Level.WARNING, String.format("[%s] Loading feature.xml resulted in a null file! Trying as jar", feature.getName()));
							Property prop = ArtifactsFactory.eINSTANCE.createProperty();
							prop.setKey(PROP_ARTIFACTS_ORIG_SOURCE);
							prop.setValue(feature.getSourceLocation());
							feature.getArtifactProperties().add(prop);
							URI recoverUri = URI.create(featureUri.toString().replace("/feature.xml", ".jar"));
							feature.setSourceLocation(recoverUri.toString());
							feature.setJar(true);
							return createRequest(contentType).async(recoverUri.toURL());
						}
						return null;
					})
					.map(f->mapFeature(f, feature))
					.map(this::loadPropertiesFromFile)
					.map(f->helper.replaceProperties(f, ArtifactsPackage.Literals.ECLIPSE_FEATURE__FEATURE_PROPERTIES))
					.map(o->(EclipseFeature)o);
			
		} catch (Exception e) {
			return promiseFactory.failed(e);
		}
	}
	
	public ArtifactRepository resolveReferences(ArtifactRepository repository) {
		if (isResolveReferences()) {
			List<ArtifactReference> references = new LinkedList<ArtifactReference>();
			repository.getAllArtifacts().stream().map(Artifact::getReferences).forEach(references::addAll);
			references.stream()
				.filter(r->ReferenceType.REQUIRE_ARTIFACT.equals(r.getType()) || 
						ReferenceType.INCLUDE_ARTIFACT.equals(r.getType()))
				.forEach(r->resolveTarget(r, repository));
			return repository;
		} else {
			return repository;
		}
	}
	
	/**
	 * Tries to resolve a target from the given reference with information from the given repository
	 * @param reference the reference to resolve the target for
	 * @param repository the repository, to resolve the reference with.
	 * @return the resolved reference
	 */
	private ArtifactReference resolveTarget(ArtifactReference reference, ArtifactRepository repository) {
		if (reference == null || repository == null) {
			throw new IllegalArgumentException("Cannot resolve target with missing arguments");
		}
		String name = reference.getTargetName() == null ? reference.getName() : reference.getTargetName();
		String version = reference.getTargetVersion() == null ? reference.getVersion() : reference.getTargetVersion();
		String versionHint = reference.getProperties().get(ArtifactConstants.PROP_TARGET_VERSION_HINT);
		String filter = reference.getFilter();
		ArtifactsHelper helper = ArtifactsHelper.createArtifactsHelper(version, versionHint, filter);
		List<Artifact> artifacts = repository.getAllArtifactsById(name);
		artifacts = artifacts.stream().filter(helper::isArtifactType).filter(helper::filterVersion).collect(Collectors.toList());
		if (artifacts != null && !artifacts.isEmpty()) {
			reference.getTarget().addAll(artifacts);
			interceptSave(reference);
		}
		return reference;
	}
	
	private HttpRequest<File> createRequest(String contentType) {
		HttpRequest<File> request = client.build().useCache().get();
		if (contentType != null) {
			Map<String, String> header = new HashMap<>();
			header.put("Content-Type", contentType);
			request = request.headers(header);
		}
		return request;
	}
	
	private EclipseFeature mapFeature(File file, EclipseFeature proxy) throws IOException, Exception {
		Feature f;
		if (file == null) {
			logger.log(Level.WARNING, String.format("[%s] Loading feature jar returned a null file for path %s", proxy.getName(), proxy.getSourceLocation()));
			return proxy;
		}
		if (proxy.isJar() && file.exists()) {
			try (Jar featureJar = new Jar(file)) {
				if (proxy.getPropertyLocation() != null) {
					try (Resource props = featureJar.getResource(proxy.getPropertyLocation())) {
						if (props != null && props.size() > 0) {
							loadPropertiesFromJar(featureJar, proxy);
						}
					}
				}
				try (Resource resource = featureJar.getResource(P2_FEATURE_FILE)) {
					DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, resource.openInputStream(), P2_FEATURE_FILE);
					f = root.getFeature();
				}
			}
		} else {
			DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, file, P2_FEATURE_FILE);
			f = root.getFeature();
		}
		helper.mapFeature(f, proxy, this::interceptSave);
		logger.log(Level.FINE, proxy.toString());
		return proxy;
	}
	
	/**
	 * Load the properties from the jar file
	 * @param jar the jar
	 * @param feature the feature the properties belong to
	 * @throws Exception
	 */
	private void loadPropertiesFromJar(Jar jar, EclipseFeature feature) throws Exception {
		if (feature.getPropertyLocation() != null) {
			try (Resource propFile = jar.getResource(feature.getPropertyLocation())) {
				if (propFile != null && propFile.size() > 0) {
					helper.loadAndSetProperties(propFile.openInputStream(), feature, ArtifactsPackage.Literals.ECLIPSE_FEATURE__FEATURE_PROPERTIES);
				}
			}
		}
	}
	
	/**
	 * Loads the properties from the file
	 * @param feature the feature to load the properties file for
	 * @return the feature
	 * @throws MalformedURLException
	 * @throws Exception
	 */
	private EclipseFeature loadPropertiesFromFile(EclipseFeature feature) throws MalformedURLException, Exception {
		if (!feature.isJar() && 
				feature.getPropertyLocation() != null) {
			File propFile = client.build().useCache().get().go(URI.create(feature.getPropertyLocation()).toURL());
			if (propFile != null) {
				try (FileInputStream fis = new FileInputStream(propFile)) {
					helper.loadAndSetProperties(fis, feature, ArtifactsPackage.Literals.ECLIPSE_FEATURE__FEATURE_PROPERTIES);
				}
			}
		}
		return feature;
	}

	/**
	 * Creates a new repository for the given {@link URI}
	 * @param base the base {@link URI} for that repository
	 * @return the {@link P2Repository} instance
	 */
	private P2Repository createRepository(URI base) {
		return createRepository(base.toString());
	}

	/**
	 * Creates a new repository for the given URI {@link String}
	 * @param base the base URI {@link String} for that repository
	 * @return the {@link P2Repository} instance
	 */
	private P2Repository createRepository(String base) {
		P2Repository baseRepository = ArtifactsFactory.eINSTANCE.createP2Repository();
		baseRepository.setLocation(base);
		return baseRepository;
	}
	
	/**
	 * Returns the parent P2 repository  
	 * @param cycles the {@link Set} of URI to don't run in cycles
	 * @param base the base URI for the repository
	 * @return the {@link Promise}for the {@link P2Repository}
	 */
	private Promise<P2Repository> getP2Repository(Set<URI> cycles, URI base, P2Repository repository) {
		URI repositoryURI = base;
		if (!cycles.add(repositoryURI)) {
			return promiseFactory.resolved(null);
		}
		URI repoUri = repository.getLocationUri();
		// Detect a sub repository
		boolean isSubRepository = !repositoryURI.toString().startsWith(repoUri.toString()) || repository.isCompositeRepository();
//		isSubRepository = isSubRepository || repository.isCompositeRepository();
		if (isSubRepository) {
			P2Repository subRepo = ArtifactsFactory.eINSTANCE.createP2Repository();
			subRepo.setLocation(repositoryURI.toString());
			repository.getRepositories().add(subRepo);
			repository = subRepo;
		}
		try {
			String type = repositoryURI.getPath();
			logger.info(String.format("getP2Repository type=%s", repositoryURI));
			if (type.endsWith("/" + P2_ARTIFACTS_COMPOSITES_FILE)) {
				return parseCompositeArtifacts(cycles, repositoryURI, repository);
			} else if (type.endsWith("/" + P2_ARTIFACTS_XZ_FILE)) {
				return parseP2Artifacts(repositoryURI, repository, isSubRepository);
			} else if (type.endsWith("/" + P2_ARTIFACTS_FILE)) {
				return parseP2Artifacts(repositoryURI, repository, isSubRepository);
			} else if (type.endsWith("/" + P2_INDEX_FILE)) {
				repository.setIndexLocation(repositoryURI.toString());
				return loadP2IndexFile(cycles, repositoryURI, repository);
			}
			URI indexUri = P2LoaderHelper.normalize(repositoryURI).resolve(P2_INDEX_FILE);
			repository.setIndexLocation(indexUri.toString());
			defaults.add(indexUri);
			return loadP2IndexFile(cycles, indexUri, repository);
		} catch (Exception e) {
			logger.log(Level.SEVERE, String.format("Error getting artifacts type=%s", repositoryURI), e);
			return promiseFactory.failed(e);
		}
	}
	
	/**
	 * Parses the artifact repository 
	 * @param repositoryUri the repository {@link URI} to load
	 * @param p2Repository the {@link P2Repository} with the location uri to parse
	 * @param isSubRepo flag that defines if we have a sub repo
	 * @return the promise for the enriched repository instance, with artifacts
	 * @throws Exception
	 */
	private Promise<P2Repository> parseP2Artifacts(URI repositoryUri, final P2Repository p2Repository, boolean isSubRepo) throws Exception {
		if (p2Repository == null) {
			logger.log(Level.WARNING, "No content for a null repository");
			return promiseFactory.resolved(null);
		}
		return promiseFactory.submit(() -> {
			InputStream is= null;
			try {
				is = hideAndSeek(repositoryUri);
				DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, is, P2_ARTIFACTS_FILE);
				Repository repository = root.getRepository();
				boolean loaded = p2Repository.isLoaded();
				p2Repository.setArtifactLocation(repositoryUri.toString());
				p2Repository.setLoaded(true);
				p2Repository.setTimestamp(new Date());
				helper.updateP2Repository(repository, p2Repository);
				interceptSave(p2Repository);
				if (repository.getArtifacts() == null) {
					return returnRepository(p2Repository, loaded);
				}
				Artifacts artifacts = root.getRepository().getArtifacts(); 
				artifacts.getArtifact().forEach(a->helper.appendArtifact(p2Repository, a, this::interceptSave));
				interceptSave(p2Repository);
				return returnRepository(p2Repository, loaded);
			} finally {
				if (is != null) {
					IO.close(is);
				}
			}
		}).onFailure(f->f.printStackTrace());
	}
	
	private P2Repository returnRepository(P2Repository repository, boolean loaded) {
		return (!loaded && repository.getParent() != null) ? (P2Repository)repository.getParent() : repository;
	}
	
	private Promise<P2Repository> parseCompositeArtifacts(Set<URI> cycles, URI uri, P2Repository p2Repository) throws Exception {
		final InputStream is = hideAndSeek(uri);
		if (is == null) {
			logger.log(Level.INFO, String.format("No such composite repository %s", base));
			return promiseFactory.resolved(p2Repository);
		}
		DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, is, "compositeArtifacts.xml");
		Repository repository = root.getRepository();
		p2Repository.setArtifactLocation(uri.toString());
		helper.updateP2Repository(repository, p2Repository);
		p2Repository.setLoaded(true);
		p2Repository.setTimestamp(new Date());
		interceptSave(p2Repository);
		ChildrenType children = repository.getChildren();
		List<URI> uris = children.getChild()
				.stream()
				.map(RepositoryRef::getLocation)
				.map(Strings::trim)
				.map(base::resolve)
				.collect(toList());

		return getRepositoryData(cycles, uris, p2Repository);
	}

	/**
	 * Calls the consumer, if it exists
	 * @param object the object to use as parameter
	 */
	private void interceptSave(EObject object) {
		if (saveCallback != null) {
			try {
				saveCallback.accept(object);
			} catch (Exception e) {
				logger.log(Level.SEVERE, String.format("Error saving object %s", object.toString()), e);
			}
		}
	}

	/**
	 * @see aQute.p2.provider.P2Impl#hideAndSeek
	 * @formatter:off
	 *  version = 1
	 *  metadata.repository.factory.order = compositeContent.xml,\!
	 *  artifact.repository.factory.order = compositeArtifacts.xml,\!
	 * @formatter:on
	 */
	private Promise<P2Repository> loadP2IndexFile(Set<URI> cycles, URI indexUri, final P2Repository p2Repository) throws Exception {
		Promise<File> file = client.build()
				.useCache()
				.get()
				.async(indexUri.toURL());
		return file.flatMap(f -> parseP2IndexFile(cycles, indexUri, p2Repository, f));
	}

	/**
	 * @see aQute.p2.provider.P2Impl#hideAndSeek
	 */
	private Promise<P2Repository> parseP2IndexFile(Set<URI> cycles, URI indexUri, P2Repository repository, File file) throws Exception {
		P2Index index;
		if (file == null) {
			index = helper.getDefaultIndex(indexUri);
			helper.setDefaults(index, defaults);
		} else {
			index = parseIndex(file, indexUri);
		}

		P2LoaderHelper.canonicalize(index.getArtifacts());
		P2LoaderHelper.canonicalize(index.getContent());

		return getRepositoryData(cycles, index.getArtifacts(), repository);
	}

	/**
	 * Loads the repository data like artifacts and metadata into the given {@link P2Repository} instance 
	 * @param cycles 
	 * @param index the {@link P2Index} file with all repository locations
	 * @param repository the {@link P2Repository} instance, to fetch the information for
	 * @return the {@link Promise} for the {@link P2Repository}
	 */
	private Promise<P2Repository> getRepositoryData(Set<URI> cycles, List<URI> uris, P2Repository repository) {
		Deferred<P2Repository> deferred = promiseFactory.deferred();
		final AtomicReference<P2Repository> repositoryRef = new AtomicReference<P2Repository>(repository);
		promiseFactory.executor().execute(() -> {
			try {
				uris.stream().map(uri-> {
					return getP2Repository(cycles, base.resolve(uri), repositoryRef.get()).recover(t -> {
						if (!defaults.contains(uri)) {
							logger.log(Level.SEVERE, String.format("Failed to get data for %s", uri), t.getFailure());
						}
						return repository;
					});
				}).map(P2LoaderHelper::safeGetValue).forEach(repositoryRef::set);
				deferred.resolve(repositoryRef.get());
			} catch (Exception e) {
				deferred.fail(e);
			}
		});
		return deferred.getPromise();
	}
	
	/**
	 * @see aQute.p2.provider.P2Impl#hideAndSeek
	 */
	private InputStream hideAndSeek(URI uri) throws Exception {
		if (uri.getPath()
				.endsWith(".xz")) {
			File f = getFile(uri);
			if (f != null)
				return tzStream(f);
			else
				return null;
		}

		URI xzname = replace(uri, "$", ".xz");
		File f = getFile(xzname);
		if (f != null)
			return tzStream(f);

		f = getFile(replace(uri, ".xml$", ".jar"));
		if (f != null)
			return jarStream(f, Strings.getLastSegment(uri.getPath(), '/'));

		f = getFile(uri);
		if (f != null)
			return IO.stream(f);

		if (!defaults.contains(uri))
			logger.log(Level.SEVERE, String.format("Invalid uri %s", uri));
		return null;
	}

	/**
	 * @see aQute.p2.provider.P2Impl#getFile
	 */
	private File getFile(URI xzname) throws Exception {
		return client.build()
				.useCache()
				.go(xzname);
	}

	/**
	 * @see aQute.p2.provider.P2Impl#jarStream
	 */
	private InputStream jarStream(File f, String name) throws IOException {
		final JarFile jaf = new JarFile(f);
		ZipEntry entry = jaf.getEntry(name);
		final InputStream inputStream = jaf.getInputStream(entry);

		return new FilterInputStream(inputStream) {
			@Override
			public void close() throws IOException {
				jaf.close();
			}
		};
	}

	/**
	 * @see aQute.p2.provider.P2Impl#tzStream
	 */
	private InputStream tzStream(File f) throws Exception {
		return new XZInputStream(IO.stream(f));
	}

	/**
	 * @see aQute.p2.provider.P2Impl#replace
	 */
	private URI replace(URI uri, String where, String replacement) {
		String path = uri.getRawPath();
		return uri.resolve(path.replaceAll(where, replacement));
	}

	/**
	 * @see aQute.p2.provider.P2Impl#parseIndex
	 */
	private P2Index parseIndex(File file, URI base) throws IOException {
		Properties p = new Properties();
		try (InputStream in = IO.stream(file)) {
			p.load(in);
		}

		String version = p.getProperty("version");
		if (version == null || Integer.parseInt(version) != 1)
			throw new UnsupportedOperationException(
					"The repository " + base + " specifies an index file with an incompatible version " + version);

		P2Index index = P2LoaderHelper.createIndex();

		addPaths(p.getProperty("metadata.repository.factory.order"), index.getContent(), base);
		addPaths(p.getProperty("artifact.repository.factory.order"), index.getArtifacts(), base);

		index.setModified(file.lastModified());
		return index;
	}


	/**
	 * @see aQute.p2.provider.P2Impl#addPaths
	 */
	private void addPaths(String p, List<URI> index, URI base) {
		Parameters content = new Parameters(p);
		for (String path : content.keySet()) {
			if ("!".equals(path)) {
				break;
			}
			URI sub = base.resolve(path);
			index.add(sub);
		}
	}

}
