/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.p2;

import static org.gecko.artifact.api.ArtifactConstants.P2_ARTIFACTS_TYPE;
import static org.gecko.artifact.api.ArtifactConstants.P2_COMPRESSED;
import static org.gecko.artifact.api.ArtifactConstants.P2_CONTENT_TYPE;
import static org.gecko.artifact.api.ArtifactConstants.P2_NAMESPACE_IU;
import static org.gecko.artifact.api.ArtifactConstants.P2_PROP_NAME;
import static org.gecko.artifact.api.ArtifactConstants.P2_TIMESTAMP;
import static org.gecko.artifact.api.ArtifactConstants.P2_TYPE_CATEGORY;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.gecko.artifact.helper.EclipseModelHelper;
import org.gecko.artifact.p2.helper.P2CreationHelper;
import org.gecko.eclipse.Artifact;
import org.gecko.eclipse.Artifacts;
import org.gecko.eclipse.DocumentRoot;
import org.gecko.eclipse.EclipseFactory;
import org.gecko.eclipse.MappingsType;
import org.gecko.eclipse.Properties;
import org.gecko.eclipse.Property;
import org.gecko.eclipse.Provided;
import org.gecko.eclipse.Provides;
import org.gecko.eclipse.Repository;
import org.gecko.eclipse.Required;
import org.gecko.eclipse.Requires;
import org.gecko.eclipse.Rule;
import org.gecko.eclipse.Touchpoint;
import org.gecko.eclipse.Unit;
import org.gecko.eclipse.UnitsType;
import org.gecko.eclipse.Update;
import org.osgi.framework.Constants;
import org.osgi.framework.Version;
import org.osgi.framework.namespace.IdentityNamespace;

import aQute.bnd.osgi.Jar;
import aQute.lib.io.IO;

/**
 * Builder to create a P2 repository from a set of bundles
 * @author Mark Hoffmann
 * @since 12.10.2020
 */
public class P2RepositoryCreator {

	private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMDDHHmmss");
	private final URI location;
	private final ResourceSet resourceSet = EclipseModelHelper.createResourceSet();
	private boolean compress = false;
	private Repository content;
	private Repository artifacts;
	private CategoryData category = null;
	private List<Jar> bundles = new LinkedList<Jar>();
	private String bundleFolderName = "plugins";
	private String featureFolderName = "features";
	private String binaryFolderName = "binary";

	/**
	 * Meta information about a P2 site category (category.xml)
	 * @author Mark Hoffmann
	 * @since 14.10.2020
	 */
	public static class CategoryData {
		public String id;
		public String name;
		public String version;
	}

	/**
	 * Creates a new instance.
	 * @param location location for the P2 site
	 * @throws Exception throw non errors
	 */
	public P2RepositoryCreator(URI location) throws Exception {
		this.location = location;
		initializeContent();
		initializeArtifacts();
	}

	/**
	 * Use the given folder name where bundles are placed
	 * @param folderName the folder name
	 * @return
	 */
	public P2RepositoryCreator bundleFolderName(String folderName) {
		this.bundleFolderName = folderName;
		return this;
	}

	public P2RepositoryCreator featureFolderName(String folderName) {
		this.featureFolderName = folderName;
		return this;
	}

	public P2RepositoryCreator binaryFolderName(String folderName) {
		this.binaryFolderName = folderName;
		return this;
	}

	public P2RepositoryCreator compress(boolean compress) {
		this.compress = compress;
		return this;
	}

	public P2RepositoryCreator name(String repositoryName) {
		content.setName(repositoryName);
		artifacts.setName(repositoryName);
		return this;
	}

	public P2RepositoryCreator javaVersion(Version javaVersion) {
		Set<Unit> defaultUnits = P2CreationHelper.createPostJava8Units(javaVersion.toString());
		content.getUnits().getUnit().addAll(defaultUnits);
		return this;
	}

	private void initializeContent() {
		content = EclipseFactory.eINSTANCE.createRepository();
		content.setType(P2_CONTENT_TYPE);
		content.setVersion("1");

		UnitsType units = EclipseFactory.eINSTANCE.createUnitsType();
		content.setUnits(units);
	}

	private void initializeArtifacts() {
		artifacts = EclipseFactory.eINSTANCE.createRepository();
		artifacts.setType(P2_ARTIFACTS_TYPE);
		artifacts.setVersion("1");
		initializeMappings();
		Artifacts a = EclipseFactory.eINSTANCE.createArtifacts();
		artifacts.setArtifacts(a);
	}

	private void initializeMappings() {
		MappingsType mappings = EclipseFactory.eINSTANCE.createMappingsType();
		artifacts.setMappings(mappings);
		String output = "${repoUrl}/%s/${id}_${version}.jar%s";
		Rule rule = P2CreationHelper.createRule("(& (classifier=osgi.bundle) (format=packed))", String.format(output, bundleFolderName, ".pack.gz"));
		mappings.getRule().add(rule);
		rule = P2CreationHelper.createRule("(& (classifier=osgi.bundle))", String.format(output, bundleFolderName, ""));
		mappings.getRule().add(rule);
		rule = P2CreationHelper.createRule("(& (classifier=org.eclipse.update.feature) (format=packed))", String.format(output, featureFolderName, ".pack.gz"));
		mappings.getRule().add(rule);
		rule = P2CreationHelper.createRule("(& (classifier=org.eclipse.update.feature))", String.format(output, featureFolderName, ""));
		mappings.getRule().add(rule);
		output = "${repoUrl}/%s/${id}_${version}";
		rule = P2CreationHelper.createRule("(& (classifier=binary))", String.format(output, binaryFolderName));
		mappings.getRule().add(rule);
		mappings.setSize(mappings.getRule().size());
	}

	public P2RepositoryCreator withCategory(CategoryData category) {
		this.category = category;
		return this;
	}

	public P2RepositoryCreator addJar(Jar bundleJar) throws Exception {
		addContent(bundleJar);
		addArtifacts(bundleJar);
		bundles.add(bundleJar);
		return this;
	}

	private void addArtifacts(Jar bundleJar) throws Exception {
		if (bundleJar.getManifest() == null) {
			return;
		}
		Artifact artifact = EclipseFactory.eINSTANCE.createArtifact();
		artifact.setClassifier(IdentityNamespace.TYPE_BUNDLE);
		artifact.setId(bundleJar.getBsn());
		artifact.setVersion(bundleJar.getVersion());
		Artifacts a = artifacts.getArtifacts();
		a.getArtifact().add(artifact);
		a.setSize(a.getArtifact().size());
		P2CreationHelper.createArtifactProperties(artifact, bundleJar);
	}

	/**
	 * @param bundleJar
	 * @throws Exception 
	 */
	private void addContent(Jar bundleJar) throws Exception {
		if (bundleJar.getManifest() == null) {
			return;
		}
		Manifest manifest = bundleJar.getManifest();
		Unit unit = EclipseFactory.eINSTANCE.createUnit();
		unit.setId(bundleJar.getBsn());
		unit.setVersion(bundleJar.getVersion());
		Optional<String> value = P2CreationHelper.getManifestValue(manifest, Constants.BUNDLE_MANIFESTVERSION);
		if (value.isPresent()) {
			unit.setGeneration(value.map(Integer::parseInt).get().intValue());
		}
		unit.setSingleton(false);
		UnitsType units = content.getUnits();
		units.getUnit().add(unit);
		units.setSize(units.getUnit().size());
		Update update = EclipseFactory.eINSTANCE.createUpdate();
		unit.setUpdate(update);
		update.setId(bundleJar.getBsn());
		update.setRange(String.format("[0.0.0,%s)", bundleJar.getVersion()));
		update.setSeverity(0);
		P2CreationHelper.createContentProperties(unit, bundleJar);
		P2CreationHelper.createContentProvides(unit, bundleJar);
		P2CreationHelper.createContentRequires(unit, bundleJar);
		P2CreationHelper.createContentUnitArtifacts(unit, bundleJar);
		P2CreationHelper.createOSGiTouchpoint(unit, bundleJar);
	}

	private void sealContent() {
		Properties properties = EclipseFactory.eINSTANCE.createProperties();
		properties.setSize(2);
		Property property = P2CreationHelper.createProperty(P2_COMPRESSED, Boolean.toString(compress));
		properties.getProperty().add(property);
		property = P2CreationHelper.createProperty(P2_TIMESTAMP, Long.toString(System.currentTimeMillis()));
		properties.getProperty().add(property);
		content.setProperties(properties);
	}

	private void sealArtifacts() {
		Properties properties = EclipseFactory.eINSTANCE.createProperties();
		properties.setSize(3);
		Property property = P2CreationHelper.createProperty(P2_COMPRESSED, Boolean.toString(compress));
		properties.getProperty().add(property);
		property = P2CreationHelper.createProperty(P2_TIMESTAMP, Long.toString(System.currentTimeMillis()));
		properties.getProperty().add(property);
		property = P2CreationHelper.createProperty("publishPackFilesAsSiblings", Boolean.TRUE.toString());
		properties.getProperty().add(property);
		artifacts.setProperties(properties);
	}

	public void create() {
		try {
			File bundles = new File(location.getPath() + "/" + bundleFolderName);
//			File features = new File(location.toString() + "/" + featureFolderName);
//			File binaries = new File(location.toString() + "/" + binaryFolderName);
			if (!bundles.exists()) {
				bundles.mkdirs();
			}
			this.bundles.forEach(jar->copyJar(bundles, jar));
			
			createCategoryData();

			sealContent();
			sealArtifacts();

			DocumentRoot contentRoot = EclipseModelHelper.createDocumentRoot(resourceSet, location.getPath(), "content.xml");
			contentRoot.setRepository(content);
			DocumentRoot artifactsRoot = EclipseModelHelper.createDocumentRoot(resourceSet, location.getPath(), "artifacts.xml");
			artifactsRoot.setRepository(artifacts);
			if (compress) {
				createZip(contentRoot, "content.xml", location.getPath());
				createZip(artifactsRoot, "artifacts.xml", location.getPath());
			} else {
				contentRoot.eResource().save(null);
				artifactsRoot.eResource().save(null);
			}
		} catch (IOException e) {
			System.out.println("IOError " + e.getMessage());
		} catch (Exception e) {
			System.out.println("Error " + e.getMessage());
		}
	}

	/**
	 * @throws Exception 
	 * 
	 */
	private void createCategoryData() throws Exception {
		if (category == null) {
			return;
		}
		Unit categoryUnit = EclipseFactory.eINSTANCE.createUnit();
		String dateString = SDF.format(new Date());
		String id = dateString + "." + category.id;
		categoryUnit.setId(id);
		categoryUnit.setVersion(category.version);
		Properties properties = EclipseFactory.eINSTANCE.createProperties();
		properties.setSize(2);
		categoryUnit.setProperties(properties);
		Property property = P2CreationHelper.createProperty(P2_PROP_NAME, category.name);
		properties.getProperty().add(property);
		property = P2CreationHelper.createProperty(P2_TYPE_CATEGORY, Boolean.TRUE.toString());
		properties.getProperty().add(property);
		Provides provides = EclipseFactory.eINSTANCE.createProvides();
		provides.setSize(1);
		categoryUnit.setProvides(provides);
		Provided provided = P2CreationHelper.createProvided(P2_NAMESPACE_IU, id, category.version);
		provides.getProvided().add(provided);

		Requires requires = EclipseFactory.eINSTANCE.createRequires();
		categoryUnit.setRequires(requires);
		String rangeTemplate = "[%s,%s]";
		for (Jar bundle : bundles) {
			String range = String.format(rangeTemplate, bundle.getVersion(), bundle.getVersion());
			Required required = P2CreationHelper.createRequired(P2_NAMESPACE_IU, bundle.getBsn(), range);
			requires.getRequired().add(required);
		}
		requires.setSize(requires.getRequired().size());

		Touchpoint touchpoint = EclipseFactory.eINSTANCE.createTouchpoint();
		categoryUnit.setTouchpoint(touchpoint);
		touchpoint.setId("null");
		touchpoint.setVersion("0.0.0");
		content.getUnits().getUnit().add(categoryUnit);
		content.getUnits().setSize(content.getUnits().getUnit().size());
	}

	/**
	 * Create the zipped xml's
	 * @param repository the repository model
	 * @param fileName the filename
	 * @param location the file location
	 * @throws IOException
	 */
	private void createZip(DocumentRoot repository, String fileName, String location) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		repository.eResource().save(baos, null);
		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());

		String path = location + "/" + fileName.replace("xml", "jar");
		ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(path));
		zipOut.putNextEntry(new ZipEntry(fileName)); 
		byte[] b = new byte[1024];
		int count;

		while ((count = bais.read(b)) > 0) {
			zipOut.write(b, 0, count);
		}
		zipOut.close();
		bais.close();
	}
	
	/**
	 * Copies the given {@link Jar} into the target folder
	 * @param targetFolder the target folder for the {@link Jar}
	 * @param jar the {@link Jar} to be copied
	 */
	private void copyJar(File targetFolder, Jar jar) {
		try {
			String jarName = jar.getSource().getName();
			if (!jar.getSource().getName().endsWith("-" + jar.getVersion() + ".jar")) {
				jarName = jar.getSource().getName().replace(".jar", "_" + jar.getVersion() + ".jar");
			} else {
				jarName = jarName.replace("-" + jar.getVersion() + ".jar", "_" + jar.getVersion() + ".jar");
			}
			File targetFile = new File(targetFolder, jarName);
			IO.copy(jar.getSource(), targetFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
