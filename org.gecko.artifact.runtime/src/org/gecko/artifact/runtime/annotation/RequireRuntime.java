/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package  org.gecko.artifact.runtime.annotation;

import static java.lang.annotation.ElementType.PACKAGE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.gecko.emf.osgi.json.annotation.RequireEMFJson;
import org.gecko.emf.osgi.rest.jaxrs.annotation.RequireEMFMessageBodyReaderWriter;
import org.gecko.emf.repository.mongo.annotations.RequireMongoEMFRepository;
import org.gecko.runtime.boot.annotation.RequireGeckoRuntime;
import org.gecko.runtime.logging.annotation.RequireSLF4J;
import org.osgi.service.jaxrs.whiteboard.annotations.RequireJaxrsWhiteboard;

@Documented
@Retention(CLASS)
@Target({ TYPE, PACKAGE })
/**
 * Requries all the neccessary Modules for the full fletched GeckoServer
 */
@RequireJaxrsWhiteboard
@RequireEMFJson
@RequireEMFMessageBodyReaderWriter
@RequireSLF4J
@RequireGeckoRuntime
@RequireMongoEMFRepository
public @interface RequireRuntime {

}
