/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.eclipse.converter.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.gecko.artifact.converter.helper.EclipseModelHelper;
import org.gecko.eclipse.Artifact;
import org.gecko.eclipse.CategoryDef;
import org.gecko.eclipse.CategoryFeature;
import org.gecko.eclipse.CategorySite;
import org.gecko.eclipse.DocumentRoot;
import org.gecko.eclipse.Feature;
import org.gecko.eclipse.LocationIncludeType;
import org.gecko.eclipse.Repository;
import org.gecko.eclipse.Rule;
import org.gecko.eclipse.Target;
import org.gecko.eclipse.TargetLocation;
import org.gecko.eclipse.TargetLocations;
import org.gecko.eclipse.TargetType;
import org.gecko.eclipse.Unit;
import org.junit.Before;
import org.junit.Test;

public class EclipseXMLTest {
	
	private ResourceSet resourceSet = null;
	
	@Before
	public void setup() {
		if (resourceSet == null) {
			resourceSet = EclipseModelHelper.createResourceSet();
		}
	}
	
	@Test
	public void testLoadFeature() throws IOException {
		DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, getClass().getResource("feature.xml"), "feature.xml");
		Feature feature = root.getFeature();
		assertNotNull(feature);
		assertNotNull(feature.getInstallHandler());
		assertEquals("install.jar", feature.getInstallHandler().getLibrary());
		assertEquals("com.test.InstallHandler", feature.getInstallHandler().getHandler());
		assertEquals("org.gecko.test.feature", feature.getId());
		assertEquals("1.0.0.qualifier", feature.getVersion());
		assertEquals("Feature", feature.getLabel());
		assertEquals("linux,macosx", feature.getOs());
		assertEquals("cocoa,gtk", feature.getWs());
		assertEquals("ar_KW,ar_LB", feature.getNl());
		assertEquals("x86,x86_64", feature.getArch());
		
		assertNotNull(feature.getDescription());
		assertEquals("http://www.example.com/description", feature.getDescription().getUrl());
		assertEquals("description", feature.getDescription().getValue());
		
		assertEquals(3, feature.getIncludes().size());
		assertNull(feature.getIncludes().get(0).getName());
		assertEquals("E4 Tools", feature.getIncludes().get(1).getName());
		assertNull(feature.getIncludes().get(2).getName());
		
		assertEquals(4, feature.getRequires().getImport().size());
		assertFalse(feature.getRequires().getImport().get(2).isPatch());
		assertTrue(feature.getRequires().getImport().get(3).isPatch());
	}
	
	@Test
	public void testLoadPluginFeature() throws IOException {
		DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, getClass().getResource("feature_plugin.xml"), "feature.xml");
		Feature feature = root.getFeature();
		assertNotNull(feature);
		assertEquals("org.eclipse.emf.ecp.view.model.controls.feature", feature.getId());
		assertEquals("org.eclipse.emf.ecp.ui.view.editor.controls", feature.getPlugin1());
		assertEquals("eclipse_update_120.jpg", feature.getImage());
	}
	
	@Test
	public void testLoadFragmentFeature() throws IOException {
		DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, getClass().getResource("feature_fragment.xml"), "feature.xml");
		Feature feature = root.getFeature();
		assertNotNull(feature);
		assertEquals("org.eclipse.xtext.runtime", feature.getId());
		assertEquals(3, feature.getPlugin().size());
		assertFalse(feature.getPlugin().get(0).isFragment());
		assertTrue(feature.getPlugin().get(1).isFragment());
		assertFalse(feature.getPlugin().get(2).isFragment());
	}

	
	@Test
	public void testLoadContent() throws IOException {
		DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, getClass().getResource("content.xml"), "content.xml");
		Repository repository = root.getRepository();
		assertNotNull(repository);
		assertEquals("file:/C:/Documents and Settings/agcattle/workspace/mirrorSourceRepo4/ - metadata", repository.getName());
		assertEquals("org.eclipse.equinox.internal.p2.metadata.repository.LocalMetadataRepository", repository.getType());
		assertEquals("1", repository.getVersion());
		assertNotNull(repository.getProperties());
		assertEquals(2, repository.getProperties().getSize());
	}
	
	@Test
	public void testLoadContent153() throws IOException {
		DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, getClass().getResource("content_15.3.xml"), "content_15.3.xml");
		Repository repository = root.getRepository();
		assertNotNull(repository);
		assertEquals("p2-repository", repository.getName());
		assertEquals("org.eclipse.equinox.internal.p2.metadata.repository.LocalMetadataRepository", repository.getType());
		assertEquals("1", repository.getVersion());
		assertNotNull(repository.getProperties());
		assertEquals(2, repository.getProperties().getSize());
	}
	
	@Test
	public void testLoadArtifacts() throws IOException {
		DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, getClass().getResource("artifacts.xml"), "artifacts.xml");
		Repository repository = root.getRepository();
		assertNotNull(repository);
		assertEquals("Bundle pool", repository.getName());
		assertEquals("org.eclipse.equinox.p2.artifact.repository.simpleRepository", repository.getType());
		assertEquals("1.0.0", repository.getVersion());
		assertNotNull(repository.getProperties());
		assertEquals(2, repository.getProperties().getSize());
		assertNotNull(repository.getMappings());
		assertEquals(3, repository.getMappings().getSize());
		Rule rule = repository.getMappings().getRule().get(1);
		assertEquals("(& (classifier=binary))", rule.getFilter());
		assertEquals("${repoUrl}/binary/${id}_${version}", rule.getOutput());
		assertEquals(2323, repository.getArtifacts().getSize());
		
		Artifact artifact = repository.getArtifacts().getArtifact().get(0);
		assertEquals("org.eclipse.rcp", artifact.getId());
		assertEquals(1, artifact.getProperties().getSize());
		assertEquals(1, artifact.getProperties().getProperty().size());
		assertEquals(1, artifact.getRepositoryProperties().getSize());
		assertEquals(1, artifact.getRepositoryProperties().getProperty().size());
		
	}
	
	@Test
	public void testLoadCompositeArtifacts() throws IOException {
		DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, getClass().getResource("compositeArtifacts.xml"), "compositeArtifacts.xml");
		Repository repository = root.getRepository();
		assertNotNull(repository);
		assertEquals("artifact name", repository.getName());
		assertEquals("org.eclipse.equinox.internal.p2.artifact.repository.CompositeArtifactRepository", repository.getType());
		assertEquals("1.0.0", repository.getVersion());
		assertNotNull(repository.getProperties());
		assertEquals(3, repository.getProperties().getSize());
		assertEquals("p2.compressed", repository.getProperties().getProperty().get(0).getName());
		assertEquals("false", repository.getProperties().getProperty().get(0).getValue());
		assertEquals("p2.timestamp", repository.getProperties().getProperty().get(1).getName());
		assertEquals("1234", repository.getProperties().getProperty().get(1).getValue());
		assertEquals("p2.atomic.composite.loading", repository.getProperties().getProperty().get(2).getName());
		assertEquals("true", repository.getProperties().getProperty().get(2).getValue());
		assertNull(repository.getMappings());
		assertNotNull(repository.getChildren());
		assertEquals(3, repository.getChildren().getSize());
		assertEquals("one", repository.getChildren().getChild().get(0).getLocation());
		assertEquals("two", repository.getChildren().getChild().get(1).getLocation());
		assertEquals("three", repository.getChildren().getChild().get(2).getLocation());
	}
	
	@Test
	public void testLoadTargetDefinition() throws IOException {
		DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, getClass().getResource("target.target"), "target.xml");
		Target target = root.getTarget();
		assertNotNull(target);
		assertEquals("Running Platform Test", target.getName());
		assertNotNull(target.getLocations());
		TargetLocations locs = target.getLocations();
		assertEquals(5, locs.getLocation().size());
		TargetLocation loc1 = locs.getLocation().get(0);
		assertEquals("${eclipse_home}", loc1.getPath());
		assertEquals(LocationIncludeType.NONE, loc1.getIncludeMode());
		assertEquals(TargetType.PROFILE, loc1.getType());
		assertTrue(loc1.isIncludeAllPlatforms());
		assertTrue(loc1.isIncludeSource());
		assertFalse(loc1.isIncludeConfigurePhase());
		assertNull(loc1.getRepository());
		assertTrue(loc1.getUnit().isEmpty());
		
		TargetLocation loc2 = locs.getLocation().get(1);
		assertEquals("/home/mark/Software/eclipse/gecko-features/eclipse", loc2.getPath());
		assertEquals(TargetType.DIRECTORY, loc2.getType());
		assertEquals(LocationIncludeType.NONE, loc2.getIncludeMode());
		assertTrue(loc2.isIncludeAllPlatforms());
		assertTrue(loc2.isIncludeSource());
		assertFalse(loc2.isIncludeConfigurePhase());
		assertNull(loc2.getRepository());
		assertTrue(loc2.getUnit().isEmpty());
		
		TargetLocation loc3 = locs.getLocation().get(2);
		assertEquals("org.eclipse.ecf.filetransfer.ssl.feature", loc3.getId());
		assertEquals("/home/mark/Software/eclipse/eclipse-installer", loc3.getPath());
		assertTrue(loc3.isIncludeAllPlatforms());
		assertTrue(loc3.isIncludeSource());
		assertFalse(loc3.isIncludeConfigurePhase());
		assertEquals(TargetType.FEATURE, loc3.getType());
		assertEquals(LocationIncludeType.NONE, loc3.getIncludeMode());
		assertNull(loc3.getRepository());
		assertTrue(loc3.getUnit().isEmpty());
		
		TargetLocation loc4 = locs.getLocation().get(4);
		assertNull(loc4.getId());
		assertNull(loc4.getPath());
		assertFalse(loc4.isIncludeAllPlatforms());
		assertFalse(loc4.isIncludeSource());
		assertTrue(loc4.isIncludeConfigurePhase());
		assertEquals(TargetType.INSTALLABLE_UNIT, loc4.getType());
		assertEquals(LocationIncludeType.PLANNER, loc4.getIncludeMode());
		assertNotNull(loc4.getRepository());
		assertEquals(2, loc4.getUnit().size());
		Unit u1 = loc4.getUnit().get(0);
		assertEquals("org.gecko.emf.osgi.feature.feature.group", u1.getId());
		assertEquals("2.2.1.REL-20180926-082720", u1.getVersion());
		Unit u2 = loc4.getUnit().get(1);
		assertEquals("org.gecko.emf.osgi.tools.feature.feature.group", u2.getId());
		assertEquals("2.2.1.REL-20180926-082720", u2.getVersion());
		
		assertNotNull(target.getEnvironment());
		assertEquals("x86_64", target.getEnvironment().getArch());
		assertEquals("linux", target.getEnvironment().getOs());
		assertEquals("gtk", target.getEnvironment().getWs());
		assertEquals("de_DE", target.getEnvironment().getNl());
		
		assertNotNull(target.getTargetJRE());
		assertEquals("org.eclipse.jdt.launching.JRE_CONTAINER/org.eclipse.jdt.internal.debug.ui.launcher.StandardVMType/JavaSE-11", target.getTargetJRE().getPath());
		
		assertNotNull(target.getLauncherArgs());
		assertEquals("-Dtest", target.getLauncherArgs().getProgramArgs());
		assertEquals("-Dvm", target.getLauncherArgs().getVmArgs());
		
		assertNotNull(target.getImplicitDependencies());
		assertEquals(4, target.getImplicitDependencies().getPlugin().size());
		assertEquals("biz.aQute.resolve", target.getImplicitDependencies().getPlugin().get(0).getId());
		assertEquals("biz.aQute.bnd.embedded-repo", target.getImplicitDependencies().getPlugin().get(1).getId());
		assertEquals("biz.aQute.repository", target.getImplicitDependencies().getPlugin().get(2).getId());
		assertEquals("biz.aQute.bndlib", target.getImplicitDependencies().getPlugin().get(3).getId());
	}
	
	@Test
	public void testLoadTargetDefinition2() throws IOException {
		DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, getClass().getResource("second.target"), "target.xml");
		Target target = root.getTarget();
		assertNotNull(target);
	}
	
	@Test
	public void testLoadCategory() throws IOException {
		DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, getClass().getResource("category.xml"), "category.xml");
		CategorySite site = root.getSite();
		assertNotNull(site);
		assertEquals(1, site.getCategoryDef().size());
		CategoryDef cat = site.getCategoryDef().get(0);
		assertEquals("gecko-emf.category", cat.getName());
		assertEquals("Gecko EMF", cat.getLabel());
		assertEquals("OSGi based EMF including development tools extensions for Eclipse, based on bnd and the Gecko project", cat.getDescription().trim());
		assertEquals(2, site.getFeature().size());
		CategoryFeature cf01 = site.getFeature().get(0);
		assertEquals("org.gecko.emf.osgi.feature", cf01.getId());
		assertEquals("0.0.0", cf01.getVersion());
		assertEquals("gecko-emf.category", cf01.getCategory().getName());
		CategoryFeature cf02 = site.getFeature().get(1);
		assertEquals("org.gecko.emf.osgi.tools.feature", cf02.getId());
		assertEquals("0.0.0", cf02.getVersion());
		assertEquals("gecko-emf.category", cf02.getCategory().getName());
	}

}
