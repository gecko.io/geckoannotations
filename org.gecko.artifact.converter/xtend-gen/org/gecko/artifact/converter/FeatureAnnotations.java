/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved.
 * 
 * This program and the accompanying materials are made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.converter;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;
import org.gecko.artifact.api.ArtifactConstants;
import org.gecko.artifacts.ArtifactReference;
import org.gecko.artifacts.EclipseFeature;
import org.gecko.artifacts.ReferenceType;

/**
 * Creates a
 * @author mark
 * @since 25.02.2020
 */
@SuppressWarnings("all")
public class FeatureAnnotations {
  public CharSequence convertFeature(final EclipseFeature feature) {
    return this.toAnnotation(feature);
  }
  
  public String getPackageSegments(final String name) {
    return name.replace("-", ".").replace("package", "p1ckage");
  }
  
  public String getImportSegments(final String name) {
    final String n = name.replace("-", ".").replace("package", "p1ckage");
    String _featureName = this.getFeatureName(n);
    return ((n + ".") + _featureName);
  }
  
  public String getFeatureName(final String name) {
    String _xblockexpression = null;
    {
      final String n = name.replace("-", ".").replace("package", "p1ckage");
      String[] segs = n.split("\\.");
      int _length = segs.length;
      boolean _greaterThan = (_length > 2);
      if (_greaterThan) {
        segs = Arrays.<String>copyOfRange(segs, 2, segs.length);
      }
      final String[] _converted_segs = (String[])segs;
      final Function<String, String> _function = (String t) -> {
        return StringExtensions.toFirstUpper(t);
      };
      _xblockexpression = ((List<String>)Conversions.doWrapArray(_converted_segs)).stream().<String>map(_function).collect(Collectors.joining(""));
    }
    return _xblockexpression;
  }
  
  public CharSequence toAnnotation(final EclipseFeature feature) {
    CharSequence _xblockexpression = null;
    {
      final Function1<ArtifactReference, Boolean> _function = (ArtifactReference r) -> {
        return Boolean.valueOf((ReferenceType.REQUIRE_ARTIFACT.equals(r.getType()) || ReferenceType.INCLUDE_ARTIFACT.equals(r.getType())));
      };
      final Function1<ArtifactReference, Boolean> _function_1 = (ArtifactReference r) -> {
        boolean _endsWith = r.getName().endsWith("source");
        return Boolean.valueOf((!_endsWith));
      };
      final Iterable<ArtifactReference> incReq = IterableExtensions.<ArtifactReference>filter(IterableExtensions.<ArtifactReference>filter(feature.getReferences(), _function), _function_1);
      final Function1<ArtifactReference, Boolean> _function_2 = (ArtifactReference r) -> {
        return Boolean.valueOf(ArtifactConstants.P2_OSGI_BUNDLE.equals(r.getFilter()));
      };
      final Iterable<ArtifactReference> includes = IterableExtensions.<ArtifactReference>filter(incReq, _function_2);
      final Function1<ArtifactReference, Boolean> _function_3 = (ArtifactReference r) -> {
        return Boolean.valueOf(ArtifactConstants.P2_ECLIPSE_FEATURE.equals(r.getFilter()));
      };
      final Iterable<ArtifactReference> requires = IterableExtensions.<ArtifactReference>filter(incReq, _function_3);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("package ");
      String _packageSegments = this.getPackageSegments(feature.getName());
      _builder.append(_packageSegments);
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _builder.newLine();
      _builder.append("import static java.lang.annotation.ElementType.PACKAGE;");
      _builder.newLine();
      _builder.append("import static java.lang.annotation.ElementType.TYPE;");
      _builder.newLine();
      _builder.append("import static java.lang.annotation.RetentionPolicy.CLASS;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("import java.lang.annotation.Documented;");
      _builder.newLine();
      _builder.append("import java.lang.annotation.Retention;");
      _builder.newLine();
      _builder.append("import java.lang.annotation.Target;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("import org.osgi.annotation.bundle.Requirement;");
      _builder.newLine();
      _builder.newLine();
      {
        for(final ArtifactReference require : requires) {
          _builder.append("import ");
          String _importSegments = this.getImportSegments(require.getName());
          _builder.append(_importSegments);
          _builder.append(";");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.newLine();
      _builder.append("/**");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("* Description:");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("* ");
      String _label = feature.getLabel();
      _builder.append(_label, " ");
      _builder.newLineIfNotEmpty();
      _builder.append(" ");
      _builder.append("*");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("*/");
      _builder.newLine();
      {
        for(final ArtifactReference require_1 : requires) {
          _builder.append(" ");
          _builder.append("@");
          String _featureName = this.getFeatureName(require_1.getName());
          _builder.append(_featureName, " ");
          _builder.newLineIfNotEmpty();
        }
      }
      {
        for(final ArtifactReference include : includes) {
          _builder.append(" ");
          _builder.append("@Requirement(namespace=\"osgi.identity\", name=\"");
          String _name = include.getName();
          _builder.append(_name, " ");
          _builder.append("\")");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append(" ");
      _builder.append("@Documented");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("@Retention(CLASS)");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("@Target({ TYPE, PACKAGE })");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("public @interface ");
      String _featureName_1 = this.getFeatureName(feature.getName());
      _builder.append(_featureName_1, " ");
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.append(" ");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("}");
      _builder.newLine();
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
}
