/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved.
 * 
 * This program and the accompanying materials are made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.converter;

import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.StringExtensions;
import org.gecko.eclipse.Feature;
import org.gecko.eclipse.Import;
import org.gecko.eclipse.Include;
import org.gecko.eclipse.Plugin;

/**
 * Creates a
 * @author mark
 * @since 25.02.2020
 */
@SuppressWarnings("all")
public class AnnotationConverter {
  public CharSequence convertFeature(final Feature feature) {
    return this.toAnnotation(feature);
  }
  
  public CharSequence toAnnotation(final Feature feature) {
    CharSequence _xblockexpression = null;
    {
      final String label = feature.getLabel().replaceAll("\\s+", "");
      final EList<Plugin> plugins = feature.getPlugin();
      final EList<Import> imports = feature.getRequires().getImport();
      final EList<Include> includes = feature.getIncludes();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("package ");
      String _id = feature.getId();
      _builder.append(_id);
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _builder.newLine();
      _builder.append("import static java.lang.annotation.ElementType.PACKAGE;");
      _builder.newLine();
      _builder.append("import static java.lang.annotation.ElementType.TYPE;");
      _builder.newLine();
      _builder.append("import static java.lang.annotation.RetentionPolicy.CLASS;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("import java.lang.annotation.Documented;");
      _builder.newLine();
      _builder.append("import java.lang.annotation.Retention;");
      _builder.newLine();
      _builder.append("import java.lang.annotation.Target;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("import org.osgi.annotation.bundle.Requirement;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("/**");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("* Description:");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("* ");
      String _value = feature.getDescription().getValue();
      _builder.append(_value, " ");
      _builder.newLineIfNotEmpty();
      _builder.append(" ");
      _builder.append("* URL: ");
      String _url = feature.getDescription().getUrl();
      _builder.append(_url, " ");
      _builder.newLineIfNotEmpty();
      _builder.append(" ");
      _builder.append("*");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("*/");
      _builder.newLine();
      _builder.append(" ");
      _builder.newLine();
      {
        for(final Include inc : includes) {
          _builder.append(" ");
          _builder.append("@Requirement(namespace=\"osgi.identity\", name=\"");
          String _id_1 = inc.getId();
          _builder.append(_id_1, " ");
          _builder.append("\")");
          _builder.newLineIfNotEmpty();
        }
      }
      {
        for(final Import imp : imports) {
          _builder.append(" ");
          _builder.append("@Requirement(namespace=\"osgi.identity\", name=\"");
          String _plugin = imp.getPlugin();
          _builder.append(_plugin, " ");
          _builder.append("\")");
          _builder.newLineIfNotEmpty();
        }
      }
      {
        for(final Plugin p : plugins) {
          _builder.append(" ");
          _builder.append("@Requirement(namespace=\"osgi.identity\", name=\"");
          String _id_2 = p.getId();
          _builder.append(_id_2, " ");
          _builder.append("\")\t");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append(" ");
      _builder.append("@Documented");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("@Retention(CLASS)");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("@Target({ TYPE, PACKAGE })");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("public @interface Feature");
      String _firstUpper = StringExtensions.toFirstUpper(label);
      _builder.append(_firstUpper, " ");
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.append(" ");
      _builder.newLine();
      _builder.append(" ");
      _builder.append("}");
      _builder.newLine();
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
}
