/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.converter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.gecko.artifact.api.ArtifactLoader;
import org.gecko.artifact.helper.ArtifactsHelper;
import org.gecko.artifacts.ArtifactsPackage;
import org.gecko.artifacts.EclipseFeature;
import org.gecko.artifacts.P2Repository;
import org.gecko.eclipse.Artifact;
import org.gecko.eclipse.EclipsePackage;
import org.gecko.emf.repository.EMFRepository;
import org.osgi.service.component.ComponentServiceObjects;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceScope;
import org.osgi.util.promise.Promise;
import org.osgi.util.promise.PromiseFactory;

import aQute.bnd.osgi.Processor;

@Component(immediate = true)
public class Example {

	private final static Logger logger = Logger.getLogger(Example.class.getName());
	@Reference(target = "(artifactRepositoryType=cached)")
	private ArtifactLoader loader;
	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED, target = "(repo_id=artifacts.artifacts)")
	private ComponentServiceObjects<EMFRepository> repository;
	private String projectPath = "/opt/git/geckoAnnotations/org.gecko.eclipse.feature/src-gen/";

	@Activate
	public void activate() {
		try {
			Processor.getPromiseFactory().resolved(null).delay(3000).onResolve(()->{
				try {
					P2Repository repository = (P2Repository) loader.loadRepository("https://devel.data-in-motion.biz/repository/eclipse-2019-09");
//					P2Repository repository = (P2Repository) loader.loadRepository("http://download.eclipse.org/releases/2019-12");
//					P2Repository repository = (P2Repository) loader.loadRepository("https://devel.data-in-motion.biz/public/repository/gecko/release/geckoEMF.eclipse.tooling");
//					ArtifactRepository repository = loader.loadRepository("http://download.eclipse.org/releases/2019-12");
//					ArtifactRepository repository = loader.loadRepository("https://devel.data-in-motion.biz/public/repository/gecko/release/geckoEMF.eclipse.tooling");
					System.out.println("Repos : " + repository);
					System.out.println("Repo parent: " + repository.getParent());
//					System.out.println("Repo parent-parent: " + repository.getParent().getParent());
					System.out.println("Repo size: " + repository.getRepositories().size());
					System.out.println("Root Artifacts size: " + repository.getArtifacts().size());
					if (repository.getRepositories().size() == 2) {
						System.out.println("Artifacts size: " + repository.getRepositories().get(0).getArtifacts().size());
						System.out.println("Artifacts size: " + repository.getRepositories().get(1).getArtifacts().size());
					}
					System.out.println("Root All Artifacts size: " + repository.getAllArtifacts().size());
					List<EclipseFeature> features = repository.getEclipseFeatures();
					System.out.println("Features size: " + features.size());
					features.stream().filter(f->!f.getName().endsWith("source")).forEach(this::generateFeatureAnnotation);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
//		Feature f = EclipseFactory.eINSTANCE.createFeature();
//		f.setId("org.eclipse.test");
//		f.setLabel("MyTest");
//		Description d = EclipseFactory.eINSTANCE.createDescription();
//		d.setUrl("https://www.datainmotion.de");
//		d.setValue("this is a typical test feature");
//		f.setDescription(d);
//		Include i01 = EclipseFactory.eINSTANCE.createInclude();
//		i01.setId("org.eclipse.core.runtime");
//		f.getIncludes().add(i01);
//		Include i02 = EclipseFactory.eINSTANCE.createInclude();
//		i02.setId("org.eclipse.core.resources");
//		f.getIncludes().add(i02);
//		Include i03 = EclipseFactory.eINSTANCE.createInclude();
//		i03.setId("org.eclipse.ui");
//		f.getIncludes().add(i03);
//		ImportRequires requires = EclipseFactory.eINSTANCE.createImportRequires();
//		f.setRequires(requires);
//		//AnnotationConverter converter = new AnnotationConverter();
//		//String test = converter.convertFeature(f).toString();
//		//System.err.println("Result: " + test);
	}

	public List<Artifact> getArtifacts(URI uri) {
		if (uri == null) {
			logger.log(Level.WARNING, "Cannot get artifacts for a null URI");
			return Collections.emptyList();
		}
		try {
			FeatureLoader loader = new FeatureLoader(uri);
			Promise<List<Artifact>> repoProm = loader.getAllP2Artifacts();
			return repoProm.getValue();
		} catch (Exception e) {
			throw new IllegalStateException(String.format("Error getting artifacts for %s", uri ), e);
		}
	}

	public Promise<Object> testLoadEclipseDIMFeatures() throws IOException, InvocationTargetException, InterruptedException {
		System.out.println("########## Gecko.io Eclipse 2019-09 Features ##########");
		PromiseFactory pf = Processor.getPromiseFactory();
		return pf.submit(()->{
			long start = System.currentTimeMillis();
			try {
				FeatureLoader loader = new FeatureLoader(URI.create("https://devel.data-in-motion.biz/repository/eclipse-2019-09"));
				List<Artifact> artifacts = loader.getFeatureArtifacts(true);
				System.out.println("Getting feature artifacts took " + (System.currentTimeMillis()-start) + " ms");
				start = System.currentTimeMillis();
				artifacts.stream()
					.map(loader::getFeature)
					.filter(f->!f.getId().startsWith("ERROR"))
					.map(EcoreUtil::copy)
					.forEach(f->{
						EMFRepository service = repository.getService();
						service.getResourceSet().getPackageRegistry().put(EclipsePackage.eNS_URI, EclipsePackage.eINSTANCE);
						service.getResourceSet().getPackageRegistry().put(ArtifactsPackage.eNS_URI, ArtifactsPackage.eINSTANCE);
						service.save(f);
						repository.ungetService(service);
						System.out.println("Saved feature: " + f);
					});
				System.out.println(String.format("Loading %s features took %s ms", artifacts.size(), (System.currentTimeMillis()-start)));
				System.out.println("Loading features took " + (System.currentTimeMillis()-start) + " ms");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		});
	}
	
	public Promise<Void> testLoadEclipseDIMBundles() throws IOException, InvocationTargetException, InterruptedException {
		System.out.println("########## Gecko.io Eclipse 2019-09 Bundles s##########");
		PromiseFactory pf = Processor.getPromiseFactory();
		return pf.submit(()->{
			long start = System.currentTimeMillis();
			try {
				FeatureLoader loader = new FeatureLoader(URI.create("https://devel.data-in-motion.biz/repository/eclipse-2019-09"));
				List<Artifact> artifacts = loader.getBundleArtifacts(true);
				System.out.println("Getting bundle artifacts took " + (System.currentTimeMillis()-start) + " ms");
				start = System.currentTimeMillis();
				artifacts.stream()
				.map(loader::getBundle)
				.filter(b->b != null)
				.map(EcoreUtil::copy)
				.forEach(b->{
					EMFRepository service = repository.getService();
					service.getResourceSet().getPackageRegistry().put(EclipsePackage.eNS_URI, EclipsePackage.eINSTANCE);
					service.getResourceSet().getPackageRegistry().put(ArtifactsPackage.eNS_URI, ArtifactsPackage.eINSTANCE);
					try {
						if (!b.getDependency().isEmpty()) {
							service.save(b.getDependency().stream().map(d->(EObject)d).collect(Collectors.toList()), null);
						}
						service.save(b);
						if (!b.getDependency().isEmpty()) {
							b.getDependency().forEach(d->d.setSource(b));
							service.save(b.getDependency().stream().map(d->(EObject)d).collect(Collectors.toList()), null);
						}
						System.out.println("Saved bundle: " + b);
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						repository.ungetService(service);
					}
				});
				System.out.println(String.format("Loading %s bundles took %s ms", artifacts.size(), (System.currentTimeMillis()-start)));
				System.out.println("Loading bundles took " + (System.currentTimeMillis()-start) + " ms");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		});
	}
	
	private void generateFeatureAnnotation(EclipseFeature feature) {
		Path base = Paths.get(URI.create("file:" + projectPath));
		File baseFolder = base.toFile();
		FeatureAnnotations fa = new FeatureAnnotations();
		String segments = fa.getPackageSegments(feature.getName());
		String fileName = fa.getFeatureName(feature.getName()) + ".java";
		String path = segments.replace(".", "/");
		File sourceFolder = new File(baseFolder, path);
		if (!sourceFolder.exists()) {
			sourceFolder.mkdirs();
		}
		File javaFile = new File(sourceFolder, fileName);
		try (FileOutputStream fos = new FileOutputStream(javaFile)) {
			String featureAnnotation = fa.convertFeature(feature).toString();
			fos.write(featureAnnotation.getBytes());
			fos.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
