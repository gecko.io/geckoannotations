/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.converter

import org.gecko.eclipse.Feature
import java.util.List
import org.gecko.eclipse.Plugin
import org.gecko.eclipse.Include
import org.gecko.eclipse.Import

/**
 * Creates a 
 * @author mark
 * @since 25.02.2020
 */
class AnnotationConverter {
	
	def CharSequence convertFeature(Feature feature) {
		feature.toAnnotation
	}
	
	//def CharSequence convertFeature(Feature feature, EclipseResourceFileSystemAccess2 fsa) {
		
		
	//}
	
	def toAnnotation(Feature feature) {
		val label = feature.label.replaceAll("\\s+","");
		val plugins = feature.plugin;
		val imports = feature.requires.import;
		val includes = feature.includes;
'''
package «feature.id»;

import static java.lang.annotation.ElementType.PACKAGE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.osgi.annotation.bundle.Requirement;

/**
 * Description:
 * «feature.description.value»
 * URL: «feature.description.url»
 *
 */
 
 «FOR inc : includes»
 @Requirement(namespace="osgi.identity", name="«inc.id»")
 «ENDFOR»
 «FOR imp : imports»
 @Requirement(namespace="osgi.identity", name="«imp.plugin»")
 «ENDFOR»
 «FOR p : plugins»
 @Requirement(namespace="osgi.identity", name="«p.id»")	
 «ENDFOR»
 @Documented
 @Retention(CLASS)
 @Target({ TYPE, PACKAGE })
 public @interface Feature«label.toFirstUpper» {
 
 }
'''
}

	
}