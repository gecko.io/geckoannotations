/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.converter;

import static aQute.bnd.header.OSGiHeader.parseHeader;
import static aQute.lib.promise.PromiseCollectors.toPromise;
import static java.util.stream.Collectors.toList;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.gecko.artifact.converter.helper.EclipseModelHelper;
import org.gecko.artifacts.ArtifactsFactory;
import org.gecko.artifacts.ArtifactsPackage;
import org.gecko.artifacts.Dependency;
import org.gecko.artifacts.DependencyType;
import org.gecko.artifacts.OSGiBundle;
import org.gecko.eclipse.Artifact;
import org.gecko.eclipse.Artifacts;
import org.gecko.eclipse.ChildrenType;
import org.gecko.eclipse.DocumentRoot;
import org.gecko.eclipse.EclipseFactory;
import org.gecko.eclipse.EclipsePackage;
import org.gecko.eclipse.Feature;
import org.gecko.eclipse.Property;
import org.gecko.eclipse.Repository;
import org.gecko.eclipse.RepositoryRef;
import org.gecko.eclipse.Rule;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.util.promise.Deferred;
import org.osgi.util.promise.Promise;
import org.osgi.util.promise.PromiseFactory;
import org.tukaani.xz.XZInputStream;

import aQute.bnd.header.Attrs;
import aQute.bnd.header.Parameters;
import aQute.bnd.http.HttpClient;
import aQute.bnd.http.HttpRequest;
import aQute.bnd.osgi.Jar;
import aQute.bnd.osgi.Resource;
import aQute.bnd.util.dto.DTO;
import aQute.lib.io.IO;
import aQute.lib.strings.Strings;

/**
 * 
 * @author mark
 * @since 27.02.2020
 */
public class FeatureLoader {

	private final static Logger logger = Logger.getLogger(FeatureLoader.class.getName());
	private PromiseFactory promiseFactory = new PromiseFactory(Executors.newFixedThreadPool(3));
	private final Set<URI> defaults	= Collections.newSetFromMap(new ConcurrentHashMap<URI, Boolean>());
	private final HttpClient client;
	private final URI base;
	private final ResourceSet resourceSet;

	private static class P2Index extends DTO {
		public long			modified;
		public List<URI>	content		= new ArrayList<>();
		public List<URI>	artifacts	= new ArrayList<>();
	}

	private static class FeatureContext {
		File file;
		Properties properties;
		boolean isJar = false;
		boolean isPacked = false;
		String path;
	}

	public FeatureLoader(URI base) throws Exception {
		client = new HttpClient();
		resourceSet = EclipseModelHelper.createResourceSet();
		this.base = normalize(base);
	}

	public Promise<List<Artifact>> getAllP2Artifacts() {
		Set<URI> cycles = Collections.newSetFromMap(new ConcurrentHashMap<URI, Boolean>());
		return getP2Artifacts(cycles, base);
	}

	public List<Artifact> getArtifacts() {
		try {
			Promise<List<Artifact>> repoProm = getAllP2Artifacts();
			return repoProm.getValue();
		} catch (Exception e) {
			throw new IllegalStateException(String.format("Error getting artifacts for %s", base ), e);
		}
	}

	public Promise<Feature> loadFeature(Artifact artifact) throws Exception {
		return loadFeatureFile(artifact).onFailure(t->logger.log(Level.SEVERE, "Error loading feature jar", t)).map(ctx->{
			if (ctx.file == null) {
				throw new IllegalStateException(String.format("Returned file for artifact %s is null", ctx.path));
			}
			Feature f;
			if (ctx.isJar) {
				try (Jar featureJar = new Jar(ctx.file)) {
					try (Resource props = featureJar.getResource("feature.properties")) {
						if (props != null && props.size() > 0) {
							Properties p = new Properties();
							p.load(props.openInputStream());
							ctx.properties = p;
						}
					}
					try (Resource resource = featureJar.getResource("feature.xml")) {
						DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, resource.openInputStream(), "feature.xml");
						f = root.getFeature();
					}
				}
			} else {
				DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, ctx.file, "feature.xml");
				f = root.getFeature();
			}
			return f != null ? (Feature)replaceProperties(f, ctx.properties) : null;
		});
	}

	private EObject replaceProperties(EObject eObject, Properties properties) {
		if (properties == null) {
			logger.log(Level.WARNING, "Cannot replace values without properties");
			return eObject;
		}
		if (eObject == null) {
			logger.log(Level.WARNING, "Cannot replace values without EObject");
			return null;
		}
		List<EAttribute> stringAttributes = eObject.eClass()
				.getEAllAttributes()
				.stream()
				.filter(a->a.getEType().getName().equals("String") || a.getEType().getName().equals("EString"))
				.collect(Collectors.toList());
		stringAttributes.stream().forEach(a->{
			Object o = eObject.eGet(a);
			if (o != null && o.toString().startsWith("%")) {
				String key = o.toString().replace("%", "");
				String value = properties.getProperty(key);
				eObject.eSet(a, value);
			}
		});
		return eObject;
	}

	public Promise<OSGiBundle> loadBundle(Artifact artifact) throws Exception {
		return loadBundleFile(artifact).onFailure(t->logger.log(Level.SEVERE, "Error loading feature jar", t)).map(ctx->{
			if (ctx.file == null) {
				throw new IllegalStateException(String.format("Returned file for artifact %s is null", ctx.path));
			}
			if (ctx.isJar) {
				try (Jar featureJar = new Jar(ctx.file)) {
					OSGiBundle bundle = ArtifactsFactory.eINSTANCE.createOSGiBundle();
					try (Resource resource = featureJar.getResource("META-INF/MANIFEST.MF")) {
						Manifest mf = new Manifest(resource.openInputStream());
						bundle.setCacheLocation(ctx.file.getPath());
						bundle.setSourceLocation(ctx.path);
						setManifestEntry(bundle, mf, "Bundle-SymbolicName", ArtifactsPackage.Literals.OS_GI_BUNDLE__BSN);
						setManifestEntry(bundle, mf, "Bundle-Name", ArtifactsPackage.Literals.OS_GI_BUNDLE__NAME);
						setManifestEntry(bundle, mf, "Bundle-License", ArtifactsPackage.Literals.OS_GI_BUNDLE__LICENSE);
						setManifestEntry(bundle, mf, "Bundle-Copyright", ArtifactsPackage.Literals.OS_GI_BUNDLE__COPYRIGHT);
						setManifestEntry(bundle, mf, "Bundle-Version", ArtifactsPackage.Literals.OS_GI_BUNDLE__VERSION);
						setManifestEntry(bundle, mf, "Bundle-Vendor", ArtifactsPackage.Literals.OS_GI_BUNDLE__VENDOR);
						setManifestEntry(bundle, mf, "Bundle-Localization", ArtifactsPackage.Literals.OS_GI_BUNDLE__LOCALIZATION);
						setManifestEntry(bundle, mf, "Bundle-RequiredExecutionEnvironment", ArtifactsPackage.Literals.OS_GI_BUNDLE__EXECUTION_ENVIRONMENT);
						createDependencies(bundle, mf, "Require-Bundle", DependencyType.REQUIRE_BUNDLE);
						createDependencies(bundle, mf, "Import-Package", DependencyType.IMPORT);
						createDependencies(bundle, mf, "Export-Package", DependencyType.EXPORT);
						createDependencies(bundle, mf, "Service-Component", DependencyType.DS);
						createDependencies(bundle, mf, "Provide-Capability", DependencyType.CAPABILITY);
						createDependencies(bundle, mf, "Require-Capability", DependencyType.REQUIREMENT);
					}
					Map<String, Resource> directory = featureJar.getDirectory("model");
					if (directory != null && !directory.isEmpty()) {
						directory.entrySet().stream().filter(e->e.getKey().endsWith("ecore")).forEach(e->createModelDependency(bundle, e.getKey(), e.getValue()));
					}
					if (bundle.getLocalization() != null) {
						String propFileName = bundle.getLocalization() + ".properties";
						try (Resource props = featureJar.getResource(propFileName)) {
							if (props != null && props.size() > 0) {
								Properties p = new Properties();
								p.load(props.openInputStream());
								ctx.properties = p;
							}
						}
						return (OSGiBundle)replaceProperties(bundle, ctx.properties);
					} else {
						return bundle;
					}
				}
			} else {
				return null;
			}
		});
	}

	/**
	 * @param bundle
	 * @param key
	 * @param value
	 */
	private void createModelDependency(OSGiBundle bundle, String key, Resource value) {
		if (bundle == null || key == null || value == null) {
			logger.log(Level.WARNING, "Cannot create dependency without file");
			return;
		}
		Dependency dep = ArtifactsFactory.eINSTANCE.createDependency();
		dep.setType(DependencyType.ECORE);
		bundle.getDependency().add(dep);
		dep.setName(key);
		org.eclipse.emf.ecore.resource.Resource r = resourceSet.createResource(org.eclipse.emf.common.util.URI.createURI(key));
		if (r != null) {
			try {
				r.load(value.openInputStream(), null);
				EPackage ePackage = (EPackage) r.getContents().get(0);
				dep.setLocation(ePackage.getNsURI());
				logger.info(String.format("Added package %s with ns %s for bundle %s", key, ePackage.getNsURI(), bundle.getBsn()));
			} catch (Exception e) {
				logger.log(Level.SEVERE, String.format("Cannot load ecore %s from bundle %s", key, bundle.getBsn()), e);
			}
		}
	}

	private void setManifestEntry(OSGiBundle bundle, Manifest mf, String attribute, EStructuralFeature feature) {
		if (bundle == null || mf == null || attribute == null || feature == null) {
			logger.log(Level.WARNING, "Cannot set a value if the parameters are not complete");
			return;
		}
		Attributes attributes = mf.getMainAttributes();
		Object value = attributes.getValue(attribute);
		if (value != null) {
			bundle.eSet(feature, value);
		}
	}

	private void createDependencies(OSGiBundle bundle, Manifest mf, String attribute, DependencyType type) {
		if (bundle == null || mf == null || attribute == null || type == null) {
			logger.log(Level.WARNING, "Cannot set a value if the parameters are not complete");
			return;
		}
		Attributes attributes = mf.getMainAttributes();
		Object value = attributes.getValue(attribute);
		if (value != null) {
			Parameters parameters = parseHeader(value.toString());
			parameters.entrySet().forEach(e->createDependency(bundle, type, e.getKey(), e.getValue()));
		}
	}

	private void createDependency(OSGiBundle bundle, DependencyType type, String key, Attrs attributes) {
		if (bundle == null || key == null || type == null || attributes == null) {
			logger.log(Level.WARNING, "Cannot create dependency if the parameters are not complete");
			return;
		}
		Dependency dep = ArtifactsFactory.eINSTANCE.createDependency();
		dep.setType(type);
		bundle.getDependency().add(dep);
		dep.setName(key);

		if (DependencyType.DS.equals(type)) {
			dep.setLocation(key);
		}

		if (checkAttribute(attributes, "resolution", "optional")) {
			dep.setOptional(true);
			attributes.remove("resolution");
		} else if (checkAttribute(attributes, "visibility", "reexport")) {
			dep.setReexport(true);
			attributes.remove("visibility");
		} else if (checkAttribute(attributes, "cardinality", "multiple")) {
			dep.setMultiple(true);
			attributes.remove("cardinality");
		} else if (checkAttribute(attributes, "effective", "active")) {
			dep.setActive(true);
			attributes.remove("effective");
		} else if (attributes.containsKey("filter")) {
			dep.setFilter(attributes.get("filter"));
			attributes.remove("filter");
		} else if (attributes.containsKey("version")) {
			dep.setVersion(attributes.get("version"));
			attributes.remove("version");
		} else if (attributes.containsKey("bundle-version")) {
			dep.setVersion(attributes.get("bundle-version"));
			attributes.remove("bundle-version");
		}

		String keyString = attributes.entrySet()
				.stream()
				.map(Entry::getKey)
				.filter(k->k.startsWith("objectClass"))
				.findFirst().orElse(null);
		if (keyString != null) {
			String v = attributes.get(keyString);
			dep.setObjectClass(v);
			org.gecko.artifacts.Property p = ArtifactsFactory.eINSTANCE.createProperty();
			p.setKey(keyString);
			p.setValue(v);
		}

		attributes.forEach((k,v)->{
			org.gecko.artifacts.Property p = ArtifactsFactory.eINSTANCE.createProperty();
			p.setKey(k);
			p.setValue(v);
			dep.getDependencyProperty().add(p);
		});
	}

	private boolean checkAttribute(Attrs attributes, String key, String expected) {
		String v = attributes.get(key);
		return expected.equalsIgnoreCase(v);
	}

	public Feature getFeature(Artifact artifact) {
		Promise<Feature> promise;
		try {
			promise = loadFeature(artifact);
			return promise.getValue();
		} catch (Exception e) {
			Map<String, String> repositoryProperties = getRepositoryProperties(artifact);
			String repoUriString = repositoryProperties.get("repositoryURI");
			logger.log(Level.SEVERE, String.format("Cannot load feature %s from location '%s'", artifact.getId(), repoUriString));
			Feature f = EclipseFactory.eINSTANCE.createFeature();
			f.setId("ERROR-" + artifact.getId());
			f.setVersion(artifact.getVersion());
			f.setLabel(repoUriString);
			return f;
		}
	}

	public OSGiBundle getBundle(Artifact artifact) {
		Promise<OSGiBundle> promise;
		try {
			promise = loadBundle(artifact);
			return promise.getValue();
		} catch (Exception e) {
			Map<String, String> repositoryProperties = getRepositoryProperties(artifact);
			String repoUriString = repositoryProperties.get("repositoryURI");
			logger.log(Level.SEVERE, String.format("Cannot load bundle %s from location '%s'", artifact.getId(), repoUriString));
			return null;
		}
	}



	private Map<String, String> getRepositoryProperties(Artifact artifact) {
		Repository repository = getRepository(artifact);
		return repository.getProperties()
				.getProperty()
				.stream()
				.collect( Collectors.toMap(Property::getName, Property::getValue));
	}

	private Promise<FeatureContext> loadFeatureFile(Artifact artifact) throws Exception {
		if (artifact == null) {
			logger.log(Level.WARNING, "Cannot load a feature without artifact");
			throw new IllegalStateException("Cannot load a feature without artifact");
		}
		Repository repository = getRepository(artifact);
		if (repository == null) {
			logger.log(Level.WARNING, "Cannot load a feature with an invalid artifact");
			throw new IllegalStateException("Cannot load a feature with an invalid artifact");
		}
		Map<String, String> repositoryProperties = getRepositoryProperties(artifact);

		final URI repoUri;
		String repoUriString = repositoryProperties.get("repositoryURI");
		if (repoUriString != null) {
			repoUri = normalize(URI.create(repoUriString));
		} else {
			repoUri = base;
		}
		List<Rule> rules = repository.getMappings() != null ? repository.getMappings().getRule() : Collections.emptyList();
		// create LDAP filter to output path map
		Map<Filter, String> filters = rules.stream()
				.filter(r->r.getFilter().contains("classifier=org.eclipse.update.feature"))
				.filter(r->createFilter(r.getFilter()) != null)
				.collect(Collectors.toMap(r->createFilter(r.getFilter()), Rule::getOutput));
		// prepare map to filter
		Dictionary<String, Object> filterProperties = new Hashtable<String, Object>();
		filterProperties.put("classifier", "org.eclipse.update.feature");

		// check content type to extend the filter map
		Map<String, String> properties = artifact.getProperties()
				.getProperty()
				.stream()
				.collect( Collectors.toMap(Property::getName, Property::getValue));

		FeatureContext ctx = new FeatureContext();
		String contentType = properties.get("download.contentType");
		if (contentType != null) {
			if (!"application/zip".equals(contentType)) {
				filterProperties.put("format", "packed");
				ctx.isPacked = true;
			} else {
				ctx.isJar = true;
			}
		}

		String path = filters.entrySet()
				.stream()
				.map(e->{
					if (e.getKey().match(filterProperties)) {
						return e.getValue();
					} else {
						return null;
					}
				})
				.filter(p->p != null)
				.findFirst()
				.map(p->p.replace("${repoUrl}/", repoUri.toString()))
				.map(p->p.replace("${id}", artifact.getId()))
				.map(p->p.replace("${version}", artifact.getVersion()))
				.orElse(null);

		if (path == null) {
			logger.log(Level.WARNING, "Cannot create a path, because repository has no rule mappings for features");
			throw new IllegalStateException("Cannot create a path, because repository has no rule mappings for features");
		}
		ctx.path = path;
		if (contentType == null) {
			path = path.replace(".jar", "/feature.xml");
		}
		//		int size = properties
		//				.stream()
		//				.filter(p->"download.size".equals(p.getName()))
		//				.map(Property::getValue)
		//				.map(Integer::parseInt)
		//				.findFirst().orElse(Integer.valueOf(-1));

		HttpRequest<File> request = client.build().useCache().get();
		if (contentType != null) {
			Map<String, String> header = new HashMap<>();
			header.put("Content-Type", contentType);
			request = request.headers(header);
		}
		final URI pathURI = URI.create(path);
		Promise<File> file = request.async(pathURI.toURL());
		return file.map(f->{
			ctx.file=f;
			if (pathURI.toString().endsWith("xml")) {
				String propPath = pathURI.toString().replace("feature.xml", "feature.properties");
				File propFile = client.build().useCache().get().go(URI.create(propPath).toURL());
				if (propFile != null) {
					Properties p = new Properties();
					try (FileInputStream fis = new FileInputStream(propFile)) {
						p.load(fis);
						ctx.properties = p;
					}
				}
			}
			return ctx;
		});
	}

	private Promise<FeatureContext> loadBundleFile(Artifact artifact) throws Exception {
		if (artifact == null) {
			logger.log(Level.WARNING, "Cannot load a feature without artifact");
			throw new IllegalStateException("Cannot load a feature without artifact");
		}
		Repository repository = getRepository(artifact);
		if (repository == null) {
			logger.log(Level.WARNING, "Cannot load a feature with an invalid artifact");
			throw new IllegalStateException("Cannot load a feature with an invalid artifact");
		}
		Map<String, String> repositoryProperties = getRepositoryProperties(artifact);

		final URI repoUri;
		String repoUriString = repositoryProperties.get("repositoryURI");
		if (repoUriString != null) {
			repoUri = normalize(URI.create(repoUriString));
		} else {
			repoUri = base;
		}
		List<Rule> rules = repository.getMappings() != null ? repository.getMappings().getRule() : Collections.emptyList();
		// create LDAP filter to output path map
		Map<Filter, String> filters = rules.stream()
				.filter(r->r.getFilter().contains("classifier=osgi.bundle"))
				.filter(r->createFilter(r.getFilter()) != null)
				.collect(Collectors.toMap(r->createFilter(r.getFilter()), Rule::getOutput));
		// prepare map to filter
		Dictionary<String, Object> filterProperties = new Hashtable<String, Object>();
		filterProperties.put("classifier", "osgi.bundle");

		// check content type to extend the filter map
		Map<String, String> properties = artifact.getProperties()
				.getProperty()
				.stream()
				.collect( Collectors.toMap(Property::getName, Property::getValue));

		FeatureContext ctx = new FeatureContext();
		String contentType = properties.get("download.contentType");
		if (contentType != null) {
			if (!"application/zip".equals(contentType)) {
				filterProperties.put("format", "packed");
				ctx.isPacked = true;
			} else {
				ctx.isJar = true;
			}
		} else {
			ctx.isJar = true;
		}

		String path = filters.entrySet()
				.stream()
				.map(e->{
					if (e.getKey().match(filterProperties)) {
						return e.getValue();
					} else {
						return null;
					}
				})
				.filter(p->p != null)
				.findFirst()
				.map(p->p.replace("${repoUrl}/", repoUri.toString()))
				.map(p->p.replace("${id}", artifact.getId()))
				.map(p->p.replace("${version}", artifact.getVersion()))
				.orElse(null);

		if (path == null) {
			logger.log(Level.WARNING, "Cannot create a path, because repository has no rule mappings for bundles");
			throw new IllegalStateException("Cannot create a path, because repository has no rule mappings for bundles");
		}
		ctx.path = path;
		//		if (contentType == null) {
		//			path = path.replace(".jar", "/feature.xml");
		//		}
		//		int size = properties
		//				.stream()
		//				.filter(p->"download.size".equals(p.getName()))
		//				.map(Property::getValue)
		//				.map(Integer::parseInt)
		//				.findFirst().orElse(Integer.valueOf(-1));

		HttpRequest<File> request = client.build().useCache().get();
		if (contentType != null) {
			Map<String, String> header = new HashMap<>();
			header.put("Content-Type", contentType);
			request = request.headers(header);
		}
		Promise<File> file = request.async(URI.create(path).toURL());
		return file.map(f->{
			ctx.file=f;
			return ctx;
		});
	}

	private Filter createFilter(String filterString) {
		try {
			return FrameworkUtil.createFilter(filterString);
		} catch (InvalidSyntaxException e) {
			logger.log(Level.SEVERE, String.format("Error creating Filter for invalid rule '%s'", filterString), e);
			return null;
		}
	}

	private Repository getRepository(Artifact artifact) {
		EObject container = artifact.eContainer();
		if (container != null && container.eContainer() != null && container.eContainer() instanceof Repository) {
			return (Repository) container.eContainer();
		}
		logger.log(Level.WARNING, String.format("artifact is not part of a artifact repository %s", artifact.getId()));
		return null;
	}

	public List<Artifact> getFeatureArtifacts(boolean excludeSourceFeatures) {
		try {
			Promise<List<Artifact>> repoProm = getAllP2Artifacts();
			Stream<Artifact> stream = repoProm.getValue().stream().filter((a)->"org.eclipse.update.feature".equals(a.getClassifier()));
			if (excludeSourceFeatures) {
				stream = stream.filter(a->!a.getId().endsWith("source"));
			}
			return stream.collect(Collectors.toList());
		} catch (Exception e) {
			throw new IllegalStateException(String.format("Error getting artifacts for %s", base ), e);
		}
	}

	public List<Artifact> getBundleArtifacts(boolean excludeSourceBundles) {
		try {
			Promise<List<Artifact>> repoProm = getAllP2Artifacts();
			Stream<Artifact> stream = repoProm.getValue().stream().filter((a)->"osgi.bundle".equals(a.getClassifier()));
			if (excludeSourceBundles) {
				stream = stream.filter(a->!a.getId().endsWith("source"));
			}
			return stream.collect(Collectors.toList());
		} catch (Exception e) {
			throw new IllegalStateException(String.format("Error getting artifacts for %s", base ), e);
		}
	}

	private Promise<List<Artifact>> getP2Artifacts(Set<URI> cycles, URI repositoryURI) {
		if (!cycles.add(repositoryURI)) {
			return promiseFactory.resolved(null);
		}
		try {
			String type = repositoryURI.getPath();
			logger.info(String.format("getArtifacts type=%s", repositoryURI));
			if (type.endsWith("/compositeArtifacts.xml")) {
				return parseCompositeArtifacts(cycles, hideAndSeek(repositoryURI), repositoryURI);
			} else if (type.endsWith("/artifacts.xml.xz")) {
				return parseArtifacts(hideAndSeek(repositoryURI), repositoryURI);
			} else if (type.endsWith("/artifacts.xml")) {
				return parseArtifacts(hideAndSeek(repositoryURI), repositoryURI);
			} else if (type.endsWith("/p2.index")) {
				return parseIndexArtifacts(cycles, repositoryURI);
			}
			repositoryURI = normalize(repositoryURI).resolve("p2.index");
			defaults.add(repositoryURI);
			return parseIndexArtifacts(cycles, repositoryURI);
		} catch (Exception e) {
			logger.log(Level.SEVERE, String.format("Error getting artifacts type=%s", repositoryURI), e);
			return promiseFactory.failed(e);
		}
	}

	private Promise<List<Artifact>> parseCompositeArtifacts(Set<URI> cycles, InputStream in, URI base)
			throws Exception {
		if (in == null) {
			logger.log(Level.INFO, String.format("No such composite repository %s", base));
			return promiseFactory.resolved(Collections.emptyList());
		}
		DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, in, "compositeArtifacts.xml");
		ChildrenType children = root.getRepository().getChildren();
		List<URI> uris = children.getChild()
				.stream()
				.map(RepositoryRef::getLocation)
				.map(Strings::trim)
				.map(base::resolve)
				.collect(toList());

		return getArtifacts(cycles, uris);
	}

	private URI normalize(URI base) throws Exception {
		String path = base.getPath();
		if (path == null || path.endsWith("/") || path.endsWith(".xml") || path.endsWith(".xml.xz") || path.endsWith("p2.index"))
			return base;

		return new URI(base.toString() + "/");
	}

	private InputStream hideAndSeek(URI uri) throws Exception {
		if (uri.getPath()
				.endsWith(".xz")) {
			File f = getFile(uri);
			if (f != null)
				return tzStream(f);
			else
				return null;
		}

		URI xzname = replace(uri, "$", ".xz");
		File f = getFile(xzname);
		if (f != null)
			return tzStream(f);

		f = getFile(replace(uri, ".xml$", ".jar"));
		if (f != null)
			return jarStream(f, Strings.getLastSegment(uri.getPath(), '/'));

		f = getFile(uri);
		if (f != null)
			return IO.stream(f);

		if (!defaults.contains(uri))
			logger.log(Level.SEVERE, String.format("Invalid uri %s", uri));
		return null;
	}

	private File getFile(URI xzname) throws Exception {
		return client.build()
				.useCache()
				.go(xzname);
	}

	private InputStream jarStream(File f, String name) throws IOException {
		final JarFile jaf = new JarFile(f);
		ZipEntry entry = jaf.getEntry(name);
		final InputStream inputStream = jaf.getInputStream(entry);

		return new FilterInputStream(inputStream) {
			@Override
			public void close() throws IOException {
				jaf.close();
			}
		};
	}

	private InputStream tzStream(File f) throws Exception {
		return new XZInputStream(IO.stream(f));
	}

	private URI replace(URI uri, String where, String replacement) {
		String path = uri.getRawPath();
		return uri.resolve(path.replaceAll(where, replacement));
	}

	private Promise<List<Artifact>> parseArtifacts(InputStream in, URI uri) throws Exception {
		if (in == null) {
			logger.log(Level.INFO, String.format("No content for %s", uri));
			return promiseFactory.resolved(null);
		}
		return promiseFactory.submit(() -> {
			try {
				DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, in, "artifacts.xml");
				Repository repository = root.getRepository();
				keepRepositoryUri(repository, uri);
				;				Artifacts artifacts = root.getRepository().getArtifacts(); 
				return artifacts == null ? Collections.emptyList() : artifacts.getArtifact();
			} finally {
				IO.close(in);
			}
		});
	}

	private void keepRepositoryUri(Repository repository, URI uri) {
		if (repository == null || uri == null) {
			logger.log(Level.WARNING, "Error creating repository property for invalid parameters");
			return;
		}
		Property uriProp = EclipseFactory.eINSTANCE.createProperty();
		org.eclipse.emf.common.util.URI eUri = org.eclipse.emf.common.util.URI.createURI(uri.toString());
		uriProp.setName("repositoryURI");
		uriProp.setValue(eUri.trimSegments(1).toString());
		repository.getProperties().getProperty().add(uriProp);
	}

	private void canonicalize(List<URI> artifacts) throws URISyntaxException {
		if (artifacts.size() < 2)
			return;

		for (URI uri : new ArrayList<>(artifacts)) {
			if (uri.getPath()
					.endsWith(".xml"))
				artifacts.remove(new URI(uri.toString() + ".xz"));
		}
	}

	/**
	 * @formatter:off
	 *  version = 1
	 *  metadata.repository.factory.order = compositeContent.xml,\!
	 *  artifact.repository.factory.order = compositeArtifacts.xml,\!
	 * @formatter:on
	 */
	private Promise<List<Artifact>> parseIndexArtifacts(Set<URI> cycles, final URI uri) throws Exception {
		Promise<File> file = client.build()
				.useCache()
				.get()
				.async(uri.toURL());
		return file.flatMap(f -> parseIndexArtifacts(cycles, uri, f));
	}

	private Promise<List<Artifact>> parseIndexArtifacts(Set<URI> cycles, URI uri, File file) throws Exception {
		P2Index index;

		if (file == null) {
			index = getDefaultIndex(uri);
		} else {
			index = parseIndex(file, uri);
		}

		canonicalize(index.artifacts);
		canonicalize(index.content);

		return getArtifacts(cycles, index.artifacts);
	}

	private Promise<List<Artifact>> getArtifacts(Set<URI> cycles, final Collection<URI> uris) {
		Deferred<List<Artifact>> deferred = promiseFactory.deferred();
		promiseFactory.executor()
		.execute(() -> {
			try {
				deferred.resolveWith(uris.stream().map(uri -> getP2Artifacts(cycles, base.resolve(uri)).recover(failed -> {
					if (!defaults.contains(uri)) {
						logger.log(Level.SEVERE, String.format("Failed to get artifacts for %s", uri), failed.getFailure());
					}
					return Collections.emptyList();
				})).collect(toPromise(promiseFactory)).map(ll -> ll.stream().flatMap(List::stream).collect(toList())));
			} catch (Throwable e) {
				deferred.fail(e);
			}
		});
		return deferred.getPromise();
	}

	private P2Index getDefaultIndex(URI base) {
		P2Index index = new P2Index();
		index.artifacts.add(base.resolve("compositeArtifacts.xml"));
		index.artifacts.add(base.resolve("artifacts.xml"));
		index.content.add(base.resolve("compositeContent.xml"));
		index.content.add(base.resolve("content.xml"));
		defaults.addAll(index.artifacts);
		defaults.addAll(index.content);
		return index;
	}

	private P2Index parseIndex(File file, URI base) throws IOException {
		Properties p = new Properties();
		try (InputStream in = IO.stream(file)) {
			p.load(in);
		}

		String version = p.getProperty("version");
		if (version == null || Integer.parseInt(version) != 1)
			throw new UnsupportedOperationException(
					"The repository " + base + " specifies an index file with an incompatible version " + version);

		P2Index index = new P2Index();

		addPaths(p.getProperty("metadata.repository.factory.order"), index.content, base);
		addPaths(p.getProperty("artifact.repository.factory.order"), index.artifacts, base);

		index.modified = file.lastModified();
		return index;
	}

	private void addPaths(String p, List<URI> index, URI base) {
		Parameters content = new Parameters(p);
		for (String path : content.keySet()) {
			if ("!".equals(path)) {
				break;
			}
			URI sub = base.resolve(path);
			index.add(sub);
		}
	}

	public static List<Artifact> getArtifacts(URI uri) {
		if (uri == null) {
			return Collections.emptyList();
		}
		try {
			FeatureLoader loader = new FeatureLoader(uri);
			Promise<List<Artifact>> repoProm = loader.getAllP2Artifacts();
			return repoProm.getValue();
		} catch (Exception e) {
			throw new IllegalStateException(String.format("Error getting artifacts for %s", uri ), e);
		}
	}

}
