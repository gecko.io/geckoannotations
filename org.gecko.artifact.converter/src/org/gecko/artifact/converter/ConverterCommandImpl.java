/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.converter;

import java.util.List;

import org.gecko.artifacts.ArtifactsPackage;
import org.gecko.artifacts.Dependency;
import org.gecko.artifacts.DependencyType;
import org.gecko.eclipse.EclipsePackage;
import org.gecko.eclipse.Feature;
import org.gecko.emf.repository.EMFRepository;
import org.gecko.emf.repository.query.IQuery;
import org.gecko.emf.repository.query.IQueryBuilder;
import org.gecko.emf.repository.query.QueryRepository;
import org.gecko.emf.repository.query.SortType;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * 
 * @author mark
 * @since 29.02.2020
 */
@Component(property = {
		"osgi.command.scope=artifact",
		"osgi.command.function=bundleExports",
		"osgi.command.function=bundleImports",
		"osgi.command.function=createAnnotation",
		"osgi.command.function=ecoreBundles",
		"osgi.command.function=featureHasBundle"
})
public class ConverterCommandImpl implements ConverterCommand{
	
	@Reference
	private EMFRepository repository;

	private String whoDoes(DependencyType type, String parameter) {
		QueryRepository qr = (QueryRepository) repository;
		IQueryBuilder queryBuilder = qr.createQueryBuilder();
		IQuery typeQuery = queryBuilder.column(ArtifactsPackage.Literals.DEPENDENCY__TYPE).simpleValue(type.getName()).build();
		IQuery valueQuery = queryBuilder.column(ArtifactsPackage.Literals.DEPENDENCY__NAME).simpleValue(parameter).build();
		IQuery query = queryBuilder
				.and(typeQuery, valueQuery)
				.sort(ArtifactsPackage.Literals.DEPENDENCY__VERSION, SortType.ASCENDING)
				.build();
		List<Dependency> deps = qr.getEObjectsByQuery(ArtifactsPackage.Literals.DEPENDENCY, query);
		if (deps == null || deps.isEmpty()) {
			return String.format("No bundles found having %s for package %s", type, parameter);
		}
		StringBuilder sb = new StringBuilder();
		deps.stream().map((d)->"- " + d.getSource().getBsn() + System.lineSeparator()).forEach(sb::append);
		return sb.toString();
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.artifact.converter.ConverterCommand#bundleExports(java.lang.String)
	 */
	@Override
	public String bundleExports(String exportPackage) {
		return whoDoes(DependencyType.EXPORT, exportPackage);
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.artifact.converter.ConverterCommand#bundleImports(java.lang.String)
	 */
	@Override
	public String bundleImports(String importPackage) {
		return whoDoes(DependencyType.IMPORT, importPackage);
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.artifact.converter.ConverterCommand#featureHasBundle(java.lang.String)
	 */
	@Override
	public String featureHasBundle(String bundle) {
		repository.getResourceSet().getPackageRegistry().put(EclipsePackage.eNS_URI, EclipsePackage.eINSTANCE);
		QueryRepository qr = (QueryRepository) repository;
		IQueryBuilder queryBuilder = qr.createQueryBuilder();
		String path = EclipsePackage.Literals.FEATURE__PLUGIN.getName() + "." + EclipsePackage.Literals.PLUGIN__ID.getName();
		IQuery valueQuery = queryBuilder
				.column(path)
				.simpleValue(bundle)
				.build();
		List<Feature> features = qr.getEObjectsByQuery(EclipsePackage.Literals.FEATURE, valueQuery);
		if (features == null || features.isEmpty()) {
			return String.format("No features found having %s as bundle", bundle);
		}
		StringBuilder sb = new StringBuilder();
		features.stream().map((d)->"- " + d.getId() + System.lineSeparator()).forEach(sb::append);
		return sb.toString();
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.artifact.converter.ConverterCommand#ecoreBundles()
	 */
	@Override
	public String ecoreBundles() {
		QueryRepository qr = (QueryRepository) repository;
		IQueryBuilder queryBuilder = qr.createQueryBuilder();
		IQuery query = queryBuilder
				.column(ArtifactsPackage.Literals.DEPENDENCY__TYPE)
				.simpleValue(DependencyType.ECORE.getName())
				.sort(ArtifactsPackage.Literals.DEPENDENCY__VERSION, SortType.ASCENDING)
				.build();
		List<Dependency> deps = qr.getEObjectsByQuery(ArtifactsPackage.Literals.DEPENDENCY, query);
		if (deps == null || deps.isEmpty()) {
			return "No bundles found having an ecore model";
		}
		StringBuilder sb = new StringBuilder();
		deps.stream().map((d)->"- " + d.getSource().getBsn() + "(" + d.getName() + ", " + d.getLocation() + ")" + System.lineSeparator()).forEach(sb::append);
		return sb.toString();
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.artifact.converter.ConverterCommand#createAnnotation(java.lang.String)
	 * createAnnotation org.eclipse.e4.rcp
	 */
	@Override
	public String createAnnotation(String feature) {
		repository.getResourceSet().getPackageRegistry().put(EclipsePackage.eNS_URI, EclipsePackage.eINSTANCE);
		Feature f = repository.getEObject(EclipsePackage.Literals.FEATURE, feature);
		if (f == null) {
			return String.format("No feature found having %s as id", feature);
		}
		AnnotationConverter converter = new AnnotationConverter();
		String featureAnnotation = converter.convertFeature(f).toString();
		return featureAnnotation;
	}

}
