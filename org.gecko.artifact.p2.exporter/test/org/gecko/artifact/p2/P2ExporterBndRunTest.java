package org.gecko.artifact.p2;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.Before;

import aQute.bnd.build.Run;
import aQute.bnd.build.Workspace;
import aQute.bnd.osgi.JarResource;
import aQute.bnd.osgi.Resource;
import aQute.lib.io.IO;

public class P2ExporterBndRunTest {
	File						base					= new File("").getAbsoluteFile();
	private File generatedDir;
	private File result;
	private File repoFolder;

	@Before
	public void before() {
		generatedDir = new File(base, "generated/tmp/");
	}

	@After
	public void after() {
		if(result != null) {
			result.delete();
		}
		if (repoFolder != null) {
			try {
				Files.walkFileTree(repoFolder.toPath(), new SimpleFileVisitor<Path>() {

					@Override
					public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
						Files.deleteIfExists(file);
						return FileVisitResult.CONTINUE;
					}

					@Override
					public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
						if (exc == null) {
							Files.delete(dir);
							return FileVisitResult.CONTINUE;
						} else {
							throw exc;
						}
					}
				});
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Testing the embedded launcher is quite tricky. This test uses a prefabricated
	 * packaged jar. Notice that you need to reexport that jar for every change in
	 * the launcher since it embeds the launcher. This jar is run twice to see if
	 * the second run will not reinstall the bundles.
	 */

//	@Test
	public void testExport() throws Exception {


		Workspace ws = Workspace.getWorkspace(base.getParentFile());
		//		
		Run run = Run.createRun(ws, IO.getFile(base, "test.bndrun"));

		Entry<String, Resource> export = run.export("p2.export", Collections.emptyMap());
		assertNotNull(export);
		String exportFileName = export.getKey();
		Object exportResource = export.getValue(); 
		assertNotNull(exportResource);

		assertTrue(exportResource instanceof JarResource);

		JarResource exportResult = (JarResource) exportResource;
		repoFolder = new File(generatedDir, exportFileName.replace(".zip", ""));
		assertTrue(repoFolder.exists());
		assertTrue(repoFolder.isDirectory());

		result = new File(generatedDir, exportFileName.replace(".jar", ".zip"));
		exportResult.getJar().write(result);

		assertTrue(result.exists());

	}
	
}
