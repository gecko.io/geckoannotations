/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.p2.exporter;

import java.io.File;
import java.io.IOException;
import java.util.AbstractMap.SimpleEntry;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.gecko.artifact.p2.P2RepositoryCreator;
import org.gecko.artifact.p2.P2RepositoryCreator.CategoryData;
import org.osgi.framework.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import aQute.bnd.annotation.plugin.BndPlugin;
import aQute.bnd.build.Container;
import aQute.bnd.build.Project;
import aQute.bnd.header.Parameters;
import aQute.bnd.osgi.Constants;
import aQute.bnd.osgi.FileResource;
import aQute.bnd.osgi.Jar;
import aQute.bnd.osgi.JarResource;
import aQute.bnd.osgi.Resource;
import aQute.bnd.service.Strategy;
import aQute.bnd.service.export.Exporter;

/**
 * Exporter for bndtools to export a P2 repository
 * @author Mark Hoffmann
 *
 */
@BndPlugin(name = "P2")
public class P2Exporter implements Exporter, P2ExporterConstants {
	
	private final static Logger logger = LoggerFactory.getLogger(P2Exporter.class);
	private static String[] TYPES = new String[] {EXPORTER_TYPE};
	
	/* 
	 * (non-Javadoc)
	 * @see aQute.bnd.service.export.Exporter#getTypes()
	 */
	@Override
	public String[] getTypes() {
		return TYPES;
	}

	/* 
	 * (non-Javadoc)
	 * @see aQute.bnd.service.export.Exporter#export(java.lang.String, aQute.bnd.build.Project, java.util.Map)
	 */
	@Override
	public Entry<String, Resource> export(String type, Project project, Map<String, String> options) throws Exception {
		project.prepare();
		Optional<CategoryData> category = createCategory(options);
		String name = options.get("name");
		if (name == null) {
			name = options.getOrDefault(REPOSITORY_NAME, project.getName().replace(".bndrun", ""));
		}
		boolean compress = Boolean.parseBoolean(options.getOrDefault(COMPRESS_METADATA, "false"));
		boolean includeAll = Boolean.parseBoolean(options.getOrDefault(INCLUDE_ALL, "false"));
		boolean noManifest = Boolean.parseBoolean(options.getOrDefault(NO_MANIFEST, "false"));
		
		logger.info("Exporting '{}' as P2 repository with options compress={}, includeAll={}, noManifest={}", name, compress, includeAll, noManifest);
		
		File target = project.getTargetDir();
		// create repository folder
		File repositoryDir = new File(target, "/tmp/" + name);
		if (!repositoryDir.exists()) {
			repositoryDir.mkdirs();
		}
		
		// create repository Jar
		Jar jar = new Jar(name);
		if (!noManifest) {
			Manifest mf = updateManifest(jar.getManifest(), project, options);
			jar.setManifest(mf);
		} else {
			jar.setDoNotTouchManifest();
		}
		
		logger.info("Configuring P2 creator");
		
		// create P2 repository
		P2RepositoryCreator creator = new P2RepositoryCreator(repositoryDir.toURI())
				.javaVersion(Version.parseVersion("11.0.0"))
				.name(name)
				.compress(compress);
		category.ifPresent(creator::withCategory);
		
		Collection<Container> runbundles = includeAll ? 
				project.getRunbundles() : getRequiredBundles(project);
				
		if (logger.isDebugEnabled()) {
			logger.debug("Collecting bundles ...");
			runbundles.forEach(c->logger.debug(c.getBundleSymbolicName()));
		}
		
		for (Container container : runbundles) {
			File source = container.getFile();
			Jar sourceJar = new Jar(source);
			creator.addJar(sourceJar);
		}
		logger.info("Create P2 repository meta data");
		creator.create();

		logger.info("Copy artifacts into P2 repository");
		if(repositoryDir.listFiles() != null) {
			for (File child : repositoryDir.listFiles()) {
				addToJar(jar, child, "");
			}
		}
		logger.info("Finished P2 repository export");
		return new SimpleEntry<>(jar.getName() + ".jar", new JarResource(jar, true));
	}
	
	/**
	 * Creates the P2 category data
	 * @param options the export options
	 * @return the category data or an empty {@link Optional}
	 */
	private Optional<CategoryData> createCategory(Map<String, String> options) {
		String categoryVersion = options.get(CATEGORY_VERSION);
		String categoryId = options.get(CATEGORY_ID);
		String categoryName = options.get(CATEGORY_NAME);
		if (categoryId != null) {
			CategoryData category = new CategoryData();
			category.id = categoryId;
			category.name = categoryName;
			category.version = categoryVersion;
			return Optional.of(category);
		}
		return Optional.empty();
	}
	
	/**
	 * Updates Manifest information, to be able to release the repository.
	 * All Maven Central relevant data will be copied to be able to publish to Maven Central
	 * @param manifest the existing manifest, if <code>null</code> a new {@link Manifest} will be created
	 * @param project the projecct
	 * @param options the options
	 * @return the {@link Manifest} instance
	 */
	private Manifest updateManifest(Manifest manifest, Project project, Map<String, String> options) {
		Manifest mf = manifest == null ? new Manifest() : manifest;
		String bsn = options.get("bsn");
		String name = project.getBundleName();
		String description = project.getBundleDescription();
		String version = options.getOrDefault("version", project.getBundleVersion());
		String vendor = project.getBundleVendor();
		String projectUrl = project.getBundleDocURL();
		String contact = project.getBundleContactAddress();
		String license = project.get(Constants.BUNDLE_LICENSE);
		String developers = project.get(Constants.BUNDLE_DEVELOPERS);
		String scm = project.get(Constants.BUNDLE_SCM);
		Map<Object, Object> entries = mf.getMainAttributes();
		bsn = project.getBundleSymbolicName() != null ? project.getBundleSymbolicName().getKey() : bsn;
		entries.put(new Attributes.Name(Constants.BUNDLE_SYMBOLICNAME), bsn);
		if (name != null) {
			entries.put(new Attributes.Name(Constants.BUNDLE_NAME), name);
		}
		if (description != null) {
			entries.put(new Attributes.Name(Constants.BUNDLE_DESCRIPTION), description);
		}
		if (version != null) {
			entries.put(new Attributes.Name(Constants.BUNDLE_VERSION), version);
		}
		if (vendor != null) {
			entries.put(new Attributes.Name(Constants.BUNDLE_VENDOR), vendor);
		}
		if (projectUrl != null) {
			entries.put(new Attributes.Name(Constants.BUNDLE_DOCURL), projectUrl);
		}
		if (contact != null) {
			entries.put(new Attributes.Name(Constants.BUNDLE_CONTACTADDRESS), contact);
		}
		if (license != null) {
			entries.put(new Attributes.Name(Constants.BUNDLE_LICENSE), license);
		}
		if (developers != null) {
			entries.put(new Attributes.Name(Constants.BUNDLE_DEVELOPERS), developers);
		}
		if (scm != null) {
			entries.put(new Attributes.Name(Constants.BUNDLE_SCM), scm);
		}
		return mf;
	}
	
	/**
	 * Adds the given file to the jar starting with directory string
	 * @param jar the jar to add the file
	 * @param child the file/folder to be added
	 * @param root the root location
	 * @throws IOException 
	 */
	private void addToJar(Jar jar, File file, String root) throws IOException {
		if(file.isFile()) {
			Resource remove = jar.remove(root + file.getName());
			jar.putResource(root + file.getName(), new FileResource(file), true);
			if(remove != null) {
				logger.info("Overwriting resource: {} with project specific Version", root + file.getName());
			} else {
				logger.info("Adding Resource {}", root + file.getName());
			}
		} else {
			for (File child : file.listFiles()) {
				addToJar(jar, child, root + file.getName() + "/");
			}
		}
	}
	
	private Collection<Container> getRequiredBundles(final Project project) throws Exception {
		List<Container> required = new LinkedList<Container>();
		Parameters mergedParameters = project.getMergedParameters(Constants.RUNREQUIRES);
		mergedParameters.forEach((k,a)->{
			if ("bnd.identity".equals(k.toString().replaceAll("~", ""))) {
				String bsn = a.get("id");
				String version = a.get("version");
				try {
					Container bundle = project.getBundle(bsn, version, Strategy.HIGHEST, a);
					if (bundle != null) {
						required.add(bundle);
					}
				} catch (Exception e) {
					logger.error("Error resolving bundle {}", bsn);
				}
			}
		});
		return required;
	}
	
}
