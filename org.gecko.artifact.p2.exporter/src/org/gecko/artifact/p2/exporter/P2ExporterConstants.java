package org.gecko.artifact.p2.exporter;

public interface P2ExporterConstants {
	
	// the exporter type
	public static final String EXPORTER_TYPE = "p2.exporter";
	// bundle symbolic name of the exported repository
	public static final String BSN = "bsn";
	// bundle version of the exported repository
	public static final String VERSION = "version";
	// include the resolved require bundles from the bndrun instead of the build dependencies
	public static final String INCLUDE_ALL = "includeAll";
	// don't create a Manifest file 
	public static final String NO_MANIFEST = "noManifest";
	// compress the artifacts.xml and content.xml
	public static final String COMPRESS_METADATA = "compress";
	// name of the P2 category
	public static final String CATEGORY_NAME = "category.name";
	// id of the P2 category
	public static final String CATEGORY_ID = "category.id";
	// version of the P2 category
	public static final String CATEGORY_VERSION = "category.version";
	// name of the resulting artifact
	public static final String REPOSITORY_NAME = "repository.name";

}
