# Extracting an Ecore out of an XML

If you need to create an Ecore out of an XML that has no schema, you have to do some manual work.
The best approach is indeed to create a XML schema for that XML first. 

Out of the XSD you can create a genmodel and ecore. This importer is so sophisticated, that it makes no real sense to take the "*EMF - Eclipse Modelling Framework*" Book.

## Creating an XSD for the XML

Designing attributes and elements is quiet easy. Tutorials can be found everywhere. I just want to hint to some specialities you may find in the wild.

Here are some examples and how you will get results in an generated Ecore. 

### Simple content

```
<description url="http://www.example.com">
	This is my text
</description>
```

This is simple content of type string, that also has some attributes.

```
<xsd:complexType name="Description">
	<xsd:simpleContent>
		<xsd:extension base="xsd:string">
  			<xsd:attribute name="url" type="xsd:string"/>
		</xsd:extension>
	</xsd:simpleContent>
</xsd:complexType>
```

In ecore you will end-up in a Description EClass, with two EAttributes:

- **url** of type EString that represents our attribute
- **value** of type EString the represents the content between start- and end-tag

### Own data types

If you want to validate the content in the attribute version, you can use regular expressions in XSD.
The generated ecore, also supports that. In case of a validation error, EMF will give you a according diagnostic for that feature.

```
<description version="1.0.0.test">
	This is my text
</description>
```

The XSD part looks like that for semantic versioning

```
<xsd:simpleType name="Version">
    <xsd:restriction base="xsd:string">
      <xsd:pattern value="[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(\..*)?"/>
    </xsd:restriction>
</xsd:simpleType>

<xsd:complexType name="Description">
	<xsd:simpleContent>
		<xsd:extension base="xsd:string">
  			<xsd:attribute name="version" type="tns:Version"/>
		</xsd:extension>
	</xsd:simpleContent>
</xsd:complexType>
```

The ecore model will contain a EDataType *Version* that contains the ExtendedMetaData Annotations with the regular expression.

### Enumerations

Enumerations are easy to create in XSD

```
<xsd:simpleType name="ValueType">
	<xsd:restriction base="xsd:NCName">
		<xsd:enumeration value="valueA" />
		<xsd:enumeration value="valueB" />
		<xsd:enumeration value="valueB" />
		<xsd:enumeration value="valueB" />
	</xsd:restriction>
</xsd:simpleType>
```
In that case be prepared, that the generated Ecore will contain the literals with the XSD's value as name. Means, when de-serializing the XML, EMF will take the Ecore's EENums literal and not the name. So, you can change the Ecore EENum name, but the literal should be the same like the XSD value.

Another point is that you should think about providing an default EENum Literal after the creation process, because EMF does not directly support *null* values for EEnum attributes.

There is a EMF [recipe](https://wiki.eclipse.org/EMF/Recipes#Recipe:_Generating_enumeration-based_attributes_that_support_null) to do that, but creating a default literal, that is placed at the first position is a pragmatic solution.

## Loading XML files without Schema-Definition

Usually the XML should look like this:

```
<?xml version="1.0" encoding="UTF-8"?>
<tns:feature xmlns:tns="http://gecko.io/FeatureSchema.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://gecko.io/FeatureSchema.xsd feature.xsd "
	id="org.gecko.test.feature">
  	<tns:description url="http://google.de">
  		test description
  	</tns:description>
</tns:feature>
```
Because the world is not as we we want it, we sometime are faced with XML files like this instead:

```
<?xml version="1.0" encoding="UTF-8"?>
<feature
	id="org.gecko.test.feature">
  	<description url="http://google.de">
  		test description
  	</description>
</feature>
```

For EMF it is hard to determine, which package to use. The easiest way to resolve that, is just to create a *BasicExtendedMetaData* instance and load the resource using this. You can customize that to your needs, if e.g. multiple packages are involved. Please set the *BasicExtendedMetaData#isFeatureNamespaceMatchingLax* to **true**.


```
ResourceSet resourceSet = new ResourceSetImpl();
URI uri = URI.createFileURI(...);
...

BasicExtendedMetaData bemd = new c() {
	@Override
	protected boolean isFeatureNamespaceMatchingLax() {
		return true;
	}
	
	@Override
	public EPackage getPackage(String namespace) {
		return FeaturePackage.eINSTANCE;
	}
};

resourceSet.getLoadOptions().put(XMLResource.OPTION_EXTENDED_META_DATA, bemd);
Resource resource = resourceSet.getResource(uri, true);
```
