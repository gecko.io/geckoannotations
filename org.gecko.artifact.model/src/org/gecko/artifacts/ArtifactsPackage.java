/**
 */
package org.gecko.artifacts;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.gecko.artifacts.ArtifactsFactory
 * @model kind="package"
 * @generated
 */
public interface ArtifactsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "artifacts";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://gecko.io/artifacts";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "artifacts";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ArtifactsPackage eINSTANCE = org.gecko.artifacts.impl.ArtifactsPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.gecko.artifacts.impl.OSGiBundleImpl <em>OS Gi Bundle</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.artifacts.impl.OSGiBundleImpl
	 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getOSGiBundle()
	 * @generated
	 */
	int OS_GI_BUNDLE = 0;

	/**
	 * The feature id for the '<em><b>Bsn</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE__BSN = 0;

	/**
	 * The feature id for the '<em><b>Source Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE__SOURCE_LOCATION = 1;

	/**
	 * The feature id for the '<em><b>Cache Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE__CACHE_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE__NAME = 3;

	/**
	 * The feature id for the '<em><b>Vendor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE__VENDOR = 4;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE__VERSION = 5;

	/**
	 * The feature id for the '<em><b>Policy Lazy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE__POLICY_LAZY = 6;

	/**
	 * The feature id for the '<em><b>Execution Environment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE__EXECUTION_ENVIRONMENT = 7;

	/**
	 * The feature id for the '<em><b>Classpath</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE__CLASSPATH = 8;

	/**
	 * The feature id for the '<em><b>Localization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE__LOCALIZATION = 9;

	/**
	 * The feature id for the '<em><b>Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE__DEPENDENCY = 10;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE__PROPERTIES = 11;

	/**
	 * The feature id for the '<em><b>Bundle Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE__BUNDLE_PROPERTY = 12;

	/**
	 * The feature id for the '<em><b>License</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE__LICENSE = 13;

	/**
	 * The feature id for the '<em><b>Copyright</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE__COPYRIGHT = 14;

	/**
	 * The feature id for the '<em><b>Fragment Host</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE__FRAGMENT_HOST = 15;

	/**
	 * The feature id for the '<em><b>Fragment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE__FRAGMENT = 16;

	/**
	 * The number of structural features of the '<em>OS Gi Bundle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE_FEATURE_COUNT = 17;

	/**
	 * The number of operations of the '<em>OS Gi Bundle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_GI_BUNDLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.artifacts.impl.DependencyImpl <em>Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.artifacts.impl.DependencyImpl
	 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getDependency()
	 * @generated
	 */
	int DEPENDENCY = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__NAME = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__TYPE = 1;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__SOURCE = 2;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__TARGET = 3;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__VERSION = 4;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__VISIBILITY = 5;

	/**
	 * The feature id for the '<em><b>Optional</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__OPTIONAL = 6;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__LOCATION = 7;

	/**
	 * The feature id for the '<em><b>Reexport</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__REEXPORT = 8;

	/**
	 * The feature id for the '<em><b>Object</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__OBJECT = 9;

	/**
	 * The feature id for the '<em><b>Filter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__FILTER = 10;

	/**
	 * The feature id for the '<em><b>Multiple</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__MULTIPLE = 11;

	/**
	 * The feature id for the '<em><b>Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__ACTIVE = 12;

	/**
	 * The feature id for the '<em><b>Dependency Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__DEPENDENCY_PROPERTY = 13;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__PROPERTIES = 14;

	/**
	 * The feature id for the '<em><b>Object Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__OBJECT_CLASS = 15;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY__ID = 16;

	/**
	 * The number of structural features of the '<em>Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY_FEATURE_COUNT = 17;

	/**
	 * The number of operations of the '<em>Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDENCY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.artifacts.impl.PropertyImpl <em>Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.artifacts.impl.PropertyImpl
	 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getProperty()
	 * @generated
	 */
	int PROPERTY = 2;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.artifacts.impl.ArtifactImpl <em>Artifact</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.artifacts.impl.ArtifactImpl
	 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getArtifact()
	 * @generated
	 */
	int ARTIFACT = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__ID = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__NAME = 1;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__LABEL = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__VERSION = 4;

	/**
	 * The feature id for the '<em><b>Source Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__SOURCE_LOCATION = 5;

	/**
	 * The feature id for the '<em><b>Cache Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__CACHE_LOCATION = 6;

	/**
	 * The feature id for the '<em><b>Repository</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__REPOSITORY = 7;

	/**
	 * The feature id for the '<em><b>License</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__LICENSE = 8;

	/**
	 * The feature id for the '<em><b>Copyright</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__COPYRIGHT = 9;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__PROPERTIES = 10;

	/**
	 * The feature id for the '<em><b>Artifact Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__ARTIFACT_PROPERTIES = 11;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__SIZE = 12;

	/**
	 * The feature id for the '<em><b>Hash</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__HASH = 13;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__TIMESTAMP = 14;

	/**
	 * The feature id for the '<em><b>References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__REFERENCES = 15;

	/**
	 * The number of structural features of the '<em>Artifact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_FEATURE_COUNT = 16;

	/**
	 * The operation id for the '<em>Put Property</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT___PUT_PROPERTY__STRING_STRING = 0;

	/**
	 * The number of operations of the '<em>Artifact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.gecko.artifacts.impl.ArtifactRepositoryImpl <em>Artifact Repository</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.artifacts.impl.ArtifactRepositoryImpl
	 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getArtifactRepository()
	 * @generated
	 */
	int ARTIFACT_REPOSITORY = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY__ID = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY__NAME = 1;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY__VERSION = 2;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY__TYPE = 3;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY__LOCATION = 4;

	/**
	 * The feature id for the '<em><b>Artifacts</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY__ARTIFACTS = 5;

	/**
	 * The feature id for the '<em><b>Repositories</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY__REPOSITORIES = 6;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY__PARENT = 7;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY__PROPERTIES = 8;

	/**
	 * The feature id for the '<em><b>Repository Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY__REPOSITORY_PROPERTIES = 9;

	/**
	 * The feature id for the '<em><b>Location Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY__LOCATION_URI = 10;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY__TIMESTAMP = 11;

	/**
	 * The feature id for the '<em><b>All Artifacts</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY__ALL_ARTIFACTS = 12;

	/**
	 * The feature id for the '<em><b>Loaded</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY__LOADED = 13;

	/**
	 * The number of structural features of the '<em>Artifact Repository</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY_FEATURE_COUNT = 14;

	/**
	 * The operation id for the '<em>Get Artifact By Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY___GET_ARTIFACT_BY_ID__STRING = 0;

	/**
	 * The operation id for the '<em>Get Artifacts By Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY___GET_ARTIFACTS_BY_ID__STRING = 1;

	/**
	 * The operation id for the '<em>Get Artifacts By Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY___GET_ARTIFACTS_BY_NAME__STRING = 2;

	/**
	 * The operation id for the '<em>Get All Artifacts By Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY___GET_ALL_ARTIFACTS_BY_ID__STRING = 3;

	/**
	 * The number of operations of the '<em>Artifact Repository</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REPOSITORY_OPERATION_COUNT = 4;

	/**
	 * The meta object id for the '{@link org.gecko.artifacts.impl.ArtifactReferenceImpl <em>Artifact Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.artifacts.impl.ArtifactReferenceImpl
	 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getArtifactReference()
	 * @generated
	 */
	int ARTIFACT_REFERENCE = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE__ID = 0;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE__VERSION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE__NAME = 2;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE__LABEL = 3;

	/**
	 * The feature id for the '<em><b>Optional</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE__OPTIONAL = 4;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE__LOCATION = 5;

	/**
	 * The feature id for the '<em><b>Filter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE__FILTER = 6;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE__TYPE = 7;

	/**
	 * The feature id for the '<em><b>Ref Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE__REF_OBJECT = 8;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE__SOURCE = 9;

	/**
	 * The feature id for the '<em><b>Source Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE__SOURCE_NAME = 10;

	/**
	 * The feature id for the '<em><b>Source Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE__SOURCE_VERSION = 11;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE__TARGET = 12;

	/**
	 * The feature id for the '<em><b>Target Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE__TARGET_NAME = 13;

	/**
	 * The feature id for the '<em><b>Target Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE__TARGET_VERSION = 14;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE__PROPERTIES = 15;

	/**
	 * The feature id for the '<em><b>Reference Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE__REFERENCE_PROPERTIES = 16;

	/**
	 * The number of structural features of the '<em>Artifact Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE_FEATURE_COUNT = 17;

	/**
	 * The operation id for the '<em>Put Property</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE___PUT_PROPERTY__STRING_STRING = 0;

	/**
	 * The number of operations of the '<em>Artifact Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_REFERENCE_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.gecko.artifacts.impl.P2RepositoryImpl <em>P2 Repository</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.artifacts.impl.P2RepositoryImpl
	 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getP2Repository()
	 * @generated
	 */
	int P2_REPOSITORY = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__ID = ARTIFACT_REPOSITORY__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__NAME = ARTIFACT_REPOSITORY__NAME;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__VERSION = ARTIFACT_REPOSITORY__VERSION;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__TYPE = ARTIFACT_REPOSITORY__TYPE;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__LOCATION = ARTIFACT_REPOSITORY__LOCATION;

	/**
	 * The feature id for the '<em><b>Artifacts</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__ARTIFACTS = ARTIFACT_REPOSITORY__ARTIFACTS;

	/**
	 * The feature id for the '<em><b>Repositories</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__REPOSITORIES = ARTIFACT_REPOSITORY__REPOSITORIES;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__PARENT = ARTIFACT_REPOSITORY__PARENT;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__PROPERTIES = ARTIFACT_REPOSITORY__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Repository Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__REPOSITORY_PROPERTIES = ARTIFACT_REPOSITORY__REPOSITORY_PROPERTIES;

	/**
	 * The feature id for the '<em><b>Location Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__LOCATION_URI = ARTIFACT_REPOSITORY__LOCATION_URI;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__TIMESTAMP = ARTIFACT_REPOSITORY__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>All Artifacts</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__ALL_ARTIFACTS = ARTIFACT_REPOSITORY__ALL_ARTIFACTS;

	/**
	 * The feature id for the '<em><b>Loaded</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__LOADED = ARTIFACT_REPOSITORY__LOADED;

	/**
	 * The feature id for the '<em><b>P2 Repository Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__P2_REPOSITORY_TYPE = ARTIFACT_REPOSITORY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Metadata Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__METADATA_LOCATION = ARTIFACT_REPOSITORY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Artifact Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__ARTIFACT_LOCATION = ARTIFACT_REPOSITORY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Index Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__INDEX_LOCATION = ARTIFACT_REPOSITORY_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Composite Repository</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__COMPOSITE_REPOSITORY = ARTIFACT_REPOSITORY_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__RULES = ARTIFACT_REPOSITORY_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Eclipse Features</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY__ECLIPSE_FEATURES = ARTIFACT_REPOSITORY_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>P2 Repository</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY_FEATURE_COUNT = ARTIFACT_REPOSITORY_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>Get Artifact By Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY___GET_ARTIFACT_BY_ID__STRING = ARTIFACT_REPOSITORY___GET_ARTIFACT_BY_ID__STRING;

	/**
	 * The operation id for the '<em>Get Artifacts By Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY___GET_ARTIFACTS_BY_ID__STRING = ARTIFACT_REPOSITORY___GET_ARTIFACTS_BY_ID__STRING;

	/**
	 * The operation id for the '<em>Get Artifacts By Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY___GET_ARTIFACTS_BY_NAME__STRING = ARTIFACT_REPOSITORY___GET_ARTIFACTS_BY_NAME__STRING;

	/**
	 * The operation id for the '<em>Get All Artifacts By Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY___GET_ALL_ARTIFACTS_BY_ID__STRING = ARTIFACT_REPOSITORY___GET_ALL_ARTIFACTS_BY_ID__STRING;

	/**
	 * The operation id for the '<em>Normalize Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY___NORMALIZE_URI = ARTIFACT_REPOSITORY_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>P2 Repository</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int P2_REPOSITORY_OPERATION_COUNT = ARTIFACT_REPOSITORY_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.gecko.artifacts.impl.BundleArtifactImpl <em>Bundle Artifact</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.artifacts.impl.BundleArtifactImpl
	 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getBundleArtifact()
	 * @generated
	 */
	int BUNDLE_ARTIFACT = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT__ID = ARTIFACT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT__NAME = ARTIFACT__NAME;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT__LABEL = ARTIFACT__LABEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT__DESCRIPTION = ARTIFACT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT__VERSION = ARTIFACT__VERSION;

	/**
	 * The feature id for the '<em><b>Source Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT__SOURCE_LOCATION = ARTIFACT__SOURCE_LOCATION;

	/**
	 * The feature id for the '<em><b>Cache Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT__CACHE_LOCATION = ARTIFACT__CACHE_LOCATION;

	/**
	 * The feature id for the '<em><b>Repository</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT__REPOSITORY = ARTIFACT__REPOSITORY;

	/**
	 * The feature id for the '<em><b>License</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT__LICENSE = ARTIFACT__LICENSE;

	/**
	 * The feature id for the '<em><b>Copyright</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT__COPYRIGHT = ARTIFACT__COPYRIGHT;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT__PROPERTIES = ARTIFACT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Artifact Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT__ARTIFACT_PROPERTIES = ARTIFACT__ARTIFACT_PROPERTIES;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT__SIZE = ARTIFACT__SIZE;

	/**
	 * The feature id for the '<em><b>Hash</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT__HASH = ARTIFACT__HASH;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT__TIMESTAMP = ARTIFACT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT__REFERENCES = ARTIFACT__REFERENCES;

	/**
	 * The number of structural features of the '<em>Bundle Artifact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT_FEATURE_COUNT = ARTIFACT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Put Property</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT___PUT_PROPERTY__STRING_STRING = ARTIFACT___PUT_PROPERTY__STRING_STRING;

	/**
	 * The number of operations of the '<em>Bundle Artifact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUNDLE_ARTIFACT_OPERATION_COUNT = ARTIFACT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.gecko.artifacts.impl.BinaryArtifactImpl <em>Binary Artifact</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.artifacts.impl.BinaryArtifactImpl
	 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getBinaryArtifact()
	 * @generated
	 */
	int BINARY_ARTIFACT = 8;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT__ID = ARTIFACT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT__NAME = ARTIFACT__NAME;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT__LABEL = ARTIFACT__LABEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT__DESCRIPTION = ARTIFACT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT__VERSION = ARTIFACT__VERSION;

	/**
	 * The feature id for the '<em><b>Source Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT__SOURCE_LOCATION = ARTIFACT__SOURCE_LOCATION;

	/**
	 * The feature id for the '<em><b>Cache Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT__CACHE_LOCATION = ARTIFACT__CACHE_LOCATION;

	/**
	 * The feature id for the '<em><b>Repository</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT__REPOSITORY = ARTIFACT__REPOSITORY;

	/**
	 * The feature id for the '<em><b>License</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT__LICENSE = ARTIFACT__LICENSE;

	/**
	 * The feature id for the '<em><b>Copyright</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT__COPYRIGHT = ARTIFACT__COPYRIGHT;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT__PROPERTIES = ARTIFACT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Artifact Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT__ARTIFACT_PROPERTIES = ARTIFACT__ARTIFACT_PROPERTIES;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT__SIZE = ARTIFACT__SIZE;

	/**
	 * The feature id for the '<em><b>Hash</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT__HASH = ARTIFACT__HASH;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT__TIMESTAMP = ARTIFACT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT__REFERENCES = ARTIFACT__REFERENCES;

	/**
	 * The number of structural features of the '<em>Binary Artifact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT_FEATURE_COUNT = ARTIFACT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Put Property</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT___PUT_PROPERTY__STRING_STRING = ARTIFACT___PUT_PROPERTY__STRING_STRING;

	/**
	 * The number of operations of the '<em>Binary Artifact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_ARTIFACT_OPERATION_COUNT = ARTIFACT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.gecko.artifacts.impl.EclipseFeatureImpl <em>Eclipse Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.artifacts.impl.EclipseFeatureImpl
	 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getEclipseFeature()
	 * @generated
	 */
	int ECLIPSE_FEATURE = 9;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__ID = ARTIFACT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__NAME = ARTIFACT__NAME;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__LABEL = ARTIFACT__LABEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__DESCRIPTION = ARTIFACT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__VERSION = ARTIFACT__VERSION;

	/**
	 * The feature id for the '<em><b>Source Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__SOURCE_LOCATION = ARTIFACT__SOURCE_LOCATION;

	/**
	 * The feature id for the '<em><b>Cache Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__CACHE_LOCATION = ARTIFACT__CACHE_LOCATION;

	/**
	 * The feature id for the '<em><b>Repository</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__REPOSITORY = ARTIFACT__REPOSITORY;

	/**
	 * The feature id for the '<em><b>License</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__LICENSE = ARTIFACT__LICENSE;

	/**
	 * The feature id for the '<em><b>Copyright</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__COPYRIGHT = ARTIFACT__COPYRIGHT;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__PROPERTIES = ARTIFACT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Artifact Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__ARTIFACT_PROPERTIES = ARTIFACT__ARTIFACT_PROPERTIES;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__SIZE = ARTIFACT__SIZE;

	/**
	 * The feature id for the '<em><b>Hash</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__HASH = ARTIFACT__HASH;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__TIMESTAMP = ARTIFACT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__REFERENCES = ARTIFACT__REFERENCES;

	/**
	 * The feature id for the '<em><b>Jar</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__JAR = ARTIFACT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Packed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__PACKED = ARTIFACT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Property Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__PROPERTY_LOCATION = ARTIFACT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Feature Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__FEATURE_PROPERTIES = ARTIFACT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__IMAGE = ARTIFACT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Provider</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE__PROVIDER = ARTIFACT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Eclipse Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE_FEATURE_COUNT = ARTIFACT_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>Put Property</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE___PUT_PROPERTY__STRING_STRING = ARTIFACT___PUT_PROPERTY__STRING_STRING;

	/**
	 * The number of operations of the '<em>Eclipse Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLIPSE_FEATURE_OPERATION_COUNT = ARTIFACT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.gecko.artifacts.impl.FilterRuleImpl <em>Filter Rule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.artifacts.impl.FilterRuleImpl
	 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getFilterRule()
	 * @generated
	 */
	int FILTER_RULE = 10;

	/**
	 * The feature id for the '<em><b>Filter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER_RULE__FILTER = 0;

	/**
	 * The feature id for the '<em><b>Rule</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER_RULE__RULE = 1;

	/**
	 * The number of structural features of the '<em>Filter Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER_RULE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Filter Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER_RULE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.artifacts.DependencyType <em>Dependency Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.artifacts.DependencyType
	 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getDependencyType()
	 * @generated
	 */
	int DEPENDENCY_TYPE = 11;

	/**
	 * The meta object id for the '{@link org.gecko.artifacts.ReferenceType <em>Reference Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.artifacts.ReferenceType
	 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getReferenceType()
	 * @generated
	 */
	int REFERENCE_TYPE = 12;

	/**
	 * The meta object id for the '{@link org.gecko.artifacts.RepositoryType <em>Repository Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.artifacts.RepositoryType
	 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getRepositoryType()
	 * @generated
	 */
	int REPOSITORY_TYPE = 13;

	/**
	 * The meta object id for the '<em>Version</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getVersion()
	 * @generated
	 */
	int VERSION = 14;


	/**
	 * The meta object id for the '<em>EJava Uri</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.net.URI
	 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getEJavaUri()
	 * @generated
	 */
	int EJAVA_URI = 15;


	/**
	 * The meta object id for the '<em>EProperties</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.Properties
	 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getEProperties()
	 * @generated
	 */
	int EPROPERTIES = 16;


	/**
	 * Returns the meta object for class '{@link org.gecko.artifacts.OSGiBundle <em>OS Gi Bundle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>OS Gi Bundle</em>'.
	 * @see org.gecko.artifacts.OSGiBundle
	 * @generated
	 */
	EClass getOSGiBundle();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.OSGiBundle#getBsn <em>Bsn</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bsn</em>'.
	 * @see org.gecko.artifacts.OSGiBundle#getBsn()
	 * @see #getOSGiBundle()
	 * @generated
	 */
	EAttribute getOSGiBundle_Bsn();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.OSGiBundle#getSourceLocation <em>Source Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Location</em>'.
	 * @see org.gecko.artifacts.OSGiBundle#getSourceLocation()
	 * @see #getOSGiBundle()
	 * @generated
	 */
	EAttribute getOSGiBundle_SourceLocation();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.OSGiBundle#getCacheLocation <em>Cache Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cache Location</em>'.
	 * @see org.gecko.artifacts.OSGiBundle#getCacheLocation()
	 * @see #getOSGiBundle()
	 * @generated
	 */
	EAttribute getOSGiBundle_CacheLocation();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.OSGiBundle#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.gecko.artifacts.OSGiBundle#getName()
	 * @see #getOSGiBundle()
	 * @generated
	 */
	EAttribute getOSGiBundle_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.OSGiBundle#getVendor <em>Vendor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Vendor</em>'.
	 * @see org.gecko.artifacts.OSGiBundle#getVendor()
	 * @see #getOSGiBundle()
	 * @generated
	 */
	EAttribute getOSGiBundle_Vendor();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.OSGiBundle#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.gecko.artifacts.OSGiBundle#getVersion()
	 * @see #getOSGiBundle()
	 * @generated
	 */
	EAttribute getOSGiBundle_Version();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.OSGiBundle#isPolicyLazy <em>Policy Lazy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Policy Lazy</em>'.
	 * @see org.gecko.artifacts.OSGiBundle#isPolicyLazy()
	 * @see #getOSGiBundle()
	 * @generated
	 */
	EAttribute getOSGiBundle_PolicyLazy();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.OSGiBundle#getExecutionEnvironment <em>Execution Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execution Environment</em>'.
	 * @see org.gecko.artifacts.OSGiBundle#getExecutionEnvironment()
	 * @see #getOSGiBundle()
	 * @generated
	 */
	EAttribute getOSGiBundle_ExecutionEnvironment();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.OSGiBundle#getClasspath <em>Classpath</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Classpath</em>'.
	 * @see org.gecko.artifacts.OSGiBundle#getClasspath()
	 * @see #getOSGiBundle()
	 * @generated
	 */
	EAttribute getOSGiBundle_Classpath();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.OSGiBundle#getLocalization <em>Localization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Localization</em>'.
	 * @see org.gecko.artifacts.OSGiBundle#getLocalization()
	 * @see #getOSGiBundle()
	 * @generated
	 */
	EAttribute getOSGiBundle_Localization();

	/**
	 * Returns the meta object for the reference list '{@link org.gecko.artifacts.OSGiBundle#getDependency <em>Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Dependency</em>'.
	 * @see org.gecko.artifacts.OSGiBundle#getDependency()
	 * @see #getOSGiBundle()
	 * @generated
	 */
	EReference getOSGiBundle_Dependency();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.OSGiBundle#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Properties</em>'.
	 * @see org.gecko.artifacts.OSGiBundle#getProperties()
	 * @see #getOSGiBundle()
	 * @generated
	 */
	EAttribute getOSGiBundle_Properties();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.artifacts.OSGiBundle#getBundleProperty <em>Bundle Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Bundle Property</em>'.
	 * @see org.gecko.artifacts.OSGiBundle#getBundleProperty()
	 * @see #getOSGiBundle()
	 * @generated
	 */
	EReference getOSGiBundle_BundleProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.OSGiBundle#getLicense <em>License</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>License</em>'.
	 * @see org.gecko.artifacts.OSGiBundle#getLicense()
	 * @see #getOSGiBundle()
	 * @generated
	 */
	EAttribute getOSGiBundle_License();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.OSGiBundle#getCopyright <em>Copyright</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Copyright</em>'.
	 * @see org.gecko.artifacts.OSGiBundle#getCopyright()
	 * @see #getOSGiBundle()
	 * @generated
	 */
	EAttribute getOSGiBundle_Copyright();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.OSGiBundle#getFragmentHost <em>Fragment Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fragment Host</em>'.
	 * @see org.gecko.artifacts.OSGiBundle#getFragmentHost()
	 * @see #getOSGiBundle()
	 * @generated
	 */
	EAttribute getOSGiBundle_FragmentHost();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.OSGiBundle#isFragment <em>Fragment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fragment</em>'.
	 * @see org.gecko.artifacts.OSGiBundle#isFragment()
	 * @see #getOSGiBundle()
	 * @generated
	 */
	EAttribute getOSGiBundle_Fragment();

	/**
	 * Returns the meta object for class '{@link org.gecko.artifacts.Dependency <em>Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dependency</em>'.
	 * @see org.gecko.artifacts.Dependency
	 * @generated
	 */
	EClass getDependency();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Dependency#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.gecko.artifacts.Dependency#getName()
	 * @see #getDependency()
	 * @generated
	 */
	EAttribute getDependency_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Dependency#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.gecko.artifacts.Dependency#getType()
	 * @see #getDependency()
	 * @generated
	 */
	EAttribute getDependency_Type();

	/**
	 * Returns the meta object for the reference '{@link org.gecko.artifacts.Dependency#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.gecko.artifacts.Dependency#getSource()
	 * @see #getDependency()
	 * @generated
	 */
	EReference getDependency_Source();

	/**
	 * Returns the meta object for the reference '{@link org.gecko.artifacts.Dependency#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.gecko.artifacts.Dependency#getTarget()
	 * @see #getDependency()
	 * @generated
	 */
	EReference getDependency_Target();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Dependency#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.gecko.artifacts.Dependency#getVersion()
	 * @see #getDependency()
	 * @generated
	 */
	EAttribute getDependency_Version();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Dependency#getVisibility <em>Visibility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Visibility</em>'.
	 * @see org.gecko.artifacts.Dependency#getVisibility()
	 * @see #getDependency()
	 * @generated
	 */
	EAttribute getDependency_Visibility();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Dependency#isOptional <em>Optional</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Optional</em>'.
	 * @see org.gecko.artifacts.Dependency#isOptional()
	 * @see #getDependency()
	 * @generated
	 */
	EAttribute getDependency_Optional();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Dependency#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Location</em>'.
	 * @see org.gecko.artifacts.Dependency#getLocation()
	 * @see #getDependency()
	 * @generated
	 */
	EAttribute getDependency_Location();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Dependency#isReexport <em>Reexport</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reexport</em>'.
	 * @see org.gecko.artifacts.Dependency#isReexport()
	 * @see #getDependency()
	 * @generated
	 */
	EAttribute getDependency_Reexport();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.artifacts.Dependency#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object</em>'.
	 * @see org.gecko.artifacts.Dependency#getObject()
	 * @see #getDependency()
	 * @generated
	 */
	EReference getDependency_Object();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Dependency#getFilter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filter</em>'.
	 * @see org.gecko.artifacts.Dependency#getFilter()
	 * @see #getDependency()
	 * @generated
	 */
	EAttribute getDependency_Filter();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Dependency#isMultiple <em>Multiple</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Multiple</em>'.
	 * @see org.gecko.artifacts.Dependency#isMultiple()
	 * @see #getDependency()
	 * @generated
	 */
	EAttribute getDependency_Multiple();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Dependency#isActive <em>Active</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Active</em>'.
	 * @see org.gecko.artifacts.Dependency#isActive()
	 * @see #getDependency()
	 * @generated
	 */
	EAttribute getDependency_Active();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.artifacts.Dependency#getDependencyProperty <em>Dependency Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dependency Property</em>'.
	 * @see org.gecko.artifacts.Dependency#getDependencyProperty()
	 * @see #getDependency()
	 * @generated
	 */
	EReference getDependency_DependencyProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Dependency#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Properties</em>'.
	 * @see org.gecko.artifacts.Dependency#getProperties()
	 * @see #getDependency()
	 * @generated
	 */
	EAttribute getDependency_Properties();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Dependency#getObjectClass <em>Object Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Object Class</em>'.
	 * @see org.gecko.artifacts.Dependency#getObjectClass()
	 * @see #getDependency()
	 * @generated
	 */
	EAttribute getDependency_ObjectClass();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Dependency#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.gecko.artifacts.Dependency#getId()
	 * @see #getDependency()
	 * @generated
	 */
	EAttribute getDependency_Id();

	/**
	 * Returns the meta object for class '{@link org.gecko.artifacts.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property</em>'.
	 * @see org.gecko.artifacts.Property
	 * @generated
	 */
	EClass getProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Property#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see org.gecko.artifacts.Property#getKey()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Key();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Property#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.gecko.artifacts.Property#getValue()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Value();

	/**
	 * Returns the meta object for class '{@link org.gecko.artifacts.Artifact <em>Artifact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Artifact</em>'.
	 * @see org.gecko.artifacts.Artifact
	 * @generated
	 */
	EClass getArtifact();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Artifact#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.gecko.artifacts.Artifact#getId()
	 * @see #getArtifact()
	 * @generated
	 */
	EAttribute getArtifact_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Artifact#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.gecko.artifacts.Artifact#getName()
	 * @see #getArtifact()
	 * @generated
	 */
	EAttribute getArtifact_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Artifact#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see org.gecko.artifacts.Artifact#getLabel()
	 * @see #getArtifact()
	 * @generated
	 */
	EAttribute getArtifact_Label();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Artifact#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.gecko.artifacts.Artifact#getVersion()
	 * @see #getArtifact()
	 * @generated
	 */
	EAttribute getArtifact_Version();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Artifact#getSourceLocation <em>Source Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Location</em>'.
	 * @see org.gecko.artifacts.Artifact#getSourceLocation()
	 * @see #getArtifact()
	 * @generated
	 */
	EAttribute getArtifact_SourceLocation();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Artifact#getCacheLocation <em>Cache Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cache Location</em>'.
	 * @see org.gecko.artifacts.Artifact#getCacheLocation()
	 * @see #getArtifact()
	 * @generated
	 */
	EAttribute getArtifact_CacheLocation();

	/**
	 * Returns the meta object for the reference '{@link org.gecko.artifacts.Artifact#getRepository <em>Repository</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repository</em>'.
	 * @see org.gecko.artifacts.Artifact#getRepository()
	 * @see #getArtifact()
	 * @generated
	 */
	EReference getArtifact_Repository();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Artifact#getLicense <em>License</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>License</em>'.
	 * @see org.gecko.artifacts.Artifact#getLicense()
	 * @see #getArtifact()
	 * @generated
	 */
	EAttribute getArtifact_License();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Artifact#getCopyright <em>Copyright</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Copyright</em>'.
	 * @see org.gecko.artifacts.Artifact#getCopyright()
	 * @see #getArtifact()
	 * @generated
	 */
	EAttribute getArtifact_Copyright();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Artifact#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Properties</em>'.
	 * @see org.gecko.artifacts.Artifact#getProperties()
	 * @see #getArtifact()
	 * @generated
	 */
	EAttribute getArtifact_Properties();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.artifacts.Artifact#getArtifactProperties <em>Artifact Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Artifact Properties</em>'.
	 * @see org.gecko.artifacts.Artifact#getArtifactProperties()
	 * @see #getArtifact()
	 * @generated
	 */
	EReference getArtifact_ArtifactProperties();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Artifact#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see org.gecko.artifacts.Artifact#getSize()
	 * @see #getArtifact()
	 * @generated
	 */
	EAttribute getArtifact_Size();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Artifact#getHash <em>Hash</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hash</em>'.
	 * @see org.gecko.artifacts.Artifact#getHash()
	 * @see #getArtifact()
	 * @generated
	 */
	EAttribute getArtifact_Hash();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Artifact#getTimestamp <em>Timestamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timestamp</em>'.
	 * @see org.gecko.artifacts.Artifact#getTimestamp()
	 * @see #getArtifact()
	 * @generated
	 */
	EAttribute getArtifact_Timestamp();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.Artifact#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.gecko.artifacts.Artifact#getDescription()
	 * @see #getArtifact()
	 * @generated
	 */
	EAttribute getArtifact_Description();

	/**
	 * Returns the meta object for the reference list '{@link org.gecko.artifacts.Artifact#getReferences <em>References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>References</em>'.
	 * @see org.gecko.artifacts.Artifact#getReferences()
	 * @see #getArtifact()
	 * @generated
	 */
	EReference getArtifact_References();

	/**
	 * Returns the meta object for the '{@link org.gecko.artifacts.Artifact#putProperty(java.lang.String, java.lang.String) <em>Put Property</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Put Property</em>' operation.
	 * @see org.gecko.artifacts.Artifact#putProperty(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getArtifact__PutProperty__String_String();

	/**
	 * Returns the meta object for class '{@link org.gecko.artifacts.ArtifactRepository <em>Artifact Repository</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Artifact Repository</em>'.
	 * @see org.gecko.artifacts.ArtifactRepository
	 * @generated
	 */
	EClass getArtifactRepository();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactRepository#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.gecko.artifacts.ArtifactRepository#getId()
	 * @see #getArtifactRepository()
	 * @generated
	 */
	EAttribute getArtifactRepository_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactRepository#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.gecko.artifacts.ArtifactRepository#getName()
	 * @see #getArtifactRepository()
	 * @generated
	 */
	EAttribute getArtifactRepository_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactRepository#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.gecko.artifacts.ArtifactRepository#getVersion()
	 * @see #getArtifactRepository()
	 * @generated
	 */
	EAttribute getArtifactRepository_Version();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactRepository#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.gecko.artifacts.ArtifactRepository#getType()
	 * @see #getArtifactRepository()
	 * @generated
	 */
	EAttribute getArtifactRepository_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactRepository#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Location</em>'.
	 * @see org.gecko.artifacts.ArtifactRepository#getLocation()
	 * @see #getArtifactRepository()
	 * @generated
	 */
	EAttribute getArtifactRepository_Location();

	/**
	 * Returns the meta object for the reference list '{@link org.gecko.artifacts.ArtifactRepository#getArtifacts <em>Artifacts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Artifacts</em>'.
	 * @see org.gecko.artifacts.ArtifactRepository#getArtifacts()
	 * @see #getArtifactRepository()
	 * @generated
	 */
	EReference getArtifactRepository_Artifacts();

	/**
	 * Returns the meta object for the reference list '{@link org.gecko.artifacts.ArtifactRepository#getRepositories <em>Repositories</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Repositories</em>'.
	 * @see org.gecko.artifacts.ArtifactRepository#getRepositories()
	 * @see #getArtifactRepository()
	 * @generated
	 */
	EReference getArtifactRepository_Repositories();

	/**
	 * Returns the meta object for the reference '{@link org.gecko.artifacts.ArtifactRepository#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent</em>'.
	 * @see org.gecko.artifacts.ArtifactRepository#getParent()
	 * @see #getArtifactRepository()
	 * @generated
	 */
	EReference getArtifactRepository_Parent();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactRepository#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Properties</em>'.
	 * @see org.gecko.artifacts.ArtifactRepository#getProperties()
	 * @see #getArtifactRepository()
	 * @generated
	 */
	EAttribute getArtifactRepository_Properties();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.artifacts.ArtifactRepository#getRepositoryProperties <em>Repository Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Repository Properties</em>'.
	 * @see org.gecko.artifacts.ArtifactRepository#getRepositoryProperties()
	 * @see #getArtifactRepository()
	 * @generated
	 */
	EReference getArtifactRepository_RepositoryProperties();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactRepository#getLocationUri <em>Location Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Location Uri</em>'.
	 * @see org.gecko.artifacts.ArtifactRepository#getLocationUri()
	 * @see #getArtifactRepository()
	 * @generated
	 */
	EAttribute getArtifactRepository_LocationUri();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactRepository#getTimestamp <em>Timestamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timestamp</em>'.
	 * @see org.gecko.artifacts.ArtifactRepository#getTimestamp()
	 * @see #getArtifactRepository()
	 * @generated
	 */
	EAttribute getArtifactRepository_Timestamp();

	/**
	 * Returns the meta object for the reference list '{@link org.gecko.artifacts.ArtifactRepository#getAllArtifacts <em>All Artifacts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Artifacts</em>'.
	 * @see org.gecko.artifacts.ArtifactRepository#getAllArtifacts()
	 * @see #getArtifactRepository()
	 * @generated
	 */
	EReference getArtifactRepository_AllArtifacts();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactRepository#isLoaded <em>Loaded</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Loaded</em>'.
	 * @see org.gecko.artifacts.ArtifactRepository#isLoaded()
	 * @see #getArtifactRepository()
	 * @generated
	 */
	EAttribute getArtifactRepository_Loaded();

	/**
	 * Returns the meta object for the '{@link org.gecko.artifacts.ArtifactRepository#getArtifactById(java.lang.String) <em>Get Artifact By Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Artifact By Id</em>' operation.
	 * @see org.gecko.artifacts.ArtifactRepository#getArtifactById(java.lang.String)
	 * @generated
	 */
	EOperation getArtifactRepository__GetArtifactById__String();

	/**
	 * Returns the meta object for the '{@link org.gecko.artifacts.ArtifactRepository#getArtifactsById(java.lang.String) <em>Get Artifacts By Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Artifacts By Id</em>' operation.
	 * @see org.gecko.artifacts.ArtifactRepository#getArtifactsById(java.lang.String)
	 * @generated
	 */
	EOperation getArtifactRepository__GetArtifactsById__String();

	/**
	 * Returns the meta object for the '{@link org.gecko.artifacts.ArtifactRepository#getArtifactsByName(java.lang.String) <em>Get Artifacts By Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Artifacts By Name</em>' operation.
	 * @see org.gecko.artifacts.ArtifactRepository#getArtifactsByName(java.lang.String)
	 * @generated
	 */
	EOperation getArtifactRepository__GetArtifactsByName__String();

	/**
	 * Returns the meta object for the '{@link org.gecko.artifacts.ArtifactRepository#getAllArtifactsById(java.lang.String) <em>Get All Artifacts By Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Artifacts By Id</em>' operation.
	 * @see org.gecko.artifacts.ArtifactRepository#getAllArtifactsById(java.lang.String)
	 * @generated
	 */
	EOperation getArtifactRepository__GetAllArtifactsById__String();

	/**
	 * Returns the meta object for class '{@link org.gecko.artifacts.ArtifactReference <em>Artifact Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Artifact Reference</em>'.
	 * @see org.gecko.artifacts.ArtifactReference
	 * @generated
	 */
	EClass getArtifactReference();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactReference#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.gecko.artifacts.ArtifactReference#getId()
	 * @see #getArtifactReference()
	 * @generated
	 */
	EAttribute getArtifactReference_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactReference#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.gecko.artifacts.ArtifactReference#getVersion()
	 * @see #getArtifactReference()
	 * @generated
	 */
	EAttribute getArtifactReference_Version();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactReference#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.gecko.artifacts.ArtifactReference#getName()
	 * @see #getArtifactReference()
	 * @generated
	 */
	EAttribute getArtifactReference_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactReference#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see org.gecko.artifacts.ArtifactReference#getLabel()
	 * @see #getArtifactReference()
	 * @generated
	 */
	EAttribute getArtifactReference_Label();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactReference#isOptional <em>Optional</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Optional</em>'.
	 * @see org.gecko.artifacts.ArtifactReference#isOptional()
	 * @see #getArtifactReference()
	 * @generated
	 */
	EAttribute getArtifactReference_Optional();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactReference#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Location</em>'.
	 * @see org.gecko.artifacts.ArtifactReference#getLocation()
	 * @see #getArtifactReference()
	 * @generated
	 */
	EAttribute getArtifactReference_Location();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactReference#getFilter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filter</em>'.
	 * @see org.gecko.artifacts.ArtifactReference#getFilter()
	 * @see #getArtifactReference()
	 * @generated
	 */
	EAttribute getArtifactReference_Filter();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactReference#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.gecko.artifacts.ArtifactReference#getType()
	 * @see #getArtifactReference()
	 * @generated
	 */
	EAttribute getArtifactReference_Type();

	/**
	 * Returns the meta object for the reference '{@link org.gecko.artifacts.ArtifactReference#getRefObject <em>Ref Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref Object</em>'.
	 * @see org.gecko.artifacts.ArtifactReference#getRefObject()
	 * @see #getArtifactReference()
	 * @generated
	 */
	EReference getArtifactReference_RefObject();

	/**
	 * Returns the meta object for the reference '{@link org.gecko.artifacts.ArtifactReference#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.gecko.artifacts.ArtifactReference#getSource()
	 * @see #getArtifactReference()
	 * @generated
	 */
	EReference getArtifactReference_Source();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactReference#getSourceName <em>Source Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Name</em>'.
	 * @see org.gecko.artifacts.ArtifactReference#getSourceName()
	 * @see #getArtifactReference()
	 * @generated
	 */
	EAttribute getArtifactReference_SourceName();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactReference#getSourceVersion <em>Source Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Version</em>'.
	 * @see org.gecko.artifacts.ArtifactReference#getSourceVersion()
	 * @see #getArtifactReference()
	 * @generated
	 */
	EAttribute getArtifactReference_SourceVersion();

	/**
	 * Returns the meta object for the reference list '{@link org.gecko.artifacts.ArtifactReference#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Target</em>'.
	 * @see org.gecko.artifacts.ArtifactReference#getTarget()
	 * @see #getArtifactReference()
	 * @generated
	 */
	EReference getArtifactReference_Target();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactReference#getTargetName <em>Target Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Name</em>'.
	 * @see org.gecko.artifacts.ArtifactReference#getTargetName()
	 * @see #getArtifactReference()
	 * @generated
	 */
	EAttribute getArtifactReference_TargetName();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactReference#getTargetVersion <em>Target Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Version</em>'.
	 * @see org.gecko.artifacts.ArtifactReference#getTargetVersion()
	 * @see #getArtifactReference()
	 * @generated
	 */
	EAttribute getArtifactReference_TargetVersion();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.ArtifactReference#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Properties</em>'.
	 * @see org.gecko.artifacts.ArtifactReference#getProperties()
	 * @see #getArtifactReference()
	 * @generated
	 */
	EAttribute getArtifactReference_Properties();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.artifacts.ArtifactReference#getReferenceProperties <em>Reference Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reference Properties</em>'.
	 * @see org.gecko.artifacts.ArtifactReference#getReferenceProperties()
	 * @see #getArtifactReference()
	 * @generated
	 */
	EReference getArtifactReference_ReferenceProperties();

	/**
	 * Returns the meta object for the '{@link org.gecko.artifacts.ArtifactReference#putProperty(java.lang.String, java.lang.String) <em>Put Property</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Put Property</em>' operation.
	 * @see org.gecko.artifacts.ArtifactReference#putProperty(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getArtifactReference__PutProperty__String_String();

	/**
	 * Returns the meta object for class '{@link org.gecko.artifacts.P2Repository <em>P2 Repository</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>P2 Repository</em>'.
	 * @see org.gecko.artifacts.P2Repository
	 * @generated
	 */
	EClass getP2Repository();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.P2Repository#getP2RepositoryType <em>P2 Repository Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>P2 Repository Type</em>'.
	 * @see org.gecko.artifacts.P2Repository#getP2RepositoryType()
	 * @see #getP2Repository()
	 * @generated
	 */
	EAttribute getP2Repository_P2RepositoryType();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.P2Repository#getMetadataLocation <em>Metadata Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Metadata Location</em>'.
	 * @see org.gecko.artifacts.P2Repository#getMetadataLocation()
	 * @see #getP2Repository()
	 * @generated
	 */
	EAttribute getP2Repository_MetadataLocation();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.P2Repository#getArtifactLocation <em>Artifact Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Artifact Location</em>'.
	 * @see org.gecko.artifacts.P2Repository#getArtifactLocation()
	 * @see #getP2Repository()
	 * @generated
	 */
	EAttribute getP2Repository_ArtifactLocation();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.P2Repository#getIndexLocation <em>Index Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Index Location</em>'.
	 * @see org.gecko.artifacts.P2Repository#getIndexLocation()
	 * @see #getP2Repository()
	 * @generated
	 */
	EAttribute getP2Repository_IndexLocation();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.P2Repository#isCompositeRepository <em>Composite Repository</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Composite Repository</em>'.
	 * @see org.gecko.artifacts.P2Repository#isCompositeRepository()
	 * @see #getP2Repository()
	 * @generated
	 */
	EAttribute getP2Repository_CompositeRepository();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.artifacts.P2Repository#getRules <em>Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rules</em>'.
	 * @see org.gecko.artifacts.P2Repository#getRules()
	 * @see #getP2Repository()
	 * @generated
	 */
	EReference getP2Repository_Rules();

	/**
	 * Returns the meta object for the reference list '{@link org.gecko.artifacts.P2Repository#getEclipseFeatures <em>Eclipse Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Eclipse Features</em>'.
	 * @see org.gecko.artifacts.P2Repository#getEclipseFeatures()
	 * @see #getP2Repository()
	 * @generated
	 */
	EReference getP2Repository_EclipseFeatures();

	/**
	 * Returns the meta object for the '{@link org.gecko.artifacts.P2Repository#normalizeUri() <em>Normalize Uri</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Normalize Uri</em>' operation.
	 * @see org.gecko.artifacts.P2Repository#normalizeUri()
	 * @generated
	 */
	EOperation getP2Repository__NormalizeUri();

	/**
	 * Returns the meta object for class '{@link org.gecko.artifacts.BundleArtifact <em>Bundle Artifact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bundle Artifact</em>'.
	 * @see org.gecko.artifacts.BundleArtifact
	 * @generated
	 */
	EClass getBundleArtifact();

	/**
	 * Returns the meta object for class '{@link org.gecko.artifacts.BinaryArtifact <em>Binary Artifact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Artifact</em>'.
	 * @see org.gecko.artifacts.BinaryArtifact
	 * @generated
	 */
	EClass getBinaryArtifact();

	/**
	 * Returns the meta object for class '{@link org.gecko.artifacts.EclipseFeature <em>Eclipse Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Eclipse Feature</em>'.
	 * @see org.gecko.artifacts.EclipseFeature
	 * @generated
	 */
	EClass getEclipseFeature();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.EclipseFeature#isJar <em>Jar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Jar</em>'.
	 * @see org.gecko.artifacts.EclipseFeature#isJar()
	 * @see #getEclipseFeature()
	 * @generated
	 */
	EAttribute getEclipseFeature_Jar();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.EclipseFeature#isPacked <em>Packed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Packed</em>'.
	 * @see org.gecko.artifacts.EclipseFeature#isPacked()
	 * @see #getEclipseFeature()
	 * @generated
	 */
	EAttribute getEclipseFeature_Packed();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.EclipseFeature#getPropertyLocation <em>Property Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Property Location</em>'.
	 * @see org.gecko.artifacts.EclipseFeature#getPropertyLocation()
	 * @see #getEclipseFeature()
	 * @generated
	 */
	EAttribute getEclipseFeature_PropertyLocation();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.EclipseFeature#getFeatureProperties <em>Feature Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Feature Properties</em>'.
	 * @see org.gecko.artifacts.EclipseFeature#getFeatureProperties()
	 * @see #getEclipseFeature()
	 * @generated
	 */
	EAttribute getEclipseFeature_FeatureProperties();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.EclipseFeature#getImage <em>Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Image</em>'.
	 * @see org.gecko.artifacts.EclipseFeature#getImage()
	 * @see #getEclipseFeature()
	 * @generated
	 */
	EAttribute getEclipseFeature_Image();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.EclipseFeature#getProvider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Provider</em>'.
	 * @see org.gecko.artifacts.EclipseFeature#getProvider()
	 * @see #getEclipseFeature()
	 * @generated
	 */
	EAttribute getEclipseFeature_Provider();

	/**
	 * Returns the meta object for class '{@link org.gecko.artifacts.FilterRule <em>Filter Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Filter Rule</em>'.
	 * @see org.gecko.artifacts.FilterRule
	 * @generated
	 */
	EClass getFilterRule();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.FilterRule#getFilter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filter</em>'.
	 * @see org.gecko.artifacts.FilterRule#getFilter()
	 * @see #getFilterRule()
	 * @generated
	 */
	EAttribute getFilterRule_Filter();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.artifacts.FilterRule#getRule <em>Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rule</em>'.
	 * @see org.gecko.artifacts.FilterRule#getRule()
	 * @see #getFilterRule()
	 * @generated
	 */
	EAttribute getFilterRule_Rule();

	/**
	 * Returns the meta object for enum '{@link org.gecko.artifacts.DependencyType <em>Dependency Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Dependency Type</em>'.
	 * @see org.gecko.artifacts.DependencyType
	 * @generated
	 */
	EEnum getDependencyType();

	/**
	 * Returns the meta object for enum '{@link org.gecko.artifacts.ReferenceType <em>Reference Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Reference Type</em>'.
	 * @see org.gecko.artifacts.ReferenceType
	 * @generated
	 */
	EEnum getReferenceType();

	/**
	 * Returns the meta object for enum '{@link org.gecko.artifacts.RepositoryType <em>Repository Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Repository Type</em>'.
	 * @see org.gecko.artifacts.RepositoryType
	 * @generated
	 */
	EEnum getRepositoryType();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Version</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 *        extendedMetaData="name='Version' baseType='http://www.eclipse.org/emf/2003/XMLType#string' pattern='[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}(\\..*)?'"
	 * @generated
	 */
	EDataType getVersion();

	/**
	 * Returns the meta object for data type '{@link java.net.URI <em>EJava Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EJava Uri</em>'.
	 * @see java.net.URI
	 * @model instanceClass="java.net.URI"
	 * @generated
	 */
	EDataType getEJavaUri();

	/**
	 * Returns the meta object for data type '{@link java.util.Properties <em>EProperties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EProperties</em>'.
	 * @see java.util.Properties
	 * @model instanceClass="java.util.Properties"
	 * @generated
	 */
	EDataType getEProperties();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ArtifactsFactory getArtifactsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.gecko.artifacts.impl.OSGiBundleImpl <em>OS Gi Bundle</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.artifacts.impl.OSGiBundleImpl
		 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getOSGiBundle()
		 * @generated
		 */
		EClass OS_GI_BUNDLE = eINSTANCE.getOSGiBundle();

		/**
		 * The meta object literal for the '<em><b>Bsn</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OS_GI_BUNDLE__BSN = eINSTANCE.getOSGiBundle_Bsn();

		/**
		 * The meta object literal for the '<em><b>Source Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OS_GI_BUNDLE__SOURCE_LOCATION = eINSTANCE.getOSGiBundle_SourceLocation();

		/**
		 * The meta object literal for the '<em><b>Cache Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OS_GI_BUNDLE__CACHE_LOCATION = eINSTANCE.getOSGiBundle_CacheLocation();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OS_GI_BUNDLE__NAME = eINSTANCE.getOSGiBundle_Name();

		/**
		 * The meta object literal for the '<em><b>Vendor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OS_GI_BUNDLE__VENDOR = eINSTANCE.getOSGiBundle_Vendor();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OS_GI_BUNDLE__VERSION = eINSTANCE.getOSGiBundle_Version();

		/**
		 * The meta object literal for the '<em><b>Policy Lazy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OS_GI_BUNDLE__POLICY_LAZY = eINSTANCE.getOSGiBundle_PolicyLazy();

		/**
		 * The meta object literal for the '<em><b>Execution Environment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OS_GI_BUNDLE__EXECUTION_ENVIRONMENT = eINSTANCE.getOSGiBundle_ExecutionEnvironment();

		/**
		 * The meta object literal for the '<em><b>Classpath</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OS_GI_BUNDLE__CLASSPATH = eINSTANCE.getOSGiBundle_Classpath();

		/**
		 * The meta object literal for the '<em><b>Localization</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OS_GI_BUNDLE__LOCALIZATION = eINSTANCE.getOSGiBundle_Localization();

		/**
		 * The meta object literal for the '<em><b>Dependency</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OS_GI_BUNDLE__DEPENDENCY = eINSTANCE.getOSGiBundle_Dependency();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OS_GI_BUNDLE__PROPERTIES = eINSTANCE.getOSGiBundle_Properties();

		/**
		 * The meta object literal for the '<em><b>Bundle Property</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OS_GI_BUNDLE__BUNDLE_PROPERTY = eINSTANCE.getOSGiBundle_BundleProperty();

		/**
		 * The meta object literal for the '<em><b>License</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OS_GI_BUNDLE__LICENSE = eINSTANCE.getOSGiBundle_License();

		/**
		 * The meta object literal for the '<em><b>Copyright</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OS_GI_BUNDLE__COPYRIGHT = eINSTANCE.getOSGiBundle_Copyright();

		/**
		 * The meta object literal for the '<em><b>Fragment Host</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OS_GI_BUNDLE__FRAGMENT_HOST = eINSTANCE.getOSGiBundle_FragmentHost();

		/**
		 * The meta object literal for the '<em><b>Fragment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OS_GI_BUNDLE__FRAGMENT = eINSTANCE.getOSGiBundle_Fragment();

		/**
		 * The meta object literal for the '{@link org.gecko.artifacts.impl.DependencyImpl <em>Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.artifacts.impl.DependencyImpl
		 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getDependency()
		 * @generated
		 */
		EClass DEPENDENCY = eINSTANCE.getDependency();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPENDENCY__NAME = eINSTANCE.getDependency_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPENDENCY__TYPE = eINSTANCE.getDependency_Type();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPENDENCY__SOURCE = eINSTANCE.getDependency_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPENDENCY__TARGET = eINSTANCE.getDependency_Target();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPENDENCY__VERSION = eINSTANCE.getDependency_Version();

		/**
		 * The meta object literal for the '<em><b>Visibility</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPENDENCY__VISIBILITY = eINSTANCE.getDependency_Visibility();

		/**
		 * The meta object literal for the '<em><b>Optional</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPENDENCY__OPTIONAL = eINSTANCE.getDependency_Optional();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPENDENCY__LOCATION = eINSTANCE.getDependency_Location();

		/**
		 * The meta object literal for the '<em><b>Reexport</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPENDENCY__REEXPORT = eINSTANCE.getDependency_Reexport();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPENDENCY__OBJECT = eINSTANCE.getDependency_Object();

		/**
		 * The meta object literal for the '<em><b>Filter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPENDENCY__FILTER = eINSTANCE.getDependency_Filter();

		/**
		 * The meta object literal for the '<em><b>Multiple</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPENDENCY__MULTIPLE = eINSTANCE.getDependency_Multiple();

		/**
		 * The meta object literal for the '<em><b>Active</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPENDENCY__ACTIVE = eINSTANCE.getDependency_Active();

		/**
		 * The meta object literal for the '<em><b>Dependency Property</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPENDENCY__DEPENDENCY_PROPERTY = eINSTANCE.getDependency_DependencyProperty();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPENDENCY__PROPERTIES = eINSTANCE.getDependency_Properties();

		/**
		 * The meta object literal for the '<em><b>Object Class</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPENDENCY__OBJECT_CLASS = eINSTANCE.getDependency_ObjectClass();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPENDENCY__ID = eINSTANCE.getDependency_Id();

		/**
		 * The meta object literal for the '{@link org.gecko.artifacts.impl.PropertyImpl <em>Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.artifacts.impl.PropertyImpl
		 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getProperty()
		 * @generated
		 */
		EClass PROPERTY = eINSTANCE.getProperty();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__KEY = eINSTANCE.getProperty_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__VALUE = eINSTANCE.getProperty_Value();

		/**
		 * The meta object literal for the '{@link org.gecko.artifacts.impl.ArtifactImpl <em>Artifact</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.artifacts.impl.ArtifactImpl
		 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getArtifact()
		 * @generated
		 */
		EClass ARTIFACT = eINSTANCE.getArtifact();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT__ID = eINSTANCE.getArtifact_Id();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT__NAME = eINSTANCE.getArtifact_Name();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT__LABEL = eINSTANCE.getArtifact_Label();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT__VERSION = eINSTANCE.getArtifact_Version();

		/**
		 * The meta object literal for the '<em><b>Source Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT__SOURCE_LOCATION = eINSTANCE.getArtifact_SourceLocation();

		/**
		 * The meta object literal for the '<em><b>Cache Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT__CACHE_LOCATION = eINSTANCE.getArtifact_CacheLocation();

		/**
		 * The meta object literal for the '<em><b>Repository</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTIFACT__REPOSITORY = eINSTANCE.getArtifact_Repository();

		/**
		 * The meta object literal for the '<em><b>License</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT__LICENSE = eINSTANCE.getArtifact_License();

		/**
		 * The meta object literal for the '<em><b>Copyright</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT__COPYRIGHT = eINSTANCE.getArtifact_Copyright();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT__PROPERTIES = eINSTANCE.getArtifact_Properties();

		/**
		 * The meta object literal for the '<em><b>Artifact Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTIFACT__ARTIFACT_PROPERTIES = eINSTANCE.getArtifact_ArtifactProperties();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT__SIZE = eINSTANCE.getArtifact_Size();

		/**
		 * The meta object literal for the '<em><b>Hash</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT__HASH = eINSTANCE.getArtifact_Hash();

		/**
		 * The meta object literal for the '<em><b>Timestamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT__TIMESTAMP = eINSTANCE.getArtifact_Timestamp();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT__DESCRIPTION = eINSTANCE.getArtifact_Description();

		/**
		 * The meta object literal for the '<em><b>References</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTIFACT__REFERENCES = eINSTANCE.getArtifact_References();

		/**
		 * The meta object literal for the '<em><b>Put Property</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ARTIFACT___PUT_PROPERTY__STRING_STRING = eINSTANCE.getArtifact__PutProperty__String_String();

		/**
		 * The meta object literal for the '{@link org.gecko.artifacts.impl.ArtifactRepositoryImpl <em>Artifact Repository</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.artifacts.impl.ArtifactRepositoryImpl
		 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getArtifactRepository()
		 * @generated
		 */
		EClass ARTIFACT_REPOSITORY = eINSTANCE.getArtifactRepository();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REPOSITORY__ID = eINSTANCE.getArtifactRepository_Id();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REPOSITORY__NAME = eINSTANCE.getArtifactRepository_Name();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REPOSITORY__VERSION = eINSTANCE.getArtifactRepository_Version();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REPOSITORY__TYPE = eINSTANCE.getArtifactRepository_Type();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REPOSITORY__LOCATION = eINSTANCE.getArtifactRepository_Location();

		/**
		 * The meta object literal for the '<em><b>Artifacts</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTIFACT_REPOSITORY__ARTIFACTS = eINSTANCE.getArtifactRepository_Artifacts();

		/**
		 * The meta object literal for the '<em><b>Repositories</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTIFACT_REPOSITORY__REPOSITORIES = eINSTANCE.getArtifactRepository_Repositories();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTIFACT_REPOSITORY__PARENT = eINSTANCE.getArtifactRepository_Parent();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REPOSITORY__PROPERTIES = eINSTANCE.getArtifactRepository_Properties();

		/**
		 * The meta object literal for the '<em><b>Repository Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTIFACT_REPOSITORY__REPOSITORY_PROPERTIES = eINSTANCE.getArtifactRepository_RepositoryProperties();

		/**
		 * The meta object literal for the '<em><b>Location Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REPOSITORY__LOCATION_URI = eINSTANCE.getArtifactRepository_LocationUri();

		/**
		 * The meta object literal for the '<em><b>Timestamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REPOSITORY__TIMESTAMP = eINSTANCE.getArtifactRepository_Timestamp();

		/**
		 * The meta object literal for the '<em><b>All Artifacts</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTIFACT_REPOSITORY__ALL_ARTIFACTS = eINSTANCE.getArtifactRepository_AllArtifacts();

		/**
		 * The meta object literal for the '<em><b>Loaded</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REPOSITORY__LOADED = eINSTANCE.getArtifactRepository_Loaded();

		/**
		 * The meta object literal for the '<em><b>Get Artifact By Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ARTIFACT_REPOSITORY___GET_ARTIFACT_BY_ID__STRING = eINSTANCE.getArtifactRepository__GetArtifactById__String();

		/**
		 * The meta object literal for the '<em><b>Get Artifacts By Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ARTIFACT_REPOSITORY___GET_ARTIFACTS_BY_ID__STRING = eINSTANCE.getArtifactRepository__GetArtifactsById__String();

		/**
		 * The meta object literal for the '<em><b>Get Artifacts By Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ARTIFACT_REPOSITORY___GET_ARTIFACTS_BY_NAME__STRING = eINSTANCE.getArtifactRepository__GetArtifactsByName__String();

		/**
		 * The meta object literal for the '<em><b>Get All Artifacts By Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ARTIFACT_REPOSITORY___GET_ALL_ARTIFACTS_BY_ID__STRING = eINSTANCE.getArtifactRepository__GetAllArtifactsById__String();

		/**
		 * The meta object literal for the '{@link org.gecko.artifacts.impl.ArtifactReferenceImpl <em>Artifact Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.artifacts.impl.ArtifactReferenceImpl
		 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getArtifactReference()
		 * @generated
		 */
		EClass ARTIFACT_REFERENCE = eINSTANCE.getArtifactReference();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REFERENCE__ID = eINSTANCE.getArtifactReference_Id();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REFERENCE__VERSION = eINSTANCE.getArtifactReference_Version();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REFERENCE__NAME = eINSTANCE.getArtifactReference_Name();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REFERENCE__LABEL = eINSTANCE.getArtifactReference_Label();

		/**
		 * The meta object literal for the '<em><b>Optional</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REFERENCE__OPTIONAL = eINSTANCE.getArtifactReference_Optional();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REFERENCE__LOCATION = eINSTANCE.getArtifactReference_Location();

		/**
		 * The meta object literal for the '<em><b>Filter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REFERENCE__FILTER = eINSTANCE.getArtifactReference_Filter();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REFERENCE__TYPE = eINSTANCE.getArtifactReference_Type();

		/**
		 * The meta object literal for the '<em><b>Ref Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTIFACT_REFERENCE__REF_OBJECT = eINSTANCE.getArtifactReference_RefObject();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTIFACT_REFERENCE__SOURCE = eINSTANCE.getArtifactReference_Source();

		/**
		 * The meta object literal for the '<em><b>Source Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REFERENCE__SOURCE_NAME = eINSTANCE.getArtifactReference_SourceName();

		/**
		 * The meta object literal for the '<em><b>Source Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REFERENCE__SOURCE_VERSION = eINSTANCE.getArtifactReference_SourceVersion();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTIFACT_REFERENCE__TARGET = eINSTANCE.getArtifactReference_Target();

		/**
		 * The meta object literal for the '<em><b>Target Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REFERENCE__TARGET_NAME = eINSTANCE.getArtifactReference_TargetName();

		/**
		 * The meta object literal for the '<em><b>Target Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REFERENCE__TARGET_VERSION = eINSTANCE.getArtifactReference_TargetVersion();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT_REFERENCE__PROPERTIES = eINSTANCE.getArtifactReference_Properties();

		/**
		 * The meta object literal for the '<em><b>Reference Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTIFACT_REFERENCE__REFERENCE_PROPERTIES = eINSTANCE.getArtifactReference_ReferenceProperties();

		/**
		 * The meta object literal for the '<em><b>Put Property</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ARTIFACT_REFERENCE___PUT_PROPERTY__STRING_STRING = eINSTANCE.getArtifactReference__PutProperty__String_String();

		/**
		 * The meta object literal for the '{@link org.gecko.artifacts.impl.P2RepositoryImpl <em>P2 Repository</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.artifacts.impl.P2RepositoryImpl
		 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getP2Repository()
		 * @generated
		 */
		EClass P2_REPOSITORY = eINSTANCE.getP2Repository();

		/**
		 * The meta object literal for the '<em><b>P2 Repository Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute P2_REPOSITORY__P2_REPOSITORY_TYPE = eINSTANCE.getP2Repository_P2RepositoryType();

		/**
		 * The meta object literal for the '<em><b>Metadata Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute P2_REPOSITORY__METADATA_LOCATION = eINSTANCE.getP2Repository_MetadataLocation();

		/**
		 * The meta object literal for the '<em><b>Artifact Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute P2_REPOSITORY__ARTIFACT_LOCATION = eINSTANCE.getP2Repository_ArtifactLocation();

		/**
		 * The meta object literal for the '<em><b>Index Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute P2_REPOSITORY__INDEX_LOCATION = eINSTANCE.getP2Repository_IndexLocation();

		/**
		 * The meta object literal for the '<em><b>Composite Repository</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute P2_REPOSITORY__COMPOSITE_REPOSITORY = eINSTANCE.getP2Repository_CompositeRepository();

		/**
		 * The meta object literal for the '<em><b>Rules</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference P2_REPOSITORY__RULES = eINSTANCE.getP2Repository_Rules();

		/**
		 * The meta object literal for the '<em><b>Eclipse Features</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference P2_REPOSITORY__ECLIPSE_FEATURES = eINSTANCE.getP2Repository_EclipseFeatures();

		/**
		 * The meta object literal for the '<em><b>Normalize Uri</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation P2_REPOSITORY___NORMALIZE_URI = eINSTANCE.getP2Repository__NormalizeUri();

		/**
		 * The meta object literal for the '{@link org.gecko.artifacts.impl.BundleArtifactImpl <em>Bundle Artifact</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.artifacts.impl.BundleArtifactImpl
		 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getBundleArtifact()
		 * @generated
		 */
		EClass BUNDLE_ARTIFACT = eINSTANCE.getBundleArtifact();

		/**
		 * The meta object literal for the '{@link org.gecko.artifacts.impl.BinaryArtifactImpl <em>Binary Artifact</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.artifacts.impl.BinaryArtifactImpl
		 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getBinaryArtifact()
		 * @generated
		 */
		EClass BINARY_ARTIFACT = eINSTANCE.getBinaryArtifact();

		/**
		 * The meta object literal for the '{@link org.gecko.artifacts.impl.EclipseFeatureImpl <em>Eclipse Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.artifacts.impl.EclipseFeatureImpl
		 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getEclipseFeature()
		 * @generated
		 */
		EClass ECLIPSE_FEATURE = eINSTANCE.getEclipseFeature();

		/**
		 * The meta object literal for the '<em><b>Jar</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ECLIPSE_FEATURE__JAR = eINSTANCE.getEclipseFeature_Jar();

		/**
		 * The meta object literal for the '<em><b>Packed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ECLIPSE_FEATURE__PACKED = eINSTANCE.getEclipseFeature_Packed();

		/**
		 * The meta object literal for the '<em><b>Property Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ECLIPSE_FEATURE__PROPERTY_LOCATION = eINSTANCE.getEclipseFeature_PropertyLocation();

		/**
		 * The meta object literal for the '<em><b>Feature Properties</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ECLIPSE_FEATURE__FEATURE_PROPERTIES = eINSTANCE.getEclipseFeature_FeatureProperties();

		/**
		 * The meta object literal for the '<em><b>Image</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ECLIPSE_FEATURE__IMAGE = eINSTANCE.getEclipseFeature_Image();

		/**
		 * The meta object literal for the '<em><b>Provider</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ECLIPSE_FEATURE__PROVIDER = eINSTANCE.getEclipseFeature_Provider();

		/**
		 * The meta object literal for the '{@link org.gecko.artifacts.impl.FilterRuleImpl <em>Filter Rule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.artifacts.impl.FilterRuleImpl
		 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getFilterRule()
		 * @generated
		 */
		EClass FILTER_RULE = eINSTANCE.getFilterRule();

		/**
		 * The meta object literal for the '<em><b>Filter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FILTER_RULE__FILTER = eINSTANCE.getFilterRule_Filter();

		/**
		 * The meta object literal for the '<em><b>Rule</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FILTER_RULE__RULE = eINSTANCE.getFilterRule_Rule();

		/**
		 * The meta object literal for the '{@link org.gecko.artifacts.DependencyType <em>Dependency Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.artifacts.DependencyType
		 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getDependencyType()
		 * @generated
		 */
		EEnum DEPENDENCY_TYPE = eINSTANCE.getDependencyType();

		/**
		 * The meta object literal for the '{@link org.gecko.artifacts.ReferenceType <em>Reference Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.artifacts.ReferenceType
		 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getReferenceType()
		 * @generated
		 */
		EEnum REFERENCE_TYPE = eINSTANCE.getReferenceType();

		/**
		 * The meta object literal for the '{@link org.gecko.artifacts.RepositoryType <em>Repository Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.artifacts.RepositoryType
		 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getRepositoryType()
		 * @generated
		 */
		EEnum REPOSITORY_TYPE = eINSTANCE.getRepositoryType();

		/**
		 * The meta object literal for the '<em>Version</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getVersion()
		 * @generated
		 */
		EDataType VERSION = eINSTANCE.getVersion();

		/**
		 * The meta object literal for the '<em>EJava Uri</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.net.URI
		 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getEJavaUri()
		 * @generated
		 */
		EDataType EJAVA_URI = eINSTANCE.getEJavaUri();

		/**
		 * The meta object literal for the '<em>EProperties</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.Properties
		 * @see org.gecko.artifacts.impl.ArtifactsPackageImpl#getEProperties()
		 * @generated
		 */
		EDataType EPROPERTIES = eINSTANCE.getEProperties();

	}

} //ArtifactsPackage
