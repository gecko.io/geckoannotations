/**
 */
package org.gecko.artifacts;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Repository Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.gecko.artifacts.ArtifactsPackage#getRepositoryType()
 * @model
 * @generated
 */
public enum RepositoryType implements Enumerator {
	/**
	 * The '<em><b>OTHER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OTHER_VALUE
	 * @generated
	 * @ordered
	 */
	OTHER(100, "OTHER", "OTHER"),

	/**
	 * The '<em><b>OSGI</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OSGI_VALUE
	 * @generated
	 * @ordered
	 */
	OSGI(0, "OSGI", "OSGI"),

	/**
	 * The '<em><b>MAVEN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MAVEN_VALUE
	 * @generated
	 * @ordered
	 */
	MAVEN(1, "MAVEN", "MAVEN"),

	/**
	 * The '<em><b>P2</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #P2_VALUE
	 * @generated
	 * @ordered
	 */
	P2(2, "P2", "P2"),

	/**
	 * The '<em><b>NPM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NPM_VALUE
	 * @generated
	 * @ordered
	 */
	NPM(3, "NPM", "NPM"),

	/**
	 * The '<em><b>DOCKER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DOCKER_VALUE
	 * @generated
	 * @ordered
	 */
	DOCKER(4, "DOCKER", "DOCKER"),

	/**
	 * The '<em><b>BND</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BND_VALUE
	 * @generated
	 * @ordered
	 */
	BND(5, "BND", "BND");

	/**
	 * The '<em><b>OTHER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OTHER
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int OTHER_VALUE = 100;

	/**
	 * The '<em><b>OSGI</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OSGI
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int OSGI_VALUE = 0;

	/**
	 * The '<em><b>MAVEN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MAVEN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MAVEN_VALUE = 1;

	/**
	 * The '<em><b>P2</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #P2
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int P2_VALUE = 2;

	/**
	 * The '<em><b>NPM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NPM
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NPM_VALUE = 3;

	/**
	 * The '<em><b>DOCKER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DOCKER
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DOCKER_VALUE = 4;

	/**
	 * The '<em><b>BND</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BND
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BND_VALUE = 5;

	/**
	 * An array of all the '<em><b>Repository Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final RepositoryType[] VALUES_ARRAY =
		new RepositoryType[] {
			OTHER,
			OSGI,
			MAVEN,
			P2,
			NPM,
			DOCKER,
			BND,
		};

	/**
	 * A public read-only list of all the '<em><b>Repository Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<RepositoryType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Repository Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static RepositoryType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			RepositoryType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Repository Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static RepositoryType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			RepositoryType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Repository Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static RepositoryType get(int value) {
		switch (value) {
			case OTHER_VALUE: return OTHER;
			case OSGI_VALUE: return OSGI;
			case MAVEN_VALUE: return MAVEN;
			case P2_VALUE: return P2;
			case NPM_VALUE: return NPM;
			case DOCKER_VALUE: return DOCKER;
			case BND_VALUE: return BND;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private RepositoryType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //RepositoryType
