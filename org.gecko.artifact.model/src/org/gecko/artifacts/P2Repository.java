/**
 */
package org.gecko.artifacts;

import java.net.URI;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>P2 Repository</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.artifacts.P2Repository#getP2RepositoryType <em>P2 Repository Type</em>}</li>
 *   <li>{@link org.gecko.artifacts.P2Repository#getMetadataLocation <em>Metadata Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.P2Repository#getArtifactLocation <em>Artifact Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.P2Repository#getIndexLocation <em>Index Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.P2Repository#isCompositeRepository <em>Composite Repository</em>}</li>
 *   <li>{@link org.gecko.artifacts.P2Repository#getRules <em>Rules</em>}</li>
 *   <li>{@link org.gecko.artifacts.P2Repository#getEclipseFeatures <em>Eclipse Features</em>}</li>
 * </ul>
 *
 * @see org.gecko.artifacts.ArtifactsPackage#getP2Repository()
 * @model
 * @generated
 */
public interface P2Repository extends ArtifactRepository {
	/**
	 * Returns the value of the '<em><b>P2 Repository Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>P2 Repository Type</em>' attribute.
	 * @see #setP2RepositoryType(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getP2Repository_P2RepositoryType()
	 * @model
	 * @generated
	 */
	String getP2RepositoryType();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.P2Repository#getP2RepositoryType <em>P2 Repository Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>P2 Repository Type</em>' attribute.
	 * @see #getP2RepositoryType()
	 * @generated
	 */
	void setP2RepositoryType(String value);

	/**
	 * Returns the value of the '<em><b>Metadata Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metadata Location</em>' attribute.
	 * @see #setMetadataLocation(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getP2Repository_MetadataLocation()
	 * @model
	 * @generated
	 */
	String getMetadataLocation();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.P2Repository#getMetadataLocation <em>Metadata Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metadata Location</em>' attribute.
	 * @see #getMetadataLocation()
	 * @generated
	 */
	void setMetadataLocation(String value);

	/**
	 * Returns the value of the '<em><b>Artifact Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Artifact Location</em>' attribute.
	 * @see #setArtifactLocation(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getP2Repository_ArtifactLocation()
	 * @model
	 * @generated
	 */
	String getArtifactLocation();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.P2Repository#getArtifactLocation <em>Artifact Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Artifact Location</em>' attribute.
	 * @see #getArtifactLocation()
	 * @generated
	 */
	void setArtifactLocation(String value);

	/**
	 * Returns the value of the '<em><b>Index Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index Location</em>' attribute.
	 * @see #setIndexLocation(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getP2Repository_IndexLocation()
	 * @model
	 * @generated
	 */
	String getIndexLocation();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.P2Repository#getIndexLocation <em>Index Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Index Location</em>' attribute.
	 * @see #getIndexLocation()
	 * @generated
	 */
	void setIndexLocation(String value);

	/**
	 * Returns the value of the '<em><b>Composite Repository</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composite Repository</em>' attribute.
	 * @see org.gecko.artifacts.ArtifactsPackage#getP2Repository_CompositeRepository()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	boolean isCompositeRepository();

	/**
	 * Returns the value of the '<em><b>Rules</b></em>' containment reference list.
	 * The list contents are of type {@link org.gecko.artifacts.FilterRule}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rules</em>' containment reference list.
	 * @see org.gecko.artifacts.ArtifactsPackage#getP2Repository_Rules()
	 * @model containment="true"
	 * @generated
	 */
	EList<FilterRule> getRules();

	/**
	 * Returns the value of the '<em><b>Eclipse Features</b></em>' reference list.
	 * The list contents are of type {@link org.gecko.artifacts.EclipseFeature}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Eclipse Features</em>' reference list.
	 * @see org.gecko.artifacts.ArtifactsPackage#getP2Repository_EclipseFeatures()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<EclipseFeature> getEclipseFeatures();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.gecko.artifacts.EJavaUri"
	 * @generated
	 */
	URI normalizeUri();

} // P2Repository
