/**
 */
package org.gecko.artifacts.impl;

import java.util.Properties;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.gecko.artifacts.ArtifactsPackage;
import org.gecko.artifacts.EclipseFeature;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Eclipse Feature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.artifacts.impl.EclipseFeatureImpl#isJar <em>Jar</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.EclipseFeatureImpl#isPacked <em>Packed</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.EclipseFeatureImpl#getPropertyLocation <em>Property Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.EclipseFeatureImpl#getFeatureProperties <em>Feature Properties</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.EclipseFeatureImpl#getImage <em>Image</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.EclipseFeatureImpl#getProvider <em>Provider</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EclipseFeatureImpl extends ArtifactImpl implements EclipseFeature {
	/**
	 * The default value of the '{@link #isJar() <em>Jar</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isJar()
	 * @generated
	 * @ordered
	 */
	protected static final boolean JAR_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isJar() <em>Jar</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isJar()
	 * @generated
	 * @ordered
	 */
	protected boolean jar = JAR_EDEFAULT;
	/**
	 * The default value of the '{@link #isPacked() <em>Packed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPacked()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PACKED_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isPacked() <em>Packed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPacked()
	 * @generated
	 * @ordered
	 */
	protected boolean packed = PACKED_EDEFAULT;

	/**
	 * The default value of the '{@link #getPropertyLocation() <em>Property Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String PROPERTY_LOCATION_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getPropertyLocation() <em>Property Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyLocation()
	 * @generated
	 * @ordered
	 */
	protected String propertyLocation = PROPERTY_LOCATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getFeatureProperties() <em>Feature Properties</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureProperties()
	 * @generated
	 * @ordered
	 */
	protected static final Properties FEATURE_PROPERTIES_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getFeatureProperties() <em>Feature Properties</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureProperties()
	 * @generated
	 * @ordered
	 */
	protected Properties featureProperties = FEATURE_PROPERTIES_EDEFAULT;

	/**
	 * The default value of the '{@link #getImage() <em>Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImage()
	 * @generated
	 * @ordered
	 */
	protected static final String IMAGE_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getImage() <em>Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImage()
	 * @generated
	 * @ordered
	 */
	protected String image = IMAGE_EDEFAULT;
	/**
	 * The default value of the '{@link #getProvider() <em>Provider</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvider()
	 * @generated
	 * @ordered
	 */
	protected static final String PROVIDER_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getProvider() <em>Provider</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvider()
	 * @generated
	 * @ordered
	 */
	protected String provider = PROVIDER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EclipseFeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArtifactsPackage.Literals.ECLIPSE_FEATURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isJar() {
		return jar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setJar(boolean newJar) {
		boolean oldJar = jar;
		jar = newJar;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ECLIPSE_FEATURE__JAR, oldJar, jar));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isPacked() {
		return packed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPacked(boolean newPacked) {
		boolean oldPacked = packed;
		packed = newPacked;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ECLIPSE_FEATURE__PACKED, oldPacked, packed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPropertyLocation() {
		return propertyLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPropertyLocation(String newPropertyLocation) {
		String oldPropertyLocation = propertyLocation;
		propertyLocation = newPropertyLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ECLIPSE_FEATURE__PROPERTY_LOCATION, oldPropertyLocation, propertyLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Properties getFeatureProperties() {
		return featureProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFeatureProperties(Properties newFeatureProperties) {
		Properties oldFeatureProperties = featureProperties;
		featureProperties = newFeatureProperties;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ECLIPSE_FEATURE__FEATURE_PROPERTIES, oldFeatureProperties, featureProperties));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getImage() {
		return image;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setImage(String newImage) {
		String oldImage = image;
		image = newImage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ECLIPSE_FEATURE__IMAGE, oldImage, image));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getProvider() {
		return provider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProvider(String newProvider) {
		String oldProvider = provider;
		provider = newProvider;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ECLIPSE_FEATURE__PROVIDER, oldProvider, provider));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArtifactsPackage.ECLIPSE_FEATURE__JAR:
				return isJar();
			case ArtifactsPackage.ECLIPSE_FEATURE__PACKED:
				return isPacked();
			case ArtifactsPackage.ECLIPSE_FEATURE__PROPERTY_LOCATION:
				return getPropertyLocation();
			case ArtifactsPackage.ECLIPSE_FEATURE__FEATURE_PROPERTIES:
				return getFeatureProperties();
			case ArtifactsPackage.ECLIPSE_FEATURE__IMAGE:
				return getImage();
			case ArtifactsPackage.ECLIPSE_FEATURE__PROVIDER:
				return getProvider();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArtifactsPackage.ECLIPSE_FEATURE__JAR:
				setJar((Boolean)newValue);
				return;
			case ArtifactsPackage.ECLIPSE_FEATURE__PACKED:
				setPacked((Boolean)newValue);
				return;
			case ArtifactsPackage.ECLIPSE_FEATURE__PROPERTY_LOCATION:
				setPropertyLocation((String)newValue);
				return;
			case ArtifactsPackage.ECLIPSE_FEATURE__FEATURE_PROPERTIES:
				setFeatureProperties((Properties)newValue);
				return;
			case ArtifactsPackage.ECLIPSE_FEATURE__IMAGE:
				setImage((String)newValue);
				return;
			case ArtifactsPackage.ECLIPSE_FEATURE__PROVIDER:
				setProvider((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArtifactsPackage.ECLIPSE_FEATURE__JAR:
				setJar(JAR_EDEFAULT);
				return;
			case ArtifactsPackage.ECLIPSE_FEATURE__PACKED:
				setPacked(PACKED_EDEFAULT);
				return;
			case ArtifactsPackage.ECLIPSE_FEATURE__PROPERTY_LOCATION:
				setPropertyLocation(PROPERTY_LOCATION_EDEFAULT);
				return;
			case ArtifactsPackage.ECLIPSE_FEATURE__FEATURE_PROPERTIES:
				setFeatureProperties(FEATURE_PROPERTIES_EDEFAULT);
				return;
			case ArtifactsPackage.ECLIPSE_FEATURE__IMAGE:
				setImage(IMAGE_EDEFAULT);
				return;
			case ArtifactsPackage.ECLIPSE_FEATURE__PROVIDER:
				setProvider(PROVIDER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArtifactsPackage.ECLIPSE_FEATURE__JAR:
				return jar != JAR_EDEFAULT;
			case ArtifactsPackage.ECLIPSE_FEATURE__PACKED:
				return packed != PACKED_EDEFAULT;
			case ArtifactsPackage.ECLIPSE_FEATURE__PROPERTY_LOCATION:
				return PROPERTY_LOCATION_EDEFAULT == null ? propertyLocation != null : !PROPERTY_LOCATION_EDEFAULT.equals(propertyLocation);
			case ArtifactsPackage.ECLIPSE_FEATURE__FEATURE_PROPERTIES:
				return FEATURE_PROPERTIES_EDEFAULT == null ? featureProperties != null : !FEATURE_PROPERTIES_EDEFAULT.equals(featureProperties);
			case ArtifactsPackage.ECLIPSE_FEATURE__IMAGE:
				return IMAGE_EDEFAULT == null ? image != null : !IMAGE_EDEFAULT.equals(image);
			case ArtifactsPackage.ECLIPSE_FEATURE__PROVIDER:
				return PROVIDER_EDEFAULT == null ? provider != null : !PROVIDER_EDEFAULT.equals(provider);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (jar: ");
		result.append(jar);
		result.append(", packed: ");
		result.append(packed);
		result.append(", propertyLocation: ");
		result.append(propertyLocation);
		result.append(", featureProperties: ");
		result.append(featureProperties);
		result.append(", image: ");
		result.append(image);
		result.append(", provider: ");
		result.append(provider);
		result.append(')');
		return result.toString();
	}

} //EclipseFeatureImpl
