/**
 */
package org.gecko.artifacts.impl;

import java.lang.reflect.InvocationTargetException;

import java.net.URI;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.gecko.artifacts.ArtifactsPackage;
import org.gecko.artifacts.EclipseFeature;
import org.gecko.artifacts.FilterRule;
import org.gecko.artifacts.P2Repository;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>P2 Repository</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.artifacts.impl.P2RepositoryImpl#getP2RepositoryType <em>P2 Repository Type</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.P2RepositoryImpl#getMetadataLocation <em>Metadata Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.P2RepositoryImpl#getArtifactLocation <em>Artifact Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.P2RepositoryImpl#getIndexLocation <em>Index Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.P2RepositoryImpl#isCompositeRepository <em>Composite Repository</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.P2RepositoryImpl#getRules <em>Rules</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.P2RepositoryImpl#getEclipseFeatures <em>Eclipse Features</em>}</li>
 * </ul>
 *
 * @generated
 */
public class P2RepositoryImpl extends ArtifactRepositoryImpl implements P2Repository {
	/**
	 * The default value of the '{@link #getP2RepositoryType() <em>P2 Repository Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getP2RepositoryType()
	 * @generated
	 * @ordered
	 */
	protected static final String P2_REPOSITORY_TYPE_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getP2RepositoryType() <em>P2 Repository Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getP2RepositoryType()
	 * @generated
	 * @ordered
	 */
	protected String p2RepositoryType = P2_REPOSITORY_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMetadataLocation() <em>Metadata Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetadataLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String METADATA_LOCATION_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getMetadataLocation() <em>Metadata Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetadataLocation()
	 * @generated
	 * @ordered
	 */
	protected String metadataLocation = METADATA_LOCATION_EDEFAULT;
	/**
	 * The default value of the '{@link #getArtifactLocation() <em>Artifact Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArtifactLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String ARTIFACT_LOCATION_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getArtifactLocation() <em>Artifact Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArtifactLocation()
	 * @generated
	 * @ordered
	 */
	protected String artifactLocation = ARTIFACT_LOCATION_EDEFAULT;
	/**
	 * The default value of the '{@link #getIndexLocation() <em>Index Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String INDEX_LOCATION_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getIndexLocation() <em>Index Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexLocation()
	 * @generated
	 * @ordered
	 */
	protected String indexLocation = INDEX_LOCATION_EDEFAULT;

	/**
	 * The default value of the '{@link #isCompositeRepository() <em>Composite Repository</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCompositeRepository()
	 * @generated
	 * @ordered
	 */
	protected static final boolean COMPOSITE_REPOSITORY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #getRules() <em>Rules</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRules()
	 * @generated
	 * @ordered
	 */
	protected EList<FilterRule> rules;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected P2RepositoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArtifactsPackage.Literals.P2_REPOSITORY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getP2RepositoryType() {
		return p2RepositoryType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setP2RepositoryType(String newP2RepositoryType) {
		String oldP2RepositoryType = p2RepositoryType;
		p2RepositoryType = newP2RepositoryType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.P2_REPOSITORY__P2_REPOSITORY_TYPE, oldP2RepositoryType, p2RepositoryType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMetadataLocation() {
		return metadataLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMetadataLocation(String newMetadataLocation) {
		String oldMetadataLocation = metadataLocation;
		metadataLocation = newMetadataLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.P2_REPOSITORY__METADATA_LOCATION, oldMetadataLocation, metadataLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getArtifactLocation() {
		return artifactLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setArtifactLocation(String newArtifactLocation) {
		String oldArtifactLocation = artifactLocation;
		artifactLocation = newArtifactLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.P2_REPOSITORY__ARTIFACT_LOCATION, oldArtifactLocation, artifactLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIndexLocation() {
		return indexLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIndexLocation(String newIndexLocation) {
		String oldIndexLocation = indexLocation;
		indexLocation = newIndexLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.P2_REPOSITORY__INDEX_LOCATION, oldIndexLocation, indexLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isCompositeRepository() {
		return "org.eclipse.equinox.internal.p2.artifact.repository.CompositeArtifactRepository".equals(getP2RepositoryType());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<FilterRule> getRules() {
		if (rules == null) {
			rules = new EObjectContainmentEList<FilterRule>(FilterRule.class, this, ArtifactsPackage.P2_REPOSITORY__RULES);
		}
		return rules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<EclipseFeature> getEclipseFeatures() {
		List<EclipseFeature> features =  getAllArtifacts().stream()
												.filter(a->(a instanceof EclipseFeature))
												.map(a->(EclipseFeature)a)
												.collect(Collectors.toList());
		return ECollections.asEList(features);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public URI normalizeUri() {
		URI base = getLocationUri();
		String path = base.getPath();
		if (path == null || path.endsWith("/") || path.endsWith(".xml") || path.endsWith(".xml.xz") || path.endsWith("p2.index"))
			return base;
		setLocation(base.toString() + "/");
		return getLocationUri();
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ArtifactsPackage.P2_REPOSITORY__RULES:
				return ((InternalEList<?>)getRules()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArtifactsPackage.P2_REPOSITORY__P2_REPOSITORY_TYPE:
				return getP2RepositoryType();
			case ArtifactsPackage.P2_REPOSITORY__METADATA_LOCATION:
				return getMetadataLocation();
			case ArtifactsPackage.P2_REPOSITORY__ARTIFACT_LOCATION:
				return getArtifactLocation();
			case ArtifactsPackage.P2_REPOSITORY__INDEX_LOCATION:
				return getIndexLocation();
			case ArtifactsPackage.P2_REPOSITORY__COMPOSITE_REPOSITORY:
				return isCompositeRepository();
			case ArtifactsPackage.P2_REPOSITORY__RULES:
				return getRules();
			case ArtifactsPackage.P2_REPOSITORY__ECLIPSE_FEATURES:
				return getEclipseFeatures();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArtifactsPackage.P2_REPOSITORY__P2_REPOSITORY_TYPE:
				setP2RepositoryType((String)newValue);
				return;
			case ArtifactsPackage.P2_REPOSITORY__METADATA_LOCATION:
				setMetadataLocation((String)newValue);
				return;
			case ArtifactsPackage.P2_REPOSITORY__ARTIFACT_LOCATION:
				setArtifactLocation((String)newValue);
				return;
			case ArtifactsPackage.P2_REPOSITORY__INDEX_LOCATION:
				setIndexLocation((String)newValue);
				return;
			case ArtifactsPackage.P2_REPOSITORY__RULES:
				getRules().clear();
				getRules().addAll((Collection<? extends FilterRule>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArtifactsPackage.P2_REPOSITORY__P2_REPOSITORY_TYPE:
				setP2RepositoryType(P2_REPOSITORY_TYPE_EDEFAULT);
				return;
			case ArtifactsPackage.P2_REPOSITORY__METADATA_LOCATION:
				setMetadataLocation(METADATA_LOCATION_EDEFAULT);
				return;
			case ArtifactsPackage.P2_REPOSITORY__ARTIFACT_LOCATION:
				setArtifactLocation(ARTIFACT_LOCATION_EDEFAULT);
				return;
			case ArtifactsPackage.P2_REPOSITORY__INDEX_LOCATION:
				setIndexLocation(INDEX_LOCATION_EDEFAULT);
				return;
			case ArtifactsPackage.P2_REPOSITORY__RULES:
				getRules().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArtifactsPackage.P2_REPOSITORY__P2_REPOSITORY_TYPE:
				return P2_REPOSITORY_TYPE_EDEFAULT == null ? p2RepositoryType != null : !P2_REPOSITORY_TYPE_EDEFAULT.equals(p2RepositoryType);
			case ArtifactsPackage.P2_REPOSITORY__METADATA_LOCATION:
				return METADATA_LOCATION_EDEFAULT == null ? metadataLocation != null : !METADATA_LOCATION_EDEFAULT.equals(metadataLocation);
			case ArtifactsPackage.P2_REPOSITORY__ARTIFACT_LOCATION:
				return ARTIFACT_LOCATION_EDEFAULT == null ? artifactLocation != null : !ARTIFACT_LOCATION_EDEFAULT.equals(artifactLocation);
			case ArtifactsPackage.P2_REPOSITORY__INDEX_LOCATION:
				return INDEX_LOCATION_EDEFAULT == null ? indexLocation != null : !INDEX_LOCATION_EDEFAULT.equals(indexLocation);
			case ArtifactsPackage.P2_REPOSITORY__COMPOSITE_REPOSITORY:
				return isCompositeRepository() != COMPOSITE_REPOSITORY_EDEFAULT;
			case ArtifactsPackage.P2_REPOSITORY__RULES:
				return rules != null && !rules.isEmpty();
			case ArtifactsPackage.P2_REPOSITORY__ECLIPSE_FEATURES:
				return !getEclipseFeatures().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ArtifactsPackage.P2_REPOSITORY___NORMALIZE_URI:
				return normalizeUri();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (p2RepositoryType: ");
		result.append(p2RepositoryType);
		result.append(", metadataLocation: ");
		result.append(metadataLocation);
		result.append(", artifactLocation: ");
		result.append(artifactLocation);
		result.append(", indexLocation: ");
		result.append(indexLocation);
		result.append(')');
		return result.toString();
	}

} //P2RepositoryImpl
