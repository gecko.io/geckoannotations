/**
 */
package org.gecko.artifacts.impl;

import org.eclipse.emf.ecore.EClass;

import org.gecko.artifacts.ArtifactsPackage;
import org.gecko.artifacts.BundleArtifact;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bundle Artifact</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BundleArtifactImpl extends ArtifactImpl implements BundleArtifact {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BundleArtifactImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArtifactsPackage.Literals.BUNDLE_ARTIFACT;
	}

} //BundleArtifactImpl
