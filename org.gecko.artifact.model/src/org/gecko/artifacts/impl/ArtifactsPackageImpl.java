/**
 */
package org.gecko.artifacts.impl;

import java.net.URI;
import java.util.Properties;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.gecko.artifacts.Artifact;
import org.gecko.artifacts.ArtifactReference;
import org.gecko.artifacts.ArtifactRepository;
import org.gecko.artifacts.ArtifactsFactory;
import org.gecko.artifacts.ArtifactsPackage;
import org.gecko.artifacts.BinaryArtifact;
import org.gecko.artifacts.BundleArtifact;
import org.gecko.artifacts.Dependency;
import org.gecko.artifacts.DependencyType;
import org.gecko.artifacts.EclipseFeature;
import org.gecko.artifacts.FilterRule;
import org.gecko.artifacts.OSGiBundle;

import org.gecko.artifacts.P2Repository;
import org.gecko.artifacts.Property;
import org.gecko.artifacts.ReferenceType;
import org.gecko.artifacts.RepositoryType;
import org.gecko.artifacts.util.ArtifactsValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ArtifactsPackageImpl extends EPackageImpl implements ArtifactsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass osGiBundleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass artifactEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass artifactRepositoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass artifactReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass p2RepositoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bundleArtifactEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass binaryArtifactEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eclipseFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass filterRuleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dependencyTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum referenceTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum repositoryTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType versionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType eJavaUriEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType ePropertiesEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.gecko.artifacts.ArtifactsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ArtifactsPackageImpl() {
		super(eNS_URI, ArtifactsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ArtifactsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ArtifactsPackage init() {
		if (isInited) return (ArtifactsPackage)EPackage.Registry.INSTANCE.getEPackage(ArtifactsPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredArtifactsPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ArtifactsPackageImpl theArtifactsPackage = registeredArtifactsPackage instanceof ArtifactsPackageImpl ? (ArtifactsPackageImpl)registeredArtifactsPackage : new ArtifactsPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theArtifactsPackage.createPackageContents();

		// Initialize created meta-data
		theArtifactsPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theArtifactsPackage,
			 new EValidator.Descriptor() {
				 @Override
				 public EValidator getEValidator() {
					 return ArtifactsValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theArtifactsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ArtifactsPackage.eNS_URI, theArtifactsPackage);
		return theArtifactsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOSGiBundle() {
		return osGiBundleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOSGiBundle_Bsn() {
		return (EAttribute)osGiBundleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOSGiBundle_SourceLocation() {
		return (EAttribute)osGiBundleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOSGiBundle_CacheLocation() {
		return (EAttribute)osGiBundleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOSGiBundle_Name() {
		return (EAttribute)osGiBundleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOSGiBundle_Vendor() {
		return (EAttribute)osGiBundleEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOSGiBundle_Version() {
		return (EAttribute)osGiBundleEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOSGiBundle_PolicyLazy() {
		return (EAttribute)osGiBundleEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOSGiBundle_ExecutionEnvironment() {
		return (EAttribute)osGiBundleEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOSGiBundle_Classpath() {
		return (EAttribute)osGiBundleEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOSGiBundle_Localization() {
		return (EAttribute)osGiBundleEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getOSGiBundle_Dependency() {
		return (EReference)osGiBundleEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOSGiBundle_Properties() {
		return (EAttribute)osGiBundleEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getOSGiBundle_BundleProperty() {
		return (EReference)osGiBundleEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOSGiBundle_License() {
		return (EAttribute)osGiBundleEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOSGiBundle_Copyright() {
		return (EAttribute)osGiBundleEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOSGiBundle_FragmentHost() {
		return (EAttribute)osGiBundleEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOSGiBundle_Fragment() {
		return (EAttribute)osGiBundleEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDependency() {
		return dependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDependency_Name() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDependency_Type() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDependency_Source() {
		return (EReference)dependencyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDependency_Target() {
		return (EReference)dependencyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDependency_Version() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDependency_Visibility() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDependency_Optional() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDependency_Location() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDependency_Reexport() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDependency_Object() {
		return (EReference)dependencyEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDependency_Filter() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDependency_Multiple() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDependency_Active() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDependency_DependencyProperty() {
		return (EReference)dependencyEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDependency_Properties() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDependency_ObjectClass() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDependency_Id() {
		return (EAttribute)dependencyEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getProperty() {
		return propertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getProperty_Key() {
		return (EAttribute)propertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getProperty_Value() {
		return (EAttribute)propertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getArtifact() {
		return artifactEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifact_Id() {
		return (EAttribute)artifactEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifact_Name() {
		return (EAttribute)artifactEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifact_Label() {
		return (EAttribute)artifactEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifact_Version() {
		return (EAttribute)artifactEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifact_SourceLocation() {
		return (EAttribute)artifactEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifact_CacheLocation() {
		return (EAttribute)artifactEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArtifact_Repository() {
		return (EReference)artifactEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifact_License() {
		return (EAttribute)artifactEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifact_Copyright() {
		return (EAttribute)artifactEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifact_Properties() {
		return (EAttribute)artifactEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArtifact_ArtifactProperties() {
		return (EReference)artifactEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifact_Size() {
		return (EAttribute)artifactEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifact_Hash() {
		return (EAttribute)artifactEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifact_Timestamp() {
		return (EAttribute)artifactEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifact_Description() {
		return (EAttribute)artifactEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArtifact_References() {
		return (EReference)artifactEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getArtifact__PutProperty__String_String() {
		return artifactEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getArtifactRepository() {
		return artifactRepositoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactRepository_Id() {
		return (EAttribute)artifactRepositoryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactRepository_Name() {
		return (EAttribute)artifactRepositoryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactRepository_Version() {
		return (EAttribute)artifactRepositoryEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactRepository_Type() {
		return (EAttribute)artifactRepositoryEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactRepository_Location() {
		return (EAttribute)artifactRepositoryEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArtifactRepository_Artifacts() {
		return (EReference)artifactRepositoryEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArtifactRepository_Repositories() {
		return (EReference)artifactRepositoryEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArtifactRepository_Parent() {
		return (EReference)artifactRepositoryEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactRepository_Properties() {
		return (EAttribute)artifactRepositoryEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArtifactRepository_RepositoryProperties() {
		return (EReference)artifactRepositoryEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactRepository_LocationUri() {
		return (EAttribute)artifactRepositoryEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactRepository_Timestamp() {
		return (EAttribute)artifactRepositoryEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArtifactRepository_AllArtifacts() {
		return (EReference)artifactRepositoryEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactRepository_Loaded() {
		return (EAttribute)artifactRepositoryEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getArtifactRepository__GetArtifactById__String() {
		return artifactRepositoryEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getArtifactRepository__GetArtifactsById__String() {
		return artifactRepositoryEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getArtifactRepository__GetArtifactsByName__String() {
		return artifactRepositoryEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getArtifactRepository__GetAllArtifactsById__String() {
		return artifactRepositoryEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getArtifactReference() {
		return artifactReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactReference_Id() {
		return (EAttribute)artifactReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactReference_Version() {
		return (EAttribute)artifactReferenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactReference_Name() {
		return (EAttribute)artifactReferenceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactReference_Label() {
		return (EAttribute)artifactReferenceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactReference_Optional() {
		return (EAttribute)artifactReferenceEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactReference_Location() {
		return (EAttribute)artifactReferenceEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactReference_Filter() {
		return (EAttribute)artifactReferenceEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactReference_Type() {
		return (EAttribute)artifactReferenceEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArtifactReference_RefObject() {
		return (EReference)artifactReferenceEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArtifactReference_Source() {
		return (EReference)artifactReferenceEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactReference_SourceName() {
		return (EAttribute)artifactReferenceEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactReference_SourceVersion() {
		return (EAttribute)artifactReferenceEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArtifactReference_Target() {
		return (EReference)artifactReferenceEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactReference_TargetName() {
		return (EAttribute)artifactReferenceEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactReference_TargetVersion() {
		return (EAttribute)artifactReferenceEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifactReference_Properties() {
		return (EAttribute)artifactReferenceEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArtifactReference_ReferenceProperties() {
		return (EReference)artifactReferenceEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getArtifactReference__PutProperty__String_String() {
		return artifactReferenceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getP2Repository() {
		return p2RepositoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getP2Repository_P2RepositoryType() {
		return (EAttribute)p2RepositoryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getP2Repository_MetadataLocation() {
		return (EAttribute)p2RepositoryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getP2Repository_ArtifactLocation() {
		return (EAttribute)p2RepositoryEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getP2Repository_IndexLocation() {
		return (EAttribute)p2RepositoryEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getP2Repository_CompositeRepository() {
		return (EAttribute)p2RepositoryEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getP2Repository_Rules() {
		return (EReference)p2RepositoryEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getP2Repository_EclipseFeatures() {
		return (EReference)p2RepositoryEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getP2Repository__NormalizeUri() {
		return p2RepositoryEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBundleArtifact() {
		return bundleArtifactEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBinaryArtifact() {
		return binaryArtifactEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEclipseFeature() {
		return eclipseFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEclipseFeature_Jar() {
		return (EAttribute)eclipseFeatureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEclipseFeature_Packed() {
		return (EAttribute)eclipseFeatureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEclipseFeature_PropertyLocation() {
		return (EAttribute)eclipseFeatureEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEclipseFeature_FeatureProperties() {
		return (EAttribute)eclipseFeatureEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEclipseFeature_Image() {
		return (EAttribute)eclipseFeatureEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEclipseFeature_Provider() {
		return (EAttribute)eclipseFeatureEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFilterRule() {
		return filterRuleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFilterRule_Filter() {
		return (EAttribute)filterRuleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFilterRule_Rule() {
		return (EAttribute)filterRuleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getDependencyType() {
		return dependencyTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getReferenceType() {
		return referenceTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getRepositoryType() {
		return repositoryTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getVersion() {
		return versionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getEJavaUri() {
		return eJavaUriEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getEProperties() {
		return ePropertiesEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ArtifactsFactory getArtifactsFactory() {
		return (ArtifactsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		osGiBundleEClass = createEClass(OS_GI_BUNDLE);
		createEAttribute(osGiBundleEClass, OS_GI_BUNDLE__BSN);
		createEAttribute(osGiBundleEClass, OS_GI_BUNDLE__SOURCE_LOCATION);
		createEAttribute(osGiBundleEClass, OS_GI_BUNDLE__CACHE_LOCATION);
		createEAttribute(osGiBundleEClass, OS_GI_BUNDLE__NAME);
		createEAttribute(osGiBundleEClass, OS_GI_BUNDLE__VENDOR);
		createEAttribute(osGiBundleEClass, OS_GI_BUNDLE__VERSION);
		createEAttribute(osGiBundleEClass, OS_GI_BUNDLE__POLICY_LAZY);
		createEAttribute(osGiBundleEClass, OS_GI_BUNDLE__EXECUTION_ENVIRONMENT);
		createEAttribute(osGiBundleEClass, OS_GI_BUNDLE__CLASSPATH);
		createEAttribute(osGiBundleEClass, OS_GI_BUNDLE__LOCALIZATION);
		createEReference(osGiBundleEClass, OS_GI_BUNDLE__DEPENDENCY);
		createEAttribute(osGiBundleEClass, OS_GI_BUNDLE__PROPERTIES);
		createEReference(osGiBundleEClass, OS_GI_BUNDLE__BUNDLE_PROPERTY);
		createEAttribute(osGiBundleEClass, OS_GI_BUNDLE__LICENSE);
		createEAttribute(osGiBundleEClass, OS_GI_BUNDLE__COPYRIGHT);
		createEAttribute(osGiBundleEClass, OS_GI_BUNDLE__FRAGMENT_HOST);
		createEAttribute(osGiBundleEClass, OS_GI_BUNDLE__FRAGMENT);

		dependencyEClass = createEClass(DEPENDENCY);
		createEAttribute(dependencyEClass, DEPENDENCY__NAME);
		createEAttribute(dependencyEClass, DEPENDENCY__TYPE);
		createEReference(dependencyEClass, DEPENDENCY__SOURCE);
		createEReference(dependencyEClass, DEPENDENCY__TARGET);
		createEAttribute(dependencyEClass, DEPENDENCY__VERSION);
		createEAttribute(dependencyEClass, DEPENDENCY__VISIBILITY);
		createEAttribute(dependencyEClass, DEPENDENCY__OPTIONAL);
		createEAttribute(dependencyEClass, DEPENDENCY__LOCATION);
		createEAttribute(dependencyEClass, DEPENDENCY__REEXPORT);
		createEReference(dependencyEClass, DEPENDENCY__OBJECT);
		createEAttribute(dependencyEClass, DEPENDENCY__FILTER);
		createEAttribute(dependencyEClass, DEPENDENCY__MULTIPLE);
		createEAttribute(dependencyEClass, DEPENDENCY__ACTIVE);
		createEReference(dependencyEClass, DEPENDENCY__DEPENDENCY_PROPERTY);
		createEAttribute(dependencyEClass, DEPENDENCY__PROPERTIES);
		createEAttribute(dependencyEClass, DEPENDENCY__OBJECT_CLASS);
		createEAttribute(dependencyEClass, DEPENDENCY__ID);

		propertyEClass = createEClass(PROPERTY);
		createEAttribute(propertyEClass, PROPERTY__KEY);
		createEAttribute(propertyEClass, PROPERTY__VALUE);

		artifactEClass = createEClass(ARTIFACT);
		createEAttribute(artifactEClass, ARTIFACT__ID);
		createEAttribute(artifactEClass, ARTIFACT__NAME);
		createEAttribute(artifactEClass, ARTIFACT__LABEL);
		createEAttribute(artifactEClass, ARTIFACT__DESCRIPTION);
		createEAttribute(artifactEClass, ARTIFACT__VERSION);
		createEAttribute(artifactEClass, ARTIFACT__SOURCE_LOCATION);
		createEAttribute(artifactEClass, ARTIFACT__CACHE_LOCATION);
		createEReference(artifactEClass, ARTIFACT__REPOSITORY);
		createEAttribute(artifactEClass, ARTIFACT__LICENSE);
		createEAttribute(artifactEClass, ARTIFACT__COPYRIGHT);
		createEAttribute(artifactEClass, ARTIFACT__PROPERTIES);
		createEReference(artifactEClass, ARTIFACT__ARTIFACT_PROPERTIES);
		createEAttribute(artifactEClass, ARTIFACT__SIZE);
		createEAttribute(artifactEClass, ARTIFACT__HASH);
		createEAttribute(artifactEClass, ARTIFACT__TIMESTAMP);
		createEReference(artifactEClass, ARTIFACT__REFERENCES);
		createEOperation(artifactEClass, ARTIFACT___PUT_PROPERTY__STRING_STRING);

		artifactRepositoryEClass = createEClass(ARTIFACT_REPOSITORY);
		createEAttribute(artifactRepositoryEClass, ARTIFACT_REPOSITORY__ID);
		createEAttribute(artifactRepositoryEClass, ARTIFACT_REPOSITORY__NAME);
		createEAttribute(artifactRepositoryEClass, ARTIFACT_REPOSITORY__VERSION);
		createEAttribute(artifactRepositoryEClass, ARTIFACT_REPOSITORY__TYPE);
		createEAttribute(artifactRepositoryEClass, ARTIFACT_REPOSITORY__LOCATION);
		createEReference(artifactRepositoryEClass, ARTIFACT_REPOSITORY__ARTIFACTS);
		createEReference(artifactRepositoryEClass, ARTIFACT_REPOSITORY__REPOSITORIES);
		createEReference(artifactRepositoryEClass, ARTIFACT_REPOSITORY__PARENT);
		createEAttribute(artifactRepositoryEClass, ARTIFACT_REPOSITORY__PROPERTIES);
		createEReference(artifactRepositoryEClass, ARTIFACT_REPOSITORY__REPOSITORY_PROPERTIES);
		createEAttribute(artifactRepositoryEClass, ARTIFACT_REPOSITORY__LOCATION_URI);
		createEAttribute(artifactRepositoryEClass, ARTIFACT_REPOSITORY__TIMESTAMP);
		createEReference(artifactRepositoryEClass, ARTIFACT_REPOSITORY__ALL_ARTIFACTS);
		createEAttribute(artifactRepositoryEClass, ARTIFACT_REPOSITORY__LOADED);
		createEOperation(artifactRepositoryEClass, ARTIFACT_REPOSITORY___GET_ARTIFACT_BY_ID__STRING);
		createEOperation(artifactRepositoryEClass, ARTIFACT_REPOSITORY___GET_ARTIFACTS_BY_ID__STRING);
		createEOperation(artifactRepositoryEClass, ARTIFACT_REPOSITORY___GET_ARTIFACTS_BY_NAME__STRING);
		createEOperation(artifactRepositoryEClass, ARTIFACT_REPOSITORY___GET_ALL_ARTIFACTS_BY_ID__STRING);

		artifactReferenceEClass = createEClass(ARTIFACT_REFERENCE);
		createEAttribute(artifactReferenceEClass, ARTIFACT_REFERENCE__ID);
		createEAttribute(artifactReferenceEClass, ARTIFACT_REFERENCE__VERSION);
		createEAttribute(artifactReferenceEClass, ARTIFACT_REFERENCE__NAME);
		createEAttribute(artifactReferenceEClass, ARTIFACT_REFERENCE__LABEL);
		createEAttribute(artifactReferenceEClass, ARTIFACT_REFERENCE__OPTIONAL);
		createEAttribute(artifactReferenceEClass, ARTIFACT_REFERENCE__LOCATION);
		createEAttribute(artifactReferenceEClass, ARTIFACT_REFERENCE__FILTER);
		createEAttribute(artifactReferenceEClass, ARTIFACT_REFERENCE__TYPE);
		createEReference(artifactReferenceEClass, ARTIFACT_REFERENCE__REF_OBJECT);
		createEReference(artifactReferenceEClass, ARTIFACT_REFERENCE__SOURCE);
		createEAttribute(artifactReferenceEClass, ARTIFACT_REFERENCE__SOURCE_NAME);
		createEAttribute(artifactReferenceEClass, ARTIFACT_REFERENCE__SOURCE_VERSION);
		createEReference(artifactReferenceEClass, ARTIFACT_REFERENCE__TARGET);
		createEAttribute(artifactReferenceEClass, ARTIFACT_REFERENCE__TARGET_NAME);
		createEAttribute(artifactReferenceEClass, ARTIFACT_REFERENCE__TARGET_VERSION);
		createEAttribute(artifactReferenceEClass, ARTIFACT_REFERENCE__PROPERTIES);
		createEReference(artifactReferenceEClass, ARTIFACT_REFERENCE__REFERENCE_PROPERTIES);
		createEOperation(artifactReferenceEClass, ARTIFACT_REFERENCE___PUT_PROPERTY__STRING_STRING);

		p2RepositoryEClass = createEClass(P2_REPOSITORY);
		createEAttribute(p2RepositoryEClass, P2_REPOSITORY__P2_REPOSITORY_TYPE);
		createEAttribute(p2RepositoryEClass, P2_REPOSITORY__METADATA_LOCATION);
		createEAttribute(p2RepositoryEClass, P2_REPOSITORY__ARTIFACT_LOCATION);
		createEAttribute(p2RepositoryEClass, P2_REPOSITORY__INDEX_LOCATION);
		createEAttribute(p2RepositoryEClass, P2_REPOSITORY__COMPOSITE_REPOSITORY);
		createEReference(p2RepositoryEClass, P2_REPOSITORY__RULES);
		createEReference(p2RepositoryEClass, P2_REPOSITORY__ECLIPSE_FEATURES);
		createEOperation(p2RepositoryEClass, P2_REPOSITORY___NORMALIZE_URI);

		bundleArtifactEClass = createEClass(BUNDLE_ARTIFACT);

		binaryArtifactEClass = createEClass(BINARY_ARTIFACT);

		eclipseFeatureEClass = createEClass(ECLIPSE_FEATURE);
		createEAttribute(eclipseFeatureEClass, ECLIPSE_FEATURE__JAR);
		createEAttribute(eclipseFeatureEClass, ECLIPSE_FEATURE__PACKED);
		createEAttribute(eclipseFeatureEClass, ECLIPSE_FEATURE__PROPERTY_LOCATION);
		createEAttribute(eclipseFeatureEClass, ECLIPSE_FEATURE__FEATURE_PROPERTIES);
		createEAttribute(eclipseFeatureEClass, ECLIPSE_FEATURE__IMAGE);
		createEAttribute(eclipseFeatureEClass, ECLIPSE_FEATURE__PROVIDER);

		filterRuleEClass = createEClass(FILTER_RULE);
		createEAttribute(filterRuleEClass, FILTER_RULE__FILTER);
		createEAttribute(filterRuleEClass, FILTER_RULE__RULE);

		// Create enums
		dependencyTypeEEnum = createEEnum(DEPENDENCY_TYPE);
		referenceTypeEEnum = createEEnum(REFERENCE_TYPE);
		repositoryTypeEEnum = createEEnum(REPOSITORY_TYPE);

		// Create data types
		versionEDataType = createEDataType(VERSION);
		eJavaUriEDataType = createEDataType(EJAVA_URI);
		ePropertiesEDataType = createEDataType(EPROPERTIES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		p2RepositoryEClass.getESuperTypes().add(this.getArtifactRepository());
		bundleArtifactEClass.getESuperTypes().add(this.getArtifact());
		binaryArtifactEClass.getESuperTypes().add(this.getArtifact());
		eclipseFeatureEClass.getESuperTypes().add(this.getArtifact());

		// Initialize classes, features, and operations; add parameters
		initEClass(osGiBundleEClass, OSGiBundle.class, "OSGiBundle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOSGiBundle_Bsn(), ecorePackage.getEString(), "bsn", null, 1, 1, OSGiBundle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSGiBundle_SourceLocation(), ecorePackage.getEString(), "sourceLocation", null, 0, 1, OSGiBundle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSGiBundle_CacheLocation(), ecorePackage.getEString(), "cacheLocation", null, 0, 1, OSGiBundle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSGiBundle_Name(), ecorePackage.getEString(), "name", null, 0, 1, OSGiBundle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSGiBundle_Vendor(), ecorePackage.getEString(), "vendor", null, 0, 1, OSGiBundle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSGiBundle_Version(), this.getVersion(), "version", null, 0, 1, OSGiBundle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSGiBundle_PolicyLazy(), ecorePackage.getEBoolean(), "policyLazy", "false", 0, 1, OSGiBundle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSGiBundle_ExecutionEnvironment(), ecorePackage.getEString(), "executionEnvironment", null, 0, 1, OSGiBundle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSGiBundle_Classpath(), ecorePackage.getEString(), "classpath", null, 0, 1, OSGiBundle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSGiBundle_Localization(), ecorePackage.getEString(), "localization", null, 0, 1, OSGiBundle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOSGiBundle_Dependency(), this.getDependency(), null, "dependency", null, 0, -1, OSGiBundle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getOSGiBundle_Dependency().getEKeys().add(this.getDependency_Id());
		getOSGiBundle_Dependency().getEKeys().add(this.getDependency_Name());
		EGenericType g1 = createEGenericType(ecorePackage.getEMap());
		EGenericType g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		initEAttribute(getOSGiBundle_Properties(), g1, "properties", null, 1, 1, OSGiBundle.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getOSGiBundle_BundleProperty(), this.getProperty(), null, "bundleProperty", null, 0, -1, OSGiBundle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSGiBundle_License(), ecorePackage.getEString(), "license", null, 0, 1, OSGiBundle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSGiBundle_Copyright(), ecorePackage.getEString(), "copyright", null, 0, 1, OSGiBundle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSGiBundle_FragmentHost(), ecorePackage.getEString(), "fragmentHost", null, 0, 1, OSGiBundle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOSGiBundle_Fragment(), ecorePackage.getEBoolean(), "fragment", null, 0, 1, OSGiBundle.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(dependencyEClass, Dependency.class, "Dependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDependency_Name(), ecorePackage.getEString(), "name", null, 1, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDependency_Type(), this.getDependencyType(), "type", null, 0, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDependency_Source(), this.getOSGiBundle(), null, "source", null, 0, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDependency_Target(), this.getOSGiBundle(), null, "target", null, 0, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDependency_Version(), ecorePackage.getEString(), "version", null, 0, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDependency_Visibility(), ecorePackage.getEString(), "visibility", null, 0, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDependency_Optional(), ecorePackage.getEBoolean(), "optional", "false", 0, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDependency_Location(), ecorePackage.getEString(), "location", null, 0, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDependency_Reexport(), ecorePackage.getEBoolean(), "reexport", "false", 0, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDependency_Object(), ecorePackage.getEObject(), null, "object", null, 0, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDependency_Filter(), ecorePackage.getEString(), "filter", null, 0, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDependency_Multiple(), ecorePackage.getEBoolean(), "multiple", "false", 0, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDependency_Active(), ecorePackage.getEBoolean(), "active", "false", 0, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDependency_DependencyProperty(), this.getProperty(), null, "dependencyProperty", null, 0, -1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		initEAttribute(getDependency_Properties(), g1, "properties", null, 1, 1, Dependency.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDependency_ObjectClass(), ecorePackage.getEString(), "objectClass", null, 0, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDependency_Id(), ecorePackage.getEString(), "id", null, 0, 1, Dependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(propertyEClass, Property.class, "Property", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getProperty_Key(), ecorePackage.getEString(), "key", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProperty_Value(), ecorePackage.getEString(), "value", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(artifactEClass, Artifact.class, "Artifact", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getArtifact_Id(), ecorePackage.getEString(), "id", null, 0, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifact_Name(), ecorePackage.getEString(), "name", null, 0, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifact_Label(), ecorePackage.getEString(), "label", null, 0, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifact_Description(), ecorePackage.getEString(), "description", null, 0, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifact_Version(), this.getVersion(), "version", null, 0, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifact_SourceLocation(), ecorePackage.getEString(), "sourceLocation", null, 0, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifact_CacheLocation(), ecorePackage.getEString(), "cacheLocation", null, 0, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArtifact_Repository(), this.getArtifactRepository(), this.getArtifactRepository_Artifacts(), "repository", null, 0, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifact_License(), ecorePackage.getEString(), "license", null, 0, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifact_Copyright(), ecorePackage.getEString(), "copyright", null, 0, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		initEAttribute(getArtifact_Properties(), g1, "properties", null, 1, 1, Artifact.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getArtifact_ArtifactProperties(), this.getProperty(), null, "artifactProperties", null, 0, -1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifact_Size(), ecorePackage.getEInt(), "size", "-1", 0, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifact_Hash(), ecorePackage.getEString(), "hash", null, 0, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifact_Timestamp(), ecorePackage.getEDate(), "timestamp", null, 0, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArtifact_References(), this.getArtifactReference(), null, "references", null, 0, -1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getArtifact__PutProperty__String_String(), null, "putProperty", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "key", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "value", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(artifactRepositoryEClass, ArtifactRepository.class, "ArtifactRepository", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getArtifactRepository_Id(), ecorePackage.getEString(), "id", null, 0, 1, ArtifactRepository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifactRepository_Name(), ecorePackage.getEString(), "name", null, 0, 1, ArtifactRepository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifactRepository_Version(), this.getVersion(), "version", null, 0, 1, ArtifactRepository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifactRepository_Type(), this.getRepositoryType(), "type", null, 0, 1, ArtifactRepository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifactRepository_Location(), ecorePackage.getEString(), "location", null, 0, 1, ArtifactRepository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArtifactRepository_Artifacts(), this.getArtifact(), this.getArtifact_Repository(), "artifacts", null, 0, -1, ArtifactRepository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getArtifactRepository_Artifacts().getEKeys().add(this.getArtifact_Id());
		initEReference(getArtifactRepository_Repositories(), this.getArtifactRepository(), this.getArtifactRepository_Parent(), "repositories", null, 0, -1, ArtifactRepository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getArtifactRepository_Repositories().getEKeys().add(this.getArtifactRepository_Id());
		initEReference(getArtifactRepository_Parent(), this.getArtifactRepository(), this.getArtifactRepository_Repositories(), "parent", null, 0, 1, ArtifactRepository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		initEAttribute(getArtifactRepository_Properties(), g1, "properties", null, 1, 1, ArtifactRepository.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getArtifactRepository_RepositoryProperties(), this.getProperty(), null, "repositoryProperties", null, 0, -1, ArtifactRepository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifactRepository_LocationUri(), this.getEJavaUri(), "locationUri", null, 0, 1, ArtifactRepository.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifactRepository_Timestamp(), ecorePackage.getEDate(), "timestamp", null, 0, 1, ArtifactRepository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArtifactRepository_AllArtifacts(), this.getArtifact(), null, "allArtifacts", null, 0, -1, ArtifactRepository.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifactRepository_Loaded(), ecorePackage.getEBoolean(), "loaded", "false", 0, 1, ArtifactRepository.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getArtifactRepository__GetArtifactById__String(), this.getArtifact(), "getArtifactById", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "artifactId", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getArtifactRepository__GetArtifactsById__String(), this.getArtifact(), "getArtifactsById", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "artifactId", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getArtifactRepository__GetArtifactsByName__String(), this.getArtifact(), "getArtifactsByName", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getArtifactRepository__GetAllArtifactsById__String(), this.getArtifact(), "getAllArtifactsById", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "artifactId", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(artifactReferenceEClass, ArtifactReference.class, "ArtifactReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getArtifactReference_Id(), ecorePackage.getEString(), "id", null, 0, 1, ArtifactReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifactReference_Version(), this.getVersion(), "version", null, 0, 1, ArtifactReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifactReference_Name(), ecorePackage.getEString(), "name", null, 1, 1, ArtifactReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifactReference_Label(), ecorePackage.getEString(), "label", null, 1, 1, ArtifactReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifactReference_Optional(), ecorePackage.getEBoolean(), "optional", "false", 0, 1, ArtifactReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifactReference_Location(), ecorePackage.getEString(), "location", null, 0, 1, ArtifactReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifactReference_Filter(), ecorePackage.getEString(), "filter", null, 0, 1, ArtifactReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifactReference_Type(), this.getReferenceType(), "type", null, 0, 1, ArtifactReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArtifactReference_RefObject(), ecorePackage.getEObject(), null, "refObject", null, 0, 1, ArtifactReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArtifactReference_Source(), this.getArtifact(), null, "source", null, 0, 1, ArtifactReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifactReference_SourceName(), ecorePackage.getEString(), "sourceName", null, 1, 1, ArtifactReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifactReference_SourceVersion(), this.getVersion(), "sourceVersion", null, 0, 1, ArtifactReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArtifactReference_Target(), this.getArtifact(), null, "target", null, 0, -1, ArtifactReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getArtifactReference_Target().getEKeys().add(this.getArtifact_Id());
		initEAttribute(getArtifactReference_TargetName(), ecorePackage.getEString(), "targetName", null, 1, 1, ArtifactReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifactReference_TargetVersion(), ecorePackage.getEString(), "targetVersion", null, 0, 1, ArtifactReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		initEAttribute(getArtifactReference_Properties(), g1, "properties", null, 1, 1, ArtifactReference.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getArtifactReference_ReferenceProperties(), this.getProperty(), null, "referenceProperties", null, 0, -1, ArtifactReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getArtifactReference__PutProperty__String_String(), null, "putProperty", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "key", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "value", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(p2RepositoryEClass, P2Repository.class, "P2Repository", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getP2Repository_P2RepositoryType(), ecorePackage.getEString(), "p2RepositoryType", null, 0, 1, P2Repository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getP2Repository_MetadataLocation(), ecorePackage.getEString(), "metadataLocation", null, 0, 1, P2Repository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getP2Repository_ArtifactLocation(), ecorePackage.getEString(), "artifactLocation", null, 0, 1, P2Repository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getP2Repository_IndexLocation(), ecorePackage.getEString(), "indexLocation", null, 0, 1, P2Repository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getP2Repository_CompositeRepository(), ecorePackage.getEBoolean(), "compositeRepository", null, 0, 1, P2Repository.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getP2Repository_Rules(), this.getFilterRule(), null, "rules", null, 0, -1, P2Repository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getP2Repository_EclipseFeatures(), this.getEclipseFeature(), null, "eclipseFeatures", null, 0, -1, P2Repository.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEOperation(getP2Repository__NormalizeUri(), this.getEJavaUri(), "normalizeUri", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(bundleArtifactEClass, BundleArtifact.class, "BundleArtifact", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(binaryArtifactEClass, BinaryArtifact.class, "BinaryArtifact", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(eclipseFeatureEClass, EclipseFeature.class, "EclipseFeature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEclipseFeature_Jar(), ecorePackage.getEBoolean(), "jar", "false", 0, 1, EclipseFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEclipseFeature_Packed(), ecorePackage.getEBoolean(), "packed", "false", 0, 1, EclipseFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEclipseFeature_PropertyLocation(), ecorePackage.getEString(), "propertyLocation", null, 0, 1, EclipseFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEclipseFeature_FeatureProperties(), this.getEProperties(), "featureProperties", null, 0, 1, EclipseFeature.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEclipseFeature_Image(), ecorePackage.getEString(), "image", null, 0, 1, EclipseFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEclipseFeature_Provider(), ecorePackage.getEString(), "provider", null, 0, 1, EclipseFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(filterRuleEClass, FilterRule.class, "FilterRule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFilterRule_Filter(), ecorePackage.getEString(), "filter", null, 0, 1, FilterRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFilterRule_Rule(), ecorePackage.getEString(), "rule", null, 0, 1, FilterRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(dependencyTypeEEnum, DependencyType.class, "DependencyType");
		addEEnumLiteral(dependencyTypeEEnum, DependencyType.OTHER);
		addEEnumLiteral(dependencyTypeEEnum, DependencyType.IMPORT);
		addEEnumLiteral(dependencyTypeEEnum, DependencyType.EXPORT);
		addEEnumLiteral(dependencyTypeEEnum, DependencyType.REQUIRE_BUNDLE);
		addEEnumLiteral(dependencyTypeEEnum, DependencyType.REQUIREMENT);
		addEEnumLiteral(dependencyTypeEEnum, DependencyType.CAPABILITY);
		addEEnumLiteral(dependencyTypeEEnum, DependencyType.DS);
		addEEnumLiteral(dependencyTypeEEnum, DependencyType.CONFIGURATION);
		addEEnumLiteral(dependencyTypeEEnum, DependencyType.ECORE);
		addEEnumLiteral(dependencyTypeEEnum, DependencyType.DYNAMIC_IMPORT);

		initEEnum(referenceTypeEEnum, ReferenceType.class, "ReferenceType");
		addEEnumLiteral(referenceTypeEEnum, ReferenceType.OTHER);
		addEEnumLiteral(referenceTypeEEnum, ReferenceType.IMPORT);
		addEEnumLiteral(referenceTypeEEnum, ReferenceType.EXPORT);
		addEEnumLiteral(referenceTypeEEnum, ReferenceType.REQUIRE_BUNDLE);
		addEEnumLiteral(referenceTypeEEnum, ReferenceType.REQUIREMENT);
		addEEnumLiteral(referenceTypeEEnum, ReferenceType.CAPABILITY);
		addEEnumLiteral(referenceTypeEEnum, ReferenceType.DS);
		addEEnumLiteral(referenceTypeEEnum, ReferenceType.CONFIGURATION);
		addEEnumLiteral(referenceTypeEEnum, ReferenceType.ECORE);
		addEEnumLiteral(referenceTypeEEnum, ReferenceType.PROJECT);
		addEEnumLiteral(referenceTypeEEnum, ReferenceType.INCLUDE_ARTIFACT);
		addEEnumLiteral(referenceTypeEEnum, ReferenceType.REQUIRE_ARTIFACT);

		initEEnum(repositoryTypeEEnum, RepositoryType.class, "RepositoryType");
		addEEnumLiteral(repositoryTypeEEnum, RepositoryType.OTHER);
		addEEnumLiteral(repositoryTypeEEnum, RepositoryType.OSGI);
		addEEnumLiteral(repositoryTypeEEnum, RepositoryType.MAVEN);
		addEEnumLiteral(repositoryTypeEEnum, RepositoryType.P2);
		addEEnumLiteral(repositoryTypeEEnum, RepositoryType.NPM);
		addEEnumLiteral(repositoryTypeEEnum, RepositoryType.DOCKER);
		addEEnumLiteral(repositoryTypeEEnum, RepositoryType.BND);

		// Initialize data types
		initEDataType(versionEDataType, String.class, "Version", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(eJavaUriEDataType, URI.class, "EJavaUri", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(ePropertiesEDataType, Properties.class, "EProperties", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";
		addAnnotation
		  (versionEDataType,
		   source,
		   new String[] {
			   "name", "Version",
			   "baseType", "http://www.eclipse.org/emf/2003/XMLType#string",
			   "pattern", "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}(\\..*)?"
		   });
	}

} //ArtifactsPackageImpl
