/**
 */
package org.gecko.artifacts.impl;

import org.eclipse.emf.ecore.EClass;

import org.gecko.artifacts.ArtifactsPackage;
import org.gecko.artifacts.BinaryArtifact;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Binary Artifact</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BinaryArtifactImpl extends ArtifactImpl implements BinaryArtifact {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryArtifactImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArtifactsPackage.Literals.BINARY_ARTIFACT;
	}

} //BinaryArtifactImpl
