/**
 */
package org.gecko.artifacts.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.gecko.artifacts.Artifact;
import org.gecko.artifacts.ArtifactReference;
import org.gecko.artifacts.ArtifactsFactory;
import org.gecko.artifacts.ArtifactsPackage;
import org.gecko.artifacts.Property;
import org.gecko.artifacts.ReferenceType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Artifact Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactReferenceImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactReferenceImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactReferenceImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactReferenceImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactReferenceImpl#isOptional <em>Optional</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactReferenceImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactReferenceImpl#getFilter <em>Filter</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactReferenceImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactReferenceImpl#getRefObject <em>Ref Object</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactReferenceImpl#getSource <em>Source</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactReferenceImpl#getSourceName <em>Source Name</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactReferenceImpl#getSourceVersion <em>Source Version</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactReferenceImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactReferenceImpl#getTargetName <em>Target Name</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactReferenceImpl#getTargetVersion <em>Target Version</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactReferenceImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactReferenceImpl#getReferenceProperties <em>Reference Properties</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ArtifactReferenceImpl extends MinimalEObjectImpl.Container implements ArtifactReference {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #isOptional() <em>Optional</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOptional()
	 * @generated
	 * @ordered
	 */
	protected static final boolean OPTIONAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isOptional() <em>Optional</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOptional()
	 * @generated
	 * @ordered
	 */
	protected boolean optional = OPTIONAL_EDEFAULT;

	/**
	 * The default value of the '{@link #getLocation() <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected String location = LOCATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getFilter() <em>Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilter()
	 * @generated
	 * @ordered
	 */
	protected static final String FILTER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFilter() <em>Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilter()
	 * @generated
	 * @ordered
	 */
	protected String filter = FILTER_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final ReferenceType TYPE_EDEFAULT = ReferenceType.OTHER;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected ReferenceType type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRefObject() <em>Ref Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefObject()
	 * @generated
	 * @ordered
	 */
	protected EObject refObject;

	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected Artifact source;

	/**
	 * The default value of the '{@link #getSourceName() <em>Source Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceName()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourceName() <em>Source Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceName()
	 * @generated
	 * @ordered
	 */
	protected String sourceName = SOURCE_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSourceVersion() <em>Source Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourceVersion() <em>Source Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceVersion()
	 * @generated
	 * @ordered
	 */
	protected String sourceVersion = SOURCE_VERSION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected EList<Artifact> target;

	/**
	 * The default value of the '{@link #getTargetName() <em>Target Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetName()
	 * @generated
	 * @ordered
	 */
	protected static final String TARGET_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTargetName() <em>Target Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetName()
	 * @generated
	 * @ordered
	 */
	protected String targetName = TARGET_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getTargetVersion() <em>Target Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String TARGET_VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTargetVersion() <em>Target Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetVersion()
	 * @generated
	 * @ordered
	 */
	protected String targetVersion = TARGET_VERSION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getReferenceProperties() <em>Reference Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<Property> referenceProperties;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArtifactReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArtifactsPackage.Literals.ARTIFACT_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REFERENCE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REFERENCE__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REFERENCE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REFERENCE__LABEL, oldLabel, label));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isOptional() {
		return optional;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOptional(boolean newOptional) {
		boolean oldOptional = optional;
		optional = newOptional;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REFERENCE__OPTIONAL, oldOptional, optional));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLocation(String newLocation) {
		String oldLocation = location;
		location = newLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REFERENCE__LOCATION, oldLocation, location));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFilter() {
		return filter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFilter(String newFilter) {
		String oldFilter = filter;
		filter = newFilter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REFERENCE__FILTER, oldFilter, filter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ReferenceType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(ReferenceType newType) {
		ReferenceType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REFERENCE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject getRefObject() {
		if (refObject != null && refObject.eIsProxy()) {
			InternalEObject oldRefObject = (InternalEObject)refObject;
			refObject = eResolveProxy(oldRefObject);
			if (refObject != oldRefObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ArtifactsPackage.ARTIFACT_REFERENCE__REF_OBJECT, oldRefObject, refObject));
			}
		}
		return refObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetRefObject() {
		return refObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRefObject(EObject newRefObject) {
		EObject oldRefObject = refObject;
		refObject = newRefObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REFERENCE__REF_OBJECT, oldRefObject, refObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Artifact getSource() {
		if (source != null && source.eIsProxy()) {
			InternalEObject oldSource = (InternalEObject)source;
			source = (Artifact)eResolveProxy(oldSource);
			if (source != oldSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ArtifactsPackage.ARTIFACT_REFERENCE__SOURCE, oldSource, source));
			}
		}
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Artifact basicGetSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSource(Artifact newSource) {
		Artifact oldSource = source;
		source = newSource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REFERENCE__SOURCE, oldSource, source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSourceName() {
		return sourceName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSourceName(String newSourceName) {
		String oldSourceName = sourceName;
		sourceName = newSourceName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REFERENCE__SOURCE_NAME, oldSourceName, sourceName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSourceVersion() {
		return sourceVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSourceVersion(String newSourceVersion) {
		String oldSourceVersion = sourceVersion;
		sourceVersion = newSourceVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REFERENCE__SOURCE_VERSION, oldSourceVersion, sourceVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Artifact> getTarget() {
		if (target == null) {
			target = new EObjectResolvingEList<Artifact>(Artifact.class, this, ArtifactsPackage.ARTIFACT_REFERENCE__TARGET);
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTargetName() {
		return targetName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTargetName(String newTargetName) {
		String oldTargetName = targetName;
		targetName = newTargetName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REFERENCE__TARGET_NAME, oldTargetName, targetName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTargetVersion() {
		return targetVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTargetVersion(String newTargetVersion) {
		String oldTargetVersion = targetVersion;
		targetVersion = newTargetVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REFERENCE__TARGET_VERSION, oldTargetVersion, targetVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Map<String, String> getProperties() {
		return getReferenceProperties().stream().collect(java.util.stream.Collectors.toMap(Property::getKey, Property::getValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Property> getReferenceProperties() {
		if (referenceProperties == null) {
			referenceProperties = new EObjectContainmentEList<Property>(Property.class, this, ArtifactsPackage.ARTIFACT_REFERENCE__REFERENCE_PROPERTIES);
		}
		return referenceProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void putProperty(final String key, final String value) {
		Property p = ArtifactsFactory.eINSTANCE.createProperty();
		p.setKey(key);
		p.setValue(value);
		getReferenceProperties().add(p);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ArtifactsPackage.ARTIFACT_REFERENCE__REFERENCE_PROPERTIES:
				return ((InternalEList<?>)getReferenceProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArtifactsPackage.ARTIFACT_REFERENCE__ID:
				return getId();
			case ArtifactsPackage.ARTIFACT_REFERENCE__VERSION:
				return getVersion();
			case ArtifactsPackage.ARTIFACT_REFERENCE__NAME:
				return getName();
			case ArtifactsPackage.ARTIFACT_REFERENCE__LABEL:
				return getLabel();
			case ArtifactsPackage.ARTIFACT_REFERENCE__OPTIONAL:
				return isOptional();
			case ArtifactsPackage.ARTIFACT_REFERENCE__LOCATION:
				return getLocation();
			case ArtifactsPackage.ARTIFACT_REFERENCE__FILTER:
				return getFilter();
			case ArtifactsPackage.ARTIFACT_REFERENCE__TYPE:
				return getType();
			case ArtifactsPackage.ARTIFACT_REFERENCE__REF_OBJECT:
				if (resolve) return getRefObject();
				return basicGetRefObject();
			case ArtifactsPackage.ARTIFACT_REFERENCE__SOURCE:
				if (resolve) return getSource();
				return basicGetSource();
			case ArtifactsPackage.ARTIFACT_REFERENCE__SOURCE_NAME:
				return getSourceName();
			case ArtifactsPackage.ARTIFACT_REFERENCE__SOURCE_VERSION:
				return getSourceVersion();
			case ArtifactsPackage.ARTIFACT_REFERENCE__TARGET:
				return getTarget();
			case ArtifactsPackage.ARTIFACT_REFERENCE__TARGET_NAME:
				return getTargetName();
			case ArtifactsPackage.ARTIFACT_REFERENCE__TARGET_VERSION:
				return getTargetVersion();
			case ArtifactsPackage.ARTIFACT_REFERENCE__PROPERTIES:
				return getProperties();
			case ArtifactsPackage.ARTIFACT_REFERENCE__REFERENCE_PROPERTIES:
				return getReferenceProperties();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArtifactsPackage.ARTIFACT_REFERENCE__ID:
				setId((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__VERSION:
				setVersion((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__NAME:
				setName((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__LABEL:
				setLabel((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__OPTIONAL:
				setOptional((Boolean)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__LOCATION:
				setLocation((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__FILTER:
				setFilter((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__TYPE:
				setType((ReferenceType)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__REF_OBJECT:
				setRefObject((EObject)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__SOURCE:
				setSource((Artifact)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__SOURCE_NAME:
				setSourceName((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__SOURCE_VERSION:
				setSourceVersion((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__TARGET:
				getTarget().clear();
				getTarget().addAll((Collection<? extends Artifact>)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__TARGET_NAME:
				setTargetName((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__TARGET_VERSION:
				setTargetVersion((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__REFERENCE_PROPERTIES:
				getReferenceProperties().clear();
				getReferenceProperties().addAll((Collection<? extends Property>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArtifactsPackage.ARTIFACT_REFERENCE__ID:
				setId(ID_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__LABEL:
				setLabel(LABEL_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__OPTIONAL:
				setOptional(OPTIONAL_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__LOCATION:
				setLocation(LOCATION_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__FILTER:
				setFilter(FILTER_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__REF_OBJECT:
				setRefObject((EObject)null);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__SOURCE:
				setSource((Artifact)null);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__SOURCE_NAME:
				setSourceName(SOURCE_NAME_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__SOURCE_VERSION:
				setSourceVersion(SOURCE_VERSION_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__TARGET:
				getTarget().clear();
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__TARGET_NAME:
				setTargetName(TARGET_NAME_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__TARGET_VERSION:
				setTargetVersion(TARGET_VERSION_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT_REFERENCE__REFERENCE_PROPERTIES:
				getReferenceProperties().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArtifactsPackage.ARTIFACT_REFERENCE__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case ArtifactsPackage.ARTIFACT_REFERENCE__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
			case ArtifactsPackage.ARTIFACT_REFERENCE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ArtifactsPackage.ARTIFACT_REFERENCE__LABEL:
				return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT.equals(label);
			case ArtifactsPackage.ARTIFACT_REFERENCE__OPTIONAL:
				return optional != OPTIONAL_EDEFAULT;
			case ArtifactsPackage.ARTIFACT_REFERENCE__LOCATION:
				return LOCATION_EDEFAULT == null ? location != null : !LOCATION_EDEFAULT.equals(location);
			case ArtifactsPackage.ARTIFACT_REFERENCE__FILTER:
				return FILTER_EDEFAULT == null ? filter != null : !FILTER_EDEFAULT.equals(filter);
			case ArtifactsPackage.ARTIFACT_REFERENCE__TYPE:
				return type != TYPE_EDEFAULT;
			case ArtifactsPackage.ARTIFACT_REFERENCE__REF_OBJECT:
				return refObject != null;
			case ArtifactsPackage.ARTIFACT_REFERENCE__SOURCE:
				return source != null;
			case ArtifactsPackage.ARTIFACT_REFERENCE__SOURCE_NAME:
				return SOURCE_NAME_EDEFAULT == null ? sourceName != null : !SOURCE_NAME_EDEFAULT.equals(sourceName);
			case ArtifactsPackage.ARTIFACT_REFERENCE__SOURCE_VERSION:
				return SOURCE_VERSION_EDEFAULT == null ? sourceVersion != null : !SOURCE_VERSION_EDEFAULT.equals(sourceVersion);
			case ArtifactsPackage.ARTIFACT_REFERENCE__TARGET:
				return target != null && !target.isEmpty();
			case ArtifactsPackage.ARTIFACT_REFERENCE__TARGET_NAME:
				return TARGET_NAME_EDEFAULT == null ? targetName != null : !TARGET_NAME_EDEFAULT.equals(targetName);
			case ArtifactsPackage.ARTIFACT_REFERENCE__TARGET_VERSION:
				return TARGET_VERSION_EDEFAULT == null ? targetVersion != null : !TARGET_VERSION_EDEFAULT.equals(targetVersion);
			case ArtifactsPackage.ARTIFACT_REFERENCE__PROPERTIES:
				return getProperties() != null;
			case ArtifactsPackage.ARTIFACT_REFERENCE__REFERENCE_PROPERTIES:
				return referenceProperties != null && !referenceProperties.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ArtifactsPackage.ARTIFACT_REFERENCE___PUT_PROPERTY__STRING_STRING:
				putProperty((String)arguments.get(0), (String)arguments.get(1));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", version: ");
		result.append(version);
		result.append(", name: ");
		result.append(name);
		result.append(", label: ");
		result.append(label);
		result.append(", optional: ");
		result.append(optional);
		result.append(", location: ");
		result.append(location);
		result.append(", filter: ");
		result.append(filter);
		result.append(", type: ");
		result.append(type);
		result.append(", sourceName: ");
		result.append(sourceName);
		result.append(", sourceVersion: ");
		result.append(sourceVersion);
		result.append(", targetName: ");
		result.append(targetName);
		result.append(", targetVersion: ");
		result.append(targetVersion);
		result.append(')');
		return result.toString();
	}

} //ArtifactReferenceImpl
