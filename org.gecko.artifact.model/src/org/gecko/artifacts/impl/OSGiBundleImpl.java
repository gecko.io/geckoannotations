/**
 */
package org.gecko.artifacts.impl;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.gecko.artifacts.ArtifactsPackage;
import org.gecko.artifacts.Dependency;
import org.gecko.artifacts.OSGiBundle;
import org.gecko.artifacts.Property;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OS Gi Bundle</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.artifacts.impl.OSGiBundleImpl#getBsn <em>Bsn</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.OSGiBundleImpl#getSourceLocation <em>Source Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.OSGiBundleImpl#getCacheLocation <em>Cache Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.OSGiBundleImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.OSGiBundleImpl#getVendor <em>Vendor</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.OSGiBundleImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.OSGiBundleImpl#isPolicyLazy <em>Policy Lazy</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.OSGiBundleImpl#getExecutionEnvironment <em>Execution Environment</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.OSGiBundleImpl#getClasspath <em>Classpath</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.OSGiBundleImpl#getLocalization <em>Localization</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.OSGiBundleImpl#getDependency <em>Dependency</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.OSGiBundleImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.OSGiBundleImpl#getBundleProperty <em>Bundle Property</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.OSGiBundleImpl#getLicense <em>License</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.OSGiBundleImpl#getCopyright <em>Copyright</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.OSGiBundleImpl#getFragmentHost <em>Fragment Host</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.OSGiBundleImpl#isFragment <em>Fragment</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OSGiBundleImpl extends MinimalEObjectImpl.Container implements OSGiBundle {
	/**
	 * The default value of the '{@link #getBsn() <em>Bsn</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBsn()
	 * @generated
	 * @ordered
	 */
	protected static final String BSN_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBsn() <em>Bsn</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBsn()
	 * @generated
	 * @ordered
	 */
	protected String bsn = BSN_EDEFAULT;

	/**
	 * The default value of the '{@link #getSourceLocation() <em>Source Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_LOCATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourceLocation() <em>Source Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceLocation()
	 * @generated
	 * @ordered
	 */
	protected String sourceLocation = SOURCE_LOCATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getCacheLocation() <em>Cache Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCacheLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String CACHE_LOCATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCacheLocation() <em>Cache Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCacheLocation()
	 * @generated
	 * @ordered
	 */
	protected String cacheLocation = CACHE_LOCATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getVendor() <em>Vendor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVendor()
	 * @generated
	 * @ordered
	 */
	protected static final String VENDOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVendor() <em>Vendor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVendor()
	 * @generated
	 * @ordered
	 */
	protected String vendor = VENDOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #isPolicyLazy() <em>Policy Lazy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPolicyLazy()
	 * @generated
	 * @ordered
	 */
	protected static final boolean POLICY_LAZY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPolicyLazy() <em>Policy Lazy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPolicyLazy()
	 * @generated
	 * @ordered
	 */
	protected boolean policyLazy = POLICY_LAZY_EDEFAULT;

	/**
	 * The default value of the '{@link #getExecutionEnvironment() <em>Execution Environment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionEnvironment()
	 * @generated
	 * @ordered
	 */
	protected static final String EXECUTION_ENVIRONMENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExecutionEnvironment() <em>Execution Environment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionEnvironment()
	 * @generated
	 * @ordered
	 */
	protected String executionEnvironment = EXECUTION_ENVIRONMENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getClasspath() <em>Classpath</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClasspath()
	 * @generated
	 * @ordered
	 */
	protected static final String CLASSPATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getClasspath() <em>Classpath</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClasspath()
	 * @generated
	 * @ordered
	 */
	protected String classpath = CLASSPATH_EDEFAULT;

	/**
	 * The default value of the '{@link #getLocalization() <em>Localization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalization()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCALIZATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLocalization() <em>Localization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalization()
	 * @generated
	 * @ordered
	 */
	protected String localization = LOCALIZATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDependency() <em>Dependency</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependency()
	 * @generated
	 * @ordered
	 */
	protected EList<Dependency> dependency;

	/**
	 * The cached value of the '{@link #getBundleProperty() <em>Bundle Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBundleProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<Property> bundleProperty;

	/**
	 * The default value of the '{@link #getLicense() <em>License</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLicense()
	 * @generated
	 * @ordered
	 */
	protected static final String LICENSE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLicense() <em>License</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLicense()
	 * @generated
	 * @ordered
	 */
	protected String license = LICENSE_EDEFAULT;

	/**
	 * The default value of the '{@link #getCopyright() <em>Copyright</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCopyright()
	 * @generated
	 * @ordered
	 */
	protected static final String COPYRIGHT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCopyright() <em>Copyright</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCopyright()
	 * @generated
	 * @ordered
	 */
	protected String copyright = COPYRIGHT_EDEFAULT;

	/**
	 * The default value of the '{@link #getFragmentHost() <em>Fragment Host</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFragmentHost()
	 * @generated
	 * @ordered
	 */
	protected static final String FRAGMENT_HOST_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFragmentHost() <em>Fragment Host</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFragmentHost()
	 * @generated
	 * @ordered
	 */
	protected String fragmentHost = FRAGMENT_HOST_EDEFAULT;

	/**
	 * The default value of the '{@link #isFragment() <em>Fragment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFragment()
	 * @generated
	 * @ordered
	 */
	protected static final boolean FRAGMENT_EDEFAULT = false;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OSGiBundleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArtifactsPackage.Literals.OS_GI_BUNDLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getBsn() {
		return bsn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBsn(String newBsn) {
		String oldBsn = bsn;
		bsn = newBsn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.OS_GI_BUNDLE__BSN, oldBsn, bsn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSourceLocation() {
		return sourceLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSourceLocation(String newSourceLocation) {
		String oldSourceLocation = sourceLocation;
		sourceLocation = newSourceLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.OS_GI_BUNDLE__SOURCE_LOCATION, oldSourceLocation, sourceLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCacheLocation() {
		return cacheLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCacheLocation(String newCacheLocation) {
		String oldCacheLocation = cacheLocation;
		cacheLocation = newCacheLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.OS_GI_BUNDLE__CACHE_LOCATION, oldCacheLocation, cacheLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.OS_GI_BUNDLE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVendor() {
		return vendor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVendor(String newVendor) {
		String oldVendor = vendor;
		vendor = newVendor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.OS_GI_BUNDLE__VENDOR, oldVendor, vendor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.OS_GI_BUNDLE__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isPolicyLazy() {
		return policyLazy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPolicyLazy(boolean newPolicyLazy) {
		boolean oldPolicyLazy = policyLazy;
		policyLazy = newPolicyLazy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.OS_GI_BUNDLE__POLICY_LAZY, oldPolicyLazy, policyLazy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getExecutionEnvironment() {
		return executionEnvironment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setExecutionEnvironment(String newExecutionEnvironment) {
		String oldExecutionEnvironment = executionEnvironment;
		executionEnvironment = newExecutionEnvironment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.OS_GI_BUNDLE__EXECUTION_ENVIRONMENT, oldExecutionEnvironment, executionEnvironment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getClasspath() {
		return classpath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setClasspath(String newClasspath) {
		String oldClasspath = classpath;
		classpath = newClasspath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.OS_GI_BUNDLE__CLASSPATH, oldClasspath, classpath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLocalization() {
		return localization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLocalization(String newLocalization) {
		String oldLocalization = localization;
		localization = newLocalization;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.OS_GI_BUNDLE__LOCALIZATION, oldLocalization, localization));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Dependency> getDependency() {
		if (dependency == null) {
			dependency = new EObjectResolvingEList<Dependency>(Dependency.class, this, ArtifactsPackage.OS_GI_BUNDLE__DEPENDENCY);
		}
		return dependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Map<String, String> getProperties() {
		return getBundleProperty().stream().collect(java.util.stream.Collectors.toMap(Property::getKey, Property::getValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Property> getBundleProperty() {
		if (bundleProperty == null) {
			bundleProperty = new EObjectContainmentEList<Property>(Property.class, this, ArtifactsPackage.OS_GI_BUNDLE__BUNDLE_PROPERTY);
		}
		return bundleProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLicense() {
		return license;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLicense(String newLicense) {
		String oldLicense = license;
		license = newLicense;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.OS_GI_BUNDLE__LICENSE, oldLicense, license));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCopyright() {
		return copyright;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCopyright(String newCopyright) {
		String oldCopyright = copyright;
		copyright = newCopyright;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.OS_GI_BUNDLE__COPYRIGHT, oldCopyright, copyright));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFragmentHost() {
		return fragmentHost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFragmentHost(String newFragmentHost) {
		String oldFragmentHost = fragmentHost;
		fragmentHost = newFragmentHost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.OS_GI_BUNDLE__FRAGMENT_HOST, oldFragmentHost, fragmentHost));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isFragment() {
		return getFragmentHost() != null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ArtifactsPackage.OS_GI_BUNDLE__BUNDLE_PROPERTY:
				return ((InternalEList<?>)getBundleProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArtifactsPackage.OS_GI_BUNDLE__BSN:
				return getBsn();
			case ArtifactsPackage.OS_GI_BUNDLE__SOURCE_LOCATION:
				return getSourceLocation();
			case ArtifactsPackage.OS_GI_BUNDLE__CACHE_LOCATION:
				return getCacheLocation();
			case ArtifactsPackage.OS_GI_BUNDLE__NAME:
				return getName();
			case ArtifactsPackage.OS_GI_BUNDLE__VENDOR:
				return getVendor();
			case ArtifactsPackage.OS_GI_BUNDLE__VERSION:
				return getVersion();
			case ArtifactsPackage.OS_GI_BUNDLE__POLICY_LAZY:
				return isPolicyLazy();
			case ArtifactsPackage.OS_GI_BUNDLE__EXECUTION_ENVIRONMENT:
				return getExecutionEnvironment();
			case ArtifactsPackage.OS_GI_BUNDLE__CLASSPATH:
				return getClasspath();
			case ArtifactsPackage.OS_GI_BUNDLE__LOCALIZATION:
				return getLocalization();
			case ArtifactsPackage.OS_GI_BUNDLE__DEPENDENCY:
				return getDependency();
			case ArtifactsPackage.OS_GI_BUNDLE__PROPERTIES:
				return getProperties();
			case ArtifactsPackage.OS_GI_BUNDLE__BUNDLE_PROPERTY:
				return getBundleProperty();
			case ArtifactsPackage.OS_GI_BUNDLE__LICENSE:
				return getLicense();
			case ArtifactsPackage.OS_GI_BUNDLE__COPYRIGHT:
				return getCopyright();
			case ArtifactsPackage.OS_GI_BUNDLE__FRAGMENT_HOST:
				return getFragmentHost();
			case ArtifactsPackage.OS_GI_BUNDLE__FRAGMENT:
				return isFragment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArtifactsPackage.OS_GI_BUNDLE__BSN:
				setBsn((String)newValue);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__SOURCE_LOCATION:
				setSourceLocation((String)newValue);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__CACHE_LOCATION:
				setCacheLocation((String)newValue);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__NAME:
				setName((String)newValue);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__VENDOR:
				setVendor((String)newValue);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__VERSION:
				setVersion((String)newValue);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__POLICY_LAZY:
				setPolicyLazy((Boolean)newValue);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__EXECUTION_ENVIRONMENT:
				setExecutionEnvironment((String)newValue);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__CLASSPATH:
				setClasspath((String)newValue);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__LOCALIZATION:
				setLocalization((String)newValue);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__DEPENDENCY:
				getDependency().clear();
				getDependency().addAll((Collection<? extends Dependency>)newValue);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__BUNDLE_PROPERTY:
				getBundleProperty().clear();
				getBundleProperty().addAll((Collection<? extends Property>)newValue);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__LICENSE:
				setLicense((String)newValue);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__COPYRIGHT:
				setCopyright((String)newValue);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__FRAGMENT_HOST:
				setFragmentHost((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArtifactsPackage.OS_GI_BUNDLE__BSN:
				setBsn(BSN_EDEFAULT);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__SOURCE_LOCATION:
				setSourceLocation(SOURCE_LOCATION_EDEFAULT);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__CACHE_LOCATION:
				setCacheLocation(CACHE_LOCATION_EDEFAULT);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__VENDOR:
				setVendor(VENDOR_EDEFAULT);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__POLICY_LAZY:
				setPolicyLazy(POLICY_LAZY_EDEFAULT);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__EXECUTION_ENVIRONMENT:
				setExecutionEnvironment(EXECUTION_ENVIRONMENT_EDEFAULT);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__CLASSPATH:
				setClasspath(CLASSPATH_EDEFAULT);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__LOCALIZATION:
				setLocalization(LOCALIZATION_EDEFAULT);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__DEPENDENCY:
				getDependency().clear();
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__BUNDLE_PROPERTY:
				getBundleProperty().clear();
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__LICENSE:
				setLicense(LICENSE_EDEFAULT);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__COPYRIGHT:
				setCopyright(COPYRIGHT_EDEFAULT);
				return;
			case ArtifactsPackage.OS_GI_BUNDLE__FRAGMENT_HOST:
				setFragmentHost(FRAGMENT_HOST_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArtifactsPackage.OS_GI_BUNDLE__BSN:
				return BSN_EDEFAULT == null ? bsn != null : !BSN_EDEFAULT.equals(bsn);
			case ArtifactsPackage.OS_GI_BUNDLE__SOURCE_LOCATION:
				return SOURCE_LOCATION_EDEFAULT == null ? sourceLocation != null : !SOURCE_LOCATION_EDEFAULT.equals(sourceLocation);
			case ArtifactsPackage.OS_GI_BUNDLE__CACHE_LOCATION:
				return CACHE_LOCATION_EDEFAULT == null ? cacheLocation != null : !CACHE_LOCATION_EDEFAULT.equals(cacheLocation);
			case ArtifactsPackage.OS_GI_BUNDLE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ArtifactsPackage.OS_GI_BUNDLE__VENDOR:
				return VENDOR_EDEFAULT == null ? vendor != null : !VENDOR_EDEFAULT.equals(vendor);
			case ArtifactsPackage.OS_GI_BUNDLE__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
			case ArtifactsPackage.OS_GI_BUNDLE__POLICY_LAZY:
				return policyLazy != POLICY_LAZY_EDEFAULT;
			case ArtifactsPackage.OS_GI_BUNDLE__EXECUTION_ENVIRONMENT:
				return EXECUTION_ENVIRONMENT_EDEFAULT == null ? executionEnvironment != null : !EXECUTION_ENVIRONMENT_EDEFAULT.equals(executionEnvironment);
			case ArtifactsPackage.OS_GI_BUNDLE__CLASSPATH:
				return CLASSPATH_EDEFAULT == null ? classpath != null : !CLASSPATH_EDEFAULT.equals(classpath);
			case ArtifactsPackage.OS_GI_BUNDLE__LOCALIZATION:
				return LOCALIZATION_EDEFAULT == null ? localization != null : !LOCALIZATION_EDEFAULT.equals(localization);
			case ArtifactsPackage.OS_GI_BUNDLE__DEPENDENCY:
				return dependency != null && !dependency.isEmpty();
			case ArtifactsPackage.OS_GI_BUNDLE__PROPERTIES:
				return getProperties() != null;
			case ArtifactsPackage.OS_GI_BUNDLE__BUNDLE_PROPERTY:
				return bundleProperty != null && !bundleProperty.isEmpty();
			case ArtifactsPackage.OS_GI_BUNDLE__LICENSE:
				return LICENSE_EDEFAULT == null ? license != null : !LICENSE_EDEFAULT.equals(license);
			case ArtifactsPackage.OS_GI_BUNDLE__COPYRIGHT:
				return COPYRIGHT_EDEFAULT == null ? copyright != null : !COPYRIGHT_EDEFAULT.equals(copyright);
			case ArtifactsPackage.OS_GI_BUNDLE__FRAGMENT_HOST:
				return FRAGMENT_HOST_EDEFAULT == null ? fragmentHost != null : !FRAGMENT_HOST_EDEFAULT.equals(fragmentHost);
			case ArtifactsPackage.OS_GI_BUNDLE__FRAGMENT:
				return isFragment() != FRAGMENT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (bsn: ");
		result.append(bsn);
		result.append(", sourceLocation: ");
		result.append(sourceLocation);
		result.append(", cacheLocation: ");
		result.append(cacheLocation);
		result.append(", name: ");
		result.append(name);
		result.append(", vendor: ");
		result.append(vendor);
		result.append(", version: ");
		result.append(version);
		result.append(", policyLazy: ");
		result.append(policyLazy);
		result.append(", executionEnvironment: ");
		result.append(executionEnvironment);
		result.append(", classpath: ");
		result.append(classpath);
		result.append(", localization: ");
		result.append(localization);
		result.append(", license: ");
		result.append(license);
		result.append(", copyright: ");
		result.append(copyright);
		result.append(", fragmentHost: ");
		result.append(fragmentHost);
		result.append(')');
		return result.toString();
	}

} //OSGiBundleImpl
