/**
 */
package org.gecko.artifacts.impl;

import java.lang.reflect.InvocationTargetException;

import java.net.URI;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import java.util.stream.Collectors;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.gecko.artifacts.Artifact;
import org.gecko.artifacts.ArtifactRepository;
import org.gecko.artifacts.ArtifactsPackage;
import org.gecko.artifacts.Property;
import org.gecko.artifacts.RepositoryType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Artifact Repository</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactRepositoryImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactRepositoryImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactRepositoryImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactRepositoryImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactRepositoryImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactRepositoryImpl#getArtifacts <em>Artifacts</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactRepositoryImpl#getRepositories <em>Repositories</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactRepositoryImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactRepositoryImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactRepositoryImpl#getRepositoryProperties <em>Repository Properties</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactRepositoryImpl#getLocationUri <em>Location Uri</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactRepositoryImpl#getTimestamp <em>Timestamp</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactRepositoryImpl#getAllArtifacts <em>All Artifacts</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactRepositoryImpl#isLoaded <em>Loaded</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ArtifactRepositoryImpl extends MinimalEObjectImpl.Container implements ArtifactRepository {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final RepositoryType TYPE_EDEFAULT = RepositoryType.OTHER;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected RepositoryType type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLocation() <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected String location = LOCATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getArtifacts() <em>Artifacts</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArtifacts()
	 * @generated
	 * @ordered
	 */
	protected EList<Artifact> artifacts;

	/**
	 * The cached value of the '{@link #getRepositories() <em>Repositories</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepositories()
	 * @generated
	 * @ordered
	 */
	protected EList<ArtifactRepository> repositories;

	/**
	 * The cached value of the '{@link #getParent() <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected ArtifactRepository parent;

	/**
	 * The cached value of the '{@link #getRepositoryProperties() <em>Repository Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepositoryProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<Property> repositoryProperties;

	/**
	 * The default value of the '{@link #getLocationUri() <em>Location Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocationUri()
	 * @generated
	 * @ordered
	 */
	protected static final URI LOCATION_URI_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getTimestamp() <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimestamp()
	 * @generated
	 * @ordered
	 */
	protected static final Date TIMESTAMP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTimestamp() <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimestamp()
	 * @generated
	 * @ordered
	 */
	protected Date timestamp = TIMESTAMP_EDEFAULT;

	/**
	 * The default value of the '{@link #isLoaded() <em>Loaded</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLoaded()
	 * @generated
	 * @ordered
	 */
	protected static final boolean LOADED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isLoaded() <em>Loaded</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLoaded()
	 * @generated
	 * @ordered
	 */
	protected boolean loaded = LOADED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArtifactRepositoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArtifactsPackage.Literals.ARTIFACT_REPOSITORY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REPOSITORY__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REPOSITORY__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REPOSITORY__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RepositoryType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(RepositoryType newType) {
		RepositoryType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REPOSITORY__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLocation(String newLocation) {
		String oldLocation = location;
		location = newLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REPOSITORY__LOCATION, oldLocation, location));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Artifact> getArtifacts() {
		if (artifacts == null) {
			artifacts = new EObjectWithInverseResolvingEList<Artifact>(Artifact.class, this, ArtifactsPackage.ARTIFACT_REPOSITORY__ARTIFACTS, ArtifactsPackage.ARTIFACT__REPOSITORY);
		}
		return artifacts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ArtifactRepository> getRepositories() {
		if (repositories == null) {
			repositories = new EObjectWithInverseResolvingEList<ArtifactRepository>(ArtifactRepository.class, this, ArtifactsPackage.ARTIFACT_REPOSITORY__REPOSITORIES, ArtifactsPackage.ARTIFACT_REPOSITORY__PARENT);
		}
		return repositories;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ArtifactRepository getParent() {
		if (parent != null && parent.eIsProxy()) {
			InternalEObject oldParent = (InternalEObject)parent;
			parent = (ArtifactRepository)eResolveProxy(oldParent);
			if (parent != oldParent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ArtifactsPackage.ARTIFACT_REPOSITORY__PARENT, oldParent, parent));
			}
		}
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArtifactRepository basicGetParent() {
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(ArtifactRepository newParent, NotificationChain msgs) {
		ArtifactRepository oldParent = parent;
		parent = newParent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REPOSITORY__PARENT, oldParent, newParent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParent(ArtifactRepository newParent) {
		if (newParent != parent) {
			NotificationChain msgs = null;
			if (parent != null)
				msgs = ((InternalEObject)parent).eInverseRemove(this, ArtifactsPackage.ARTIFACT_REPOSITORY__REPOSITORIES, ArtifactRepository.class, msgs);
			if (newParent != null)
				msgs = ((InternalEObject)newParent).eInverseAdd(this, ArtifactsPackage.ARTIFACT_REPOSITORY__REPOSITORIES, ArtifactRepository.class, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REPOSITORY__PARENT, newParent, newParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Map<String, String> getProperties() {
		return getRepositoryProperties().stream().collect(java.util.stream.Collectors.toMap(Property::getKey, Property::getValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Property> getRepositoryProperties() {
		if (repositoryProperties == null) {
			repositoryProperties = new EObjectContainmentEList<Property>(Property.class, this, ArtifactsPackage.ARTIFACT_REPOSITORY__REPOSITORY_PROPERTIES);
		}
		return repositoryProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public URI getLocationUri() {
		if (getLocation() == null || getLocation().isEmpty()) {
			return null;
		}
		try {
			return new URI(getLocation());
		} catch (Exception e) {
			throw new IllegalStateException("Cannot convert location string into an URI: " + getLocation(), e );
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTimestamp(Date newTimestamp) {
		Date oldTimestamp = timestamp;
		timestamp = newTimestamp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REPOSITORY__TIMESTAMP, oldTimestamp, timestamp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Artifact> getAllArtifacts() {
		List<Artifact> allArtifacts = new LinkedList<>();
		allArtifacts.addAll(getArtifacts());
		getRepositories().stream().map(ArtifactRepository::getArtifacts).flatMap(List::stream).collect(Collectors.toCollection(()-> allArtifacts));
		return ECollections.asEList(allArtifacts);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isLoaded() {
		return loaded;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLoaded(boolean newLoaded) {
		boolean oldLoaded = loaded;
		loaded = newLoaded;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT_REPOSITORY__LOADED, oldLoaded, loaded));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Artifact getArtifactById(final String artifactId) {
		return getArtifacts().stream().filter(a->a != null && a.getName().equals(artifactId)).findFirst().orElse(null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Artifact> getArtifactsById(final String artifactId) {
		List<Artifact> artifacts = getArtifacts().stream().filter(a->a != null && a.getName().equals(artifactId)).collect(Collectors.toList());
		return ECollections.asEList(artifacts);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Artifact> getArtifactsByName(final String name) {
		List<Artifact> artifacts = getArtifacts().stream().filter(a->a != null && a.getLabel().equals(name)).collect(Collectors.toList());
		if (artifacts == null || artifacts.isEmpty()) {
			return  ECollections.emptyEList();
		}
		return ECollections.asEList(artifacts);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Artifact> getAllArtifactsById(final String artifactId) {
		List<Artifact> artifacts = getAllArtifacts().stream().filter(a->a != null && a.getName().equals(artifactId)).collect(Collectors.toList());
		return ECollections.asEList(artifacts);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ArtifactsPackage.ARTIFACT_REPOSITORY__ARTIFACTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getArtifacts()).basicAdd(otherEnd, msgs);
			case ArtifactsPackage.ARTIFACT_REPOSITORY__REPOSITORIES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getRepositories()).basicAdd(otherEnd, msgs);
			case ArtifactsPackage.ARTIFACT_REPOSITORY__PARENT:
				if (parent != null)
					msgs = ((InternalEObject)parent).eInverseRemove(this, ArtifactsPackage.ARTIFACT_REPOSITORY__REPOSITORIES, ArtifactRepository.class, msgs);
				return basicSetParent((ArtifactRepository)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ArtifactsPackage.ARTIFACT_REPOSITORY__ARTIFACTS:
				return ((InternalEList<?>)getArtifacts()).basicRemove(otherEnd, msgs);
			case ArtifactsPackage.ARTIFACT_REPOSITORY__REPOSITORIES:
				return ((InternalEList<?>)getRepositories()).basicRemove(otherEnd, msgs);
			case ArtifactsPackage.ARTIFACT_REPOSITORY__PARENT:
				return basicSetParent(null, msgs);
			case ArtifactsPackage.ARTIFACT_REPOSITORY__REPOSITORY_PROPERTIES:
				return ((InternalEList<?>)getRepositoryProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArtifactsPackage.ARTIFACT_REPOSITORY__ID:
				return getId();
			case ArtifactsPackage.ARTIFACT_REPOSITORY__NAME:
				return getName();
			case ArtifactsPackage.ARTIFACT_REPOSITORY__VERSION:
				return getVersion();
			case ArtifactsPackage.ARTIFACT_REPOSITORY__TYPE:
				return getType();
			case ArtifactsPackage.ARTIFACT_REPOSITORY__LOCATION:
				return getLocation();
			case ArtifactsPackage.ARTIFACT_REPOSITORY__ARTIFACTS:
				return getArtifacts();
			case ArtifactsPackage.ARTIFACT_REPOSITORY__REPOSITORIES:
				return getRepositories();
			case ArtifactsPackage.ARTIFACT_REPOSITORY__PARENT:
				if (resolve) return getParent();
				return basicGetParent();
			case ArtifactsPackage.ARTIFACT_REPOSITORY__PROPERTIES:
				return getProperties();
			case ArtifactsPackage.ARTIFACT_REPOSITORY__REPOSITORY_PROPERTIES:
				return getRepositoryProperties();
			case ArtifactsPackage.ARTIFACT_REPOSITORY__LOCATION_URI:
				return getLocationUri();
			case ArtifactsPackage.ARTIFACT_REPOSITORY__TIMESTAMP:
				return getTimestamp();
			case ArtifactsPackage.ARTIFACT_REPOSITORY__ALL_ARTIFACTS:
				return getAllArtifacts();
			case ArtifactsPackage.ARTIFACT_REPOSITORY__LOADED:
				return isLoaded();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArtifactsPackage.ARTIFACT_REPOSITORY__ID:
				setId((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__NAME:
				setName((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__VERSION:
				setVersion((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__TYPE:
				setType((RepositoryType)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__LOCATION:
				setLocation((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__ARTIFACTS:
				getArtifacts().clear();
				getArtifacts().addAll((Collection<? extends Artifact>)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__REPOSITORIES:
				getRepositories().clear();
				getRepositories().addAll((Collection<? extends ArtifactRepository>)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__PARENT:
				setParent((ArtifactRepository)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__REPOSITORY_PROPERTIES:
				getRepositoryProperties().clear();
				getRepositoryProperties().addAll((Collection<? extends Property>)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__TIMESTAMP:
				setTimestamp((Date)newValue);
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__LOADED:
				setLoaded((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArtifactsPackage.ARTIFACT_REPOSITORY__ID:
				setId(ID_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__LOCATION:
				setLocation(LOCATION_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__ARTIFACTS:
				getArtifacts().clear();
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__REPOSITORIES:
				getRepositories().clear();
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__PARENT:
				setParent((ArtifactRepository)null);
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__REPOSITORY_PROPERTIES:
				getRepositoryProperties().clear();
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__TIMESTAMP:
				setTimestamp(TIMESTAMP_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__LOADED:
				setLoaded(LOADED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArtifactsPackage.ARTIFACT_REPOSITORY__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case ArtifactsPackage.ARTIFACT_REPOSITORY__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ArtifactsPackage.ARTIFACT_REPOSITORY__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
			case ArtifactsPackage.ARTIFACT_REPOSITORY__TYPE:
				return type != TYPE_EDEFAULT;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__LOCATION:
				return LOCATION_EDEFAULT == null ? location != null : !LOCATION_EDEFAULT.equals(location);
			case ArtifactsPackage.ARTIFACT_REPOSITORY__ARTIFACTS:
				return artifacts != null && !artifacts.isEmpty();
			case ArtifactsPackage.ARTIFACT_REPOSITORY__REPOSITORIES:
				return repositories != null && !repositories.isEmpty();
			case ArtifactsPackage.ARTIFACT_REPOSITORY__PARENT:
				return parent != null;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__PROPERTIES:
				return getProperties() != null;
			case ArtifactsPackage.ARTIFACT_REPOSITORY__REPOSITORY_PROPERTIES:
				return repositoryProperties != null && !repositoryProperties.isEmpty();
			case ArtifactsPackage.ARTIFACT_REPOSITORY__LOCATION_URI:
				return LOCATION_URI_EDEFAULT == null ? getLocationUri() != null : !LOCATION_URI_EDEFAULT.equals(getLocationUri());
			case ArtifactsPackage.ARTIFACT_REPOSITORY__TIMESTAMP:
				return TIMESTAMP_EDEFAULT == null ? timestamp != null : !TIMESTAMP_EDEFAULT.equals(timestamp);
			case ArtifactsPackage.ARTIFACT_REPOSITORY__ALL_ARTIFACTS:
				return !getAllArtifacts().isEmpty();
			case ArtifactsPackage.ARTIFACT_REPOSITORY__LOADED:
				return loaded != LOADED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ArtifactsPackage.ARTIFACT_REPOSITORY___GET_ARTIFACT_BY_ID__STRING:
				return getArtifactById((String)arguments.get(0));
			case ArtifactsPackage.ARTIFACT_REPOSITORY___GET_ARTIFACTS_BY_ID__STRING:
				return getArtifactsById((String)arguments.get(0));
			case ArtifactsPackage.ARTIFACT_REPOSITORY___GET_ARTIFACTS_BY_NAME__STRING:
				return getArtifactsByName((String)arguments.get(0));
			case ArtifactsPackage.ARTIFACT_REPOSITORY___GET_ALL_ARTIFACTS_BY_ID__STRING:
				return getAllArtifactsById((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", name: ");
		result.append(name);
		result.append(", version: ");
		result.append(version);
		result.append(", type: ");
		result.append(type);
		result.append(", location: ");
		result.append(location);
		result.append(", timestamp: ");
		result.append(timestamp);
		result.append(", loaded: ");
		result.append(loaded);
		result.append(')');
		return result.toString();
	}

} //ArtifactRepositoryImpl
