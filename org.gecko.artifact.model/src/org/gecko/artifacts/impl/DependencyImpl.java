/**
 */
package org.gecko.artifacts.impl;

import java.util.Collection;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.gecko.artifacts.ArtifactsPackage;
import org.gecko.artifacts.Dependency;
import org.gecko.artifacts.DependencyType;
import org.gecko.artifacts.OSGiBundle;
import org.gecko.artifacts.Property;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dependency</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.artifacts.impl.DependencyImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.DependencyImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.DependencyImpl#getSource <em>Source</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.DependencyImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.DependencyImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.DependencyImpl#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.DependencyImpl#isOptional <em>Optional</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.DependencyImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.DependencyImpl#isReexport <em>Reexport</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.DependencyImpl#getObject <em>Object</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.DependencyImpl#getFilter <em>Filter</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.DependencyImpl#isMultiple <em>Multiple</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.DependencyImpl#isActive <em>Active</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.DependencyImpl#getDependencyProperty <em>Dependency Property</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.DependencyImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.DependencyImpl#getObjectClass <em>Object Class</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.DependencyImpl#getId <em>Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DependencyImpl extends MinimalEObjectImpl.Container implements Dependency {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final DependencyType TYPE_EDEFAULT = DependencyType.OTHER;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected DependencyType type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected OSGiBundle source;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected OSGiBundle target;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibility()
	 * @generated
	 * @ordered
	 */
	protected static final String VISIBILITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibility()
	 * @generated
	 * @ordered
	 */
	protected String visibility = VISIBILITY_EDEFAULT;

	/**
	 * The default value of the '{@link #isOptional() <em>Optional</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOptional()
	 * @generated
	 * @ordered
	 */
	protected static final boolean OPTIONAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isOptional() <em>Optional</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOptional()
	 * @generated
	 * @ordered
	 */
	protected boolean optional = OPTIONAL_EDEFAULT;

	/**
	 * The default value of the '{@link #getLocation() <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected String location = LOCATION_EDEFAULT;

	/**
	 * The default value of the '{@link #isReexport() <em>Reexport</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isReexport()
	 * @generated
	 * @ordered
	 */
	protected static final boolean REEXPORT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isReexport() <em>Reexport</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isReexport()
	 * @generated
	 * @ordered
	 */
	protected boolean reexport = REEXPORT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getObject() <em>Object</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObject()
	 * @generated
	 * @ordered
	 */
	protected EObject object;

	/**
	 * The default value of the '{@link #getFilter() <em>Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilter()
	 * @generated
	 * @ordered
	 */
	protected static final String FILTER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFilter() <em>Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilter()
	 * @generated
	 * @ordered
	 */
	protected String filter = FILTER_EDEFAULT;

	/**
	 * The default value of the '{@link #isMultiple() <em>Multiple</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMultiple()
	 * @generated
	 * @ordered
	 */
	protected static final boolean MULTIPLE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isMultiple() <em>Multiple</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMultiple()
	 * @generated
	 * @ordered
	 */
	protected boolean multiple = MULTIPLE_EDEFAULT;

	/**
	 * The default value of the '{@link #isActive() <em>Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isActive()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ACTIVE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isActive() <em>Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isActive()
	 * @generated
	 * @ordered
	 */
	protected boolean active = ACTIVE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDependencyProperty() <em>Dependency Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependencyProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<Property> dependencyProperty;

	/**
	 * The default value of the '{@link #getObjectClass() <em>Object Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectClass()
	 * @generated
	 * @ordered
	 */
	protected static final String OBJECT_CLASS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getObjectClass() <em>Object Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectClass()
	 * @generated
	 * @ordered
	 */
	protected String objectClass = OBJECT_CLASS_EDEFAULT;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DependencyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArtifactsPackage.Literals.DEPENDENCY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.DEPENDENCY__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DependencyType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(DependencyType newType) {
		DependencyType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.DEPENDENCY__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OSGiBundle getSource() {
		if (source != null && source.eIsProxy()) {
			InternalEObject oldSource = (InternalEObject)source;
			source = (OSGiBundle)eResolveProxy(oldSource);
			if (source != oldSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ArtifactsPackage.DEPENDENCY__SOURCE, oldSource, source));
			}
		}
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSGiBundle basicGetSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSource(OSGiBundle newSource) {
		OSGiBundle oldSource = source;
		source = newSource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.DEPENDENCY__SOURCE, oldSource, source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OSGiBundle getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject)target;
			target = (OSGiBundle)eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ArtifactsPackage.DEPENDENCY__TARGET, oldTarget, target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSGiBundle basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTarget(OSGiBundle newTarget) {
		OSGiBundle oldTarget = target;
		target = newTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.DEPENDENCY__TARGET, oldTarget, target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.DEPENDENCY__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVisibility() {
		return visibility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVisibility(String newVisibility) {
		String oldVisibility = visibility;
		visibility = newVisibility;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.DEPENDENCY__VISIBILITY, oldVisibility, visibility));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isOptional() {
		return optional;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOptional(boolean newOptional) {
		boolean oldOptional = optional;
		optional = newOptional;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.DEPENDENCY__OPTIONAL, oldOptional, optional));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLocation(String newLocation) {
		String oldLocation = location;
		location = newLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.DEPENDENCY__LOCATION, oldLocation, location));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isReexport() {
		return reexport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setReexport(boolean newReexport) {
		boolean oldReexport = reexport;
		reexport = newReexport;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.DEPENDENCY__REEXPORT, oldReexport, reexport));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject getObject() {
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObject(EObject newObject, NotificationChain msgs) {
		EObject oldObject = object;
		object = newObject;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ArtifactsPackage.DEPENDENCY__OBJECT, oldObject, newObject);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setObject(EObject newObject) {
		if (newObject != object) {
			NotificationChain msgs = null;
			if (object != null)
				msgs = ((InternalEObject)object).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ArtifactsPackage.DEPENDENCY__OBJECT, null, msgs);
			if (newObject != null)
				msgs = ((InternalEObject)newObject).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ArtifactsPackage.DEPENDENCY__OBJECT, null, msgs);
			msgs = basicSetObject(newObject, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.DEPENDENCY__OBJECT, newObject, newObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFilter() {
		return filter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFilter(String newFilter) {
		String oldFilter = filter;
		filter = newFilter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.DEPENDENCY__FILTER, oldFilter, filter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isMultiple() {
		return multiple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMultiple(boolean newMultiple) {
		boolean oldMultiple = multiple;
		multiple = newMultiple;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.DEPENDENCY__MULTIPLE, oldMultiple, multiple));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isActive() {
		return active;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setActive(boolean newActive) {
		boolean oldActive = active;
		active = newActive;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.DEPENDENCY__ACTIVE, oldActive, active));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Property> getDependencyProperty() {
		if (dependencyProperty == null) {
			dependencyProperty = new EObjectContainmentEList<Property>(Property.class, this, ArtifactsPackage.DEPENDENCY__DEPENDENCY_PROPERTY);
		}
		return dependencyProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Map<String, String> getProperties() {
		return getDependencyProperty().stream().collect(java.util.stream.Collectors.toMap(Property::getKey, Property::getValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getObjectClass() {
		return objectClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setObjectClass(String newObjectClass) {
		String oldObjectClass = objectClass;
		objectClass = newObjectClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.DEPENDENCY__OBJECT_CLASS, oldObjectClass, objectClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.DEPENDENCY__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ArtifactsPackage.DEPENDENCY__OBJECT:
				return basicSetObject(null, msgs);
			case ArtifactsPackage.DEPENDENCY__DEPENDENCY_PROPERTY:
				return ((InternalEList<?>)getDependencyProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArtifactsPackage.DEPENDENCY__NAME:
				return getName();
			case ArtifactsPackage.DEPENDENCY__TYPE:
				return getType();
			case ArtifactsPackage.DEPENDENCY__SOURCE:
				if (resolve) return getSource();
				return basicGetSource();
			case ArtifactsPackage.DEPENDENCY__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
			case ArtifactsPackage.DEPENDENCY__VERSION:
				return getVersion();
			case ArtifactsPackage.DEPENDENCY__VISIBILITY:
				return getVisibility();
			case ArtifactsPackage.DEPENDENCY__OPTIONAL:
				return isOptional();
			case ArtifactsPackage.DEPENDENCY__LOCATION:
				return getLocation();
			case ArtifactsPackage.DEPENDENCY__REEXPORT:
				return isReexport();
			case ArtifactsPackage.DEPENDENCY__OBJECT:
				return getObject();
			case ArtifactsPackage.DEPENDENCY__FILTER:
				return getFilter();
			case ArtifactsPackage.DEPENDENCY__MULTIPLE:
				return isMultiple();
			case ArtifactsPackage.DEPENDENCY__ACTIVE:
				return isActive();
			case ArtifactsPackage.DEPENDENCY__DEPENDENCY_PROPERTY:
				return getDependencyProperty();
			case ArtifactsPackage.DEPENDENCY__PROPERTIES:
				return getProperties();
			case ArtifactsPackage.DEPENDENCY__OBJECT_CLASS:
				return getObjectClass();
			case ArtifactsPackage.DEPENDENCY__ID:
				return getId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArtifactsPackage.DEPENDENCY__NAME:
				setName((String)newValue);
				return;
			case ArtifactsPackage.DEPENDENCY__TYPE:
				setType((DependencyType)newValue);
				return;
			case ArtifactsPackage.DEPENDENCY__SOURCE:
				setSource((OSGiBundle)newValue);
				return;
			case ArtifactsPackage.DEPENDENCY__TARGET:
				setTarget((OSGiBundle)newValue);
				return;
			case ArtifactsPackage.DEPENDENCY__VERSION:
				setVersion((String)newValue);
				return;
			case ArtifactsPackage.DEPENDENCY__VISIBILITY:
				setVisibility((String)newValue);
				return;
			case ArtifactsPackage.DEPENDENCY__OPTIONAL:
				setOptional((Boolean)newValue);
				return;
			case ArtifactsPackage.DEPENDENCY__LOCATION:
				setLocation((String)newValue);
				return;
			case ArtifactsPackage.DEPENDENCY__REEXPORT:
				setReexport((Boolean)newValue);
				return;
			case ArtifactsPackage.DEPENDENCY__OBJECT:
				setObject((EObject)newValue);
				return;
			case ArtifactsPackage.DEPENDENCY__FILTER:
				setFilter((String)newValue);
				return;
			case ArtifactsPackage.DEPENDENCY__MULTIPLE:
				setMultiple((Boolean)newValue);
				return;
			case ArtifactsPackage.DEPENDENCY__ACTIVE:
				setActive((Boolean)newValue);
				return;
			case ArtifactsPackage.DEPENDENCY__DEPENDENCY_PROPERTY:
				getDependencyProperty().clear();
				getDependencyProperty().addAll((Collection<? extends Property>)newValue);
				return;
			case ArtifactsPackage.DEPENDENCY__OBJECT_CLASS:
				setObjectClass((String)newValue);
				return;
			case ArtifactsPackage.DEPENDENCY__ID:
				setId((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArtifactsPackage.DEPENDENCY__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ArtifactsPackage.DEPENDENCY__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case ArtifactsPackage.DEPENDENCY__SOURCE:
				setSource((OSGiBundle)null);
				return;
			case ArtifactsPackage.DEPENDENCY__TARGET:
				setTarget((OSGiBundle)null);
				return;
			case ArtifactsPackage.DEPENDENCY__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
			case ArtifactsPackage.DEPENDENCY__VISIBILITY:
				setVisibility(VISIBILITY_EDEFAULT);
				return;
			case ArtifactsPackage.DEPENDENCY__OPTIONAL:
				setOptional(OPTIONAL_EDEFAULT);
				return;
			case ArtifactsPackage.DEPENDENCY__LOCATION:
				setLocation(LOCATION_EDEFAULT);
				return;
			case ArtifactsPackage.DEPENDENCY__REEXPORT:
				setReexport(REEXPORT_EDEFAULT);
				return;
			case ArtifactsPackage.DEPENDENCY__OBJECT:
				setObject((EObject)null);
				return;
			case ArtifactsPackage.DEPENDENCY__FILTER:
				setFilter(FILTER_EDEFAULT);
				return;
			case ArtifactsPackage.DEPENDENCY__MULTIPLE:
				setMultiple(MULTIPLE_EDEFAULT);
				return;
			case ArtifactsPackage.DEPENDENCY__ACTIVE:
				setActive(ACTIVE_EDEFAULT);
				return;
			case ArtifactsPackage.DEPENDENCY__DEPENDENCY_PROPERTY:
				getDependencyProperty().clear();
				return;
			case ArtifactsPackage.DEPENDENCY__OBJECT_CLASS:
				setObjectClass(OBJECT_CLASS_EDEFAULT);
				return;
			case ArtifactsPackage.DEPENDENCY__ID:
				setId(ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArtifactsPackage.DEPENDENCY__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ArtifactsPackage.DEPENDENCY__TYPE:
				return type != TYPE_EDEFAULT;
			case ArtifactsPackage.DEPENDENCY__SOURCE:
				return source != null;
			case ArtifactsPackage.DEPENDENCY__TARGET:
				return target != null;
			case ArtifactsPackage.DEPENDENCY__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
			case ArtifactsPackage.DEPENDENCY__VISIBILITY:
				return VISIBILITY_EDEFAULT == null ? visibility != null : !VISIBILITY_EDEFAULT.equals(visibility);
			case ArtifactsPackage.DEPENDENCY__OPTIONAL:
				return optional != OPTIONAL_EDEFAULT;
			case ArtifactsPackage.DEPENDENCY__LOCATION:
				return LOCATION_EDEFAULT == null ? location != null : !LOCATION_EDEFAULT.equals(location);
			case ArtifactsPackage.DEPENDENCY__REEXPORT:
				return reexport != REEXPORT_EDEFAULT;
			case ArtifactsPackage.DEPENDENCY__OBJECT:
				return object != null;
			case ArtifactsPackage.DEPENDENCY__FILTER:
				return FILTER_EDEFAULT == null ? filter != null : !FILTER_EDEFAULT.equals(filter);
			case ArtifactsPackage.DEPENDENCY__MULTIPLE:
				return multiple != MULTIPLE_EDEFAULT;
			case ArtifactsPackage.DEPENDENCY__ACTIVE:
				return active != ACTIVE_EDEFAULT;
			case ArtifactsPackage.DEPENDENCY__DEPENDENCY_PROPERTY:
				return dependencyProperty != null && !dependencyProperty.isEmpty();
			case ArtifactsPackage.DEPENDENCY__PROPERTIES:
				return getProperties() != null;
			case ArtifactsPackage.DEPENDENCY__OBJECT_CLASS:
				return OBJECT_CLASS_EDEFAULT == null ? objectClass != null : !OBJECT_CLASS_EDEFAULT.equals(objectClass);
			case ArtifactsPackage.DEPENDENCY__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", type: ");
		result.append(type);
		result.append(", version: ");
		result.append(version);
		result.append(", visibility: ");
		result.append(visibility);
		result.append(", optional: ");
		result.append(optional);
		result.append(", location: ");
		result.append(location);
		result.append(", reexport: ");
		result.append(reexport);
		result.append(", filter: ");
		result.append(filter);
		result.append(", multiple: ");
		result.append(multiple);
		result.append(", active: ");
		result.append(active);
		result.append(", objectClass: ");
		result.append(objectClass);
		result.append(", id: ");
		result.append(id);
		result.append(')');
		return result.toString();
	}

} //DependencyImpl
