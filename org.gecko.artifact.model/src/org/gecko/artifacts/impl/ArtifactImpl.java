/**
 */
package org.gecko.artifacts.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.gecko.artifacts.Artifact;
import org.gecko.artifacts.ArtifactReference;
import org.gecko.artifacts.ArtifactRepository;
import org.gecko.artifacts.ArtifactsFactory;
import org.gecko.artifacts.ArtifactsPackage;
import org.gecko.artifacts.Property;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Artifact</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactImpl#getSourceLocation <em>Source Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactImpl#getCacheLocation <em>Cache Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactImpl#getRepository <em>Repository</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactImpl#getLicense <em>License</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactImpl#getCopyright <em>Copyright</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactImpl#getArtifactProperties <em>Artifact Properties</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactImpl#getSize <em>Size</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactImpl#getHash <em>Hash</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactImpl#getTimestamp <em>Timestamp</em>}</li>
 *   <li>{@link org.gecko.artifacts.impl.ArtifactImpl#getReferences <em>References</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ArtifactImpl extends MinimalEObjectImpl.Container implements Artifact {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getSourceLocation() <em>Source Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_LOCATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourceLocation() <em>Source Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceLocation()
	 * @generated
	 * @ordered
	 */
	protected String sourceLocation = SOURCE_LOCATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getCacheLocation() <em>Cache Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCacheLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String CACHE_LOCATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCacheLocation() <em>Cache Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCacheLocation()
	 * @generated
	 * @ordered
	 */
	protected String cacheLocation = CACHE_LOCATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRepository() <em>Repository</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepository()
	 * @generated
	 * @ordered
	 */
	protected ArtifactRepository repository;

	/**
	 * The default value of the '{@link #getLicense() <em>License</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLicense()
	 * @generated
	 * @ordered
	 */
	protected static final String LICENSE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLicense() <em>License</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLicense()
	 * @generated
	 * @ordered
	 */
	protected String license = LICENSE_EDEFAULT;

	/**
	 * The default value of the '{@link #getCopyright() <em>Copyright</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCopyright()
	 * @generated
	 * @ordered
	 */
	protected static final String COPYRIGHT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCopyright() <em>Copyright</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCopyright()
	 * @generated
	 * @ordered
	 */
	protected String copyright = COPYRIGHT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getArtifactProperties() <em>Artifact Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArtifactProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<Property> artifactProperties;

	/**
	 * The default value of the '{@link #getSize() <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected static final int SIZE_EDEFAULT = -1;

	/**
	 * The cached value of the '{@link #getSize() <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected int size = SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getHash() <em>Hash</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHash()
	 * @generated
	 * @ordered
	 */
	protected static final String HASH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHash() <em>Hash</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHash()
	 * @generated
	 * @ordered
	 */
	protected String hash = HASH_EDEFAULT;

	/**
	 * The default value of the '{@link #getTimestamp() <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimestamp()
	 * @generated
	 * @ordered
	 */
	protected static final Date TIMESTAMP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTimestamp() <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimestamp()
	 * @generated
	 * @ordered
	 */
	protected Date timestamp = TIMESTAMP_EDEFAULT;

	/**
	 * The cached value of the '{@link #getReferences() <em>References</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferences()
	 * @generated
	 * @ordered
	 */
	protected EList<ArtifactReference> references;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArtifactImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArtifactsPackage.Literals.ARTIFACT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT__LABEL, oldLabel, label));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSourceLocation() {
		return sourceLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSourceLocation(String newSourceLocation) {
		String oldSourceLocation = sourceLocation;
		sourceLocation = newSourceLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT__SOURCE_LOCATION, oldSourceLocation, sourceLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCacheLocation() {
		return cacheLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCacheLocation(String newCacheLocation) {
		String oldCacheLocation = cacheLocation;
		cacheLocation = newCacheLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT__CACHE_LOCATION, oldCacheLocation, cacheLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ArtifactRepository getRepository() {
		if (repository != null && repository.eIsProxy()) {
			InternalEObject oldRepository = (InternalEObject)repository;
			repository = (ArtifactRepository)eResolveProxy(oldRepository);
			if (repository != oldRepository) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ArtifactsPackage.ARTIFACT__REPOSITORY, oldRepository, repository));
			}
		}
		return repository;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArtifactRepository basicGetRepository() {
		return repository;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRepository(ArtifactRepository newRepository, NotificationChain msgs) {
		ArtifactRepository oldRepository = repository;
		repository = newRepository;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT__REPOSITORY, oldRepository, newRepository);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRepository(ArtifactRepository newRepository) {
		if (newRepository != repository) {
			NotificationChain msgs = null;
			if (repository != null)
				msgs = ((InternalEObject)repository).eInverseRemove(this, ArtifactsPackage.ARTIFACT_REPOSITORY__ARTIFACTS, ArtifactRepository.class, msgs);
			if (newRepository != null)
				msgs = ((InternalEObject)newRepository).eInverseAdd(this, ArtifactsPackage.ARTIFACT_REPOSITORY__ARTIFACTS, ArtifactRepository.class, msgs);
			msgs = basicSetRepository(newRepository, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT__REPOSITORY, newRepository, newRepository));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLicense() {
		return license;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLicense(String newLicense) {
		String oldLicense = license;
		license = newLicense;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT__LICENSE, oldLicense, license));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCopyright() {
		return copyright;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCopyright(String newCopyright) {
		String oldCopyright = copyright;
		copyright = newCopyright;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT__COPYRIGHT, oldCopyright, copyright));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Map<String, String> getProperties() {
		return getArtifactProperties().stream().collect(java.util.stream.Collectors.toMap(Property::getKey, Property::getValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Property> getArtifactProperties() {
		if (artifactProperties == null) {
			artifactProperties = new EObjectContainmentEList<Property>(Property.class, this, ArtifactsPackage.ARTIFACT__ARTIFACT_PROPERTIES);
		}
		return artifactProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getSize() {
		return size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSize(int newSize) {
		int oldSize = size;
		size = newSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT__SIZE, oldSize, size));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getHash() {
		return hash;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setHash(String newHash) {
		String oldHash = hash;
		hash = newHash;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT__HASH, oldHash, hash));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTimestamp(Date newTimestamp) {
		Date oldTimestamp = timestamp;
		timestamp = newTimestamp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT__TIMESTAMP, oldTimestamp, timestamp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ArtifactsPackage.ARTIFACT__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ArtifactReference> getReferences() {
		if (references == null) {
			references = new EObjectResolvingEList<ArtifactReference>(ArtifactReference.class, this, ArtifactsPackage.ARTIFACT__REFERENCES);
		}
		return references;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void putProperty(final String key, final String value) {
		Property p = ArtifactsFactory.eINSTANCE.createProperty();
		p.setKey(key);
		p.setValue(value);
		getArtifactProperties().add(p);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ArtifactsPackage.ARTIFACT__REPOSITORY:
				if (repository != null)
					msgs = ((InternalEObject)repository).eInverseRemove(this, ArtifactsPackage.ARTIFACT_REPOSITORY__ARTIFACTS, ArtifactRepository.class, msgs);
				return basicSetRepository((ArtifactRepository)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ArtifactsPackage.ARTIFACT__REPOSITORY:
				return basicSetRepository(null, msgs);
			case ArtifactsPackage.ARTIFACT__ARTIFACT_PROPERTIES:
				return ((InternalEList<?>)getArtifactProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArtifactsPackage.ARTIFACT__ID:
				return getId();
			case ArtifactsPackage.ARTIFACT__NAME:
				return getName();
			case ArtifactsPackage.ARTIFACT__LABEL:
				return getLabel();
			case ArtifactsPackage.ARTIFACT__DESCRIPTION:
				return getDescription();
			case ArtifactsPackage.ARTIFACT__VERSION:
				return getVersion();
			case ArtifactsPackage.ARTIFACT__SOURCE_LOCATION:
				return getSourceLocation();
			case ArtifactsPackage.ARTIFACT__CACHE_LOCATION:
				return getCacheLocation();
			case ArtifactsPackage.ARTIFACT__REPOSITORY:
				if (resolve) return getRepository();
				return basicGetRepository();
			case ArtifactsPackage.ARTIFACT__LICENSE:
				return getLicense();
			case ArtifactsPackage.ARTIFACT__COPYRIGHT:
				return getCopyright();
			case ArtifactsPackage.ARTIFACT__PROPERTIES:
				return getProperties();
			case ArtifactsPackage.ARTIFACT__ARTIFACT_PROPERTIES:
				return getArtifactProperties();
			case ArtifactsPackage.ARTIFACT__SIZE:
				return getSize();
			case ArtifactsPackage.ARTIFACT__HASH:
				return getHash();
			case ArtifactsPackage.ARTIFACT__TIMESTAMP:
				return getTimestamp();
			case ArtifactsPackage.ARTIFACT__REFERENCES:
				return getReferences();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArtifactsPackage.ARTIFACT__ID:
				setId((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT__NAME:
				setName((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT__LABEL:
				setLabel((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT__VERSION:
				setVersion((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT__SOURCE_LOCATION:
				setSourceLocation((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT__CACHE_LOCATION:
				setCacheLocation((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT__REPOSITORY:
				setRepository((ArtifactRepository)newValue);
				return;
			case ArtifactsPackage.ARTIFACT__LICENSE:
				setLicense((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT__COPYRIGHT:
				setCopyright((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT__ARTIFACT_PROPERTIES:
				getArtifactProperties().clear();
				getArtifactProperties().addAll((Collection<? extends Property>)newValue);
				return;
			case ArtifactsPackage.ARTIFACT__SIZE:
				setSize((Integer)newValue);
				return;
			case ArtifactsPackage.ARTIFACT__HASH:
				setHash((String)newValue);
				return;
			case ArtifactsPackage.ARTIFACT__TIMESTAMP:
				setTimestamp((Date)newValue);
				return;
			case ArtifactsPackage.ARTIFACT__REFERENCES:
				getReferences().clear();
				getReferences().addAll((Collection<? extends ArtifactReference>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArtifactsPackage.ARTIFACT__ID:
				setId(ID_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT__LABEL:
				setLabel(LABEL_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT__SOURCE_LOCATION:
				setSourceLocation(SOURCE_LOCATION_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT__CACHE_LOCATION:
				setCacheLocation(CACHE_LOCATION_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT__REPOSITORY:
				setRepository((ArtifactRepository)null);
				return;
			case ArtifactsPackage.ARTIFACT__LICENSE:
				setLicense(LICENSE_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT__COPYRIGHT:
				setCopyright(COPYRIGHT_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT__ARTIFACT_PROPERTIES:
				getArtifactProperties().clear();
				return;
			case ArtifactsPackage.ARTIFACT__SIZE:
				setSize(SIZE_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT__HASH:
				setHash(HASH_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT__TIMESTAMP:
				setTimestamp(TIMESTAMP_EDEFAULT);
				return;
			case ArtifactsPackage.ARTIFACT__REFERENCES:
				getReferences().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArtifactsPackage.ARTIFACT__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case ArtifactsPackage.ARTIFACT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ArtifactsPackage.ARTIFACT__LABEL:
				return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT.equals(label);
			case ArtifactsPackage.ARTIFACT__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case ArtifactsPackage.ARTIFACT__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
			case ArtifactsPackage.ARTIFACT__SOURCE_LOCATION:
				return SOURCE_LOCATION_EDEFAULT == null ? sourceLocation != null : !SOURCE_LOCATION_EDEFAULT.equals(sourceLocation);
			case ArtifactsPackage.ARTIFACT__CACHE_LOCATION:
				return CACHE_LOCATION_EDEFAULT == null ? cacheLocation != null : !CACHE_LOCATION_EDEFAULT.equals(cacheLocation);
			case ArtifactsPackage.ARTIFACT__REPOSITORY:
				return repository != null;
			case ArtifactsPackage.ARTIFACT__LICENSE:
				return LICENSE_EDEFAULT == null ? license != null : !LICENSE_EDEFAULT.equals(license);
			case ArtifactsPackage.ARTIFACT__COPYRIGHT:
				return COPYRIGHT_EDEFAULT == null ? copyright != null : !COPYRIGHT_EDEFAULT.equals(copyright);
			case ArtifactsPackage.ARTIFACT__PROPERTIES:
				return getProperties() != null;
			case ArtifactsPackage.ARTIFACT__ARTIFACT_PROPERTIES:
				return artifactProperties != null && !artifactProperties.isEmpty();
			case ArtifactsPackage.ARTIFACT__SIZE:
				return size != SIZE_EDEFAULT;
			case ArtifactsPackage.ARTIFACT__HASH:
				return HASH_EDEFAULT == null ? hash != null : !HASH_EDEFAULT.equals(hash);
			case ArtifactsPackage.ARTIFACT__TIMESTAMP:
				return TIMESTAMP_EDEFAULT == null ? timestamp != null : !TIMESTAMP_EDEFAULT.equals(timestamp);
			case ArtifactsPackage.ARTIFACT__REFERENCES:
				return references != null && !references.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ArtifactsPackage.ARTIFACT___PUT_PROPERTY__STRING_STRING:
				putProperty((String)arguments.get(0), (String)arguments.get(1));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", name: ");
		result.append(name);
		result.append(", label: ");
		result.append(label);
		result.append(", description: ");
		result.append(description);
		result.append(", version: ");
		result.append(version);
		result.append(", sourceLocation: ");
		result.append(sourceLocation);
		result.append(", cacheLocation: ");
		result.append(cacheLocation);
		result.append(", license: ");
		result.append(license);
		result.append(", copyright: ");
		result.append(copyright);
		result.append(", size: ");
		result.append(size);
		result.append(", hash: ");
		result.append(hash);
		result.append(", timestamp: ");
		result.append(timestamp);
		result.append(')');
		return result.toString();
	}

} //ArtifactImpl
