/**
 */
package org.gecko.artifacts;

import java.net.URI;
import java.util.Date;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Artifact Repository</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.artifacts.ArtifactRepository#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactRepository#getName <em>Name</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactRepository#getVersion <em>Version</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactRepository#getType <em>Type</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactRepository#getLocation <em>Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactRepository#getArtifacts <em>Artifacts</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactRepository#getRepositories <em>Repositories</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactRepository#getParent <em>Parent</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactRepository#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactRepository#getRepositoryProperties <em>Repository Properties</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactRepository#getLocationUri <em>Location Uri</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactRepository#getTimestamp <em>Timestamp</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactRepository#getAllArtifacts <em>All Artifacts</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactRepository#isLoaded <em>Loaded</em>}</li>
 * </ul>
 *
 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactRepository()
 * @model
 * @generated
 */
public interface ArtifactRepository extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactRepository_Id()
	 * @model id="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactRepository#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactRepository_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactRepository#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactRepository_Version()
	 * @model dataType="org.gecko.artifacts.Version"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactRepository#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.gecko.artifacts.RepositoryType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.gecko.artifacts.RepositoryType
	 * @see #setType(RepositoryType)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactRepository_Type()
	 * @model
	 * @generated
	 */
	RepositoryType getType();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactRepository#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.gecko.artifacts.RepositoryType
	 * @see #getType()
	 * @generated
	 */
	void setType(RepositoryType value);

	/**
	 * Returns the value of the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' attribute.
	 * @see #setLocation(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactRepository_Location()
	 * @model
	 * @generated
	 */
	String getLocation();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactRepository#getLocation <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' attribute.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(String value);

	/**
	 * Returns the value of the '<em><b>Artifacts</b></em>' reference list.
	 * The list contents are of type {@link org.gecko.artifacts.Artifact}.
	 * It is bidirectional and its opposite is '{@link org.gecko.artifacts.Artifact#getRepository <em>Repository</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Artifacts</em>' reference list.
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactRepository_Artifacts()
	 * @see org.gecko.artifacts.Artifact#getRepository
	 * @model opposite="repository" keys="id"
	 * @generated
	 */
	EList<Artifact> getArtifacts();

	/**
	 * Returns the value of the '<em><b>Repositories</b></em>' reference list.
	 * The list contents are of type {@link org.gecko.artifacts.ArtifactRepository}.
	 * It is bidirectional and its opposite is '{@link org.gecko.artifacts.ArtifactRepository#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Repositories</em>' reference list.
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactRepository_Repositories()
	 * @see org.gecko.artifacts.ArtifactRepository#getParent
	 * @model opposite="parent" keys="id"
	 * @generated
	 */
	EList<ArtifactRepository> getRepositories();

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.gecko.artifacts.ArtifactRepository#getRepositories <em>Repositories</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see #setParent(ArtifactRepository)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactRepository_Parent()
	 * @see org.gecko.artifacts.ArtifactRepository#getRepositories
	 * @model opposite="repositories"
	 * @generated
	 */
	ArtifactRepository getParent();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactRepository#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(ArtifactRepository value);

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' attribute.
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactRepository_Properties()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	Map<String, String> getProperties();

	/**
	 * Returns the value of the '<em><b>Repository Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.gecko.artifacts.Property}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Repository Properties</em>' containment reference list.
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactRepository_RepositoryProperties()
	 * @model containment="true"
	 * @generated
	 */
	EList<Property> getRepositoryProperties();

	/**
	 * Returns the value of the '<em><b>Location Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location Uri</em>' attribute.
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactRepository_LocationUri()
	 * @model dataType="org.gecko.artifacts.EJavaUri" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	URI getLocationUri();

	/**
	 * Returns the value of the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timestamp</em>' attribute.
	 * @see #setTimestamp(Date)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactRepository_Timestamp()
	 * @model
	 * @generated
	 */
	Date getTimestamp();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactRepository#getTimestamp <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timestamp</em>' attribute.
	 * @see #getTimestamp()
	 * @generated
	 */
	void setTimestamp(Date value);

	/**
	 * Returns the value of the '<em><b>All Artifacts</b></em>' reference list.
	 * The list contents are of type {@link org.gecko.artifacts.Artifact}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Artifacts</em>' reference list.
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactRepository_AllArtifacts()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Artifact> getAllArtifacts();

	/**
	 * Returns the value of the '<em><b>Loaded</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loaded</em>' attribute.
	 * @see #setLoaded(boolean)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactRepository_Loaded()
	 * @model default="false" transient="true"
	 * @generated
	 */
	boolean isLoaded();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactRepository#isLoaded <em>Loaded</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loaded</em>' attribute.
	 * @see #isLoaded()
	 * @generated
	 */
	void setLoaded(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model artifactIdRequired="true"
	 * @generated
	 */
	Artifact getArtifactById(String artifactId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model artifactIdRequired="true"
	 * @generated
	 */
	EList<Artifact> getArtifactsById(String artifactId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nameRequired="true"
	 * @generated
	 */
	EList<Artifact> getArtifactsByName(String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model artifactIdRequired="true"
	 * @generated
	 */
	EList<Artifact> getAllArtifactsById(String artifactId);

} // ArtifactRepository
