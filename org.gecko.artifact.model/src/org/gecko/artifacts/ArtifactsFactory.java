/**
 */
package org.gecko.artifacts;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.gecko.artifacts.ArtifactsPackage
 * @generated
 */
public interface ArtifactsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ArtifactsFactory eINSTANCE = org.gecko.artifacts.impl.ArtifactsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>OS Gi Bundle</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>OS Gi Bundle</em>'.
	 * @generated
	 */
	OSGiBundle createOSGiBundle();

	/**
	 * Returns a new object of class '<em>Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dependency</em>'.
	 * @generated
	 */
	Dependency createDependency();

	/**
	 * Returns a new object of class '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Property</em>'.
	 * @generated
	 */
	Property createProperty();

	/**
	 * Returns a new object of class '<em>Artifact</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Artifact</em>'.
	 * @generated
	 */
	Artifact createArtifact();

	/**
	 * Returns a new object of class '<em>Artifact Repository</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Artifact Repository</em>'.
	 * @generated
	 */
	ArtifactRepository createArtifactRepository();

	/**
	 * Returns a new object of class '<em>Artifact Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Artifact Reference</em>'.
	 * @generated
	 */
	ArtifactReference createArtifactReference();

	/**
	 * Returns a new object of class '<em>P2 Repository</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>P2 Repository</em>'.
	 * @generated
	 */
	P2Repository createP2Repository();

	/**
	 * Returns a new object of class '<em>Bundle Artifact</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bundle Artifact</em>'.
	 * @generated
	 */
	BundleArtifact createBundleArtifact();

	/**
	 * Returns a new object of class '<em>Binary Artifact</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Binary Artifact</em>'.
	 * @generated
	 */
	BinaryArtifact createBinaryArtifact();

	/**
	 * Returns a new object of class '<em>Eclipse Feature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Eclipse Feature</em>'.
	 * @generated
	 */
	EclipseFeature createEclipseFeature();

	/**
	 * Returns a new object of class '<em>Filter Rule</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Filter Rule</em>'.
	 * @generated
	 */
	FilterRule createFilterRule();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ArtifactsPackage getArtifactsPackage();

} //ArtifactsFactory
