/**
 */
package org.gecko.artifacts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bundle Artifact</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.gecko.artifacts.ArtifactsPackage#getBundleArtifact()
 * @model
 * @generated
 */
public interface BundleArtifact extends Artifact {
} // BundleArtifact
