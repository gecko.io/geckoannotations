/**
 */
package org.gecko.artifacts;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Reference Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.gecko.artifacts.ArtifactsPackage#getReferenceType()
 * @model
 * @generated
 */
public enum ReferenceType implements Enumerator {
	/**
	 * The '<em><b>OTHER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OTHER_VALUE
	 * @generated
	 * @ordered
	 */
	OTHER(100, "OTHER", "OTHER"),

	/**
	 * The '<em><b>IMPORT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IMPORT_VALUE
	 * @generated
	 * @ordered
	 */
	IMPORT(0, "IMPORT", "IMPORT"),

	/**
	 * The '<em><b>EXPORT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EXPORT_VALUE
	 * @generated
	 * @ordered
	 */
	EXPORT(1, "EXPORT", "EXPORT"),

	/**
	 * The '<em><b>REQUIRE BUNDLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUIRE_BUNDLE_VALUE
	 * @generated
	 * @ordered
	 */
	REQUIRE_BUNDLE(2, "REQUIRE_BUNDLE", "REQUIRE_BUNDLE"),

	/**
	 * The '<em><b>REQUIREMENT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUIREMENT_VALUE
	 * @generated
	 * @ordered
	 */
	REQUIREMENT(3, "REQUIREMENT", "REQUIREMENT"),

	/**
	 * The '<em><b>CAPABILITY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CAPABILITY_VALUE
	 * @generated
	 * @ordered
	 */
	CAPABILITY(4, "CAPABILITY", "CAPABILITY"),

	/**
	 * The '<em><b>DS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DS_VALUE
	 * @generated
	 * @ordered
	 */
	DS(5, "DS", "DS"),

	/**
	 * The '<em><b>CONFIGURATION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONFIGURATION_VALUE
	 * @generated
	 * @ordered
	 */
	CONFIGURATION(6, "CONFIGURATION", "CONFIGURATION"),

	/**
	 * The '<em><b>ECORE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ECORE_VALUE
	 * @generated
	 * @ordered
	 */
	ECORE(7, "ECORE", "ECORE"),

	/**
	 * The '<em><b>PROJECT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PROJECT_VALUE
	 * @generated
	 * @ordered
	 */
	PROJECT(8, "PROJECT", "PROJECT"), /**
	 * The '<em><b>INCLUDE ARTIFACT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INCLUDE_ARTIFACT_VALUE
	 * @generated
	 * @ordered
	 */
	INCLUDE_ARTIFACT(9, "INCLUDE_ARTIFACT", "INCLUDE_ARTIFACT"), /**
	 * The '<em><b>REQUIRE ARTIFACT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUIRE_ARTIFACT_VALUE
	 * @generated
	 * @ordered
	 */
	REQUIRE_ARTIFACT(10, "REQUIRE_ARTIFACT", "REQUIRE_ARTIFACT");

	/**
	 * The '<em><b>OTHER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OTHER
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int OTHER_VALUE = 100;

	/**
	 * The '<em><b>IMPORT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IMPORT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int IMPORT_VALUE = 0;

	/**
	 * The '<em><b>EXPORT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EXPORT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int EXPORT_VALUE = 1;

	/**
	 * The '<em><b>REQUIRE BUNDLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUIRE_BUNDLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int REQUIRE_BUNDLE_VALUE = 2;

	/**
	 * The '<em><b>REQUIREMENT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUIREMENT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int REQUIREMENT_VALUE = 3;

	/**
	 * The '<em><b>CAPABILITY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CAPABILITY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CAPABILITY_VALUE = 4;

	/**
	 * The '<em><b>DS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DS_VALUE = 5;

	/**
	 * The '<em><b>CONFIGURATION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONFIGURATION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CONFIGURATION_VALUE = 6;

	/**
	 * The '<em><b>ECORE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ECORE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ECORE_VALUE = 7;

	/**
	 * The '<em><b>PROJECT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PROJECT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PROJECT_VALUE = 8;

	/**
	 * The '<em><b>INCLUDE ARTIFACT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INCLUDE_ARTIFACT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int INCLUDE_ARTIFACT_VALUE = 9;

	/**
	 * The '<em><b>REQUIRE ARTIFACT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUIRE_ARTIFACT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int REQUIRE_ARTIFACT_VALUE = 10;

	/**
	 * An array of all the '<em><b>Reference Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ReferenceType[] VALUES_ARRAY =
		new ReferenceType[] {
			OTHER,
			IMPORT,
			EXPORT,
			REQUIRE_BUNDLE,
			REQUIREMENT,
			CAPABILITY,
			DS,
			CONFIGURATION,
			ECORE,
			PROJECT,
			INCLUDE_ARTIFACT,
			REQUIRE_ARTIFACT,
		};

	/**
	 * A public read-only list of all the '<em><b>Reference Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ReferenceType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Reference Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ReferenceType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ReferenceType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Reference Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ReferenceType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ReferenceType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Reference Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ReferenceType get(int value) {
		switch (value) {
			case OTHER_VALUE: return OTHER;
			case IMPORT_VALUE: return IMPORT;
			case EXPORT_VALUE: return EXPORT;
			case REQUIRE_BUNDLE_VALUE: return REQUIRE_BUNDLE;
			case REQUIREMENT_VALUE: return REQUIREMENT;
			case CAPABILITY_VALUE: return CAPABILITY;
			case DS_VALUE: return DS;
			case CONFIGURATION_VALUE: return CONFIGURATION;
			case ECORE_VALUE: return ECORE;
			case PROJECT_VALUE: return PROJECT;
			case INCLUDE_ARTIFACT_VALUE: return INCLUDE_ARTIFACT;
			case REQUIRE_ARTIFACT_VALUE: return REQUIRE_ARTIFACT;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ReferenceType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ReferenceType
