/**
 */
package org.gecko.artifacts;

import java.util.Properties;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Eclipse Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.artifacts.EclipseFeature#isJar <em>Jar</em>}</li>
 *   <li>{@link org.gecko.artifacts.EclipseFeature#isPacked <em>Packed</em>}</li>
 *   <li>{@link org.gecko.artifacts.EclipseFeature#getPropertyLocation <em>Property Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.EclipseFeature#getFeatureProperties <em>Feature Properties</em>}</li>
 *   <li>{@link org.gecko.artifacts.EclipseFeature#getImage <em>Image</em>}</li>
 *   <li>{@link org.gecko.artifacts.EclipseFeature#getProvider <em>Provider</em>}</li>
 * </ul>
 *
 * @see org.gecko.artifacts.ArtifactsPackage#getEclipseFeature()
 * @model
 * @generated
 */
public interface EclipseFeature extends Artifact {

	/**
	 * Returns the value of the '<em><b>Jar</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Jar</em>' attribute.
	 * @see #setJar(boolean)
	 * @see org.gecko.artifacts.ArtifactsPackage#getEclipseFeature_Jar()
	 * @model default="false"
	 * @generated
	 */
	boolean isJar();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.EclipseFeature#isJar <em>Jar</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Jar</em>' attribute.
	 * @see #isJar()
	 * @generated
	 */
	void setJar(boolean value);

	/**
	 * Returns the value of the '<em><b>Packed</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packed</em>' attribute.
	 * @see #setPacked(boolean)
	 * @see org.gecko.artifacts.ArtifactsPackage#getEclipseFeature_Packed()
	 * @model default="false"
	 * @generated
	 */
	boolean isPacked();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.EclipseFeature#isPacked <em>Packed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Packed</em>' attribute.
	 * @see #isPacked()
	 * @generated
	 */
	void setPacked(boolean value);

	/**
	 * Returns the value of the '<em><b>Property Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Location</em>' attribute.
	 * @see #setPropertyLocation(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getEclipseFeature_PropertyLocation()
	 * @model
	 * @generated
	 */
	String getPropertyLocation();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.EclipseFeature#getPropertyLocation <em>Property Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Location</em>' attribute.
	 * @see #getPropertyLocation()
	 * @generated
	 */
	void setPropertyLocation(String value);

	/**
	 * Returns the value of the '<em><b>Feature Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Properties</em>' attribute.
	 * @see #setFeatureProperties(Properties)
	 * @see org.gecko.artifacts.ArtifactsPackage#getEclipseFeature_FeatureProperties()
	 * @model dataType="org.gecko.artifacts.EProperties" transient="true"
	 * @generated
	 */
	Properties getFeatureProperties();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.EclipseFeature#getFeatureProperties <em>Feature Properties</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Properties</em>' attribute.
	 * @see #getFeatureProperties()
	 * @generated
	 */
	void setFeatureProperties(Properties value);

	/**
	 * Returns the value of the '<em><b>Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image</em>' attribute.
	 * @see #setImage(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getEclipseFeature_Image()
	 * @model
	 * @generated
	 */
	String getImage();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.EclipseFeature#getImage <em>Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image</em>' attribute.
	 * @see #getImage()
	 * @generated
	 */
	void setImage(String value);

	/**
	 * Returns the value of the '<em><b>Provider</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provider</em>' attribute.
	 * @see #setProvider(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getEclipseFeature_Provider()
	 * @model
	 * @generated
	 */
	String getProvider();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.EclipseFeature#getProvider <em>Provider</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Provider</em>' attribute.
	 * @see #getProvider()
	 * @generated
	 */
	void setProvider(String value);
} // EclipseFeature
