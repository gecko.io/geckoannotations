/**
 */
package org.gecko.artifacts;

import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.artifacts.Dependency#getName <em>Name</em>}</li>
 *   <li>{@link org.gecko.artifacts.Dependency#getType <em>Type</em>}</li>
 *   <li>{@link org.gecko.artifacts.Dependency#getSource <em>Source</em>}</li>
 *   <li>{@link org.gecko.artifacts.Dependency#getTarget <em>Target</em>}</li>
 *   <li>{@link org.gecko.artifacts.Dependency#getVersion <em>Version</em>}</li>
 *   <li>{@link org.gecko.artifacts.Dependency#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link org.gecko.artifacts.Dependency#isOptional <em>Optional</em>}</li>
 *   <li>{@link org.gecko.artifacts.Dependency#getLocation <em>Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.Dependency#isReexport <em>Reexport</em>}</li>
 *   <li>{@link org.gecko.artifacts.Dependency#getObject <em>Object</em>}</li>
 *   <li>{@link org.gecko.artifacts.Dependency#getFilter <em>Filter</em>}</li>
 *   <li>{@link org.gecko.artifacts.Dependency#isMultiple <em>Multiple</em>}</li>
 *   <li>{@link org.gecko.artifacts.Dependency#isActive <em>Active</em>}</li>
 *   <li>{@link org.gecko.artifacts.Dependency#getDependencyProperty <em>Dependency Property</em>}</li>
 *   <li>{@link org.gecko.artifacts.Dependency#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.gecko.artifacts.Dependency#getObjectClass <em>Object Class</em>}</li>
 *   <li>{@link org.gecko.artifacts.Dependency#getId <em>Id</em>}</li>
 * </ul>
 *
 * @see org.gecko.artifacts.ArtifactsPackage#getDependency()
 * @model
 * @generated
 */
public interface Dependency extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getDependency_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Dependency#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.gecko.artifacts.DependencyType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.gecko.artifacts.DependencyType
	 * @see #setType(DependencyType)
	 * @see org.gecko.artifacts.ArtifactsPackage#getDependency_Type()
	 * @model
	 * @generated
	 */
	DependencyType getType();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Dependency#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.gecko.artifacts.DependencyType
	 * @see #getType()
	 * @generated
	 */
	void setType(DependencyType value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(OSGiBundle)
	 * @see org.gecko.artifacts.ArtifactsPackage#getDependency_Source()
	 * @model
	 * @generated
	 */
	OSGiBundle getSource();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Dependency#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(OSGiBundle value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(OSGiBundle)
	 * @see org.gecko.artifacts.ArtifactsPackage#getDependency_Target()
	 * @model
	 * @generated
	 */
	OSGiBundle getTarget();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Dependency#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(OSGiBundle value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getDependency_Version()
	 * @model
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Dependency#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visibility</em>' attribute.
	 * @see #setVisibility(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getDependency_Visibility()
	 * @model
	 * @generated
	 */
	String getVisibility();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Dependency#getVisibility <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visibility</em>' attribute.
	 * @see #getVisibility()
	 * @generated
	 */
	void setVisibility(String value);

	/**
	 * Returns the value of the '<em><b>Optional</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optional</em>' attribute.
	 * @see #setOptional(boolean)
	 * @see org.gecko.artifacts.ArtifactsPackage#getDependency_Optional()
	 * @model default="false"
	 * @generated
	 */
	boolean isOptional();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Dependency#isOptional <em>Optional</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Optional</em>' attribute.
	 * @see #isOptional()
	 * @generated
	 */
	void setOptional(boolean value);

	/**
	 * Returns the value of the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' attribute.
	 * @see #setLocation(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getDependency_Location()
	 * @model
	 * @generated
	 */
	String getLocation();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Dependency#getLocation <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' attribute.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(String value);

	/**
	 * Returns the value of the '<em><b>Reexport</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reexport</em>' attribute.
	 * @see #setReexport(boolean)
	 * @see org.gecko.artifacts.ArtifactsPackage#getDependency_Reexport()
	 * @model default="false"
	 * @generated
	 */
	boolean isReexport();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Dependency#isReexport <em>Reexport</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reexport</em>' attribute.
	 * @see #isReexport()
	 * @generated
	 */
	void setReexport(boolean value);

	/**
	 * Returns the value of the '<em><b>Object</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' containment reference.
	 * @see #setObject(EObject)
	 * @see org.gecko.artifacts.ArtifactsPackage#getDependency_Object()
	 * @model containment="true"
	 * @generated
	 */
	EObject getObject();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Dependency#getObject <em>Object</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object</em>' containment reference.
	 * @see #getObject()
	 * @generated
	 */
	void setObject(EObject value);

	/**
	 * Returns the value of the '<em><b>Filter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filter</em>' attribute.
	 * @see #setFilter(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getDependency_Filter()
	 * @model
	 * @generated
	 */
	String getFilter();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Dependency#getFilter <em>Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filter</em>' attribute.
	 * @see #getFilter()
	 * @generated
	 */
	void setFilter(String value);

	/**
	 * Returns the value of the '<em><b>Multiple</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiple</em>' attribute.
	 * @see #setMultiple(boolean)
	 * @see org.gecko.artifacts.ArtifactsPackage#getDependency_Multiple()
	 * @model default="false"
	 * @generated
	 */
	boolean isMultiple();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Dependency#isMultiple <em>Multiple</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multiple</em>' attribute.
	 * @see #isMultiple()
	 * @generated
	 */
	void setMultiple(boolean value);

	/**
	 * Returns the value of the '<em><b>Active</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active</em>' attribute.
	 * @see #setActive(boolean)
	 * @see org.gecko.artifacts.ArtifactsPackage#getDependency_Active()
	 * @model default="false"
	 * @generated
	 */
	boolean isActive();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Dependency#isActive <em>Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Active</em>' attribute.
	 * @see #isActive()
	 * @generated
	 */
	void setActive(boolean value);

	/**
	 * Returns the value of the '<em><b>Dependency Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.gecko.artifacts.Property}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependency Property</em>' containment reference list.
	 * @see org.gecko.artifacts.ArtifactsPackage#getDependency_DependencyProperty()
	 * @model containment="true"
	 * @generated
	 */
	EList<Property> getDependencyProperty();

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' attribute.
	 * @see org.gecko.artifacts.ArtifactsPackage#getDependency_Properties()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	Map<String, String> getProperties();

	/**
	 * Returns the value of the '<em><b>Object Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Class</em>' attribute.
	 * @see #setObjectClass(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getDependency_ObjectClass()
	 * @model
	 * @generated
	 */
	String getObjectClass();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Dependency#getObjectClass <em>Object Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Class</em>' attribute.
	 * @see #getObjectClass()
	 * @generated
	 */
	void setObjectClass(String value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getDependency_Id()
	 * @model id="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Dependency#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

} // Dependency
