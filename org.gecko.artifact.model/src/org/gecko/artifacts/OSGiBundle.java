/**
 */
package org.gecko.artifacts;

import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OS Gi Bundle</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.artifacts.OSGiBundle#getBsn <em>Bsn</em>}</li>
 *   <li>{@link org.gecko.artifacts.OSGiBundle#getSourceLocation <em>Source Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.OSGiBundle#getCacheLocation <em>Cache Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.OSGiBundle#getName <em>Name</em>}</li>
 *   <li>{@link org.gecko.artifacts.OSGiBundle#getVendor <em>Vendor</em>}</li>
 *   <li>{@link org.gecko.artifacts.OSGiBundle#getVersion <em>Version</em>}</li>
 *   <li>{@link org.gecko.artifacts.OSGiBundle#isPolicyLazy <em>Policy Lazy</em>}</li>
 *   <li>{@link org.gecko.artifacts.OSGiBundle#getExecutionEnvironment <em>Execution Environment</em>}</li>
 *   <li>{@link org.gecko.artifacts.OSGiBundle#getClasspath <em>Classpath</em>}</li>
 *   <li>{@link org.gecko.artifacts.OSGiBundle#getLocalization <em>Localization</em>}</li>
 *   <li>{@link org.gecko.artifacts.OSGiBundle#getDependency <em>Dependency</em>}</li>
 *   <li>{@link org.gecko.artifacts.OSGiBundle#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.gecko.artifacts.OSGiBundle#getBundleProperty <em>Bundle Property</em>}</li>
 *   <li>{@link org.gecko.artifacts.OSGiBundle#getLicense <em>License</em>}</li>
 *   <li>{@link org.gecko.artifacts.OSGiBundle#getCopyright <em>Copyright</em>}</li>
 *   <li>{@link org.gecko.artifacts.OSGiBundle#getFragmentHost <em>Fragment Host</em>}</li>
 *   <li>{@link org.gecko.artifacts.OSGiBundle#isFragment <em>Fragment</em>}</li>
 * </ul>
 *
 * @see org.gecko.artifacts.ArtifactsPackage#getOSGiBundle()
 * @model
 * @generated
 */
public interface OSGiBundle extends EObject {
	/**
	 * Returns the value of the '<em><b>Bsn</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bsn</em>' attribute.
	 * @see #setBsn(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getOSGiBundle_Bsn()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getBsn();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.OSGiBundle#getBsn <em>Bsn</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bsn</em>' attribute.
	 * @see #getBsn()
	 * @generated
	 */
	void setBsn(String value);

	/**
	 * Returns the value of the '<em><b>Source Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Location</em>' attribute.
	 * @see #setSourceLocation(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getOSGiBundle_SourceLocation()
	 * @model
	 * @generated
	 */
	String getSourceLocation();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.OSGiBundle#getSourceLocation <em>Source Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Location</em>' attribute.
	 * @see #getSourceLocation()
	 * @generated
	 */
	void setSourceLocation(String value);

	/**
	 * Returns the value of the '<em><b>Cache Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cache Location</em>' attribute.
	 * @see #setCacheLocation(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getOSGiBundle_CacheLocation()
	 * @model
	 * @generated
	 */
	String getCacheLocation();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.OSGiBundle#getCacheLocation <em>Cache Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cache Location</em>' attribute.
	 * @see #getCacheLocation()
	 * @generated
	 */
	void setCacheLocation(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getOSGiBundle_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.OSGiBundle#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Vendor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vendor</em>' attribute.
	 * @see #setVendor(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getOSGiBundle_Vendor()
	 * @model
	 * @generated
	 */
	String getVendor();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.OSGiBundle#getVendor <em>Vendor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vendor</em>' attribute.
	 * @see #getVendor()
	 * @generated
	 */
	void setVendor(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getOSGiBundle_Version()
	 * @model dataType="org.gecko.artifacts.Version"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.OSGiBundle#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Policy Lazy</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Policy Lazy</em>' attribute.
	 * @see #setPolicyLazy(boolean)
	 * @see org.gecko.artifacts.ArtifactsPackage#getOSGiBundle_PolicyLazy()
	 * @model default="false"
	 * @generated
	 */
	boolean isPolicyLazy();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.OSGiBundle#isPolicyLazy <em>Policy Lazy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Policy Lazy</em>' attribute.
	 * @see #isPolicyLazy()
	 * @generated
	 */
	void setPolicyLazy(boolean value);

	/**
	 * Returns the value of the '<em><b>Execution Environment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Environment</em>' attribute.
	 * @see #setExecutionEnvironment(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getOSGiBundle_ExecutionEnvironment()
	 * @model
	 * @generated
	 */
	String getExecutionEnvironment();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.OSGiBundle#getExecutionEnvironment <em>Execution Environment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Environment</em>' attribute.
	 * @see #getExecutionEnvironment()
	 * @generated
	 */
	void setExecutionEnvironment(String value);

	/**
	 * Returns the value of the '<em><b>Classpath</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classpath</em>' attribute.
	 * @see #setClasspath(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getOSGiBundle_Classpath()
	 * @model
	 * @generated
	 */
	String getClasspath();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.OSGiBundle#getClasspath <em>Classpath</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Classpath</em>' attribute.
	 * @see #getClasspath()
	 * @generated
	 */
	void setClasspath(String value);

	/**
	 * Returns the value of the '<em><b>Localization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Localization</em>' attribute.
	 * @see #setLocalization(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getOSGiBundle_Localization()
	 * @model
	 * @generated
	 */
	String getLocalization();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.OSGiBundle#getLocalization <em>Localization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Localization</em>' attribute.
	 * @see #getLocalization()
	 * @generated
	 */
	void setLocalization(String value);

	/**
	 * Returns the value of the '<em><b>Dependency</b></em>' reference list.
	 * The list contents are of type {@link org.gecko.artifacts.Dependency}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependency</em>' reference list.
	 * @see org.gecko.artifacts.ArtifactsPackage#getOSGiBundle_Dependency()
	 * @model keys="id name"
	 * @generated
	 */
	EList<Dependency> getDependency();

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' attribute.
	 * @see org.gecko.artifacts.ArtifactsPackage#getOSGiBundle_Properties()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	Map<String, String> getProperties();

	/**
	 * Returns the value of the '<em><b>Bundle Property</b></em>' containment reference list.
	 * The list contents are of type {@link org.gecko.artifacts.Property}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bundle Property</em>' containment reference list.
	 * @see org.gecko.artifacts.ArtifactsPackage#getOSGiBundle_BundleProperty()
	 * @model containment="true"
	 * @generated
	 */
	EList<Property> getBundleProperty();

	/**
	 * Returns the value of the '<em><b>License</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>License</em>' attribute.
	 * @see #setLicense(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getOSGiBundle_License()
	 * @model
	 * @generated
	 */
	String getLicense();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.OSGiBundle#getLicense <em>License</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>License</em>' attribute.
	 * @see #getLicense()
	 * @generated
	 */
	void setLicense(String value);

	/**
	 * Returns the value of the '<em><b>Copyright</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Copyright</em>' attribute.
	 * @see #setCopyright(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getOSGiBundle_Copyright()
	 * @model
	 * @generated
	 */
	String getCopyright();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.OSGiBundle#getCopyright <em>Copyright</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Copyright</em>' attribute.
	 * @see #getCopyright()
	 * @generated
	 */
	void setCopyright(String value);

	/**
	 * Returns the value of the '<em><b>Fragment Host</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fragment Host</em>' attribute.
	 * @see #setFragmentHost(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getOSGiBundle_FragmentHost()
	 * @model
	 * @generated
	 */
	String getFragmentHost();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.OSGiBundle#getFragmentHost <em>Fragment Host</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fragment Host</em>' attribute.
	 * @see #getFragmentHost()
	 * @generated
	 */
	void setFragmentHost(String value);

	/**
	 * Returns the value of the '<em><b>Fragment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fragment</em>' attribute.
	 * @see org.gecko.artifacts.ArtifactsPackage#getOSGiBundle_Fragment()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	boolean isFragment();

} // OSGiBundle
