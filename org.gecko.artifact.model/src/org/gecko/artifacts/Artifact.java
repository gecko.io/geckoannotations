/**
 */
package org.gecko.artifacts;

import java.util.Date;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Artifact</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.artifacts.Artifact#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.artifacts.Artifact#getName <em>Name</em>}</li>
 *   <li>{@link org.gecko.artifacts.Artifact#getLabel <em>Label</em>}</li>
 *   <li>{@link org.gecko.artifacts.Artifact#getDescription <em>Description</em>}</li>
 *   <li>{@link org.gecko.artifacts.Artifact#getVersion <em>Version</em>}</li>
 *   <li>{@link org.gecko.artifacts.Artifact#getSourceLocation <em>Source Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.Artifact#getCacheLocation <em>Cache Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.Artifact#getRepository <em>Repository</em>}</li>
 *   <li>{@link org.gecko.artifacts.Artifact#getLicense <em>License</em>}</li>
 *   <li>{@link org.gecko.artifacts.Artifact#getCopyright <em>Copyright</em>}</li>
 *   <li>{@link org.gecko.artifacts.Artifact#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.gecko.artifacts.Artifact#getArtifactProperties <em>Artifact Properties</em>}</li>
 *   <li>{@link org.gecko.artifacts.Artifact#getSize <em>Size</em>}</li>
 *   <li>{@link org.gecko.artifacts.Artifact#getHash <em>Hash</em>}</li>
 *   <li>{@link org.gecko.artifacts.Artifact#getTimestamp <em>Timestamp</em>}</li>
 *   <li>{@link org.gecko.artifacts.Artifact#getReferences <em>References</em>}</li>
 * </ul>
 *
 * @see org.gecko.artifacts.ArtifactsPackage#getArtifact()
 * @model
 * @generated
 */
public interface Artifact extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifact_Id()
	 * @model id="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Artifact#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifact_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Artifact#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifact_Label()
	 * @model
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Artifact#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifact_Version()
	 * @model dataType="org.gecko.artifacts.Version"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Artifact#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Source Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Location</em>' attribute.
	 * @see #setSourceLocation(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifact_SourceLocation()
	 * @model
	 * @generated
	 */
	String getSourceLocation();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Artifact#getSourceLocation <em>Source Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Location</em>' attribute.
	 * @see #getSourceLocation()
	 * @generated
	 */
	void setSourceLocation(String value);

	/**
	 * Returns the value of the '<em><b>Cache Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cache Location</em>' attribute.
	 * @see #setCacheLocation(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifact_CacheLocation()
	 * @model
	 * @generated
	 */
	String getCacheLocation();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Artifact#getCacheLocation <em>Cache Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cache Location</em>' attribute.
	 * @see #getCacheLocation()
	 * @generated
	 */
	void setCacheLocation(String value);

	/**
	 * Returns the value of the '<em><b>Repository</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.gecko.artifacts.ArtifactRepository#getArtifacts <em>Artifacts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Repository</em>' reference.
	 * @see #setRepository(ArtifactRepository)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifact_Repository()
	 * @see org.gecko.artifacts.ArtifactRepository#getArtifacts
	 * @model opposite="artifacts"
	 * @generated
	 */
	ArtifactRepository getRepository();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Artifact#getRepository <em>Repository</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Repository</em>' reference.
	 * @see #getRepository()
	 * @generated
	 */
	void setRepository(ArtifactRepository value);

	/**
	 * Returns the value of the '<em><b>License</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>License</em>' attribute.
	 * @see #setLicense(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifact_License()
	 * @model
	 * @generated
	 */
	String getLicense();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Artifact#getLicense <em>License</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>License</em>' attribute.
	 * @see #getLicense()
	 * @generated
	 */
	void setLicense(String value);

	/**
	 * Returns the value of the '<em><b>Copyright</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Copyright</em>' attribute.
	 * @see #setCopyright(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifact_Copyright()
	 * @model
	 * @generated
	 */
	String getCopyright();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Artifact#getCopyright <em>Copyright</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Copyright</em>' attribute.
	 * @see #getCopyright()
	 * @generated
	 */
	void setCopyright(String value);

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' attribute.
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifact_Properties()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	Map<String, String> getProperties();

	/**
	 * Returns the value of the '<em><b>Artifact Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.gecko.artifacts.Property}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Artifact Properties</em>' containment reference list.
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifact_ArtifactProperties()
	 * @model containment="true"
	 * @generated
	 */
	EList<Property> getArtifactProperties();

	/**
	 * Returns the value of the '<em><b>Size</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size</em>' attribute.
	 * @see #setSize(int)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifact_Size()
	 * @model default="-1"
	 * @generated
	 */
	int getSize();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Artifact#getSize <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Size</em>' attribute.
	 * @see #getSize()
	 * @generated
	 */
	void setSize(int value);

	/**
	 * Returns the value of the '<em><b>Hash</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hash</em>' attribute.
	 * @see #setHash(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifact_Hash()
	 * @model
	 * @generated
	 */
	String getHash();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Artifact#getHash <em>Hash</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hash</em>' attribute.
	 * @see #getHash()
	 * @generated
	 */
	void setHash(String value);

	/**
	 * Returns the value of the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timestamp</em>' attribute.
	 * @see #setTimestamp(Date)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifact_Timestamp()
	 * @model
	 * @generated
	 */
	Date getTimestamp();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Artifact#getTimestamp <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timestamp</em>' attribute.
	 * @see #getTimestamp()
	 * @generated
	 */
	void setTimestamp(Date value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifact_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.Artifact#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>References</b></em>' reference list.
	 * The list contents are of type {@link org.gecko.artifacts.ArtifactReference}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>References</em>' reference list.
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifact_References()
	 * @model
	 * @generated
	 */
	EList<ArtifactReference> getReferences();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model keyRequired="true" valueRequired="true"
	 * @generated
	 */
	void putProperty(String key, String value);

} // Artifact
