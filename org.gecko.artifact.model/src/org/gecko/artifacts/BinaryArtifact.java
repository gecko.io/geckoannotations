/**
 */
package org.gecko.artifacts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Artifact</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.gecko.artifacts.ArtifactsPackage#getBinaryArtifact()
 * @model
 * @generated
 */
public interface BinaryArtifact extends Artifact {
} // BinaryArtifact
