/**
 */
package org.gecko.artifacts;

import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Artifact Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.artifacts.ArtifactReference#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactReference#getVersion <em>Version</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactReference#getName <em>Name</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactReference#getLabel <em>Label</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactReference#isOptional <em>Optional</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactReference#getLocation <em>Location</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactReference#getFilter <em>Filter</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactReference#getType <em>Type</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactReference#getRefObject <em>Ref Object</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactReference#getSource <em>Source</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactReference#getSourceName <em>Source Name</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactReference#getSourceVersion <em>Source Version</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactReference#getTarget <em>Target</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactReference#getTargetName <em>Target Name</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactReference#getTargetVersion <em>Target Version</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactReference#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.gecko.artifacts.ArtifactReference#getReferenceProperties <em>Reference Properties</em>}</li>
 * </ul>
 *
 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactReference()
 * @model
 * @generated
 */
public interface ArtifactReference extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactReference_Id()
	 * @model id="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactReference#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactReference_Version()
	 * @model dataType="org.gecko.artifacts.Version"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactReference#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactReference_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactReference#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactReference_Label()
	 * @model required="true"
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactReference#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Optional</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optional</em>' attribute.
	 * @see #setOptional(boolean)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactReference_Optional()
	 * @model default="false"
	 * @generated
	 */
	boolean isOptional();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactReference#isOptional <em>Optional</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Optional</em>' attribute.
	 * @see #isOptional()
	 * @generated
	 */
	void setOptional(boolean value);

	/**
	 * Returns the value of the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' attribute.
	 * @see #setLocation(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactReference_Location()
	 * @model
	 * @generated
	 */
	String getLocation();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactReference#getLocation <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' attribute.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(String value);

	/**
	 * Returns the value of the '<em><b>Filter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filter</em>' attribute.
	 * @see #setFilter(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactReference_Filter()
	 * @model
	 * @generated
	 */
	String getFilter();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactReference#getFilter <em>Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filter</em>' attribute.
	 * @see #getFilter()
	 * @generated
	 */
	void setFilter(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.gecko.artifacts.ReferenceType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.gecko.artifacts.ReferenceType
	 * @see #setType(ReferenceType)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactReference_Type()
	 * @model
	 * @generated
	 */
	ReferenceType getType();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactReference#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.gecko.artifacts.ReferenceType
	 * @see #getType()
	 * @generated
	 */
	void setType(ReferenceType value);

	/**
	 * Returns the value of the '<em><b>Ref Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref Object</em>' reference.
	 * @see #setRefObject(EObject)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactReference_RefObject()
	 * @model
	 * @generated
	 */
	EObject getRefObject();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactReference#getRefObject <em>Ref Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ref Object</em>' reference.
	 * @see #getRefObject()
	 * @generated
	 */
	void setRefObject(EObject value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(Artifact)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactReference_Source()
	 * @model
	 * @generated
	 */
	Artifact getSource();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactReference#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(Artifact value);

	/**
	 * Returns the value of the '<em><b>Source Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Name</em>' attribute.
	 * @see #setSourceName(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactReference_SourceName()
	 * @model required="true"
	 * @generated
	 */
	String getSourceName();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactReference#getSourceName <em>Source Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Name</em>' attribute.
	 * @see #getSourceName()
	 * @generated
	 */
	void setSourceName(String value);

	/**
	 * Returns the value of the '<em><b>Source Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Version</em>' attribute.
	 * @see #setSourceVersion(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactReference_SourceVersion()
	 * @model dataType="org.gecko.artifacts.Version"
	 * @generated
	 */
	String getSourceVersion();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactReference#getSourceVersion <em>Source Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Version</em>' attribute.
	 * @see #getSourceVersion()
	 * @generated
	 */
	void setSourceVersion(String value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference list.
	 * The list contents are of type {@link org.gecko.artifacts.Artifact}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference list.
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactReference_Target()
	 * @model keys="id"
	 * @generated
	 */
	EList<Artifact> getTarget();

	/**
	 * Returns the value of the '<em><b>Target Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Name</em>' attribute.
	 * @see #setTargetName(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactReference_TargetName()
	 * @model required="true"
	 * @generated
	 */
	String getTargetName();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactReference#getTargetName <em>Target Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Name</em>' attribute.
	 * @see #getTargetName()
	 * @generated
	 */
	void setTargetName(String value);

	/**
	 * Returns the value of the '<em><b>Target Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Version</em>' attribute.
	 * @see #setTargetVersion(String)
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactReference_TargetVersion()
	 * @model
	 * @generated
	 */
	String getTargetVersion();

	/**
	 * Sets the value of the '{@link org.gecko.artifacts.ArtifactReference#getTargetVersion <em>Target Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Version</em>' attribute.
	 * @see #getTargetVersion()
	 * @generated
	 */
	void setTargetVersion(String value);

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' attribute.
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactReference_Properties()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	Map<String, String> getProperties();

	/**
	 * Returns the value of the '<em><b>Reference Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.gecko.artifacts.Property}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Properties</em>' containment reference list.
	 * @see org.gecko.artifacts.ArtifactsPackage#getArtifactReference_ReferenceProperties()
	 * @model containment="true"
	 * @generated
	 */
	EList<Property> getReferenceProperties();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model keyRequired="true" valueRequired="true"
	 * @generated
	 */
	void putProperty(String key, String value);

} // ArtifactReference
