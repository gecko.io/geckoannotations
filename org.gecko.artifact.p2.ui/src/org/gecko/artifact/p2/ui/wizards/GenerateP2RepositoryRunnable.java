package org.gecko.artifact.p2.ui.wizards;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.Map.Entry;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

import aQute.bnd.build.Project;
import aQute.bnd.osgi.Jar;
import aQute.bnd.osgi.JarResource;
import aQute.bnd.osgi.Resource;

class GenerateP2RepositoryRunnable implements IRunnableWithProgress {

	private final Project	project;
	private final String	path;
	private final boolean	folder;

	GenerateP2RepositoryRunnable(Project project, String path, boolean folder) {
		this.project = project;
		this.path = path;
		this.folder = folder;
	}

	@Override
	public void run(IProgressMonitor monitor) throws InvocationTargetException {
		try {
			Entry<String, Resource> export = project.export("p2.export", Collections.emptyMap());
			if (export != null) {
				try (JarResource r = (JarResource) export.getValue()) {
					File destination = new File(path);
					Jar jar = r.getJar();
					jar.write(destination);
				}
			}
		} catch (Exception e) {
			throw new InvocationTargetException(e);
		}
	}

}
