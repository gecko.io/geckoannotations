/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Artifact</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.Artifact#getProcessing <em>Processing</em>}</li>
 *   <li>{@link org.gecko.eclipse.Artifact#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.gecko.eclipse.Artifact#getRepositoryProperties <em>Repository Properties</em>}</li>
 *   <li>{@link org.gecko.eclipse.Artifact#getClassifier <em>Classifier</em>}</li>
 *   <li>{@link org.gecko.eclipse.Artifact#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.eclipse.Artifact#getVersion <em>Version</em>}</li>
 * </ul>
 *
 * @see org.gecko.eclipse.EclipsePackage#getArtifact()
 * @model extendedMetaData="name='Artifact' kind='elementOnly'"
 * @generated
 */
public interface Artifact extends EObject {
	/**
	 * Returns the value of the '<em><b>Processing</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processing</em>' containment reference.
	 * @see #setProcessing(Processing)
	 * @see org.gecko.eclipse.EclipsePackage#getArtifact_Processing()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='processing' namespace='##targetNamespace'"
	 * @generated
	 */
	Processing getProcessing();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Artifact#getProcessing <em>Processing</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Processing</em>' containment reference.
	 * @see #getProcessing()
	 * @generated
	 */
	void setProcessing(Processing value);

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference.
	 * @see #setProperties(Properties)
	 * @see org.gecko.eclipse.EclipsePackage#getArtifact_Properties()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='properties' namespace='##targetNamespace'"
	 * @generated
	 */
	Properties getProperties();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Artifact#getProperties <em>Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Properties</em>' containment reference.
	 * @see #getProperties()
	 * @generated
	 */
	void setProperties(Properties value);

	/**
	 * Returns the value of the '<em><b>Repository Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Repository Properties</em>' containment reference.
	 * @see #setRepositoryProperties(Properties)
	 * @see org.gecko.eclipse.EclipsePackage#getArtifact_RepositoryProperties()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='repositoryProperties' namespace='##targetNamespace'"
	 * @generated
	 */
	Properties getRepositoryProperties();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Artifact#getRepositoryProperties <em>Repository Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Repository Properties</em>' containment reference.
	 * @see #getRepositoryProperties()
	 * @generated
	 */
	void setRepositoryProperties(Properties value);

	/**
	 * Returns the value of the '<em><b>Classifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classifier</em>' attribute.
	 * @see #setClassifier(String)
	 * @see org.gecko.eclipse.EclipsePackage#getArtifact_Classifier()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='classifier'"
	 * @generated
	 */
	String getClassifier();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Artifact#getClassifier <em>Classifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Classifier</em>' attribute.
	 * @see #getClassifier()
	 * @generated
	 */
	void setClassifier(String value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.gecko.eclipse.EclipsePackage#getArtifact_Id()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='id'"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Artifact#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.gecko.eclipse.EclipsePackage#getArtifact_Version()
	 * @model dataType="org.gecko.eclipse.Version"
	 *        extendedMetaData="kind='attribute' name='version'"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Artifact#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

} // Artifact
