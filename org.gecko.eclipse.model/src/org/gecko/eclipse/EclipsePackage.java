/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.gecko.eclipse.EclipseFactory
 * @model kind="package"
 * @generated
 */
public interface EclipsePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "eclipse";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://gecko.io/eclipse";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "eclipse";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EclipsePackage eINSTANCE = org.gecko.eclipse.impl.EclipsePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.ArtifactImpl <em>Artifact</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.ArtifactImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getArtifact()
	 * @generated
	 */
	int ARTIFACT = 0;

	/**
	 * The feature id for the '<em><b>Processing</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__PROCESSING = 0;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__PROPERTIES = 1;

	/**
	 * The feature id for the '<em><b>Repository Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__REPOSITORY_PROPERTIES = 2;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__CLASSIFIER = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__ID = 4;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT__VERSION = 5;

	/**
	 * The number of structural features of the '<em>Artifact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Artifact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.ArtifactsImpl <em>Artifacts</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.ArtifactsImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getArtifacts()
	 * @generated
	 */
	int ARTIFACTS = 1;

	/**
	 * The feature id for the '<em><b>Artifact</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACTS__ARTIFACT = 0;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACTS__SIZE = 1;

	/**
	 * The number of structural features of the '<em>Artifacts</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACTS_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Artifacts</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFACTS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.CategoryDefImpl <em>Category Def</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.CategoryDefImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getCategoryDef()
	 * @generated
	 */
	int CATEGORY_DEF = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_DEF__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_DEF__LABEL = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_DEF__NAME = 2;

	/**
	 * The number of structural features of the '<em>Category Def</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_DEF_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Category Def</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_DEF_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.CategoryFeatureImpl <em>Category Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.CategoryFeatureImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getCategoryFeature()
	 * @generated
	 */
	int CATEGORY_FEATURE = 3;

	/**
	 * The feature id for the '<em><b>Category</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_FEATURE__CATEGORY = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_FEATURE__ID = 1;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_FEATURE__VERSION = 2;

	/**
	 * The number of structural features of the '<em>Category Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_FEATURE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Category Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_FEATURE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.CategorySiteImpl <em>Category Site</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.CategorySiteImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getCategorySite()
	 * @generated
	 */
	int CATEGORY_SITE = 4;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_SITE__FEATURE = 0;

	/**
	 * The feature id for the '<em><b>Category Def</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_SITE__CATEGORY_DEF = 1;

	/**
	 * The number of structural features of the '<em>Category Site</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_SITE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Category Site</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_SITE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.CategoryTypeImpl <em>Category Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.CategoryTypeImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getCategoryType()
	 * @generated
	 */
	int CATEGORY_TYPE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TYPE__NAME = 0;

	/**
	 * The number of structural features of the '<em>Category Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TYPE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Category Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.ChildrenTypeImpl <em>Children Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.ChildrenTypeImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getChildrenType()
	 * @generated
	 */
	int CHILDREN_TYPE = 6;

	/**
	 * The feature id for the '<em><b>Child</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHILDREN_TYPE__CHILD = 0;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHILDREN_TYPE__SIZE = 1;

	/**
	 * The number of structural features of the '<em>Children Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHILDREN_TYPE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Children Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHILDREN_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.DescriptionImpl <em>Description</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.DescriptionImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getDescription()
	 * @generated
	 */
	int DESCRIPTION = 7;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIPTION__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIPTION__URL = 1;

	/**
	 * The number of structural features of the '<em>Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIPTION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIPTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.DocumentRootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.DocumentRootImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getDocumentRoot()
	 * @generated
	 */
	int DOCUMENT_ROOT = 8;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__FEATURE = 3;

	/**
	 * The feature id for the '<em><b>Repository</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__REPOSITORY = 4;

	/**
	 * The feature id for the '<em><b>Site</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__SITE = 5;

	/**
	 * The feature id for the '<em><b>Target</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__TARGET = 6;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.FeatureImpl <em>Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.FeatureImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getFeature()
	 * @generated
	 */
	int FEATURE = 9;

	/**
	 * The feature id for the '<em><b>Install Handler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__INSTALL_HANDLER = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Copyright</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__COPYRIGHT = 2;

	/**
	 * The feature id for the '<em><b>License</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__LICENSE = 3;

	/**
	 * The feature id for the '<em><b>Url</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__URL = 4;

	/**
	 * The feature id for the '<em><b>Includes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__INCLUDES = 5;

	/**
	 * The feature id for the '<em><b>Requires</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__REQUIRES = 6;

	/**
	 * The feature id for the '<em><b>Plugin</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__PLUGIN = 7;

	/**
	 * The feature id for the '<em><b>Arch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__ARCH = 8;

	/**
	 * The feature id for the '<em><b>Colocation Affinity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__COLOCATION_AFFINITY = 9;

	/**
	 * The feature id for the '<em><b>Exclusive</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__EXCLUSIVE = 10;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__ID = 11;

	/**
	 * The feature id for the '<em><b>Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__IMAGE = 12;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__LABEL = 13;

	/**
	 * The feature id for the '<em><b>Nl</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__NL = 14;

	/**
	 * The feature id for the '<em><b>Os</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__OS = 15;

	/**
	 * The feature id for the '<em><b>Plugin1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__PLUGIN1 = 16;

	/**
	 * The feature id for the '<em><b>Provider Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__PROVIDER_NAME = 17;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__VERSION = 18;

	/**
	 * The feature id for the '<em><b>Ws</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__WS = 19;

	/**
	 * The number of structural features of the '<em>Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_FEATURE_COUNT = 20;

	/**
	 * The number of operations of the '<em>Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.ImportImpl <em>Import</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.ImportImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getImport()
	 * @generated
	 */
	int IMPORT = 10;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__FEATURE = 0;

	/**
	 * The feature id for the '<em><b>Match</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__MATCH = 1;

	/**
	 * The feature id for the '<em><b>Patch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__PATCH = 2;

	/**
	 * The feature id for the '<em><b>Plugin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__PLUGIN = 3;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__VERSION = 4;

	/**
	 * The number of structural features of the '<em>Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.ImportRequiresImpl <em>Import Requires</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.ImportRequiresImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getImportRequires()
	 * @generated
	 */
	int IMPORT_REQUIRES = 11;

	/**
	 * The feature id for the '<em><b>Import</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_REQUIRES__IMPORT = 0;

	/**
	 * The number of structural features of the '<em>Import Requires</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_REQUIRES_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Import Requires</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_REQUIRES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.IncludeImpl <em>Include</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.IncludeImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getInclude()
	 * @generated
	 */
	int INCLUDE = 12;

	/**
	 * The feature id for the '<em><b>Arch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__ARCH = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__ID = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__NAME = 2;

	/**
	 * The feature id for the '<em><b>Nl</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__NL = 3;

	/**
	 * The feature id for the '<em><b>Optional</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__OPTIONAL = 4;

	/**
	 * The feature id for the '<em><b>Os</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__OS = 5;

	/**
	 * The feature id for the '<em><b>Search Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__SEARCH_LOCATION = 6;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__VERSION = 7;

	/**
	 * The feature id for the '<em><b>Ws</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__WS = 8;

	/**
	 * The number of structural features of the '<em>Include</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Include</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.InstallHandlerImpl <em>Install Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.InstallHandlerImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getInstallHandler()
	 * @generated
	 */
	int INSTALL_HANDLER = 13;

	/**
	 * The feature id for the '<em><b>Handler</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTALL_HANDLER__HANDLER = 0;

	/**
	 * The feature id for the '<em><b>Library</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTALL_HANDLER__LIBRARY = 1;

	/**
	 * The number of structural features of the '<em>Install Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTALL_HANDLER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Install Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTALL_HANDLER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.InstructionImpl <em>Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.InstructionImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getInstruction()
	 * @generated
	 */
	int INSTRUCTION = 14;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION__KEY = 1;

	/**
	 * The number of structural features of the '<em>Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.InstructionsTypeImpl <em>Instructions Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.InstructionsTypeImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getInstructionsType()
	 * @generated
	 */
	int INSTRUCTIONS_TYPE = 15;

	/**
	 * The feature id for the '<em><b>Instruction</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTIONS_TYPE__INSTRUCTION = 0;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTIONS_TYPE__SIZE = 1;

	/**
	 * The number of structural features of the '<em>Instructions Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTIONS_TYPE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Instructions Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTIONS_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.LauncherArgsImpl <em>Launcher Args</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.LauncherArgsImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getLauncherArgs()
	 * @generated
	 */
	int LAUNCHER_ARGS = 16;

	/**
	 * The feature id for the '<em><b>Program Args</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAUNCHER_ARGS__PROGRAM_ARGS = 0;

	/**
	 * The feature id for the '<em><b>Vm Args</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAUNCHER_ARGS__VM_ARGS = 1;

	/**
	 * The number of structural features of the '<em>Launcher Args</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAUNCHER_ARGS_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Launcher Args</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAUNCHER_ARGS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.LicensesTypeImpl <em>Licenses Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.LicensesTypeImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getLicensesType()
	 * @generated
	 */
	int LICENSES_TYPE = 17;

	/**
	 * The feature id for the '<em><b>License</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LICENSES_TYPE__LICENSE = 0;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LICENSES_TYPE__SIZE = 1;

	/**
	 * The number of structural features of the '<em>Licenses Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LICENSES_TYPE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Licenses Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LICENSES_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.MappingsTypeImpl <em>Mappings Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.MappingsTypeImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getMappingsType()
	 * @generated
	 */
	int MAPPINGS_TYPE = 18;

	/**
	 * The feature id for the '<em><b>Rule</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPINGS_TYPE__RULE = 0;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPINGS_TYPE__SIZE = 1;

	/**
	 * The number of structural features of the '<em>Mappings Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPINGS_TYPE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Mappings Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPINGS_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.PluginImpl <em>Plugin</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.PluginImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getPlugin()
	 * @generated
	 */
	int PLUGIN = 19;

	/**
	 * The feature id for the '<em><b>Arch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__ARCH = 0;

	/**
	 * The feature id for the '<em><b>Download Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__DOWNLOAD_SIZE = 1;

	/**
	 * The feature id for the '<em><b>Fragment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__FRAGMENT = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__ID = 3;

	/**
	 * The feature id for the '<em><b>Install Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__INSTALL_SIZE = 4;

	/**
	 * The feature id for the '<em><b>Nl</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__NL = 5;

	/**
	 * The feature id for the '<em><b>Os</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__OS = 6;

	/**
	 * The feature id for the '<em><b>Unpack</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__UNPACK = 7;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__VERSION = 8;

	/**
	 * The feature id for the '<em><b>Ws</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN__WS = 9;

	/**
	 * The number of structural features of the '<em>Plugin</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_FEATURE_COUNT = 10;

	/**
	 * The number of operations of the '<em>Plugin</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.ProcessingImpl <em>Processing</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.ProcessingImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getProcessing()
	 * @generated
	 */
	int PROCESSING = 20;

	/**
	 * The feature id for the '<em><b>Step</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSING__STEP = 0;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSING__SIZE = 1;

	/**
	 * The number of structural features of the '<em>Processing</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSING_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Processing</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.ProcessStepImpl <em>Process Step</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.ProcessStepImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getProcessStep()
	 * @generated
	 */
	int PROCESS_STEP = 21;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_STEP__ID = 0;

	/**
	 * The feature id for the '<em><b>Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_STEP__REQUIRED = 1;

	/**
	 * The number of structural features of the '<em>Process Step</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_STEP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Process Step</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_STEP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.PropertiesImpl <em>Properties</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.PropertiesImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getProperties()
	 * @generated
	 */
	int PROPERTIES = 22;

	/**
	 * The feature id for the '<em><b>Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTIES__PROPERTY = 0;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTIES__SIZE = 1;

	/**
	 * The number of structural features of the '<em>Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTIES_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTIES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.PropertyImpl <em>Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.PropertyImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getProperty()
	 * @generated
	 */
	int PROPERTY = 23;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__NAME = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.ProvidedImpl <em>Provided</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.ProvidedImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getProvided()
	 * @generated
	 */
	int PROVIDED = 24;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED__NAME = 0;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED__NAMESPACE = 1;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED__VERSION = 2;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED__PROPERTIES = 3;

	/**
	 * The number of structural features of the '<em>Provided</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Provided</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.ProvidesImpl <em>Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.ProvidesImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getProvides()
	 * @generated
	 */
	int PROVIDES = 25;

	/**
	 * The feature id for the '<em><b>Provided</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDES__PROVIDED = 0;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDES__SIZE = 1;

	/**
	 * The number of structural features of the '<em>Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDES_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.ReferencesTypeImpl <em>References Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.ReferencesTypeImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getReferencesType()
	 * @generated
	 */
	int REFERENCES_TYPE = 26;

	/**
	 * The feature id for the '<em><b>Repository</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCES_TYPE__REPOSITORY = 0;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCES_TYPE__SIZE = 1;

	/**
	 * The number of structural features of the '<em>References Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCES_TYPE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>References Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCES_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.RepositoryImpl <em>Repository</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.RepositoryImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getRepository()
	 * @generated
	 */
	int REPOSITORY = 27;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY__PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Mappings</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY__MAPPINGS = 1;

	/**
	 * The feature id for the '<em><b>References</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY__REFERENCES = 2;

	/**
	 * The feature id for the '<em><b>Units</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY__UNITS = 3;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY__CHILDREN = 4;

	/**
	 * The feature id for the '<em><b>Artifacts</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY__ARTIFACTS = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY__NAME = 6;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY__TYPE = 7;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY__VERSION = 8;

	/**
	 * The number of structural features of the '<em>Repository</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Repository</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.RepositoryRefImpl <em>Repository Ref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.RepositoryRefImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getRepositoryRef()
	 * @generated
	 */
	int REPOSITORY_REF = 28;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY_REF__LOCATION = 0;

	/**
	 * The number of structural features of the '<em>Repository Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY_REF_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Repository Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY_REF_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.RepositoryRefsImpl <em>Repository Refs</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.RepositoryRefsImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getRepositoryRefs()
	 * @generated
	 */
	int REPOSITORY_REFS = 29;

	/**
	 * The feature id for the '<em><b>Options</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY_REFS__OPTIONS = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY_REFS__TYPE = 1;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY_REFS__URI = 2;

	/**
	 * The feature id for the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY_REFS__URL = 3;

	/**
	 * The number of structural features of the '<em>Repository Refs</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY_REFS_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Repository Refs</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY_REFS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.RequiredImpl <em>Required</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.RequiredImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getRequired()
	 * @generated
	 */
	int REQUIRED = 30;

	/**
	 * The feature id for the '<em><b>Filter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED__FILTER = 0;

	/**
	 * The feature id for the '<em><b>Greedy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED__GREEDY = 1;

	/**
	 * The feature id for the '<em><b>Multiple</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED__MULTIPLE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED__NAME = 3;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED__NAMESPACE = 4;

	/**
	 * The feature id for the '<em><b>Optional</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED__OPTIONAL = 5;

	/**
	 * The feature id for the '<em><b>Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED__RANGE = 6;

	/**
	 * The number of structural features of the '<em>Required</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Required</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.RequiresImpl <em>Requires</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.RequiresImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getRequires()
	 * @generated
	 */
	int REQUIRES = 32;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.RuleImpl <em>Rule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.RuleImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getRule()
	 * @generated
	 */
	int RULE = 33;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.SiteImpl <em>Site</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.SiteImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getSite()
	 * @generated
	 */
	int SITE = 34;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.TargetImpl <em>Target</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.TargetImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTarget()
	 * @generated
	 */
	int TARGET = 35;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.TargetDependenciesImpl <em>Target Dependencies</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.TargetDependenciesImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTargetDependencies()
	 * @generated
	 */
	int TARGET_DEPENDENCIES = 36;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.TargetEnvironmentImpl <em>Target Environment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.TargetEnvironmentImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTargetEnvironment()
	 * @generated
	 */
	int TARGET_ENVIRONMENT = 37;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.TargetJREImpl <em>Target JRE</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.TargetJREImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTargetJRE()
	 * @generated
	 */
	int TARGET_JRE = 38;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.TargetLocationImpl <em>Target Location</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.TargetLocationImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTargetLocation()
	 * @generated
	 */
	int TARGET_LOCATION = 39;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.TargetLocationsImpl <em>Target Locations</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.TargetLocationsImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTargetLocations()
	 * @generated
	 */
	int TARGET_LOCATIONS = 40;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.TouchpointImpl <em>Touchpoint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.TouchpointImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTouchpoint()
	 * @generated
	 */
	int TOUCHPOINT = 41;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.TouchpointDataImpl <em>Touchpoint Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.TouchpointDataImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTouchpointData()
	 * @generated
	 */
	int TOUCHPOINT_DATA = 42;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.TouchpointDataTypeImpl <em>Touchpoint Data Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.TouchpointDataTypeImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTouchpointDataType()
	 * @generated
	 */
	int TOUCHPOINT_DATA_TYPE = 45;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.UnitImpl <em>Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.UnitImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getUnit()
	 * @generated
	 */
	int UNIT = 43;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.UnitsTypeImpl <em>Units Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.UnitsTypeImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getUnitsType()
	 * @generated
	 */
	int UNITS_TYPE = 44;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.UpdateImpl <em>Update</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.UpdateImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getUpdate()
	 * @generated
	 */
	int UPDATE = 47;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.UrlTypeImpl <em>Url Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.UrlTypeImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getUrlType()
	 * @generated
	 */
	int URL_TYPE = 46;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.impl.RequiredPropertiesImpl <em>Required Properties</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.impl.RequiredPropertiesImpl
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getRequiredProperties()
	 * @generated
	 */
	int REQUIRED_PROPERTIES = 31;

	/**
	 * The feature id for the '<em><b>Match</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_PROPERTIES__MATCH = 0;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_PROPERTIES__NAMESPACE = 1;

	/**
	 * The number of structural features of the '<em>Required Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_PROPERTIES_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Required Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_PROPERTIES_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Required</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRES__REQUIRED = 0;

	/**
	 * The feature id for the '<em><b>Required Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRES__REQUIRED_PROPERTIES = 1;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRES__SIZE = 2;

	/**
	 * The number of structural features of the '<em>Requires</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRES_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Requires</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRES_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Filter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE__FILTER = 0;

	/**
	 * The feature id for the '<em><b>Output</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE__OUTPUT = 1;

	/**
	 * The number of structural features of the '<em>Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SITE__LABEL = 0;

	/**
	 * The feature id for the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SITE__URL = 1;

	/**
	 * The number of structural features of the '<em>Site</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SITE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Site</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SITE_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Locations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET__LOCATIONS = 0;

	/**
	 * The feature id for the '<em><b>Environment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET__ENVIRONMENT = 1;

	/**
	 * The feature id for the '<em><b>Target JRE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET__TARGET_JRE = 2;

	/**
	 * The feature id for the '<em><b>Launcher Args</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET__LAUNCHER_ARGS = 3;

	/**
	 * The feature id for the '<em><b>Implicit Dependencies</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET__IMPLICIT_DEPENDENCIES = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET__NAME = 5;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET__SEQUENCE_NUMBER = 6;

	/**
	 * The number of structural features of the '<em>Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Plugin</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_DEPENDENCIES__PLUGIN = 0;

	/**
	 * The number of structural features of the '<em>Target Dependencies</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_DEPENDENCIES_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Target Dependencies</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_DEPENDENCIES_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Arch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_ENVIRONMENT__ARCH = 0;

	/**
	 * The feature id for the '<em><b>Os</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_ENVIRONMENT__OS = 1;

	/**
	 * The feature id for the '<em><b>Ws</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_ENVIRONMENT__WS = 2;

	/**
	 * The feature id for the '<em><b>Nl</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_ENVIRONMENT__NL = 3;

	/**
	 * The number of structural features of the '<em>Target Environment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_ENVIRONMENT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Target Environment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_ENVIRONMENT_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_JRE__PATH = 0;

	/**
	 * The number of structural features of the '<em>Target JRE</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_JRE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Target JRE</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_JRE_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Repository</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_LOCATION__REPOSITORY = 0;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_LOCATION__UNIT = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_LOCATION__ID = 2;

	/**
	 * The feature id for the '<em><b>Include All Platforms</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_LOCATION__INCLUDE_ALL_PLATFORMS = 3;

	/**
	 * The feature id for the '<em><b>Include Configure Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_LOCATION__INCLUDE_CONFIGURE_PHASE = 4;

	/**
	 * The feature id for the '<em><b>Include Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_LOCATION__INCLUDE_MODE = 5;

	/**
	 * The feature id for the '<em><b>Include Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_LOCATION__INCLUDE_SOURCE = 6;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_LOCATION__PATH = 7;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_LOCATION__TYPE = 8;

	/**
	 * The number of structural features of the '<em>Target Location</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_LOCATION_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Target Location</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_LOCATION_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Location</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_LOCATIONS__LOCATION = 0;

	/**
	 * The number of structural features of the '<em>Target Locations</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_LOCATIONS_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Target Locations</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_LOCATIONS_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOUCHPOINT__ID = 0;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOUCHPOINT__VERSION = 1;

	/**
	 * The number of structural features of the '<em>Touchpoint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOUCHPOINT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Touchpoint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOUCHPOINT_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOUCHPOINT_DATA__ID = 0;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOUCHPOINT_DATA__VERSION = 1;

	/**
	 * The number of structural features of the '<em>Touchpoint Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOUCHPOINT_DATA_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Touchpoint Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOUCHPOINT_DATA_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Update</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__UPDATE = 0;

	/**
	 * The feature id for the '<em><b>Artifacts</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__ARTIFACTS = 1;

	/**
	 * The feature id for the '<em><b>Host Requirements</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__HOST_REQUIREMENTS = 2;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__PROPERTIES = 3;

	/**
	 * The feature id for the '<em><b>Provides</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__PROVIDES = 4;

	/**
	 * The feature id for the '<em><b>Requires</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__REQUIRES = 5;

	/**
	 * The feature id for the '<em><b>Filter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__FILTER = 6;

	/**
	 * The feature id for the '<em><b>Touchpoint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__TOUCHPOINT = 7;

	/**
	 * The feature id for the '<em><b>Touchpoint Data</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__TOUCHPOINT_DATA = 8;

	/**
	 * The feature id for the '<em><b>Licenses</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__LICENSES = 9;

	/**
	 * The feature id for the '<em><b>Copyright</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__COPYRIGHT = 10;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__ID = 11;

	/**
	 * The feature id for the '<em><b>Singleton</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__SINGLETON = 12;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__VERSION = 13;

	/**
	 * The feature id for the '<em><b>Generation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__GENERATION = 14;

	/**
	 * The number of structural features of the '<em>Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_FEATURE_COUNT = 15;

	/**
	 * The number of operations of the '<em>Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITS_TYPE__UNIT = 0;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITS_TYPE__SIZE = 1;

	/**
	 * The number of structural features of the '<em>Units Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITS_TYPE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Units Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITS_TYPE_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Instructions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOUCHPOINT_DATA_TYPE__INSTRUCTIONS = 0;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOUCHPOINT_DATA_TYPE__SIZE = 1;

	/**
	 * The number of structural features of the '<em>Touchpoint Data Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOUCHPOINT_DATA_TYPE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Touchpoint Data Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOUCHPOINT_DATA_TYPE_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Update</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int URL_TYPE__UPDATE = 0;

	/**
	 * The feature id for the '<em><b>Discovery</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int URL_TYPE__DISCOVERY = 1;

	/**
	 * The number of structural features of the '<em>Url Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int URL_TYPE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Url Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int URL_TYPE_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPDATE__ID = 0;

	/**
	 * The feature id for the '<em><b>Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPDATE__RANGE = 1;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPDATE__SEVERITY = 2;

	/**
	 * The number of structural features of the '<em>Update</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPDATE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Update</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPDATE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.LocationIncludeType <em>Location Include Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.LocationIncludeType
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getLocationIncludeType()
	 * @generated
	 */
	int LOCATION_INCLUDE_TYPE = 48;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.MatchType <em>Match Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.MatchType
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getMatchType()
	 * @generated
	 */
	int MATCH_TYPE = 49;

	/**
	 * The meta object id for the '{@link org.gecko.eclipse.TargetType <em>Target Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.TargetType
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTargetType()
	 * @generated
	 */
	int TARGET_TYPE = 50;

	/**
	 * The meta object id for the '<em>Location Include Type Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.LocationIncludeType
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getLocationIncludeTypeObject()
	 * @generated
	 */
	int LOCATION_INCLUDE_TYPE_OBJECT = 51;

	/**
	 * The meta object id for the '<em>Match Type Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.MatchType
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getMatchTypeObject()
	 * @generated
	 */
	int MATCH_TYPE_OBJECT = 52;

	/**
	 * The meta object id for the '<em>Target Type Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.gecko.eclipse.TargetType
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTargetTypeObject()
	 * @generated
	 */
	int TARGET_TYPE_OBJECT = 53;

	/**
	 * The meta object id for the '<em>Version</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getVersion()
	 * @generated
	 */
	int VERSION = 54;


	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Artifact <em>Artifact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Artifact</em>'.
	 * @see org.gecko.eclipse.Artifact
	 * @generated
	 */
	EClass getArtifact();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Artifact#getProcessing <em>Processing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Processing</em>'.
	 * @see org.gecko.eclipse.Artifact#getProcessing()
	 * @see #getArtifact()
	 * @generated
	 */
	EReference getArtifact_Processing();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Artifact#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Properties</em>'.
	 * @see org.gecko.eclipse.Artifact#getProperties()
	 * @see #getArtifact()
	 * @generated
	 */
	EReference getArtifact_Properties();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Artifact#getRepositoryProperties <em>Repository Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Repository Properties</em>'.
	 * @see org.gecko.eclipse.Artifact#getRepositoryProperties()
	 * @see #getArtifact()
	 * @generated
	 */
	EReference getArtifact_RepositoryProperties();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Artifact#getClassifier <em>Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Classifier</em>'.
	 * @see org.gecko.eclipse.Artifact#getClassifier()
	 * @see #getArtifact()
	 * @generated
	 */
	EAttribute getArtifact_Classifier();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Artifact#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.gecko.eclipse.Artifact#getId()
	 * @see #getArtifact()
	 * @generated
	 */
	EAttribute getArtifact_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Artifact#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.gecko.eclipse.Artifact#getVersion()
	 * @see #getArtifact()
	 * @generated
	 */
	EAttribute getArtifact_Version();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Artifacts <em>Artifacts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Artifacts</em>'.
	 * @see org.gecko.eclipse.Artifacts
	 * @generated
	 */
	EClass getArtifacts();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.Artifacts#getArtifact <em>Artifact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Artifact</em>'.
	 * @see org.gecko.eclipse.Artifacts#getArtifact()
	 * @see #getArtifacts()
	 * @generated
	 */
	EReference getArtifacts_Artifact();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Artifacts#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see org.gecko.eclipse.Artifacts#getSize()
	 * @see #getArtifacts()
	 * @generated
	 */
	EAttribute getArtifacts_Size();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.CategoryDef <em>Category Def</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Category Def</em>'.
	 * @see org.gecko.eclipse.CategoryDef
	 * @generated
	 */
	EClass getCategoryDef();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.CategoryDef#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.gecko.eclipse.CategoryDef#getDescription()
	 * @see #getCategoryDef()
	 * @generated
	 */
	EAttribute getCategoryDef_Description();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.CategoryDef#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see org.gecko.eclipse.CategoryDef#getLabel()
	 * @see #getCategoryDef()
	 * @generated
	 */
	EAttribute getCategoryDef_Label();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.CategoryDef#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.gecko.eclipse.CategoryDef#getName()
	 * @see #getCategoryDef()
	 * @generated
	 */
	EAttribute getCategoryDef_Name();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.CategoryFeature <em>Category Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Category Feature</em>'.
	 * @see org.gecko.eclipse.CategoryFeature
	 * @generated
	 */
	EClass getCategoryFeature();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.CategoryFeature#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Category</em>'.
	 * @see org.gecko.eclipse.CategoryFeature#getCategory()
	 * @see #getCategoryFeature()
	 * @generated
	 */
	EReference getCategoryFeature_Category();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.CategoryFeature#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.gecko.eclipse.CategoryFeature#getId()
	 * @see #getCategoryFeature()
	 * @generated
	 */
	EAttribute getCategoryFeature_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.CategoryFeature#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.gecko.eclipse.CategoryFeature#getVersion()
	 * @see #getCategoryFeature()
	 * @generated
	 */
	EAttribute getCategoryFeature_Version();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.CategorySite <em>Category Site</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Category Site</em>'.
	 * @see org.gecko.eclipse.CategorySite
	 * @generated
	 */
	EClass getCategorySite();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.CategorySite#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Feature</em>'.
	 * @see org.gecko.eclipse.CategorySite#getFeature()
	 * @see #getCategorySite()
	 * @generated
	 */
	EReference getCategorySite_Feature();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.CategorySite#getCategoryDef <em>Category Def</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Category Def</em>'.
	 * @see org.gecko.eclipse.CategorySite#getCategoryDef()
	 * @see #getCategorySite()
	 * @generated
	 */
	EReference getCategorySite_CategoryDef();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.CategoryType <em>Category Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Category Type</em>'.
	 * @see org.gecko.eclipse.CategoryType
	 * @generated
	 */
	EClass getCategoryType();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.CategoryType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.gecko.eclipse.CategoryType#getName()
	 * @see #getCategoryType()
	 * @generated
	 */
	EAttribute getCategoryType_Name();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.ChildrenType <em>Children Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Children Type</em>'.
	 * @see org.gecko.eclipse.ChildrenType
	 * @generated
	 */
	EClass getChildrenType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.ChildrenType#getChild <em>Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Child</em>'.
	 * @see org.gecko.eclipse.ChildrenType#getChild()
	 * @see #getChildrenType()
	 * @generated
	 */
	EReference getChildrenType_Child();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.ChildrenType#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see org.gecko.eclipse.ChildrenType#getSize()
	 * @see #getChildrenType()
	 * @generated
	 */
	EAttribute getChildrenType_Size();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Description <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Description</em>'.
	 * @see org.gecko.eclipse.Description
	 * @generated
	 */
	EClass getDescription();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Description#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.gecko.eclipse.Description#getValue()
	 * @see #getDescription()
	 * @generated
	 */
	EAttribute getDescription_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Description#getUrl <em>Url</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Url</em>'.
	 * @see org.gecko.eclipse.Description#getUrl()
	 * @see #getDescription()
	 * @generated
	 */
	EAttribute getDescription_Url();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see org.gecko.eclipse.DocumentRoot
	 * @generated
	 */
	EClass getDocumentRoot();

	/**
	 * Returns the meta object for the attribute list '{@link org.gecko.eclipse.DocumentRoot#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see org.gecko.eclipse.DocumentRoot#getMixed()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Mixed();

	/**
	 * Returns the meta object for the map '{@link org.gecko.eclipse.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see org.gecko.eclipse.DocumentRoot#getXMLNSPrefixMap()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link org.gecko.eclipse.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see org.gecko.eclipse.DocumentRoot#getXSISchemaLocation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.DocumentRoot#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature</em>'.
	 * @see org.gecko.eclipse.DocumentRoot#getFeature()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Feature();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.DocumentRoot#getRepository <em>Repository</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Repository</em>'.
	 * @see org.gecko.eclipse.DocumentRoot#getRepository()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Repository();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.DocumentRoot#getSite <em>Site</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Site</em>'.
	 * @see org.gecko.eclipse.DocumentRoot#getSite()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Site();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.DocumentRoot#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Target</em>'.
	 * @see org.gecko.eclipse.DocumentRoot#getTarget()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Target();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Feature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature</em>'.
	 * @see org.gecko.eclipse.Feature
	 * @generated
	 */
	EClass getFeature();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Feature#getInstallHandler <em>Install Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Install Handler</em>'.
	 * @see org.gecko.eclipse.Feature#getInstallHandler()
	 * @see #getFeature()
	 * @generated
	 */
	EReference getFeature_InstallHandler();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Feature#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Description</em>'.
	 * @see org.gecko.eclipse.Feature#getDescription()
	 * @see #getFeature()
	 * @generated
	 */
	EReference getFeature_Description();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Feature#getCopyright <em>Copyright</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Copyright</em>'.
	 * @see org.gecko.eclipse.Feature#getCopyright()
	 * @see #getFeature()
	 * @generated
	 */
	EReference getFeature_Copyright();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Feature#getLicense <em>License</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>License</em>'.
	 * @see org.gecko.eclipse.Feature#getLicense()
	 * @see #getFeature()
	 * @generated
	 */
	EReference getFeature_License();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Feature#getUrl <em>Url</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Url</em>'.
	 * @see org.gecko.eclipse.Feature#getUrl()
	 * @see #getFeature()
	 * @generated
	 */
	EReference getFeature_Url();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.Feature#getIncludes <em>Includes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Includes</em>'.
	 * @see org.gecko.eclipse.Feature#getIncludes()
	 * @see #getFeature()
	 * @generated
	 */
	EReference getFeature_Includes();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Feature#getRequires <em>Requires</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Requires</em>'.
	 * @see org.gecko.eclipse.Feature#getRequires()
	 * @see #getFeature()
	 * @generated
	 */
	EReference getFeature_Requires();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.Feature#getPlugin <em>Plugin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Plugin</em>'.
	 * @see org.gecko.eclipse.Feature#getPlugin()
	 * @see #getFeature()
	 * @generated
	 */
	EReference getFeature_Plugin();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Feature#getArch <em>Arch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Arch</em>'.
	 * @see org.gecko.eclipse.Feature#getArch()
	 * @see #getFeature()
	 * @generated
	 */
	EAttribute getFeature_Arch();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Feature#getColocationAffinity <em>Colocation Affinity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Colocation Affinity</em>'.
	 * @see org.gecko.eclipse.Feature#getColocationAffinity()
	 * @see #getFeature()
	 * @generated
	 */
	EAttribute getFeature_ColocationAffinity();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Feature#isExclusive <em>Exclusive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exclusive</em>'.
	 * @see org.gecko.eclipse.Feature#isExclusive()
	 * @see #getFeature()
	 * @generated
	 */
	EAttribute getFeature_Exclusive();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Feature#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.gecko.eclipse.Feature#getId()
	 * @see #getFeature()
	 * @generated
	 */
	EAttribute getFeature_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Feature#getImage <em>Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Image</em>'.
	 * @see org.gecko.eclipse.Feature#getImage()
	 * @see #getFeature()
	 * @generated
	 */
	EAttribute getFeature_Image();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Feature#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see org.gecko.eclipse.Feature#getLabel()
	 * @see #getFeature()
	 * @generated
	 */
	EAttribute getFeature_Label();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Feature#getNl <em>Nl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nl</em>'.
	 * @see org.gecko.eclipse.Feature#getNl()
	 * @see #getFeature()
	 * @generated
	 */
	EAttribute getFeature_Nl();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Feature#getOs <em>Os</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Os</em>'.
	 * @see org.gecko.eclipse.Feature#getOs()
	 * @see #getFeature()
	 * @generated
	 */
	EAttribute getFeature_Os();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Feature#getPlugin1 <em>Plugin1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Plugin1</em>'.
	 * @see org.gecko.eclipse.Feature#getPlugin1()
	 * @see #getFeature()
	 * @generated
	 */
	EAttribute getFeature_Plugin1();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Feature#getProviderName <em>Provider Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Provider Name</em>'.
	 * @see org.gecko.eclipse.Feature#getProviderName()
	 * @see #getFeature()
	 * @generated
	 */
	EAttribute getFeature_ProviderName();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Feature#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.gecko.eclipse.Feature#getVersion()
	 * @see #getFeature()
	 * @generated
	 */
	EAttribute getFeature_Version();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Feature#getWs <em>Ws</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ws</em>'.
	 * @see org.gecko.eclipse.Feature#getWs()
	 * @see #getFeature()
	 * @generated
	 */
	EAttribute getFeature_Ws();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Import <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import</em>'.
	 * @see org.gecko.eclipse.Import
	 * @generated
	 */
	EClass getImport();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Import#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Feature</em>'.
	 * @see org.gecko.eclipse.Import#getFeature()
	 * @see #getImport()
	 * @generated
	 */
	EAttribute getImport_Feature();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Import#getMatch <em>Match</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Match</em>'.
	 * @see org.gecko.eclipse.Import#getMatch()
	 * @see #getImport()
	 * @generated
	 */
	EAttribute getImport_Match();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Import#isPatch <em>Patch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Patch</em>'.
	 * @see org.gecko.eclipse.Import#isPatch()
	 * @see #getImport()
	 * @generated
	 */
	EAttribute getImport_Patch();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Import#getPlugin <em>Plugin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Plugin</em>'.
	 * @see org.gecko.eclipse.Import#getPlugin()
	 * @see #getImport()
	 * @generated
	 */
	EAttribute getImport_Plugin();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Import#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.gecko.eclipse.Import#getVersion()
	 * @see #getImport()
	 * @generated
	 */
	EAttribute getImport_Version();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.ImportRequires <em>Import Requires</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import Requires</em>'.
	 * @see org.gecko.eclipse.ImportRequires
	 * @generated
	 */
	EClass getImportRequires();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.ImportRequires#getImport <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Import</em>'.
	 * @see org.gecko.eclipse.ImportRequires#getImport()
	 * @see #getImportRequires()
	 * @generated
	 */
	EReference getImportRequires_Import();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Include <em>Include</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Include</em>'.
	 * @see org.gecko.eclipse.Include
	 * @generated
	 */
	EClass getInclude();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Include#getArch <em>Arch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Arch</em>'.
	 * @see org.gecko.eclipse.Include#getArch()
	 * @see #getInclude()
	 * @generated
	 */
	EAttribute getInclude_Arch();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Include#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.gecko.eclipse.Include#getId()
	 * @see #getInclude()
	 * @generated
	 */
	EAttribute getInclude_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Include#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.gecko.eclipse.Include#getName()
	 * @see #getInclude()
	 * @generated
	 */
	EAttribute getInclude_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Include#getNl <em>Nl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nl</em>'.
	 * @see org.gecko.eclipse.Include#getNl()
	 * @see #getInclude()
	 * @generated
	 */
	EAttribute getInclude_Nl();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Include#isOptional <em>Optional</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Optional</em>'.
	 * @see org.gecko.eclipse.Include#isOptional()
	 * @see #getInclude()
	 * @generated
	 */
	EAttribute getInclude_Optional();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Include#getOs <em>Os</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Os</em>'.
	 * @see org.gecko.eclipse.Include#getOs()
	 * @see #getInclude()
	 * @generated
	 */
	EAttribute getInclude_Os();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Include#getSearchLocation <em>Search Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Search Location</em>'.
	 * @see org.gecko.eclipse.Include#getSearchLocation()
	 * @see #getInclude()
	 * @generated
	 */
	EAttribute getInclude_SearchLocation();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Include#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.gecko.eclipse.Include#getVersion()
	 * @see #getInclude()
	 * @generated
	 */
	EAttribute getInclude_Version();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Include#getWs <em>Ws</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ws</em>'.
	 * @see org.gecko.eclipse.Include#getWs()
	 * @see #getInclude()
	 * @generated
	 */
	EAttribute getInclude_Ws();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.InstallHandler <em>Install Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Install Handler</em>'.
	 * @see org.gecko.eclipse.InstallHandler
	 * @generated
	 */
	EClass getInstallHandler();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.InstallHandler#getHandler <em>Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Handler</em>'.
	 * @see org.gecko.eclipse.InstallHandler#getHandler()
	 * @see #getInstallHandler()
	 * @generated
	 */
	EAttribute getInstallHandler_Handler();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.InstallHandler#getLibrary <em>Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Library</em>'.
	 * @see org.gecko.eclipse.InstallHandler#getLibrary()
	 * @see #getInstallHandler()
	 * @generated
	 */
	EAttribute getInstallHandler_Library();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Instruction <em>Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Instruction</em>'.
	 * @see org.gecko.eclipse.Instruction
	 * @generated
	 */
	EClass getInstruction();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Instruction#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.gecko.eclipse.Instruction#getValue()
	 * @see #getInstruction()
	 * @generated
	 */
	EAttribute getInstruction_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Instruction#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see org.gecko.eclipse.Instruction#getKey()
	 * @see #getInstruction()
	 * @generated
	 */
	EAttribute getInstruction_Key();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.InstructionsType <em>Instructions Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Instructions Type</em>'.
	 * @see org.gecko.eclipse.InstructionsType
	 * @generated
	 */
	EClass getInstructionsType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.InstructionsType#getInstruction <em>Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Instruction</em>'.
	 * @see org.gecko.eclipse.InstructionsType#getInstruction()
	 * @see #getInstructionsType()
	 * @generated
	 */
	EReference getInstructionsType_Instruction();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.InstructionsType#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see org.gecko.eclipse.InstructionsType#getSize()
	 * @see #getInstructionsType()
	 * @generated
	 */
	EAttribute getInstructionsType_Size();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.LauncherArgs <em>Launcher Args</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Launcher Args</em>'.
	 * @see org.gecko.eclipse.LauncherArgs
	 * @generated
	 */
	EClass getLauncherArgs();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.LauncherArgs#getProgramArgs <em>Program Args</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Program Args</em>'.
	 * @see org.gecko.eclipse.LauncherArgs#getProgramArgs()
	 * @see #getLauncherArgs()
	 * @generated
	 */
	EAttribute getLauncherArgs_ProgramArgs();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.LauncherArgs#getVmArgs <em>Vm Args</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Vm Args</em>'.
	 * @see org.gecko.eclipse.LauncherArgs#getVmArgs()
	 * @see #getLauncherArgs()
	 * @generated
	 */
	EAttribute getLauncherArgs_VmArgs();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.LicensesType <em>Licenses Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Licenses Type</em>'.
	 * @see org.gecko.eclipse.LicensesType
	 * @generated
	 */
	EClass getLicensesType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.LicensesType#getLicense <em>License</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>License</em>'.
	 * @see org.gecko.eclipse.LicensesType#getLicense()
	 * @see #getLicensesType()
	 * @generated
	 */
	EReference getLicensesType_License();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.LicensesType#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see org.gecko.eclipse.LicensesType#getSize()
	 * @see #getLicensesType()
	 * @generated
	 */
	EAttribute getLicensesType_Size();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.MappingsType <em>Mappings Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mappings Type</em>'.
	 * @see org.gecko.eclipse.MappingsType
	 * @generated
	 */
	EClass getMappingsType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.MappingsType#getRule <em>Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rule</em>'.
	 * @see org.gecko.eclipse.MappingsType#getRule()
	 * @see #getMappingsType()
	 * @generated
	 */
	EReference getMappingsType_Rule();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.MappingsType#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see org.gecko.eclipse.MappingsType#getSize()
	 * @see #getMappingsType()
	 * @generated
	 */
	EAttribute getMappingsType_Size();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Plugin <em>Plugin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Plugin</em>'.
	 * @see org.gecko.eclipse.Plugin
	 * @generated
	 */
	EClass getPlugin();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Plugin#getArch <em>Arch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Arch</em>'.
	 * @see org.gecko.eclipse.Plugin#getArch()
	 * @see #getPlugin()
	 * @generated
	 */
	EAttribute getPlugin_Arch();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Plugin#getDownloadSize <em>Download Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Download Size</em>'.
	 * @see org.gecko.eclipse.Plugin#getDownloadSize()
	 * @see #getPlugin()
	 * @generated
	 */
	EAttribute getPlugin_DownloadSize();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Plugin#isFragment <em>Fragment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fragment</em>'.
	 * @see org.gecko.eclipse.Plugin#isFragment()
	 * @see #getPlugin()
	 * @generated
	 */
	EAttribute getPlugin_Fragment();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Plugin#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.gecko.eclipse.Plugin#getId()
	 * @see #getPlugin()
	 * @generated
	 */
	EAttribute getPlugin_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Plugin#getInstallSize <em>Install Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Install Size</em>'.
	 * @see org.gecko.eclipse.Plugin#getInstallSize()
	 * @see #getPlugin()
	 * @generated
	 */
	EAttribute getPlugin_InstallSize();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Plugin#getNl <em>Nl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nl</em>'.
	 * @see org.gecko.eclipse.Plugin#getNl()
	 * @see #getPlugin()
	 * @generated
	 */
	EAttribute getPlugin_Nl();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Plugin#getOs <em>Os</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Os</em>'.
	 * @see org.gecko.eclipse.Plugin#getOs()
	 * @see #getPlugin()
	 * @generated
	 */
	EAttribute getPlugin_Os();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Plugin#isUnpack <em>Unpack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unpack</em>'.
	 * @see org.gecko.eclipse.Plugin#isUnpack()
	 * @see #getPlugin()
	 * @generated
	 */
	EAttribute getPlugin_Unpack();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Plugin#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.gecko.eclipse.Plugin#getVersion()
	 * @see #getPlugin()
	 * @generated
	 */
	EAttribute getPlugin_Version();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Plugin#getWs <em>Ws</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ws</em>'.
	 * @see org.gecko.eclipse.Plugin#getWs()
	 * @see #getPlugin()
	 * @generated
	 */
	EAttribute getPlugin_Ws();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Processing <em>Processing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Processing</em>'.
	 * @see org.gecko.eclipse.Processing
	 * @generated
	 */
	EClass getProcessing();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.Processing#getStep <em>Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Step</em>'.
	 * @see org.gecko.eclipse.Processing#getStep()
	 * @see #getProcessing()
	 * @generated
	 */
	EReference getProcessing_Step();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Processing#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see org.gecko.eclipse.Processing#getSize()
	 * @see #getProcessing()
	 * @generated
	 */
	EAttribute getProcessing_Size();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.ProcessStep <em>Process Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Process Step</em>'.
	 * @see org.gecko.eclipse.ProcessStep
	 * @generated
	 */
	EClass getProcessStep();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.ProcessStep#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.gecko.eclipse.ProcessStep#getId()
	 * @see #getProcessStep()
	 * @generated
	 */
	EAttribute getProcessStep_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.ProcessStep#isRequired <em>Required</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Required</em>'.
	 * @see org.gecko.eclipse.ProcessStep#isRequired()
	 * @see #getProcessStep()
	 * @generated
	 */
	EAttribute getProcessStep_Required();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Properties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Properties</em>'.
	 * @see org.gecko.eclipse.Properties
	 * @generated
	 */
	EClass getProperties();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.Properties#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Property</em>'.
	 * @see org.gecko.eclipse.Properties#getProperty()
	 * @see #getProperties()
	 * @generated
	 */
	EReference getProperties_Property();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Properties#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see org.gecko.eclipse.Properties#getSize()
	 * @see #getProperties()
	 * @generated
	 */
	EAttribute getProperties_Size();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property</em>'.
	 * @see org.gecko.eclipse.Property
	 * @generated
	 */
	EClass getProperty();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Property#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.gecko.eclipse.Property#getName()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Property#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.gecko.eclipse.Property#getValue()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Value();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Provided <em>Provided</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provided</em>'.
	 * @see org.gecko.eclipse.Provided
	 * @generated
	 */
	EClass getProvided();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Provided#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.gecko.eclipse.Provided#getName()
	 * @see #getProvided()
	 * @generated
	 */
	EAttribute getProvided_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Provided#getNamespace <em>Namespace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Namespace</em>'.
	 * @see org.gecko.eclipse.Provided#getNamespace()
	 * @see #getProvided()
	 * @generated
	 */
	EAttribute getProvided_Namespace();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Provided#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.gecko.eclipse.Provided#getVersion()
	 * @see #getProvided()
	 * @generated
	 */
	EAttribute getProvided_Version();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Provided#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Properties</em>'.
	 * @see org.gecko.eclipse.Provided#getProperties()
	 * @see #getProvided()
	 * @generated
	 */
	EReference getProvided_Properties();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Provides <em>Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provides</em>'.
	 * @see org.gecko.eclipse.Provides
	 * @generated
	 */
	EClass getProvides();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.Provides#getProvided <em>Provided</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Provided</em>'.
	 * @see org.gecko.eclipse.Provides#getProvided()
	 * @see #getProvides()
	 * @generated
	 */
	EReference getProvides_Provided();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Provides#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see org.gecko.eclipse.Provides#getSize()
	 * @see #getProvides()
	 * @generated
	 */
	EAttribute getProvides_Size();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.ReferencesType <em>References Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>References Type</em>'.
	 * @see org.gecko.eclipse.ReferencesType
	 * @generated
	 */
	EClass getReferencesType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.ReferencesType#getRepository <em>Repository</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Repository</em>'.
	 * @see org.gecko.eclipse.ReferencesType#getRepository()
	 * @see #getReferencesType()
	 * @generated
	 */
	EReference getReferencesType_Repository();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.ReferencesType#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see org.gecko.eclipse.ReferencesType#getSize()
	 * @see #getReferencesType()
	 * @generated
	 */
	EAttribute getReferencesType_Size();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Repository <em>Repository</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Repository</em>'.
	 * @see org.gecko.eclipse.Repository
	 * @generated
	 */
	EClass getRepository();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Repository#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Properties</em>'.
	 * @see org.gecko.eclipse.Repository#getProperties()
	 * @see #getRepository()
	 * @generated
	 */
	EReference getRepository_Properties();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Repository#getMappings <em>Mappings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Mappings</em>'.
	 * @see org.gecko.eclipse.Repository#getMappings()
	 * @see #getRepository()
	 * @generated
	 */
	EReference getRepository_Mappings();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Repository#getReferences <em>References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>References</em>'.
	 * @see org.gecko.eclipse.Repository#getReferences()
	 * @see #getRepository()
	 * @generated
	 */
	EReference getRepository_References();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Repository#getUnits <em>Units</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Units</em>'.
	 * @see org.gecko.eclipse.Repository#getUnits()
	 * @see #getRepository()
	 * @generated
	 */
	EReference getRepository_Units();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Repository#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Children</em>'.
	 * @see org.gecko.eclipse.Repository#getChildren()
	 * @see #getRepository()
	 * @generated
	 */
	EReference getRepository_Children();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Repository#getArtifacts <em>Artifacts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Artifacts</em>'.
	 * @see org.gecko.eclipse.Repository#getArtifacts()
	 * @see #getRepository()
	 * @generated
	 */
	EReference getRepository_Artifacts();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Repository#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.gecko.eclipse.Repository#getName()
	 * @see #getRepository()
	 * @generated
	 */
	EAttribute getRepository_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Repository#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.gecko.eclipse.Repository#getType()
	 * @see #getRepository()
	 * @generated
	 */
	EAttribute getRepository_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Repository#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.gecko.eclipse.Repository#getVersion()
	 * @see #getRepository()
	 * @generated
	 */
	EAttribute getRepository_Version();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.RepositoryRef <em>Repository Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Repository Ref</em>'.
	 * @see org.gecko.eclipse.RepositoryRef
	 * @generated
	 */
	EClass getRepositoryRef();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.RepositoryRef#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Location</em>'.
	 * @see org.gecko.eclipse.RepositoryRef#getLocation()
	 * @see #getRepositoryRef()
	 * @generated
	 */
	EAttribute getRepositoryRef_Location();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.RepositoryRefs <em>Repository Refs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Repository Refs</em>'.
	 * @see org.gecko.eclipse.RepositoryRefs
	 * @generated
	 */
	EClass getRepositoryRefs();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.RepositoryRefs#getOptions <em>Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Options</em>'.
	 * @see org.gecko.eclipse.RepositoryRefs#getOptions()
	 * @see #getRepositoryRefs()
	 * @generated
	 */
	EAttribute getRepositoryRefs_Options();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.RepositoryRefs#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.gecko.eclipse.RepositoryRefs#getType()
	 * @see #getRepositoryRefs()
	 * @generated
	 */
	EAttribute getRepositoryRefs_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.RepositoryRefs#getUri <em>Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uri</em>'.
	 * @see org.gecko.eclipse.RepositoryRefs#getUri()
	 * @see #getRepositoryRefs()
	 * @generated
	 */
	EAttribute getRepositoryRefs_Uri();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.RepositoryRefs#getUrl <em>Url</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Url</em>'.
	 * @see org.gecko.eclipse.RepositoryRefs#getUrl()
	 * @see #getRepositoryRefs()
	 * @generated
	 */
	EAttribute getRepositoryRefs_Url();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Required <em>Required</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Required</em>'.
	 * @see org.gecko.eclipse.Required
	 * @generated
	 */
	EClass getRequired();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Required#getFilter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filter</em>'.
	 * @see org.gecko.eclipse.Required#getFilter()
	 * @see #getRequired()
	 * @generated
	 */
	EAttribute getRequired_Filter();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Required#isGreedy <em>Greedy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Greedy</em>'.
	 * @see org.gecko.eclipse.Required#isGreedy()
	 * @see #getRequired()
	 * @generated
	 */
	EAttribute getRequired_Greedy();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Required#isMultiple <em>Multiple</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Multiple</em>'.
	 * @see org.gecko.eclipse.Required#isMultiple()
	 * @see #getRequired()
	 * @generated
	 */
	EAttribute getRequired_Multiple();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Required#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.gecko.eclipse.Required#getName()
	 * @see #getRequired()
	 * @generated
	 */
	EAttribute getRequired_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Required#getNamespace <em>Namespace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Namespace</em>'.
	 * @see org.gecko.eclipse.Required#getNamespace()
	 * @see #getRequired()
	 * @generated
	 */
	EAttribute getRequired_Namespace();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Required#isOptional <em>Optional</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Optional</em>'.
	 * @see org.gecko.eclipse.Required#isOptional()
	 * @see #getRequired()
	 * @generated
	 */
	EAttribute getRequired_Optional();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Required#getRange <em>Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Range</em>'.
	 * @see org.gecko.eclipse.Required#getRange()
	 * @see #getRequired()
	 * @generated
	 */
	EAttribute getRequired_Range();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Requires <em>Requires</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Requires</em>'.
	 * @see org.gecko.eclipse.Requires
	 * @generated
	 */
	EClass getRequires();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.Requires#getRequired <em>Required</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Required</em>'.
	 * @see org.gecko.eclipse.Requires#getRequired()
	 * @see #getRequires()
	 * @generated
	 */
	EReference getRequires_Required();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.Requires#getRequiredProperties <em>Required Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Required Properties</em>'.
	 * @see org.gecko.eclipse.Requires#getRequiredProperties()
	 * @see #getRequires()
	 * @generated
	 */
	EReference getRequires_RequiredProperties();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Requires#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see org.gecko.eclipse.Requires#getSize()
	 * @see #getRequires()
	 * @generated
	 */
	EAttribute getRequires_Size();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Rule <em>Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule</em>'.
	 * @see org.gecko.eclipse.Rule
	 * @generated
	 */
	EClass getRule();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Rule#getFilter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filter</em>'.
	 * @see org.gecko.eclipse.Rule#getFilter()
	 * @see #getRule()
	 * @generated
	 */
	EAttribute getRule_Filter();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Rule#getOutput <em>Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output</em>'.
	 * @see org.gecko.eclipse.Rule#getOutput()
	 * @see #getRule()
	 * @generated
	 */
	EAttribute getRule_Output();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Site <em>Site</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Site</em>'.
	 * @see org.gecko.eclipse.Site
	 * @generated
	 */
	EClass getSite();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Site#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see org.gecko.eclipse.Site#getLabel()
	 * @see #getSite()
	 * @generated
	 */
	EAttribute getSite_Label();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Site#getUrl <em>Url</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Url</em>'.
	 * @see org.gecko.eclipse.Site#getUrl()
	 * @see #getSite()
	 * @generated
	 */
	EAttribute getSite_Url();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Target <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Target</em>'.
	 * @see org.gecko.eclipse.Target
	 * @generated
	 */
	EClass getTarget();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Target#getLocations <em>Locations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Locations</em>'.
	 * @see org.gecko.eclipse.Target#getLocations()
	 * @see #getTarget()
	 * @generated
	 */
	EReference getTarget_Locations();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Target#getEnvironment <em>Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Environment</em>'.
	 * @see org.gecko.eclipse.Target#getEnvironment()
	 * @see #getTarget()
	 * @generated
	 */
	EReference getTarget_Environment();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Target#getTargetJRE <em>Target JRE</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Target JRE</em>'.
	 * @see org.gecko.eclipse.Target#getTargetJRE()
	 * @see #getTarget()
	 * @generated
	 */
	EReference getTarget_TargetJRE();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Target#getLauncherArgs <em>Launcher Args</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Launcher Args</em>'.
	 * @see org.gecko.eclipse.Target#getLauncherArgs()
	 * @see #getTarget()
	 * @generated
	 */
	EReference getTarget_LauncherArgs();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Target#getImplicitDependencies <em>Implicit Dependencies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Implicit Dependencies</em>'.
	 * @see org.gecko.eclipse.Target#getImplicitDependencies()
	 * @see #getTarget()
	 * @generated
	 */
	EReference getTarget_ImplicitDependencies();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Target#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.gecko.eclipse.Target#getName()
	 * @see #getTarget()
	 * @generated
	 */
	EAttribute getTarget_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Target#getSequenceNumber <em>Sequence Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sequence Number</em>'.
	 * @see org.gecko.eclipse.Target#getSequenceNumber()
	 * @see #getTarget()
	 * @generated
	 */
	EAttribute getTarget_SequenceNumber();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.TargetDependencies <em>Target Dependencies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Target Dependencies</em>'.
	 * @see org.gecko.eclipse.TargetDependencies
	 * @generated
	 */
	EClass getTargetDependencies();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.TargetDependencies#getPlugin <em>Plugin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Plugin</em>'.
	 * @see org.gecko.eclipse.TargetDependencies#getPlugin()
	 * @see #getTargetDependencies()
	 * @generated
	 */
	EReference getTargetDependencies_Plugin();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.TargetEnvironment <em>Target Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Target Environment</em>'.
	 * @see org.gecko.eclipse.TargetEnvironment
	 * @generated
	 */
	EClass getTargetEnvironment();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.TargetEnvironment#getArch <em>Arch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Arch</em>'.
	 * @see org.gecko.eclipse.TargetEnvironment#getArch()
	 * @see #getTargetEnvironment()
	 * @generated
	 */
	EAttribute getTargetEnvironment_Arch();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.TargetEnvironment#getOs <em>Os</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Os</em>'.
	 * @see org.gecko.eclipse.TargetEnvironment#getOs()
	 * @see #getTargetEnvironment()
	 * @generated
	 */
	EAttribute getTargetEnvironment_Os();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.TargetEnvironment#getWs <em>Ws</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ws</em>'.
	 * @see org.gecko.eclipse.TargetEnvironment#getWs()
	 * @see #getTargetEnvironment()
	 * @generated
	 */
	EAttribute getTargetEnvironment_Ws();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.TargetEnvironment#getNl <em>Nl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nl</em>'.
	 * @see org.gecko.eclipse.TargetEnvironment#getNl()
	 * @see #getTargetEnvironment()
	 * @generated
	 */
	EAttribute getTargetEnvironment_Nl();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.TargetJRE <em>Target JRE</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Target JRE</em>'.
	 * @see org.gecko.eclipse.TargetJRE
	 * @generated
	 */
	EClass getTargetJRE();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.TargetJRE#getPath <em>Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Path</em>'.
	 * @see org.gecko.eclipse.TargetJRE#getPath()
	 * @see #getTargetJRE()
	 * @generated
	 */
	EAttribute getTargetJRE_Path();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.TargetLocation <em>Target Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Target Location</em>'.
	 * @see org.gecko.eclipse.TargetLocation
	 * @generated
	 */
	EClass getTargetLocation();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.TargetLocation#getRepository <em>Repository</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Repository</em>'.
	 * @see org.gecko.eclipse.TargetLocation#getRepository()
	 * @see #getTargetLocation()
	 * @generated
	 */
	EReference getTargetLocation_Repository();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.TargetLocation#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Unit</em>'.
	 * @see org.gecko.eclipse.TargetLocation#getUnit()
	 * @see #getTargetLocation()
	 * @generated
	 */
	EReference getTargetLocation_Unit();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.TargetLocation#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.gecko.eclipse.TargetLocation#getId()
	 * @see #getTargetLocation()
	 * @generated
	 */
	EAttribute getTargetLocation_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.TargetLocation#isIncludeAllPlatforms <em>Include All Platforms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Include All Platforms</em>'.
	 * @see org.gecko.eclipse.TargetLocation#isIncludeAllPlatforms()
	 * @see #getTargetLocation()
	 * @generated
	 */
	EAttribute getTargetLocation_IncludeAllPlatforms();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.TargetLocation#isIncludeConfigurePhase <em>Include Configure Phase</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Include Configure Phase</em>'.
	 * @see org.gecko.eclipse.TargetLocation#isIncludeConfigurePhase()
	 * @see #getTargetLocation()
	 * @generated
	 */
	EAttribute getTargetLocation_IncludeConfigurePhase();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.TargetLocation#getIncludeMode <em>Include Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Include Mode</em>'.
	 * @see org.gecko.eclipse.TargetLocation#getIncludeMode()
	 * @see #getTargetLocation()
	 * @generated
	 */
	EAttribute getTargetLocation_IncludeMode();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.TargetLocation#isIncludeSource <em>Include Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Include Source</em>'.
	 * @see org.gecko.eclipse.TargetLocation#isIncludeSource()
	 * @see #getTargetLocation()
	 * @generated
	 */
	EAttribute getTargetLocation_IncludeSource();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.TargetLocation#getPath <em>Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Path</em>'.
	 * @see org.gecko.eclipse.TargetLocation#getPath()
	 * @see #getTargetLocation()
	 * @generated
	 */
	EAttribute getTargetLocation_Path();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.TargetLocation#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.gecko.eclipse.TargetLocation#getType()
	 * @see #getTargetLocation()
	 * @generated
	 */
	EAttribute getTargetLocation_Type();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.TargetLocations <em>Target Locations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Target Locations</em>'.
	 * @see org.gecko.eclipse.TargetLocations
	 * @generated
	 */
	EClass getTargetLocations();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.TargetLocations#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Location</em>'.
	 * @see org.gecko.eclipse.TargetLocations#getLocation()
	 * @see #getTargetLocations()
	 * @generated
	 */
	EReference getTargetLocations_Location();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Touchpoint <em>Touchpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Touchpoint</em>'.
	 * @see org.gecko.eclipse.Touchpoint
	 * @generated
	 */
	EClass getTouchpoint();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Touchpoint#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.gecko.eclipse.Touchpoint#getId()
	 * @see #getTouchpoint()
	 * @generated
	 */
	EAttribute getTouchpoint_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Touchpoint#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.gecko.eclipse.Touchpoint#getVersion()
	 * @see #getTouchpoint()
	 * @generated
	 */
	EAttribute getTouchpoint_Version();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.TouchpointData <em>Touchpoint Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Touchpoint Data</em>'.
	 * @see org.gecko.eclipse.TouchpointData
	 * @generated
	 */
	EClass getTouchpointData();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.TouchpointData#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.gecko.eclipse.TouchpointData#getId()
	 * @see #getTouchpointData()
	 * @generated
	 */
	EAttribute getTouchpointData_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.TouchpointData#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.gecko.eclipse.TouchpointData#getVersion()
	 * @see #getTouchpointData()
	 * @generated
	 */
	EAttribute getTouchpointData_Version();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.TouchpointDataType <em>Touchpoint Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Touchpoint Data Type</em>'.
	 * @see org.gecko.eclipse.TouchpointDataType
	 * @generated
	 */
	EClass getTouchpointDataType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.TouchpointDataType#getInstructions <em>Instructions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Instructions</em>'.
	 * @see org.gecko.eclipse.TouchpointDataType#getInstructions()
	 * @see #getTouchpointDataType()
	 * @generated
	 */
	EReference getTouchpointDataType_Instructions();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.TouchpointDataType#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see org.gecko.eclipse.TouchpointDataType#getSize()
	 * @see #getTouchpointDataType()
	 * @generated
	 */
	EAttribute getTouchpointDataType_Size();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Unit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unit</em>'.
	 * @see org.gecko.eclipse.Unit
	 * @generated
	 */
	EClass getUnit();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Unit#getUpdate <em>Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Update</em>'.
	 * @see org.gecko.eclipse.Unit#getUpdate()
	 * @see #getUnit()
	 * @generated
	 */
	EReference getUnit_Update();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Unit#getArtifacts <em>Artifacts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Artifacts</em>'.
	 * @see org.gecko.eclipse.Unit#getArtifacts()
	 * @see #getUnit()
	 * @generated
	 */
	EReference getUnit_Artifacts();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Unit#getHostRequirements <em>Host Requirements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Host Requirements</em>'.
	 * @see org.gecko.eclipse.Unit#getHostRequirements()
	 * @see #getUnit()
	 * @generated
	 */
	EReference getUnit_HostRequirements();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Unit#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Properties</em>'.
	 * @see org.gecko.eclipse.Unit#getProperties()
	 * @see #getUnit()
	 * @generated
	 */
	EReference getUnit_Properties();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Unit#getProvides <em>Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Provides</em>'.
	 * @see org.gecko.eclipse.Unit#getProvides()
	 * @see #getUnit()
	 * @generated
	 */
	EReference getUnit_Provides();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Unit#getRequires <em>Requires</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Requires</em>'.
	 * @see org.gecko.eclipse.Unit#getRequires()
	 * @see #getUnit()
	 * @generated
	 */
	EReference getUnit_Requires();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Unit#getFilter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filter</em>'.
	 * @see org.gecko.eclipse.Unit#getFilter()
	 * @see #getUnit()
	 * @generated
	 */
	EAttribute getUnit_Filter();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Unit#getTouchpoint <em>Touchpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Touchpoint</em>'.
	 * @see org.gecko.eclipse.Unit#getTouchpoint()
	 * @see #getUnit()
	 * @generated
	 */
	EReference getUnit_Touchpoint();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Unit#getTouchpointData <em>Touchpoint Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Touchpoint Data</em>'.
	 * @see org.gecko.eclipse.Unit#getTouchpointData()
	 * @see #getUnit()
	 * @generated
	 */
	EReference getUnit_TouchpointData();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Unit#getLicenses <em>Licenses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Licenses</em>'.
	 * @see org.gecko.eclipse.Unit#getLicenses()
	 * @see #getUnit()
	 * @generated
	 */
	EReference getUnit_Licenses();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.Unit#getCopyright <em>Copyright</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Copyright</em>'.
	 * @see org.gecko.eclipse.Unit#getCopyright()
	 * @see #getUnit()
	 * @generated
	 */
	EReference getUnit_Copyright();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Unit#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.gecko.eclipse.Unit#getId()
	 * @see #getUnit()
	 * @generated
	 */
	EAttribute getUnit_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Unit#isSingleton <em>Singleton</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Singleton</em>'.
	 * @see org.gecko.eclipse.Unit#isSingleton()
	 * @see #getUnit()
	 * @generated
	 */
	EAttribute getUnit_Singleton();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Unit#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.gecko.eclipse.Unit#getVersion()
	 * @see #getUnit()
	 * @generated
	 */
	EAttribute getUnit_Version();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Unit#getGeneration <em>Generation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Generation</em>'.
	 * @see org.gecko.eclipse.Unit#getGeneration()
	 * @see #getUnit()
	 * @generated
	 */
	EAttribute getUnit_Generation();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.UnitsType <em>Units Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Units Type</em>'.
	 * @see org.gecko.eclipse.UnitsType
	 * @generated
	 */
	EClass getUnitsType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.UnitsType#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Unit</em>'.
	 * @see org.gecko.eclipse.UnitsType#getUnit()
	 * @see #getUnitsType()
	 * @generated
	 */
	EReference getUnitsType_Unit();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.UnitsType#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see org.gecko.eclipse.UnitsType#getSize()
	 * @see #getUnitsType()
	 * @generated
	 */
	EAttribute getUnitsType_Size();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.Update <em>Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Update</em>'.
	 * @see org.gecko.eclipse.Update
	 * @generated
	 */
	EClass getUpdate();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Update#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.gecko.eclipse.Update#getId()
	 * @see #getUpdate()
	 * @generated
	 */
	EAttribute getUpdate_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Update#getRange <em>Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Range</em>'.
	 * @see org.gecko.eclipse.Update#getRange()
	 * @see #getUpdate()
	 * @generated
	 */
	EAttribute getUpdate_Range();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.Update#getSeverity <em>Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Severity</em>'.
	 * @see org.gecko.eclipse.Update#getSeverity()
	 * @see #getUpdate()
	 * @generated
	 */
	EAttribute getUpdate_Severity();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.UrlType <em>Url Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Url Type</em>'.
	 * @see org.gecko.eclipse.UrlType
	 * @generated
	 */
	EClass getUrlType();

	/**
	 * Returns the meta object for the containment reference '{@link org.gecko.eclipse.UrlType#getUpdate <em>Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Update</em>'.
	 * @see org.gecko.eclipse.UrlType#getUpdate()
	 * @see #getUrlType()
	 * @generated
	 */
	EReference getUrlType_Update();

	/**
	 * Returns the meta object for the containment reference list '{@link org.gecko.eclipse.UrlType#getDiscovery <em>Discovery</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Discovery</em>'.
	 * @see org.gecko.eclipse.UrlType#getDiscovery()
	 * @see #getUrlType()
	 * @generated
	 */
	EReference getUrlType_Discovery();

	/**
	 * Returns the meta object for class '{@link org.gecko.eclipse.RequiredProperties <em>Required Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Required Properties</em>'.
	 * @see org.gecko.eclipse.RequiredProperties
	 * @generated
	 */
	EClass getRequiredProperties();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.RequiredProperties#getMatch <em>Match</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Match</em>'.
	 * @see org.gecko.eclipse.RequiredProperties#getMatch()
	 * @see #getRequiredProperties()
	 * @generated
	 */
	EAttribute getRequiredProperties_Match();

	/**
	 * Returns the meta object for the attribute '{@link org.gecko.eclipse.RequiredProperties#getNamespace <em>Namespace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Namespace</em>'.
	 * @see org.gecko.eclipse.RequiredProperties#getNamespace()
	 * @see #getRequiredProperties()
	 * @generated
	 */
	EAttribute getRequiredProperties_Namespace();

	/**
	 * Returns the meta object for enum '{@link org.gecko.eclipse.LocationIncludeType <em>Location Include Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Location Include Type</em>'.
	 * @see org.gecko.eclipse.LocationIncludeType
	 * @generated
	 */
	EEnum getLocationIncludeType();

	/**
	 * Returns the meta object for enum '{@link org.gecko.eclipse.MatchType <em>Match Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Match Type</em>'.
	 * @see org.gecko.eclipse.MatchType
	 * @generated
	 */
	EEnum getMatchType();

	/**
	 * Returns the meta object for enum '{@link org.gecko.eclipse.TargetType <em>Target Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Target Type</em>'.
	 * @see org.gecko.eclipse.TargetType
	 * @generated
	 */
	EEnum getTargetType();

	/**
	 * Returns the meta object for data type '{@link org.gecko.eclipse.LocationIncludeType <em>Location Include Type Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Location Include Type Object</em>'.
	 * @see org.gecko.eclipse.LocationIncludeType
	 * @model instanceClass="org.gecko.eclipse.LocationIncludeType"
	 *        extendedMetaData="name='LocationIncludeType:Object' baseType='LocationIncludeType'"
	 * @generated
	 */
	EDataType getLocationIncludeTypeObject();

	/**
	 * Returns the meta object for data type '{@link org.gecko.eclipse.MatchType <em>Match Type Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Match Type Object</em>'.
	 * @see org.gecko.eclipse.MatchType
	 * @model instanceClass="org.gecko.eclipse.MatchType"
	 *        extendedMetaData="name='MatchType:Object' baseType='MatchType'"
	 * @generated
	 */
	EDataType getMatchTypeObject();

	/**
	 * Returns the meta object for data type '{@link org.gecko.eclipse.TargetType <em>Target Type Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Target Type Object</em>'.
	 * @see org.gecko.eclipse.TargetType
	 * @model instanceClass="org.gecko.eclipse.TargetType"
	 *        extendedMetaData="name='TargetType:Object' baseType='TargetType'"
	 * @generated
	 */
	EDataType getTargetTypeObject();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Version</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 *        extendedMetaData="name='Version' baseType='http://www.eclipse.org/emf/2003/XMLType#string' pattern='[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}(\\..*)?'"
	 * @generated
	 */
	EDataType getVersion();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	EclipseFactory getEclipseFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.ArtifactImpl <em>Artifact</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.ArtifactImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getArtifact()
		 * @generated
		 */
		EClass ARTIFACT = eINSTANCE.getArtifact();

		/**
		 * The meta object literal for the '<em><b>Processing</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTIFACT__PROCESSING = eINSTANCE.getArtifact_Processing();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTIFACT__PROPERTIES = eINSTANCE.getArtifact_Properties();

		/**
		 * The meta object literal for the '<em><b>Repository Properties</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTIFACT__REPOSITORY_PROPERTIES = eINSTANCE.getArtifact_RepositoryProperties();

		/**
		 * The meta object literal for the '<em><b>Classifier</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT__CLASSIFIER = eINSTANCE.getArtifact_Classifier();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT__ID = eINSTANCE.getArtifact_Id();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACT__VERSION = eINSTANCE.getArtifact_Version();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.ArtifactsImpl <em>Artifacts</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.ArtifactsImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getArtifacts()
		 * @generated
		 */
		EClass ARTIFACTS = eINSTANCE.getArtifacts();

		/**
		 * The meta object literal for the '<em><b>Artifact</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTIFACTS__ARTIFACT = eINSTANCE.getArtifacts_Artifact();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFACTS__SIZE = eINSTANCE.getArtifacts_Size();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.CategoryDefImpl <em>Category Def</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.CategoryDefImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getCategoryDef()
		 * @generated
		 */
		EClass CATEGORY_DEF = eINSTANCE.getCategoryDef();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY_DEF__DESCRIPTION = eINSTANCE.getCategoryDef_Description();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY_DEF__LABEL = eINSTANCE.getCategoryDef_Label();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY_DEF__NAME = eINSTANCE.getCategoryDef_Name();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.CategoryFeatureImpl <em>Category Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.CategoryFeatureImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getCategoryFeature()
		 * @generated
		 */
		EClass CATEGORY_FEATURE = eINSTANCE.getCategoryFeature();

		/**
		 * The meta object literal for the '<em><b>Category</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_FEATURE__CATEGORY = eINSTANCE.getCategoryFeature_Category();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY_FEATURE__ID = eINSTANCE.getCategoryFeature_Id();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY_FEATURE__VERSION = eINSTANCE.getCategoryFeature_Version();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.CategorySiteImpl <em>Category Site</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.CategorySiteImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getCategorySite()
		 * @generated
		 */
		EClass CATEGORY_SITE = eINSTANCE.getCategorySite();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_SITE__FEATURE = eINSTANCE.getCategorySite_Feature();

		/**
		 * The meta object literal for the '<em><b>Category Def</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY_SITE__CATEGORY_DEF = eINSTANCE.getCategorySite_CategoryDef();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.CategoryTypeImpl <em>Category Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.CategoryTypeImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getCategoryType()
		 * @generated
		 */
		EClass CATEGORY_TYPE = eINSTANCE.getCategoryType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY_TYPE__NAME = eINSTANCE.getCategoryType_Name();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.ChildrenTypeImpl <em>Children Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.ChildrenTypeImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getChildrenType()
		 * @generated
		 */
		EClass CHILDREN_TYPE = eINSTANCE.getChildrenType();

		/**
		 * The meta object literal for the '<em><b>Child</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHILDREN_TYPE__CHILD = eINSTANCE.getChildrenType_Child();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHILDREN_TYPE__SIZE = eINSTANCE.getChildrenType_Size();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.DescriptionImpl <em>Description</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.DescriptionImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getDescription()
		 * @generated
		 */
		EClass DESCRIPTION = eINSTANCE.getDescription();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DESCRIPTION__VALUE = eINSTANCE.getDescription_Value();

		/**
		 * The meta object literal for the '<em><b>Url</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DESCRIPTION__URL = eINSTANCE.getDescription_Url();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.DocumentRootImpl <em>Document Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.DocumentRootImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getDocumentRoot()
		 * @generated
		 */
		EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

		/**
		 * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

		/**
		 * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__FEATURE = eINSTANCE.getDocumentRoot_Feature();

		/**
		 * The meta object literal for the '<em><b>Repository</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__REPOSITORY = eINSTANCE.getDocumentRoot_Repository();

		/**
		 * The meta object literal for the '<em><b>Site</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__SITE = eINSTANCE.getDocumentRoot_Site();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__TARGET = eINSTANCE.getDocumentRoot_Target();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.FeatureImpl <em>Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.FeatureImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getFeature()
		 * @generated
		 */
		EClass FEATURE = eINSTANCE.getFeature();

		/**
		 * The meta object literal for the '<em><b>Install Handler</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE__INSTALL_HANDLER = eINSTANCE.getFeature_InstallHandler();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE__DESCRIPTION = eINSTANCE.getFeature_Description();

		/**
		 * The meta object literal for the '<em><b>Copyright</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE__COPYRIGHT = eINSTANCE.getFeature_Copyright();

		/**
		 * The meta object literal for the '<em><b>License</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE__LICENSE = eINSTANCE.getFeature_License();

		/**
		 * The meta object literal for the '<em><b>Url</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE__URL = eINSTANCE.getFeature_Url();

		/**
		 * The meta object literal for the '<em><b>Includes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE__INCLUDES = eINSTANCE.getFeature_Includes();

		/**
		 * The meta object literal for the '<em><b>Requires</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE__REQUIRES = eINSTANCE.getFeature_Requires();

		/**
		 * The meta object literal for the '<em><b>Plugin</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE__PLUGIN = eINSTANCE.getFeature_Plugin();

		/**
		 * The meta object literal for the '<em><b>Arch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE__ARCH = eINSTANCE.getFeature_Arch();

		/**
		 * The meta object literal for the '<em><b>Colocation Affinity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE__COLOCATION_AFFINITY = eINSTANCE.getFeature_ColocationAffinity();

		/**
		 * The meta object literal for the '<em><b>Exclusive</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE__EXCLUSIVE = eINSTANCE.getFeature_Exclusive();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE__ID = eINSTANCE.getFeature_Id();

		/**
		 * The meta object literal for the '<em><b>Image</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE__IMAGE = eINSTANCE.getFeature_Image();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE__LABEL = eINSTANCE.getFeature_Label();

		/**
		 * The meta object literal for the '<em><b>Nl</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE__NL = eINSTANCE.getFeature_Nl();

		/**
		 * The meta object literal for the '<em><b>Os</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE__OS = eINSTANCE.getFeature_Os();

		/**
		 * The meta object literal for the '<em><b>Plugin1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE__PLUGIN1 = eINSTANCE.getFeature_Plugin1();

		/**
		 * The meta object literal for the '<em><b>Provider Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE__PROVIDER_NAME = eINSTANCE.getFeature_ProviderName();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE__VERSION = eINSTANCE.getFeature_Version();

		/**
		 * The meta object literal for the '<em><b>Ws</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE__WS = eINSTANCE.getFeature_Ws();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.ImportImpl <em>Import</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.ImportImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getImport()
		 * @generated
		 */
		EClass IMPORT = eINSTANCE.getImport();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORT__FEATURE = eINSTANCE.getImport_Feature();

		/**
		 * The meta object literal for the '<em><b>Match</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORT__MATCH = eINSTANCE.getImport_Match();

		/**
		 * The meta object literal for the '<em><b>Patch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORT__PATCH = eINSTANCE.getImport_Patch();

		/**
		 * The meta object literal for the '<em><b>Plugin</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORT__PLUGIN = eINSTANCE.getImport_Plugin();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORT__VERSION = eINSTANCE.getImport_Version();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.ImportRequiresImpl <em>Import Requires</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.ImportRequiresImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getImportRequires()
		 * @generated
		 */
		EClass IMPORT_REQUIRES = eINSTANCE.getImportRequires();

		/**
		 * The meta object literal for the '<em><b>Import</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMPORT_REQUIRES__IMPORT = eINSTANCE.getImportRequires_Import();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.IncludeImpl <em>Include</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.IncludeImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getInclude()
		 * @generated
		 */
		EClass INCLUDE = eINSTANCE.getInclude();

		/**
		 * The meta object literal for the '<em><b>Arch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INCLUDE__ARCH = eINSTANCE.getInclude_Arch();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INCLUDE__ID = eINSTANCE.getInclude_Id();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INCLUDE__NAME = eINSTANCE.getInclude_Name();

		/**
		 * The meta object literal for the '<em><b>Nl</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INCLUDE__NL = eINSTANCE.getInclude_Nl();

		/**
		 * The meta object literal for the '<em><b>Optional</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INCLUDE__OPTIONAL = eINSTANCE.getInclude_Optional();

		/**
		 * The meta object literal for the '<em><b>Os</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INCLUDE__OS = eINSTANCE.getInclude_Os();

		/**
		 * The meta object literal for the '<em><b>Search Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INCLUDE__SEARCH_LOCATION = eINSTANCE.getInclude_SearchLocation();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INCLUDE__VERSION = eINSTANCE.getInclude_Version();

		/**
		 * The meta object literal for the '<em><b>Ws</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INCLUDE__WS = eINSTANCE.getInclude_Ws();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.InstallHandlerImpl <em>Install Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.InstallHandlerImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getInstallHandler()
		 * @generated
		 */
		EClass INSTALL_HANDLER = eINSTANCE.getInstallHandler();

		/**
		 * The meta object literal for the '<em><b>Handler</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INSTALL_HANDLER__HANDLER = eINSTANCE.getInstallHandler_Handler();

		/**
		 * The meta object literal for the '<em><b>Library</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INSTALL_HANDLER__LIBRARY = eINSTANCE.getInstallHandler_Library();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.InstructionImpl <em>Instruction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.InstructionImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getInstruction()
		 * @generated
		 */
		EClass INSTRUCTION = eINSTANCE.getInstruction();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INSTRUCTION__VALUE = eINSTANCE.getInstruction_Value();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INSTRUCTION__KEY = eINSTANCE.getInstruction_Key();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.InstructionsTypeImpl <em>Instructions Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.InstructionsTypeImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getInstructionsType()
		 * @generated
		 */
		EClass INSTRUCTIONS_TYPE = eINSTANCE.getInstructionsType();

		/**
		 * The meta object literal for the '<em><b>Instruction</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INSTRUCTIONS_TYPE__INSTRUCTION = eINSTANCE.getInstructionsType_Instruction();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INSTRUCTIONS_TYPE__SIZE = eINSTANCE.getInstructionsType_Size();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.LauncherArgsImpl <em>Launcher Args</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.LauncherArgsImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getLauncherArgs()
		 * @generated
		 */
		EClass LAUNCHER_ARGS = eINSTANCE.getLauncherArgs();

		/**
		 * The meta object literal for the '<em><b>Program Args</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LAUNCHER_ARGS__PROGRAM_ARGS = eINSTANCE.getLauncherArgs_ProgramArgs();

		/**
		 * The meta object literal for the '<em><b>Vm Args</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LAUNCHER_ARGS__VM_ARGS = eINSTANCE.getLauncherArgs_VmArgs();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.LicensesTypeImpl <em>Licenses Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.LicensesTypeImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getLicensesType()
		 * @generated
		 */
		EClass LICENSES_TYPE = eINSTANCE.getLicensesType();

		/**
		 * The meta object literal for the '<em><b>License</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LICENSES_TYPE__LICENSE = eINSTANCE.getLicensesType_License();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LICENSES_TYPE__SIZE = eINSTANCE.getLicensesType_Size();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.MappingsTypeImpl <em>Mappings Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.MappingsTypeImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getMappingsType()
		 * @generated
		 */
		EClass MAPPINGS_TYPE = eINSTANCE.getMappingsType();

		/**
		 * The meta object literal for the '<em><b>Rule</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAPPINGS_TYPE__RULE = eINSTANCE.getMappingsType_Rule();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAPPINGS_TYPE__SIZE = eINSTANCE.getMappingsType_Size();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.PluginImpl <em>Plugin</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.PluginImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getPlugin()
		 * @generated
		 */
		EClass PLUGIN = eINSTANCE.getPlugin();

		/**
		 * The meta object literal for the '<em><b>Arch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLUGIN__ARCH = eINSTANCE.getPlugin_Arch();

		/**
		 * The meta object literal for the '<em><b>Download Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLUGIN__DOWNLOAD_SIZE = eINSTANCE.getPlugin_DownloadSize();

		/**
		 * The meta object literal for the '<em><b>Fragment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLUGIN__FRAGMENT = eINSTANCE.getPlugin_Fragment();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLUGIN__ID = eINSTANCE.getPlugin_Id();

		/**
		 * The meta object literal for the '<em><b>Install Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLUGIN__INSTALL_SIZE = eINSTANCE.getPlugin_InstallSize();

		/**
		 * The meta object literal for the '<em><b>Nl</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLUGIN__NL = eINSTANCE.getPlugin_Nl();

		/**
		 * The meta object literal for the '<em><b>Os</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLUGIN__OS = eINSTANCE.getPlugin_Os();

		/**
		 * The meta object literal for the '<em><b>Unpack</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLUGIN__UNPACK = eINSTANCE.getPlugin_Unpack();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLUGIN__VERSION = eINSTANCE.getPlugin_Version();

		/**
		 * The meta object literal for the '<em><b>Ws</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLUGIN__WS = eINSTANCE.getPlugin_Ws();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.ProcessingImpl <em>Processing</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.ProcessingImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getProcessing()
		 * @generated
		 */
		EClass PROCESSING = eINSTANCE.getProcessing();

		/**
		 * The meta object literal for the '<em><b>Step</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESSING__STEP = eINSTANCE.getProcessing_Step();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESSING__SIZE = eINSTANCE.getProcessing_Size();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.ProcessStepImpl <em>Process Step</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.ProcessStepImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getProcessStep()
		 * @generated
		 */
		EClass PROCESS_STEP = eINSTANCE.getProcessStep();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_STEP__ID = eINSTANCE.getProcessStep_Id();

		/**
		 * The meta object literal for the '<em><b>Required</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS_STEP__REQUIRED = eINSTANCE.getProcessStep_Required();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.PropertiesImpl <em>Properties</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.PropertiesImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getProperties()
		 * @generated
		 */
		EClass PROPERTIES = eINSTANCE.getProperties();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTIES__PROPERTY = eINSTANCE.getProperties_Property();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTIES__SIZE = eINSTANCE.getProperties_Size();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.PropertyImpl <em>Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.PropertyImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getProperty()
		 * @generated
		 */
		EClass PROPERTY = eINSTANCE.getProperty();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__NAME = eINSTANCE.getProperty_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__VALUE = eINSTANCE.getProperty_Value();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.ProvidedImpl <em>Provided</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.ProvidedImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getProvided()
		 * @generated
		 */
		EClass PROVIDED = eINSTANCE.getProvided();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROVIDED__NAME = eINSTANCE.getProvided_Name();

		/**
		 * The meta object literal for the '<em><b>Namespace</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROVIDED__NAMESPACE = eINSTANCE.getProvided_Namespace();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROVIDED__VERSION = eINSTANCE.getProvided_Version();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROVIDED__PROPERTIES = eINSTANCE.getProvided_Properties();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.ProvidesImpl <em>Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.ProvidesImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getProvides()
		 * @generated
		 */
		EClass PROVIDES = eINSTANCE.getProvides();

		/**
		 * The meta object literal for the '<em><b>Provided</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROVIDES__PROVIDED = eINSTANCE.getProvides_Provided();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROVIDES__SIZE = eINSTANCE.getProvides_Size();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.ReferencesTypeImpl <em>References Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.ReferencesTypeImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getReferencesType()
		 * @generated
		 */
		EClass REFERENCES_TYPE = eINSTANCE.getReferencesType();

		/**
		 * The meta object literal for the '<em><b>Repository</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCES_TYPE__REPOSITORY = eINSTANCE.getReferencesType_Repository();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REFERENCES_TYPE__SIZE = eINSTANCE.getReferencesType_Size();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.RepositoryImpl <em>Repository</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.RepositoryImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getRepository()
		 * @generated
		 */
		EClass REPOSITORY = eINSTANCE.getRepository();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPOSITORY__PROPERTIES = eINSTANCE.getRepository_Properties();

		/**
		 * The meta object literal for the '<em><b>Mappings</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPOSITORY__MAPPINGS = eINSTANCE.getRepository_Mappings();

		/**
		 * The meta object literal for the '<em><b>References</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPOSITORY__REFERENCES = eINSTANCE.getRepository_References();

		/**
		 * The meta object literal for the '<em><b>Units</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPOSITORY__UNITS = eINSTANCE.getRepository_Units();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPOSITORY__CHILDREN = eINSTANCE.getRepository_Children();

		/**
		 * The meta object literal for the '<em><b>Artifacts</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPOSITORY__ARTIFACTS = eINSTANCE.getRepository_Artifacts();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPOSITORY__NAME = eINSTANCE.getRepository_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPOSITORY__TYPE = eINSTANCE.getRepository_Type();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPOSITORY__VERSION = eINSTANCE.getRepository_Version();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.RepositoryRefImpl <em>Repository Ref</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.RepositoryRefImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getRepositoryRef()
		 * @generated
		 */
		EClass REPOSITORY_REF = eINSTANCE.getRepositoryRef();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPOSITORY_REF__LOCATION = eINSTANCE.getRepositoryRef_Location();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.RepositoryRefsImpl <em>Repository Refs</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.RepositoryRefsImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getRepositoryRefs()
		 * @generated
		 */
		EClass REPOSITORY_REFS = eINSTANCE.getRepositoryRefs();

		/**
		 * The meta object literal for the '<em><b>Options</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPOSITORY_REFS__OPTIONS = eINSTANCE.getRepositoryRefs_Options();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPOSITORY_REFS__TYPE = eINSTANCE.getRepositoryRefs_Type();

		/**
		 * The meta object literal for the '<em><b>Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPOSITORY_REFS__URI = eINSTANCE.getRepositoryRefs_Uri();

		/**
		 * The meta object literal for the '<em><b>Url</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPOSITORY_REFS__URL = eINSTANCE.getRepositoryRefs_Url();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.RequiredImpl <em>Required</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.RequiredImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getRequired()
		 * @generated
		 */
		EClass REQUIRED = eINSTANCE.getRequired();

		/**
		 * The meta object literal for the '<em><b>Filter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIRED__FILTER = eINSTANCE.getRequired_Filter();

		/**
		 * The meta object literal for the '<em><b>Greedy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIRED__GREEDY = eINSTANCE.getRequired_Greedy();

		/**
		 * The meta object literal for the '<em><b>Multiple</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIRED__MULTIPLE = eINSTANCE.getRequired_Multiple();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIRED__NAME = eINSTANCE.getRequired_Name();

		/**
		 * The meta object literal for the '<em><b>Namespace</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIRED__NAMESPACE = eINSTANCE.getRequired_Namespace();

		/**
		 * The meta object literal for the '<em><b>Optional</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIRED__OPTIONAL = eINSTANCE.getRequired_Optional();

		/**
		 * The meta object literal for the '<em><b>Range</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIRED__RANGE = eINSTANCE.getRequired_Range();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.RequiresImpl <em>Requires</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.RequiresImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getRequires()
		 * @generated
		 */
		EClass REQUIRES = eINSTANCE.getRequires();

		/**
		 * The meta object literal for the '<em><b>Required</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIRES__REQUIRED = eINSTANCE.getRequires_Required();

		/**
		 * The meta object literal for the '<em><b>Required Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIRES__REQUIRED_PROPERTIES = eINSTANCE.getRequires_RequiredProperties();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIRES__SIZE = eINSTANCE.getRequires_Size();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.RuleImpl <em>Rule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.RuleImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getRule()
		 * @generated
		 */
		EClass RULE = eINSTANCE.getRule();

		/**
		 * The meta object literal for the '<em><b>Filter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RULE__FILTER = eINSTANCE.getRule_Filter();

		/**
		 * The meta object literal for the '<em><b>Output</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RULE__OUTPUT = eINSTANCE.getRule_Output();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.SiteImpl <em>Site</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.SiteImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getSite()
		 * @generated
		 */
		EClass SITE = eINSTANCE.getSite();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SITE__LABEL = eINSTANCE.getSite_Label();

		/**
		 * The meta object literal for the '<em><b>Url</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SITE__URL = eINSTANCE.getSite_Url();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.TargetImpl <em>Target</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.TargetImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTarget()
		 * @generated
		 */
		EClass TARGET = eINSTANCE.getTarget();

		/**
		 * The meta object literal for the '<em><b>Locations</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TARGET__LOCATIONS = eINSTANCE.getTarget_Locations();

		/**
		 * The meta object literal for the '<em><b>Environment</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TARGET__ENVIRONMENT = eINSTANCE.getTarget_Environment();

		/**
		 * The meta object literal for the '<em><b>Target JRE</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TARGET__TARGET_JRE = eINSTANCE.getTarget_TargetJRE();

		/**
		 * The meta object literal for the '<em><b>Launcher Args</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TARGET__LAUNCHER_ARGS = eINSTANCE.getTarget_LauncherArgs();

		/**
		 * The meta object literal for the '<em><b>Implicit Dependencies</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TARGET__IMPLICIT_DEPENDENCIES = eINSTANCE.getTarget_ImplicitDependencies();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TARGET__NAME = eINSTANCE.getTarget_Name();

		/**
		 * The meta object literal for the '<em><b>Sequence Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TARGET__SEQUENCE_NUMBER = eINSTANCE.getTarget_SequenceNumber();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.TargetDependenciesImpl <em>Target Dependencies</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.TargetDependenciesImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTargetDependencies()
		 * @generated
		 */
		EClass TARGET_DEPENDENCIES = eINSTANCE.getTargetDependencies();

		/**
		 * The meta object literal for the '<em><b>Plugin</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TARGET_DEPENDENCIES__PLUGIN = eINSTANCE.getTargetDependencies_Plugin();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.TargetEnvironmentImpl <em>Target Environment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.TargetEnvironmentImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTargetEnvironment()
		 * @generated
		 */
		EClass TARGET_ENVIRONMENT = eINSTANCE.getTargetEnvironment();

		/**
		 * The meta object literal for the '<em><b>Arch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TARGET_ENVIRONMENT__ARCH = eINSTANCE.getTargetEnvironment_Arch();

		/**
		 * The meta object literal for the '<em><b>Os</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TARGET_ENVIRONMENT__OS = eINSTANCE.getTargetEnvironment_Os();

		/**
		 * The meta object literal for the '<em><b>Ws</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TARGET_ENVIRONMENT__WS = eINSTANCE.getTargetEnvironment_Ws();

		/**
		 * The meta object literal for the '<em><b>Nl</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TARGET_ENVIRONMENT__NL = eINSTANCE.getTargetEnvironment_Nl();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.TargetJREImpl <em>Target JRE</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.TargetJREImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTargetJRE()
		 * @generated
		 */
		EClass TARGET_JRE = eINSTANCE.getTargetJRE();

		/**
		 * The meta object literal for the '<em><b>Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TARGET_JRE__PATH = eINSTANCE.getTargetJRE_Path();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.TargetLocationImpl <em>Target Location</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.TargetLocationImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTargetLocation()
		 * @generated
		 */
		EClass TARGET_LOCATION = eINSTANCE.getTargetLocation();

		/**
		 * The meta object literal for the '<em><b>Repository</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TARGET_LOCATION__REPOSITORY = eINSTANCE.getTargetLocation_Repository();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TARGET_LOCATION__UNIT = eINSTANCE.getTargetLocation_Unit();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TARGET_LOCATION__ID = eINSTANCE.getTargetLocation_Id();

		/**
		 * The meta object literal for the '<em><b>Include All Platforms</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TARGET_LOCATION__INCLUDE_ALL_PLATFORMS = eINSTANCE.getTargetLocation_IncludeAllPlatforms();

		/**
		 * The meta object literal for the '<em><b>Include Configure Phase</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TARGET_LOCATION__INCLUDE_CONFIGURE_PHASE = eINSTANCE.getTargetLocation_IncludeConfigurePhase();

		/**
		 * The meta object literal for the '<em><b>Include Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TARGET_LOCATION__INCLUDE_MODE = eINSTANCE.getTargetLocation_IncludeMode();

		/**
		 * The meta object literal for the '<em><b>Include Source</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TARGET_LOCATION__INCLUDE_SOURCE = eINSTANCE.getTargetLocation_IncludeSource();

		/**
		 * The meta object literal for the '<em><b>Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TARGET_LOCATION__PATH = eINSTANCE.getTargetLocation_Path();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TARGET_LOCATION__TYPE = eINSTANCE.getTargetLocation_Type();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.TargetLocationsImpl <em>Target Locations</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.TargetLocationsImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTargetLocations()
		 * @generated
		 */
		EClass TARGET_LOCATIONS = eINSTANCE.getTargetLocations();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TARGET_LOCATIONS__LOCATION = eINSTANCE.getTargetLocations_Location();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.TouchpointImpl <em>Touchpoint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.TouchpointImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTouchpoint()
		 * @generated
		 */
		EClass TOUCHPOINT = eINSTANCE.getTouchpoint();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TOUCHPOINT__ID = eINSTANCE.getTouchpoint_Id();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TOUCHPOINT__VERSION = eINSTANCE.getTouchpoint_Version();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.TouchpointDataImpl <em>Touchpoint Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.TouchpointDataImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTouchpointData()
		 * @generated
		 */
		EClass TOUCHPOINT_DATA = eINSTANCE.getTouchpointData();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TOUCHPOINT_DATA__ID = eINSTANCE.getTouchpointData_Id();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TOUCHPOINT_DATA__VERSION = eINSTANCE.getTouchpointData_Version();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.TouchpointDataTypeImpl <em>Touchpoint Data Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.TouchpointDataTypeImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTouchpointDataType()
		 * @generated
		 */
		EClass TOUCHPOINT_DATA_TYPE = eINSTANCE.getTouchpointDataType();

		/**
		 * The meta object literal for the '<em><b>Instructions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOUCHPOINT_DATA_TYPE__INSTRUCTIONS = eINSTANCE.getTouchpointDataType_Instructions();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TOUCHPOINT_DATA_TYPE__SIZE = eINSTANCE.getTouchpointDataType_Size();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.UnitImpl <em>Unit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.UnitImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getUnit()
		 * @generated
		 */
		EClass UNIT = eINSTANCE.getUnit();

		/**
		 * The meta object literal for the '<em><b>Update</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIT__UPDATE = eINSTANCE.getUnit_Update();

		/**
		 * The meta object literal for the '<em><b>Artifacts</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIT__ARTIFACTS = eINSTANCE.getUnit_Artifacts();

		/**
		 * The meta object literal for the '<em><b>Host Requirements</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIT__HOST_REQUIREMENTS = eINSTANCE.getUnit_HostRequirements();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIT__PROPERTIES = eINSTANCE.getUnit_Properties();

		/**
		 * The meta object literal for the '<em><b>Provides</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIT__PROVIDES = eINSTANCE.getUnit_Provides();

		/**
		 * The meta object literal for the '<em><b>Requires</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIT__REQUIRES = eINSTANCE.getUnit_Requires();

		/**
		 * The meta object literal for the '<em><b>Filter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNIT__FILTER = eINSTANCE.getUnit_Filter();

		/**
		 * The meta object literal for the '<em><b>Touchpoint</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIT__TOUCHPOINT = eINSTANCE.getUnit_Touchpoint();

		/**
		 * The meta object literal for the '<em><b>Touchpoint Data</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIT__TOUCHPOINT_DATA = eINSTANCE.getUnit_TouchpointData();

		/**
		 * The meta object literal for the '<em><b>Licenses</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIT__LICENSES = eINSTANCE.getUnit_Licenses();

		/**
		 * The meta object literal for the '<em><b>Copyright</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIT__COPYRIGHT = eINSTANCE.getUnit_Copyright();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNIT__ID = eINSTANCE.getUnit_Id();

		/**
		 * The meta object literal for the '<em><b>Singleton</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNIT__SINGLETON = eINSTANCE.getUnit_Singleton();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNIT__VERSION = eINSTANCE.getUnit_Version();

		/**
		 * The meta object literal for the '<em><b>Generation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNIT__GENERATION = eINSTANCE.getUnit_Generation();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.UnitsTypeImpl <em>Units Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.UnitsTypeImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getUnitsType()
		 * @generated
		 */
		EClass UNITS_TYPE = eINSTANCE.getUnitsType();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNITS_TYPE__UNIT = eINSTANCE.getUnitsType_Unit();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNITS_TYPE__SIZE = eINSTANCE.getUnitsType_Size();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.UpdateImpl <em>Update</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.UpdateImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getUpdate()
		 * @generated
		 */
		EClass UPDATE = eINSTANCE.getUpdate();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UPDATE__ID = eINSTANCE.getUpdate_Id();

		/**
		 * The meta object literal for the '<em><b>Range</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UPDATE__RANGE = eINSTANCE.getUpdate_Range();

		/**
		 * The meta object literal for the '<em><b>Severity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UPDATE__SEVERITY = eINSTANCE.getUpdate_Severity();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.UrlTypeImpl <em>Url Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.UrlTypeImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getUrlType()
		 * @generated
		 */
		EClass URL_TYPE = eINSTANCE.getUrlType();

		/**
		 * The meta object literal for the '<em><b>Update</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference URL_TYPE__UPDATE = eINSTANCE.getUrlType_Update();

		/**
		 * The meta object literal for the '<em><b>Discovery</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference URL_TYPE__DISCOVERY = eINSTANCE.getUrlType_Discovery();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.impl.RequiredPropertiesImpl <em>Required Properties</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.impl.RequiredPropertiesImpl
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getRequiredProperties()
		 * @generated
		 */
		EClass REQUIRED_PROPERTIES = eINSTANCE.getRequiredProperties();

		/**
		 * The meta object literal for the '<em><b>Match</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIRED_PROPERTIES__MATCH = eINSTANCE.getRequiredProperties_Match();

		/**
		 * The meta object literal for the '<em><b>Namespace</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIRED_PROPERTIES__NAMESPACE = eINSTANCE.getRequiredProperties_Namespace();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.LocationIncludeType <em>Location Include Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.LocationIncludeType
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getLocationIncludeType()
		 * @generated
		 */
		EEnum LOCATION_INCLUDE_TYPE = eINSTANCE.getLocationIncludeType();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.MatchType <em>Match Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.MatchType
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getMatchType()
		 * @generated
		 */
		EEnum MATCH_TYPE = eINSTANCE.getMatchType();

		/**
		 * The meta object literal for the '{@link org.gecko.eclipse.TargetType <em>Target Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.TargetType
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTargetType()
		 * @generated
		 */
		EEnum TARGET_TYPE = eINSTANCE.getTargetType();

		/**
		 * The meta object literal for the '<em>Location Include Type Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.LocationIncludeType
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getLocationIncludeTypeObject()
		 * @generated
		 */
		EDataType LOCATION_INCLUDE_TYPE_OBJECT = eINSTANCE.getLocationIncludeTypeObject();

		/**
		 * The meta object literal for the '<em>Match Type Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.MatchType
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getMatchTypeObject()
		 * @generated
		 */
		EDataType MATCH_TYPE_OBJECT = eINSTANCE.getMatchTypeObject();

		/**
		 * The meta object literal for the '<em>Target Type Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.gecko.eclipse.TargetType
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getTargetTypeObject()
		 * @generated
		 */
		EDataType TARGET_TYPE_OBJECT = eINSTANCE.getTargetTypeObject();

		/**
		 * The meta object literal for the '<em>Version</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see org.gecko.eclipse.impl.EclipsePackageImpl#getVersion()
		 * @generated
		 */
		EDataType VERSION = eINSTANCE.getVersion();

	}

} //EclipsePackage
