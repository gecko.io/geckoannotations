/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Target Environment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.TargetEnvironment#getArch <em>Arch</em>}</li>
 *   <li>{@link org.gecko.eclipse.TargetEnvironment#getOs <em>Os</em>}</li>
 *   <li>{@link org.gecko.eclipse.TargetEnvironment#getWs <em>Ws</em>}</li>
 *   <li>{@link org.gecko.eclipse.TargetEnvironment#getNl <em>Nl</em>}</li>
 * </ul>
 *
 * @see org.gecko.eclipse.EclipsePackage#getTargetEnvironment()
 * @model extendedMetaData="name='TargetEnvironment' kind='elementOnly'"
 * @generated
 */
public interface TargetEnvironment extends EObject {
	/**
	 * Returns the value of the '<em><b>Arch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arch</em>' attribute.
	 * @see #setArch(String)
	 * @see org.gecko.eclipse.EclipsePackage#getTargetEnvironment_Arch()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='arch' namespace='##targetNamespace'"
	 * @generated
	 */
	String getArch();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.TargetEnvironment#getArch <em>Arch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Arch</em>' attribute.
	 * @see #getArch()
	 * @generated
	 */
	void setArch(String value);

	/**
	 * Returns the value of the '<em><b>Os</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Os</em>' attribute.
	 * @see #setOs(String)
	 * @see org.gecko.eclipse.EclipsePackage#getTargetEnvironment_Os()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='os' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOs();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.TargetEnvironment#getOs <em>Os</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Os</em>' attribute.
	 * @see #getOs()
	 * @generated
	 */
	void setOs(String value);

	/**
	 * Returns the value of the '<em><b>Ws</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ws</em>' attribute.
	 * @see #setWs(String)
	 * @see org.gecko.eclipse.EclipsePackage#getTargetEnvironment_Ws()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='ws' namespace='##targetNamespace'"
	 * @generated
	 */
	String getWs();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.TargetEnvironment#getWs <em>Ws</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ws</em>' attribute.
	 * @see #getWs()
	 * @generated
	 */
	void setWs(String value);

	/**
	 * Returns the value of the '<em><b>Nl</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nl</em>' attribute.
	 * @see #setNl(String)
	 * @see org.gecko.eclipse.EclipsePackage#getTargetEnvironment_Nl()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='nl' namespace='##targetNamespace'"
	 * @generated
	 */
	String getNl();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.TargetEnvironment#getNl <em>Nl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nl</em>' attribute.
	 * @see #getNl()
	 * @generated
	 */
	void setNl(String value);

} // TargetEnvironment
