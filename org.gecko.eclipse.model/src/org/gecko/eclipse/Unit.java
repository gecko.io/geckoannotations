/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.Unit#getUpdate <em>Update</em>}</li>
 *   <li>{@link org.gecko.eclipse.Unit#getArtifacts <em>Artifacts</em>}</li>
 *   <li>{@link org.gecko.eclipse.Unit#getHostRequirements <em>Host Requirements</em>}</li>
 *   <li>{@link org.gecko.eclipse.Unit#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.gecko.eclipse.Unit#getProvides <em>Provides</em>}</li>
 *   <li>{@link org.gecko.eclipse.Unit#getRequires <em>Requires</em>}</li>
 *   <li>{@link org.gecko.eclipse.Unit#getFilter <em>Filter</em>}</li>
 *   <li>{@link org.gecko.eclipse.Unit#getTouchpoint <em>Touchpoint</em>}</li>
 *   <li>{@link org.gecko.eclipse.Unit#getTouchpointData <em>Touchpoint Data</em>}</li>
 *   <li>{@link org.gecko.eclipse.Unit#getLicenses <em>Licenses</em>}</li>
 *   <li>{@link org.gecko.eclipse.Unit#getCopyright <em>Copyright</em>}</li>
 *   <li>{@link org.gecko.eclipse.Unit#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.eclipse.Unit#isSingleton <em>Singleton</em>}</li>
 *   <li>{@link org.gecko.eclipse.Unit#getVersion <em>Version</em>}</li>
 *   <li>{@link org.gecko.eclipse.Unit#getGeneration <em>Generation</em>}</li>
 * </ul>
 *
 * @see org.gecko.eclipse.EclipsePackage#getUnit()
 * @model extendedMetaData="name='Unit' kind='elementOnly'"
 * @generated
 */
public interface Unit extends EObject {
	/**
	 * Returns the value of the '<em><b>Update</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Update</em>' containment reference.
	 * @see #setUpdate(Update)
	 * @see org.gecko.eclipse.EclipsePackage#getUnit_Update()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='update' namespace='##targetNamespace'"
	 * @generated
	 */
	Update getUpdate();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Unit#getUpdate <em>Update</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Update</em>' containment reference.
	 * @see #getUpdate()
	 * @generated
	 */
	void setUpdate(Update value);

	/**
	 * Returns the value of the '<em><b>Artifacts</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Artifacts</em>' containment reference.
	 * @see #setArtifacts(Artifacts)
	 * @see org.gecko.eclipse.EclipsePackage#getUnit_Artifacts()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='artifacts' namespace='##targetNamespace'"
	 * @generated
	 */
	Artifacts getArtifacts();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Unit#getArtifacts <em>Artifacts</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Artifacts</em>' containment reference.
	 * @see #getArtifacts()
	 * @generated
	 */
	void setArtifacts(Artifacts value);

	/**
	 * Returns the value of the '<em><b>Host Requirements</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Host Requirements</em>' containment reference.
	 * @see #setHostRequirements(Requires)
	 * @see org.gecko.eclipse.EclipsePackage#getUnit_HostRequirements()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='hostRequirements' namespace='##targetNamespace'"
	 * @generated
	 */
	Requires getHostRequirements();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Unit#getHostRequirements <em>Host Requirements</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Host Requirements</em>' containment reference.
	 * @see #getHostRequirements()
	 * @generated
	 */
	void setHostRequirements(Requires value);

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference.
	 * @see #setProperties(Properties)
	 * @see org.gecko.eclipse.EclipsePackage#getUnit_Properties()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='properties' namespace='##targetNamespace'"
	 * @generated
	 */
	Properties getProperties();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Unit#getProperties <em>Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Properties</em>' containment reference.
	 * @see #getProperties()
	 * @generated
	 */
	void setProperties(Properties value);

	/**
	 * Returns the value of the '<em><b>Provides</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provides</em>' containment reference.
	 * @see #setProvides(Provides)
	 * @see org.gecko.eclipse.EclipsePackage#getUnit_Provides()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='provides' namespace='##targetNamespace'"
	 * @generated
	 */
	Provides getProvides();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Unit#getProvides <em>Provides</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Provides</em>' containment reference.
	 * @see #getProvides()
	 * @generated
	 */
	void setProvides(Provides value);

	/**
	 * Returns the value of the '<em><b>Requires</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requires</em>' containment reference.
	 * @see #setRequires(Requires)
	 * @see org.gecko.eclipse.EclipsePackage#getUnit_Requires()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='requires' namespace='##targetNamespace'"
	 * @generated
	 */
	Requires getRequires();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Unit#getRequires <em>Requires</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Requires</em>' containment reference.
	 * @see #getRequires()
	 * @generated
	 */
	void setRequires(Requires value);

	/**
	 * Returns the value of the '<em><b>Filter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filter</em>' attribute.
	 * @see #setFilter(String)
	 * @see org.gecko.eclipse.EclipsePackage#getUnit_Filter()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='filter' namespace='##targetNamespace'"
	 * @generated
	 */
	String getFilter();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Unit#getFilter <em>Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filter</em>' attribute.
	 * @see #getFilter()
	 * @generated
	 */
	void setFilter(String value);

	/**
	 * Returns the value of the '<em><b>Touchpoint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Touchpoint</em>' containment reference.
	 * @see #setTouchpoint(Touchpoint)
	 * @see org.gecko.eclipse.EclipsePackage#getUnit_Touchpoint()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='touchpoint' namespace='##targetNamespace'"
	 * @generated
	 */
	Touchpoint getTouchpoint();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Unit#getTouchpoint <em>Touchpoint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Touchpoint</em>' containment reference.
	 * @see #getTouchpoint()
	 * @generated
	 */
	void setTouchpoint(Touchpoint value);

	/**
	 * Returns the value of the '<em><b>Touchpoint Data</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Touchpoint Data</em>' containment reference.
	 * @see #setTouchpointData(TouchpointDataType)
	 * @see org.gecko.eclipse.EclipsePackage#getUnit_TouchpointData()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='touchpointData' namespace='##targetNamespace'"
	 * @generated
	 */
	TouchpointDataType getTouchpointData();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Unit#getTouchpointData <em>Touchpoint Data</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Touchpoint Data</em>' containment reference.
	 * @see #getTouchpointData()
	 * @generated
	 */
	void setTouchpointData(TouchpointDataType value);

	/**
	 * Returns the value of the '<em><b>Licenses</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Licenses</em>' containment reference.
	 * @see #setLicenses(LicensesType)
	 * @see org.gecko.eclipse.EclipsePackage#getUnit_Licenses()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='licenses' namespace='##targetNamespace'"
	 * @generated
	 */
	LicensesType getLicenses();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Unit#getLicenses <em>Licenses</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Licenses</em>' containment reference.
	 * @see #getLicenses()
	 * @generated
	 */
	void setLicenses(LicensesType value);

	/**
	 * Returns the value of the '<em><b>Copyright</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Copyright</em>' containment reference.
	 * @see #setCopyright(Description)
	 * @see org.gecko.eclipse.EclipsePackage#getUnit_Copyright()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='copyright' namespace='##targetNamespace'"
	 * @generated
	 */
	Description getCopyright();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Unit#getCopyright <em>Copyright</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Copyright</em>' containment reference.
	 * @see #getCopyright()
	 * @generated
	 */
	void setCopyright(Description value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.gecko.eclipse.EclipsePackage#getUnit_Id()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='id'"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Unit#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Singleton</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Singleton</em>' attribute.
	 * @see #isSetSingleton()
	 * @see #unsetSingleton()
	 * @see #setSingleton(boolean)
	 * @see org.gecko.eclipse.EclipsePackage#getUnit_Singleton()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='singleton'"
	 * @generated
	 */
	boolean isSingleton();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Unit#isSingleton <em>Singleton</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Singleton</em>' attribute.
	 * @see #isSetSingleton()
	 * @see #unsetSingleton()
	 * @see #isSingleton()
	 * @generated
	 */
	void setSingleton(boolean value);

	/**
	 * Unsets the value of the '{@link org.gecko.eclipse.Unit#isSingleton <em>Singleton</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSingleton()
	 * @see #isSingleton()
	 * @see #setSingleton(boolean)
	 * @generated
	 */
	void unsetSingleton();

	/**
	 * Returns whether the value of the '{@link org.gecko.eclipse.Unit#isSingleton <em>Singleton</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Singleton</em>' attribute is set.
	 * @see #unsetSingleton()
	 * @see #isSingleton()
	 * @see #setSingleton(boolean)
	 * @generated
	 */
	boolean isSetSingleton();

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.gecko.eclipse.EclipsePackage#getUnit_Version()
	 * @model dataType="org.gecko.eclipse.Version"
	 *        extendedMetaData="kind='attribute' name='version'"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Unit#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Generation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generation</em>' attribute.
	 * @see #setGeneration(Integer)
	 * @see org.gecko.eclipse.EclipsePackage#getUnit_Generation()
	 * @model extendedMetaData="kind='attribute' name='generation'"
	 * @generated
	 */
	Integer getGeneration();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Unit#getGeneration <em>Generation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generation</em>' attribute.
	 * @see #getGeneration()
	 * @generated
	 */
	void setGeneration(Integer value);

} // Unit
