/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Target Dependencies</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.TargetDependencies#getPlugin <em>Plugin</em>}</li>
 * </ul>
 *
 * @see org.gecko.eclipse.EclipsePackage#getTargetDependencies()
 * @model extendedMetaData="name='TargetDependencies' kind='elementOnly'"
 * @generated
 */
public interface TargetDependencies extends EObject {
	/**
	 * Returns the value of the '<em><b>Plugin</b></em>' containment reference list.
	 * The list contents are of type {@link org.gecko.eclipse.Artifact}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Plugin</em>' containment reference list.
	 * @see org.gecko.eclipse.EclipsePackage#getTargetDependencies_Plugin()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='plugin' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Artifact> getPlugin();

} // TargetDependencies
