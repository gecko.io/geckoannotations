/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.Feature#getInstallHandler <em>Install Handler</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#getDescription <em>Description</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#getCopyright <em>Copyright</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#getLicense <em>License</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#getUrl <em>Url</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#getIncludes <em>Includes</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#getRequires <em>Requires</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#getPlugin <em>Plugin</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#getArch <em>Arch</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#getColocationAffinity <em>Colocation Affinity</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#isExclusive <em>Exclusive</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#getImage <em>Image</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#getLabel <em>Label</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#getNl <em>Nl</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#getOs <em>Os</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#getPlugin1 <em>Plugin1</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#getProviderName <em>Provider Name</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#getVersion <em>Version</em>}</li>
 *   <li>{@link org.gecko.eclipse.Feature#getWs <em>Ws</em>}</li>
 * </ul>
 *
 * @see org.gecko.eclipse.EclipsePackage#getFeature()
 * @model extendedMetaData="name='Feature' kind='elementOnly'"
 * @generated
 */
public interface Feature extends EObject {
	/**
	 * Returns the value of the '<em><b>Install Handler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Install Handler</em>' containment reference.
	 * @see #setInstallHandler(InstallHandler)
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_InstallHandler()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='install-handler' namespace='##targetNamespace'"
	 * @generated
	 */
	InstallHandler getInstallHandler();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Feature#getInstallHandler <em>Install Handler</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Install Handler</em>' containment reference.
	 * @see #getInstallHandler()
	 * @generated
	 */
	void setInstallHandler(InstallHandler value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference.
	 * @see #setDescription(Description)
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='description' namespace='##targetNamespace'"
	 * @generated
	 */
	Description getDescription();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Feature#getDescription <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' containment reference.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(Description value);

	/**
	 * Returns the value of the '<em><b>Copyright</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Copyright</em>' containment reference.
	 * @see #setCopyright(Description)
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_Copyright()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='copyright' namespace='##targetNamespace'"
	 * @generated
	 */
	Description getCopyright();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Feature#getCopyright <em>Copyright</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Copyright</em>' containment reference.
	 * @see #getCopyright()
	 * @generated
	 */
	void setCopyright(Description value);

	/**
	 * Returns the value of the '<em><b>License</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>License</em>' containment reference.
	 * @see #setLicense(Description)
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_License()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='license' namespace='##targetNamespace'"
	 * @generated
	 */
	Description getLicense();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Feature#getLicense <em>License</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>License</em>' containment reference.
	 * @see #getLicense()
	 * @generated
	 */
	void setLicense(Description value);

	/**
	 * Returns the value of the '<em><b>Url</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Url</em>' containment reference.
	 * @see #setUrl(UrlType)
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_Url()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='url' namespace='##targetNamespace'"
	 * @generated
	 */
	UrlType getUrl();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Feature#getUrl <em>Url</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Url</em>' containment reference.
	 * @see #getUrl()
	 * @generated
	 */
	void setUrl(UrlType value);

	/**
	 * Returns the value of the '<em><b>Includes</b></em>' containment reference list.
	 * The list contents are of type {@link org.gecko.eclipse.Include}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Includes</em>' containment reference list.
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_Includes()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='includes' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Include> getIncludes();

	/**
	 * Returns the value of the '<em><b>Requires</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requires</em>' containment reference.
	 * @see #setRequires(ImportRequires)
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_Requires()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='requires' namespace='##targetNamespace'"
	 * @generated
	 */
	ImportRequires getRequires();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Feature#getRequires <em>Requires</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Requires</em>' containment reference.
	 * @see #getRequires()
	 * @generated
	 */
	void setRequires(ImportRequires value);

	/**
	 * Returns the value of the '<em><b>Plugin</b></em>' containment reference list.
	 * The list contents are of type {@link org.gecko.eclipse.Plugin}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Plugin</em>' containment reference list.
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_Plugin()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='plugin' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Plugin> getPlugin();

	/**
	 * Returns the value of the '<em><b>Arch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arch</em>' attribute.
	 * @see #setArch(String)
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_Arch()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='arch'"
	 * @generated
	 */
	String getArch();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Feature#getArch <em>Arch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Arch</em>' attribute.
	 * @see #getArch()
	 * @generated
	 */
	void setArch(String value);

	/**
	 * Returns the value of the '<em><b>Colocation Affinity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Colocation Affinity</em>' attribute.
	 * @see #setColocationAffinity(String)
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_ColocationAffinity()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='colocation-affinity'"
	 * @generated
	 */
	String getColocationAffinity();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Feature#getColocationAffinity <em>Colocation Affinity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Colocation Affinity</em>' attribute.
	 * @see #getColocationAffinity()
	 * @generated
	 */
	void setColocationAffinity(String value);

	/**
	 * Returns the value of the '<em><b>Exclusive</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exclusive</em>' attribute.
	 * @see #isSetExclusive()
	 * @see #unsetExclusive()
	 * @see #setExclusive(boolean)
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_Exclusive()
	 * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='exclusive'"
	 * @generated
	 */
	boolean isExclusive();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Feature#isExclusive <em>Exclusive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exclusive</em>' attribute.
	 * @see #isSetExclusive()
	 * @see #unsetExclusive()
	 * @see #isExclusive()
	 * @generated
	 */
	void setExclusive(boolean value);

	/**
	 * Unsets the value of the '{@link org.gecko.eclipse.Feature#isExclusive <em>Exclusive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExclusive()
	 * @see #isExclusive()
	 * @see #setExclusive(boolean)
	 * @generated
	 */
	void unsetExclusive();

	/**
	 * Returns whether the value of the '{@link org.gecko.eclipse.Feature#isExclusive <em>Exclusive</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Exclusive</em>' attribute is set.
	 * @see #unsetExclusive()
	 * @see #isExclusive()
	 * @see #setExclusive(boolean)
	 * @generated
	 */
	boolean isSetExclusive();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_Id()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID"
	 *        extendedMetaData="kind='attribute' name='id'"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Feature#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image</em>' attribute.
	 * @see #setImage(String)
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_Image()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='image'"
	 * @generated
	 */
	String getImage();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Feature#getImage <em>Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image</em>' attribute.
	 * @see #getImage()
	 * @generated
	 */
	void setImage(String value);

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_Label()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='label'"
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Feature#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Nl</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nl</em>' attribute.
	 * @see #setNl(String)
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_Nl()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='nl'"
	 * @generated
	 */
	String getNl();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Feature#getNl <em>Nl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nl</em>' attribute.
	 * @see #getNl()
	 * @generated
	 */
	void setNl(String value);

	/**
	 * Returns the value of the '<em><b>Os</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Os</em>' attribute.
	 * @see #setOs(String)
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_Os()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='os'"
	 * @generated
	 */
	String getOs();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Feature#getOs <em>Os</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Os</em>' attribute.
	 * @see #getOs()
	 * @generated
	 */
	void setOs(String value);

	/**
	 * Returns the value of the '<em><b>Plugin1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Plugin1</em>' attribute.
	 * @see #setPlugin1(String)
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_Plugin1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='plugin'"
	 * @generated
	 */
	String getPlugin1();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Feature#getPlugin1 <em>Plugin1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Plugin1</em>' attribute.
	 * @see #getPlugin1()
	 * @generated
	 */
	void setPlugin1(String value);

	/**
	 * Returns the value of the '<em><b>Provider Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provider Name</em>' attribute.
	 * @see #setProviderName(String)
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_ProviderName()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='provider-name'"
	 * @generated
	 */
	String getProviderName();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Feature#getProviderName <em>Provider Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Provider Name</em>' attribute.
	 * @see #getProviderName()
	 * @generated
	 */
	void setProviderName(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_Version()
	 * @model dataType="org.gecko.eclipse.Version"
	 *        extendedMetaData="kind='attribute' name='version'"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Feature#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Ws</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ws</em>' attribute.
	 * @see #setWs(String)
	 * @see org.gecko.eclipse.EclipsePackage#getFeature_Ws()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='ws'"
	 * @generated
	 */
	String getWs();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Feature#getWs <em>Ws</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ws</em>' attribute.
	 * @see #getWs()
	 * @generated
	 */
	void setWs(String value);

} // Feature
