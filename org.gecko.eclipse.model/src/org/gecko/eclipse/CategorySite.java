/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Category Site</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.CategorySite#getFeature <em>Feature</em>}</li>
 *   <li>{@link org.gecko.eclipse.CategorySite#getCategoryDef <em>Category Def</em>}</li>
 * </ul>
 *
 * @see org.gecko.eclipse.EclipsePackage#getCategorySite()
 * @model extendedMetaData="name='CategorySite' kind='elementOnly'"
 * @generated
 */
public interface CategorySite extends EObject {
	/**
	 * Returns the value of the '<em><b>Feature</b></em>' containment reference list.
	 * The list contents are of type {@link org.gecko.eclipse.CategoryFeature}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' containment reference list.
	 * @see org.gecko.eclipse.EclipsePackage#getCategorySite_Feature()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='feature' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<CategoryFeature> getFeature();

	/**
	 * Returns the value of the '<em><b>Category Def</b></em>' containment reference list.
	 * The list contents are of type {@link org.gecko.eclipse.CategoryDef}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Category Def</em>' containment reference list.
	 * @see org.gecko.eclipse.EclipsePackage#getCategorySite_CategoryDef()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='category-def' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<CategoryDef> getCategoryDef();

} // CategorySite
