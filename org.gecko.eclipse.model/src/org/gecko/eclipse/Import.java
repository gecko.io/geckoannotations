/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Import</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.Import#getFeature <em>Feature</em>}</li>
 *   <li>{@link org.gecko.eclipse.Import#getMatch <em>Match</em>}</li>
 *   <li>{@link org.gecko.eclipse.Import#isPatch <em>Patch</em>}</li>
 *   <li>{@link org.gecko.eclipse.Import#getPlugin <em>Plugin</em>}</li>
 *   <li>{@link org.gecko.eclipse.Import#getVersion <em>Version</em>}</li>
 * </ul>
 *
 * @see org.gecko.eclipse.EclipsePackage#getImport()
 * @model extendedMetaData="name='Import' kind='empty'"
 * @generated
 */
public interface Import extends EObject {
	/**
	 * Returns the value of the '<em><b>Feature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' attribute.
	 * @see #setFeature(String)
	 * @see org.gecko.eclipse.EclipsePackage#getImport_Feature()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='feature'"
	 * @generated
	 */
	String getFeature();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Import#getFeature <em>Feature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' attribute.
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(String value);

	/**
	 * Returns the value of the '<em><b>Match</b></em>' attribute.
	 * The literals are from the enumeration {@link org.gecko.eclipse.MatchType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Match</em>' attribute.
	 * @see org.gecko.eclipse.MatchType
	 * @see #isSetMatch()
	 * @see #unsetMatch()
	 * @see #setMatch(MatchType)
	 * @see org.gecko.eclipse.EclipsePackage#getImport_Match()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='match'"
	 * @generated
	 */
	MatchType getMatch();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Import#getMatch <em>Match</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Match</em>' attribute.
	 * @see org.gecko.eclipse.MatchType
	 * @see #isSetMatch()
	 * @see #unsetMatch()
	 * @see #getMatch()
	 * @generated
	 */
	void setMatch(MatchType value);

	/**
	 * Unsets the value of the '{@link org.gecko.eclipse.Import#getMatch <em>Match</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMatch()
	 * @see #getMatch()
	 * @see #setMatch(MatchType)
	 * @generated
	 */
	void unsetMatch();

	/**
	 * Returns whether the value of the '{@link org.gecko.eclipse.Import#getMatch <em>Match</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Match</em>' attribute is set.
	 * @see #unsetMatch()
	 * @see #getMatch()
	 * @see #setMatch(MatchType)
	 * @generated
	 */
	boolean isSetMatch();

	/**
	 * Returns the value of the '<em><b>Patch</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Patch</em>' attribute.
	 * @see #isSetPatch()
	 * @see #unsetPatch()
	 * @see #setPatch(boolean)
	 * @see org.gecko.eclipse.EclipsePackage#getImport_Patch()
	 * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='patch'"
	 * @generated
	 */
	boolean isPatch();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Import#isPatch <em>Patch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Patch</em>' attribute.
	 * @see #isSetPatch()
	 * @see #unsetPatch()
	 * @see #isPatch()
	 * @generated
	 */
	void setPatch(boolean value);

	/**
	 * Unsets the value of the '{@link org.gecko.eclipse.Import#isPatch <em>Patch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPatch()
	 * @see #isPatch()
	 * @see #setPatch(boolean)
	 * @generated
	 */
	void unsetPatch();

	/**
	 * Returns whether the value of the '{@link org.gecko.eclipse.Import#isPatch <em>Patch</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Patch</em>' attribute is set.
	 * @see #unsetPatch()
	 * @see #isPatch()
	 * @see #setPatch(boolean)
	 * @generated
	 */
	boolean isSetPatch();

	/**
	 * Returns the value of the '<em><b>Plugin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Plugin</em>' attribute.
	 * @see #setPlugin(String)
	 * @see org.gecko.eclipse.EclipsePackage#getImport_Plugin()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='plugin'"
	 * @generated
	 */
	String getPlugin();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Import#getPlugin <em>Plugin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Plugin</em>' attribute.
	 * @see #getPlugin()
	 * @generated
	 */
	void setPlugin(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.gecko.eclipse.EclipsePackage#getImport_Version()
	 * @model dataType="org.gecko.eclipse.Version"
	 *        extendedMetaData="kind='attribute' name='version'"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Import#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

} // Import
