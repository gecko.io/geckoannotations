/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.Provides#getProvided <em>Provided</em>}</li>
 *   <li>{@link org.gecko.eclipse.Provides#getSize <em>Size</em>}</li>
 * </ul>
 *
 * @see org.gecko.eclipse.EclipsePackage#getProvides()
 * @model extendedMetaData="name='Provides' kind='elementOnly'"
 * @generated
 */
public interface Provides extends EObject {
	/**
	 * Returns the value of the '<em><b>Provided</b></em>' containment reference list.
	 * The list contents are of type {@link org.gecko.eclipse.Provided}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provided</em>' containment reference list.
	 * @see org.gecko.eclipse.EclipsePackage#getProvides_Provided()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='provided' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Provided> getProvided();

	/**
	 * Returns the value of the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size</em>' attribute.
	 * @see #isSetSize()
	 * @see #unsetSize()
	 * @see #setSize(int)
	 * @see org.gecko.eclipse.EclipsePackage#getProvides_Size()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Int"
	 *        extendedMetaData="kind='attribute' name='size'"
	 * @generated
	 */
	int getSize();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Provides#getSize <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Size</em>' attribute.
	 * @see #isSetSize()
	 * @see #unsetSize()
	 * @see #getSize()
	 * @generated
	 */
	void setSize(int value);

	/**
	 * Unsets the value of the '{@link org.gecko.eclipse.Provides#getSize <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSize()
	 * @see #getSize()
	 * @see #setSize(int)
	 * @generated
	 */
	void unsetSize();

	/**
	 * Returns whether the value of the '{@link org.gecko.eclipse.Provides#getSize <em>Size</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Size</em>' attribute is set.
	 * @see #unsetSize()
	 * @see #getSize()
	 * @see #setSize(int)
	 * @generated
	 */
	boolean isSetSize();

} // Provides
