/**
 */
package org.gecko.eclipse.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.gecko.eclipse.util.EclipseResourceFactoryImpl
 * @generated
 */
public class EclipseResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public EclipseResourceImpl(URI uri) {
		super(uri);
	}

} //EclipseResourceImpl
