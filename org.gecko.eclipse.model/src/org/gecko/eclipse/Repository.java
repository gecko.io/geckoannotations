/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Repository</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.Repository#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.gecko.eclipse.Repository#getMappings <em>Mappings</em>}</li>
 *   <li>{@link org.gecko.eclipse.Repository#getReferences <em>References</em>}</li>
 *   <li>{@link org.gecko.eclipse.Repository#getUnits <em>Units</em>}</li>
 *   <li>{@link org.gecko.eclipse.Repository#getChildren <em>Children</em>}</li>
 *   <li>{@link org.gecko.eclipse.Repository#getArtifacts <em>Artifacts</em>}</li>
 *   <li>{@link org.gecko.eclipse.Repository#getName <em>Name</em>}</li>
 *   <li>{@link org.gecko.eclipse.Repository#getType <em>Type</em>}</li>
 *   <li>{@link org.gecko.eclipse.Repository#getVersion <em>Version</em>}</li>
 * </ul>
 *
 * @see org.gecko.eclipse.EclipsePackage#getRepository()
 * @model extendedMetaData="name='Repository' kind='elementOnly'"
 * @generated
 */
public interface Repository extends EObject {
	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference.
	 * @see #setProperties(Properties)
	 * @see org.gecko.eclipse.EclipsePackage#getRepository_Properties()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='properties' namespace='##targetNamespace'"
	 * @generated
	 */
	Properties getProperties();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Repository#getProperties <em>Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Properties</em>' containment reference.
	 * @see #getProperties()
	 * @generated
	 */
	void setProperties(Properties value);

	/**
	 * Returns the value of the '<em><b>Mappings</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mappings</em>' containment reference.
	 * @see #setMappings(MappingsType)
	 * @see org.gecko.eclipse.EclipsePackage#getRepository_Mappings()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='mappings' namespace='##targetNamespace'"
	 * @generated
	 */
	MappingsType getMappings();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Repository#getMappings <em>Mappings</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mappings</em>' containment reference.
	 * @see #getMappings()
	 * @generated
	 */
	void setMappings(MappingsType value);

	/**
	 * Returns the value of the '<em><b>References</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>References</em>' containment reference.
	 * @see #setReferences(ReferencesType)
	 * @see org.gecko.eclipse.EclipsePackage#getRepository_References()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='references' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferencesType getReferences();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Repository#getReferences <em>References</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>References</em>' containment reference.
	 * @see #getReferences()
	 * @generated
	 */
	void setReferences(ReferencesType value);

	/**
	 * Returns the value of the '<em><b>Units</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Units</em>' containment reference.
	 * @see #setUnits(UnitsType)
	 * @see org.gecko.eclipse.EclipsePackage#getRepository_Units()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='units' namespace='##targetNamespace'"
	 * @generated
	 */
	UnitsType getUnits();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Repository#getUnits <em>Units</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Units</em>' containment reference.
	 * @see #getUnits()
	 * @generated
	 */
	void setUnits(UnitsType value);

	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' containment reference.
	 * @see #setChildren(ChildrenType)
	 * @see org.gecko.eclipse.EclipsePackage#getRepository_Children()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='children' namespace='##targetNamespace'"
	 * @generated
	 */
	ChildrenType getChildren();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Repository#getChildren <em>Children</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Children</em>' containment reference.
	 * @see #getChildren()
	 * @generated
	 */
	void setChildren(ChildrenType value);

	/**
	 * Returns the value of the '<em><b>Artifacts</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Artifacts</em>' containment reference.
	 * @see #setArtifacts(Artifacts)
	 * @see org.gecko.eclipse.EclipsePackage#getRepository_Artifacts()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='artifacts' namespace='##targetNamespace'"
	 * @generated
	 */
	Artifacts getArtifacts();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Repository#getArtifacts <em>Artifacts</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Artifacts</em>' containment reference.
	 * @see #getArtifacts()
	 * @generated
	 */
	void setArtifacts(Artifacts value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.gecko.eclipse.EclipsePackage#getRepository_Name()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Repository#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see org.gecko.eclipse.EclipsePackage#getRepository_Type()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='type'"
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Repository#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.gecko.eclipse.EclipsePackage#getRepository_Version()
	 * @model dataType="org.gecko.eclipse.Version"
	 *        extendedMetaData="kind='attribute' name='version'"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Repository#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

} // Repository
