/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Target</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.Target#getLocations <em>Locations</em>}</li>
 *   <li>{@link org.gecko.eclipse.Target#getEnvironment <em>Environment</em>}</li>
 *   <li>{@link org.gecko.eclipse.Target#getTargetJRE <em>Target JRE</em>}</li>
 *   <li>{@link org.gecko.eclipse.Target#getLauncherArgs <em>Launcher Args</em>}</li>
 *   <li>{@link org.gecko.eclipse.Target#getImplicitDependencies <em>Implicit Dependencies</em>}</li>
 *   <li>{@link org.gecko.eclipse.Target#getName <em>Name</em>}</li>
 *   <li>{@link org.gecko.eclipse.Target#getSequenceNumber <em>Sequence Number</em>}</li>
 * </ul>
 *
 * @see org.gecko.eclipse.EclipsePackage#getTarget()
 * @model extendedMetaData="name='Target' kind='elementOnly'"
 * @generated
 */
public interface Target extends EObject {
	/**
	 * Returns the value of the '<em><b>Locations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locations</em>' containment reference.
	 * @see #setLocations(TargetLocations)
	 * @see org.gecko.eclipse.EclipsePackage#getTarget_Locations()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='locations' namespace='##targetNamespace'"
	 * @generated
	 */
	TargetLocations getLocations();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Target#getLocations <em>Locations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Locations</em>' containment reference.
	 * @see #getLocations()
	 * @generated
	 */
	void setLocations(TargetLocations value);

	/**
	 * Returns the value of the '<em><b>Environment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Environment</em>' containment reference.
	 * @see #setEnvironment(TargetEnvironment)
	 * @see org.gecko.eclipse.EclipsePackage#getTarget_Environment()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='environment' namespace='##targetNamespace'"
	 * @generated
	 */
	TargetEnvironment getEnvironment();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Target#getEnvironment <em>Environment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Environment</em>' containment reference.
	 * @see #getEnvironment()
	 * @generated
	 */
	void setEnvironment(TargetEnvironment value);

	/**
	 * Returns the value of the '<em><b>Target JRE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target JRE</em>' containment reference.
	 * @see #setTargetJRE(TargetJRE)
	 * @see org.gecko.eclipse.EclipsePackage#getTarget_TargetJRE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='targetJRE' namespace='##targetNamespace'"
	 * @generated
	 */
	TargetJRE getTargetJRE();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Target#getTargetJRE <em>Target JRE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target JRE</em>' containment reference.
	 * @see #getTargetJRE()
	 * @generated
	 */
	void setTargetJRE(TargetJRE value);

	/**
	 * Returns the value of the '<em><b>Launcher Args</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Launcher Args</em>' containment reference.
	 * @see #setLauncherArgs(LauncherArgs)
	 * @see org.gecko.eclipse.EclipsePackage#getTarget_LauncherArgs()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='launcherArgs' namespace='##targetNamespace'"
	 * @generated
	 */
	LauncherArgs getLauncherArgs();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Target#getLauncherArgs <em>Launcher Args</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Launcher Args</em>' containment reference.
	 * @see #getLauncherArgs()
	 * @generated
	 */
	void setLauncherArgs(LauncherArgs value);

	/**
	 * Returns the value of the '<em><b>Implicit Dependencies</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implicit Dependencies</em>' containment reference.
	 * @see #setImplicitDependencies(TargetDependencies)
	 * @see org.gecko.eclipse.EclipsePackage#getTarget_ImplicitDependencies()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='implicitDependencies' namespace='##targetNamespace'"
	 * @generated
	 */
	TargetDependencies getImplicitDependencies();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Target#getImplicitDependencies <em>Implicit Dependencies</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Implicit Dependencies</em>' containment reference.
	 * @see #getImplicitDependencies()
	 * @generated
	 */
	void setImplicitDependencies(TargetDependencies value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.gecko.eclipse.EclipsePackage#getTarget_Name()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Target#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sequence Number</em>' attribute.
	 * @see #setSequenceNumber(Long)
	 * @see org.gecko.eclipse.EclipsePackage#getTarget_SequenceNumber()
	 * @model extendedMetaData="kind='attribute' name='sequenceNumber'"
	 * @generated
	 */
	Long getSequenceNumber();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Target#getSequenceNumber <em>Sequence Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sequence Number</em>' attribute.
	 * @see #getSequenceNumber()
	 * @generated
	 */
	void setSequenceNumber(Long value);

} // Target
