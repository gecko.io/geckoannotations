/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Plugin</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.Plugin#getArch <em>Arch</em>}</li>
 *   <li>{@link org.gecko.eclipse.Plugin#getDownloadSize <em>Download Size</em>}</li>
 *   <li>{@link org.gecko.eclipse.Plugin#isFragment <em>Fragment</em>}</li>
 *   <li>{@link org.gecko.eclipse.Plugin#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.eclipse.Plugin#getInstallSize <em>Install Size</em>}</li>
 *   <li>{@link org.gecko.eclipse.Plugin#getNl <em>Nl</em>}</li>
 *   <li>{@link org.gecko.eclipse.Plugin#getOs <em>Os</em>}</li>
 *   <li>{@link org.gecko.eclipse.Plugin#isUnpack <em>Unpack</em>}</li>
 *   <li>{@link org.gecko.eclipse.Plugin#getVersion <em>Version</em>}</li>
 *   <li>{@link org.gecko.eclipse.Plugin#getWs <em>Ws</em>}</li>
 * </ul>
 *
 * @see org.gecko.eclipse.EclipsePackage#getPlugin()
 * @model extendedMetaData="name='Plugin' kind='empty'"
 * @generated
 */
public interface Plugin extends EObject {
	/**
	 * Returns the value of the '<em><b>Arch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arch</em>' attribute.
	 * @see #setArch(String)
	 * @see org.gecko.eclipse.EclipsePackage#getPlugin_Arch()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='arch'"
	 * @generated
	 */
	String getArch();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Plugin#getArch <em>Arch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Arch</em>' attribute.
	 * @see #getArch()
	 * @generated
	 */
	void setArch(String value);

	/**
	 * Returns the value of the '<em><b>Download Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Download Size</em>' attribute.
	 * @see #isSetDownloadSize()
	 * @see #unsetDownloadSize()
	 * @see #setDownloadSize(int)
	 * @see org.gecko.eclipse.EclipsePackage#getPlugin_DownloadSize()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Int"
	 *        extendedMetaData="kind='attribute' name='download-size'"
	 * @generated
	 */
	int getDownloadSize();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Plugin#getDownloadSize <em>Download Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Download Size</em>' attribute.
	 * @see #isSetDownloadSize()
	 * @see #unsetDownloadSize()
	 * @see #getDownloadSize()
	 * @generated
	 */
	void setDownloadSize(int value);

	/**
	 * Unsets the value of the '{@link org.gecko.eclipse.Plugin#getDownloadSize <em>Download Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDownloadSize()
	 * @see #getDownloadSize()
	 * @see #setDownloadSize(int)
	 * @generated
	 */
	void unsetDownloadSize();

	/**
	 * Returns whether the value of the '{@link org.gecko.eclipse.Plugin#getDownloadSize <em>Download Size</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Download Size</em>' attribute is set.
	 * @see #unsetDownloadSize()
	 * @see #getDownloadSize()
	 * @see #setDownloadSize(int)
	 * @generated
	 */
	boolean isSetDownloadSize();

	/**
	 * Returns the value of the '<em><b>Fragment</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fragment</em>' attribute.
	 * @see #isSetFragment()
	 * @see #unsetFragment()
	 * @see #setFragment(boolean)
	 * @see org.gecko.eclipse.EclipsePackage#getPlugin_Fragment()
	 * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='fragment'"
	 * @generated
	 */
	boolean isFragment();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Plugin#isFragment <em>Fragment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fragment</em>' attribute.
	 * @see #isSetFragment()
	 * @see #unsetFragment()
	 * @see #isFragment()
	 * @generated
	 */
	void setFragment(boolean value);

	/**
	 * Unsets the value of the '{@link org.gecko.eclipse.Plugin#isFragment <em>Fragment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFragment()
	 * @see #isFragment()
	 * @see #setFragment(boolean)
	 * @generated
	 */
	void unsetFragment();

	/**
	 * Returns whether the value of the '{@link org.gecko.eclipse.Plugin#isFragment <em>Fragment</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Fragment</em>' attribute is set.
	 * @see #unsetFragment()
	 * @see #isFragment()
	 * @see #setFragment(boolean)
	 * @generated
	 */
	boolean isSetFragment();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.gecko.eclipse.EclipsePackage#getPlugin_Id()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='id'"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Plugin#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Install Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Install Size</em>' attribute.
	 * @see #isSetInstallSize()
	 * @see #unsetInstallSize()
	 * @see #setInstallSize(int)
	 * @see org.gecko.eclipse.EclipsePackage#getPlugin_InstallSize()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Int"
	 *        extendedMetaData="kind='attribute' name='install-size'"
	 * @generated
	 */
	int getInstallSize();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Plugin#getInstallSize <em>Install Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Install Size</em>' attribute.
	 * @see #isSetInstallSize()
	 * @see #unsetInstallSize()
	 * @see #getInstallSize()
	 * @generated
	 */
	void setInstallSize(int value);

	/**
	 * Unsets the value of the '{@link org.gecko.eclipse.Plugin#getInstallSize <em>Install Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInstallSize()
	 * @see #getInstallSize()
	 * @see #setInstallSize(int)
	 * @generated
	 */
	void unsetInstallSize();

	/**
	 * Returns whether the value of the '{@link org.gecko.eclipse.Plugin#getInstallSize <em>Install Size</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Install Size</em>' attribute is set.
	 * @see #unsetInstallSize()
	 * @see #getInstallSize()
	 * @see #setInstallSize(int)
	 * @generated
	 */
	boolean isSetInstallSize();

	/**
	 * Returns the value of the '<em><b>Nl</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nl</em>' attribute.
	 * @see #setNl(String)
	 * @see org.gecko.eclipse.EclipsePackage#getPlugin_Nl()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='nl'"
	 * @generated
	 */
	String getNl();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Plugin#getNl <em>Nl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nl</em>' attribute.
	 * @see #getNl()
	 * @generated
	 */
	void setNl(String value);

	/**
	 * Returns the value of the '<em><b>Os</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Os</em>' attribute.
	 * @see #setOs(String)
	 * @see org.gecko.eclipse.EclipsePackage#getPlugin_Os()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='os'"
	 * @generated
	 */
	String getOs();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Plugin#getOs <em>Os</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Os</em>' attribute.
	 * @see #getOs()
	 * @generated
	 */
	void setOs(String value);

	/**
	 * Returns the value of the '<em><b>Unpack</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unpack</em>' attribute.
	 * @see #isSetUnpack()
	 * @see #unsetUnpack()
	 * @see #setUnpack(boolean)
	 * @see org.gecko.eclipse.EclipsePackage#getPlugin_Unpack()
	 * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='unpack'"
	 * @generated
	 */
	boolean isUnpack();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Plugin#isUnpack <em>Unpack</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unpack</em>' attribute.
	 * @see #isSetUnpack()
	 * @see #unsetUnpack()
	 * @see #isUnpack()
	 * @generated
	 */
	void setUnpack(boolean value);

	/**
	 * Unsets the value of the '{@link org.gecko.eclipse.Plugin#isUnpack <em>Unpack</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetUnpack()
	 * @see #isUnpack()
	 * @see #setUnpack(boolean)
	 * @generated
	 */
	void unsetUnpack();

	/**
	 * Returns whether the value of the '{@link org.gecko.eclipse.Plugin#isUnpack <em>Unpack</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Unpack</em>' attribute is set.
	 * @see #unsetUnpack()
	 * @see #isUnpack()
	 * @see #setUnpack(boolean)
	 * @generated
	 */
	boolean isSetUnpack();

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.gecko.eclipse.EclipsePackage#getPlugin_Version()
	 * @model dataType="org.gecko.eclipse.Version"
	 *        extendedMetaData="kind='attribute' name='version'"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Plugin#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Ws</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ws</em>' attribute.
	 * @see #setWs(String)
	 * @see org.gecko.eclipse.EclipsePackage#getPlugin_Ws()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='ws'"
	 * @generated
	 */
	String getWs();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.Plugin#getWs <em>Ws</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ws</em>' attribute.
	 * @see #getWs()
	 * @generated
	 */
	void setWs(String value);

} // Plugin
