/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Url Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.UrlType#getUpdate <em>Update</em>}</li>
 *   <li>{@link org.gecko.eclipse.UrlType#getDiscovery <em>Discovery</em>}</li>
 * </ul>
 *
 * @see org.gecko.eclipse.EclipsePackage#getUrlType()
 * @model extendedMetaData="name='url_._type' kind='elementOnly'"
 * @generated
 */
public interface UrlType extends EObject {
	/**
	 * Returns the value of the '<em><b>Update</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Update</em>' containment reference.
	 * @see #setUpdate(Site)
	 * @see org.gecko.eclipse.EclipsePackage#getUrlType_Update()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='update' namespace='##targetNamespace'"
	 * @generated
	 */
	Site getUpdate();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.UrlType#getUpdate <em>Update</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Update</em>' containment reference.
	 * @see #getUpdate()
	 * @generated
	 */
	void setUpdate(Site value);

	/**
	 * Returns the value of the '<em><b>Discovery</b></em>' containment reference list.
	 * The list contents are of type {@link org.gecko.eclipse.Site}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discovery</em>' containment reference list.
	 * @see org.gecko.eclipse.EclipsePackage#getUrlType_Discovery()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='discovery' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Site> getDiscovery();

} // UrlType
