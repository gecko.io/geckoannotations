/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Target Location</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.TargetLocation#getRepository <em>Repository</em>}</li>
 *   <li>{@link org.gecko.eclipse.TargetLocation#getUnit <em>Unit</em>}</li>
 *   <li>{@link org.gecko.eclipse.TargetLocation#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.eclipse.TargetLocation#isIncludeAllPlatforms <em>Include All Platforms</em>}</li>
 *   <li>{@link org.gecko.eclipse.TargetLocation#isIncludeConfigurePhase <em>Include Configure Phase</em>}</li>
 *   <li>{@link org.gecko.eclipse.TargetLocation#getIncludeMode <em>Include Mode</em>}</li>
 *   <li>{@link org.gecko.eclipse.TargetLocation#isIncludeSource <em>Include Source</em>}</li>
 *   <li>{@link org.gecko.eclipse.TargetLocation#getPath <em>Path</em>}</li>
 *   <li>{@link org.gecko.eclipse.TargetLocation#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see org.gecko.eclipse.EclipsePackage#getTargetLocation()
 * @model extendedMetaData="name='TargetLocation' kind='elementOnly'"
 * @generated
 */
public interface TargetLocation extends EObject {
	/**
	 * Returns the value of the '<em><b>Repository</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Repository</em>' containment reference.
	 * @see #setRepository(RepositoryRef)
	 * @see org.gecko.eclipse.EclipsePackage#getTargetLocation_Repository()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='repository' namespace='##targetNamespace'"
	 * @generated
	 */
	RepositoryRef getRepository();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.TargetLocation#getRepository <em>Repository</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Repository</em>' containment reference.
	 * @see #getRepository()
	 * @generated
	 */
	void setRepository(RepositoryRef value);

	/**
	 * Returns the value of the '<em><b>Unit</b></em>' containment reference list.
	 * The list contents are of type {@link org.gecko.eclipse.Unit}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit</em>' containment reference list.
	 * @see org.gecko.eclipse.EclipsePackage#getTargetLocation_Unit()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='unit' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Unit> getUnit();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.gecko.eclipse.EclipsePackage#getTargetLocation_Id()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='id'"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.TargetLocation#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Include All Platforms</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Include All Platforms</em>' attribute.
	 * @see #isSetIncludeAllPlatforms()
	 * @see #unsetIncludeAllPlatforms()
	 * @see #setIncludeAllPlatforms(boolean)
	 * @see org.gecko.eclipse.EclipsePackage#getTargetLocation_IncludeAllPlatforms()
	 * @model default="true" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='includeAllPlatforms'"
	 * @generated
	 */
	boolean isIncludeAllPlatforms();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.TargetLocation#isIncludeAllPlatforms <em>Include All Platforms</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Include All Platforms</em>' attribute.
	 * @see #isSetIncludeAllPlatforms()
	 * @see #unsetIncludeAllPlatforms()
	 * @see #isIncludeAllPlatforms()
	 * @generated
	 */
	void setIncludeAllPlatforms(boolean value);

	/**
	 * Unsets the value of the '{@link org.gecko.eclipse.TargetLocation#isIncludeAllPlatforms <em>Include All Platforms</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIncludeAllPlatforms()
	 * @see #isIncludeAllPlatforms()
	 * @see #setIncludeAllPlatforms(boolean)
	 * @generated
	 */
	void unsetIncludeAllPlatforms();

	/**
	 * Returns whether the value of the '{@link org.gecko.eclipse.TargetLocation#isIncludeAllPlatforms <em>Include All Platforms</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Include All Platforms</em>' attribute is set.
	 * @see #unsetIncludeAllPlatforms()
	 * @see #isIncludeAllPlatforms()
	 * @see #setIncludeAllPlatforms(boolean)
	 * @generated
	 */
	boolean isSetIncludeAllPlatforms();

	/**
	 * Returns the value of the '<em><b>Include Configure Phase</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Include Configure Phase</em>' attribute.
	 * @see #isSetIncludeConfigurePhase()
	 * @see #unsetIncludeConfigurePhase()
	 * @see #setIncludeConfigurePhase(boolean)
	 * @see org.gecko.eclipse.EclipsePackage#getTargetLocation_IncludeConfigurePhase()
	 * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='includeConfigurePhase'"
	 * @generated
	 */
	boolean isIncludeConfigurePhase();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.TargetLocation#isIncludeConfigurePhase <em>Include Configure Phase</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Include Configure Phase</em>' attribute.
	 * @see #isSetIncludeConfigurePhase()
	 * @see #unsetIncludeConfigurePhase()
	 * @see #isIncludeConfigurePhase()
	 * @generated
	 */
	void setIncludeConfigurePhase(boolean value);

	/**
	 * Unsets the value of the '{@link org.gecko.eclipse.TargetLocation#isIncludeConfigurePhase <em>Include Configure Phase</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIncludeConfigurePhase()
	 * @see #isIncludeConfigurePhase()
	 * @see #setIncludeConfigurePhase(boolean)
	 * @generated
	 */
	void unsetIncludeConfigurePhase();

	/**
	 * Returns whether the value of the '{@link org.gecko.eclipse.TargetLocation#isIncludeConfigurePhase <em>Include Configure Phase</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Include Configure Phase</em>' attribute is set.
	 * @see #unsetIncludeConfigurePhase()
	 * @see #isIncludeConfigurePhase()
	 * @see #setIncludeConfigurePhase(boolean)
	 * @generated
	 */
	boolean isSetIncludeConfigurePhase();

	/**
	 * Returns the value of the '<em><b>Include Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link org.gecko.eclipse.LocationIncludeType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Include Mode</em>' attribute.
	 * @see org.gecko.eclipse.LocationIncludeType
	 * @see #isSetIncludeMode()
	 * @see #unsetIncludeMode()
	 * @see #setIncludeMode(LocationIncludeType)
	 * @see org.gecko.eclipse.EclipsePackage#getTargetLocation_IncludeMode()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='includeMode'"
	 * @generated
	 */
	LocationIncludeType getIncludeMode();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.TargetLocation#getIncludeMode <em>Include Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Include Mode</em>' attribute.
	 * @see org.gecko.eclipse.LocationIncludeType
	 * @see #isSetIncludeMode()
	 * @see #unsetIncludeMode()
	 * @see #getIncludeMode()
	 * @generated
	 */
	void setIncludeMode(LocationIncludeType value);

	/**
	 * Unsets the value of the '{@link org.gecko.eclipse.TargetLocation#getIncludeMode <em>Include Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIncludeMode()
	 * @see #getIncludeMode()
	 * @see #setIncludeMode(LocationIncludeType)
	 * @generated
	 */
	void unsetIncludeMode();

	/**
	 * Returns whether the value of the '{@link org.gecko.eclipse.TargetLocation#getIncludeMode <em>Include Mode</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Include Mode</em>' attribute is set.
	 * @see #unsetIncludeMode()
	 * @see #getIncludeMode()
	 * @see #setIncludeMode(LocationIncludeType)
	 * @generated
	 */
	boolean isSetIncludeMode();

	/**
	 * Returns the value of the '<em><b>Include Source</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Include Source</em>' attribute.
	 * @see #isSetIncludeSource()
	 * @see #unsetIncludeSource()
	 * @see #setIncludeSource(boolean)
	 * @see org.gecko.eclipse.EclipsePackage#getTargetLocation_IncludeSource()
	 * @model default="true" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='includeSource'"
	 * @generated
	 */
	boolean isIncludeSource();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.TargetLocation#isIncludeSource <em>Include Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Include Source</em>' attribute.
	 * @see #isSetIncludeSource()
	 * @see #unsetIncludeSource()
	 * @see #isIncludeSource()
	 * @generated
	 */
	void setIncludeSource(boolean value);

	/**
	 * Unsets the value of the '{@link org.gecko.eclipse.TargetLocation#isIncludeSource <em>Include Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIncludeSource()
	 * @see #isIncludeSource()
	 * @see #setIncludeSource(boolean)
	 * @generated
	 */
	void unsetIncludeSource();

	/**
	 * Returns whether the value of the '{@link org.gecko.eclipse.TargetLocation#isIncludeSource <em>Include Source</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Include Source</em>' attribute is set.
	 * @see #unsetIncludeSource()
	 * @see #isIncludeSource()
	 * @see #setIncludeSource(boolean)
	 * @generated
	 */
	boolean isSetIncludeSource();

	/**
	 * Returns the value of the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Path</em>' attribute.
	 * @see #setPath(String)
	 * @see org.gecko.eclipse.EclipsePackage#getTargetLocation_Path()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='path'"
	 * @generated
	 */
	String getPath();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.TargetLocation#getPath <em>Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Path</em>' attribute.
	 * @see #getPath()
	 * @generated
	 */
	void setPath(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.gecko.eclipse.TargetType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.gecko.eclipse.TargetType
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #setType(TargetType)
	 * @see org.gecko.eclipse.EclipsePackage#getTargetLocation_Type()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='type'"
	 * @generated
	 */
	TargetType getType();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.TargetLocation#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.gecko.eclipse.TargetType
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #getType()
	 * @generated
	 */
	void setType(TargetType value);

	/**
	 * Unsets the value of the '{@link org.gecko.eclipse.TargetLocation#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetType()
	 * @see #getType()
	 * @see #setType(TargetType)
	 * @generated
	 */
	void unsetType();

	/**
	 * Returns whether the value of the '{@link org.gecko.eclipse.TargetLocation#getType <em>Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Type</em>' attribute is set.
	 * @see #unsetType()
	 * @see #getType()
	 * @see #setType(TargetType)
	 * @generated
	 */
	boolean isSetType();

} // TargetLocation
