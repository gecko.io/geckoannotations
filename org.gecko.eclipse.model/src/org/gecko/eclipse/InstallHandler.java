/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Install Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.InstallHandler#getHandler <em>Handler</em>}</li>
 *   <li>{@link org.gecko.eclipse.InstallHandler#getLibrary <em>Library</em>}</li>
 * </ul>
 *
 * @see org.gecko.eclipse.EclipsePackage#getInstallHandler()
 * @model extendedMetaData="name='InstallHandler' kind='empty'"
 * @generated
 */
public interface InstallHandler extends EObject {
	/**
	 * Returns the value of the '<em><b>Handler</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Handler</em>' attribute.
	 * @see #setHandler(String)
	 * @see org.gecko.eclipse.EclipsePackage#getInstallHandler_Handler()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='handler'"
	 * @generated
	 */
	String getHandler();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.InstallHandler#getHandler <em>Handler</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Handler</em>' attribute.
	 * @see #getHandler()
	 * @generated
	 */
	void setHandler(String value);

	/**
	 * Returns the value of the '<em><b>Library</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Library</em>' attribute.
	 * @see #setLibrary(String)
	 * @see org.gecko.eclipse.EclipsePackage#getInstallHandler_Library()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='library'"
	 * @generated
	 */
	String getLibrary();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.InstallHandler#getLibrary <em>Library</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Library</em>' attribute.
	 * @see #getLibrary()
	 * @generated
	 */
	void setLibrary(String value);

} // InstallHandler
