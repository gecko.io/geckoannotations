/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Repository Refs</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.RepositoryRefs#getOptions <em>Options</em>}</li>
 *   <li>{@link org.gecko.eclipse.RepositoryRefs#getType <em>Type</em>}</li>
 *   <li>{@link org.gecko.eclipse.RepositoryRefs#getUri <em>Uri</em>}</li>
 *   <li>{@link org.gecko.eclipse.RepositoryRefs#getUrl <em>Url</em>}</li>
 * </ul>
 *
 * @see org.gecko.eclipse.EclipsePackage#getRepositoryRefs()
 * @model extendedMetaData="name='RepositoryRefs' kind='empty'"
 * @generated
 */
public interface RepositoryRefs extends EObject {
	/**
	 * Returns the value of the '<em><b>Options</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Options</em>' attribute.
	 * @see #isSetOptions()
	 * @see #unsetOptions()
	 * @see #setOptions(int)
	 * @see org.gecko.eclipse.EclipsePackage#getRepositoryRefs_Options()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Int"
	 *        extendedMetaData="kind='attribute' name='options'"
	 * @generated
	 */
	int getOptions();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.RepositoryRefs#getOptions <em>Options</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Options</em>' attribute.
	 * @see #isSetOptions()
	 * @see #unsetOptions()
	 * @see #getOptions()
	 * @generated
	 */
	void setOptions(int value);

	/**
	 * Unsets the value of the '{@link org.gecko.eclipse.RepositoryRefs#getOptions <em>Options</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOptions()
	 * @see #getOptions()
	 * @see #setOptions(int)
	 * @generated
	 */
	void unsetOptions();

	/**
	 * Returns whether the value of the '{@link org.gecko.eclipse.RepositoryRefs#getOptions <em>Options</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Options</em>' attribute is set.
	 * @see #unsetOptions()
	 * @see #getOptions()
	 * @see #setOptions(int)
	 * @generated
	 */
	boolean isSetOptions();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #setType(int)
	 * @see org.gecko.eclipse.EclipsePackage#getRepositoryRefs_Type()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Int"
	 *        extendedMetaData="kind='attribute' name='type'"
	 * @generated
	 */
	int getType();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.RepositoryRefs#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #getType()
	 * @generated
	 */
	void setType(int value);

	/**
	 * Unsets the value of the '{@link org.gecko.eclipse.RepositoryRefs#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetType()
	 * @see #getType()
	 * @see #setType(int)
	 * @generated
	 */
	void unsetType();

	/**
	 * Returns whether the value of the '{@link org.gecko.eclipse.RepositoryRefs#getType <em>Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Type</em>' attribute is set.
	 * @see #unsetType()
	 * @see #getType()
	 * @see #setType(int)
	 * @generated
	 */
	boolean isSetType();

	/**
	 * Returns the value of the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uri</em>' attribute.
	 * @see #setUri(String)
	 * @see org.gecko.eclipse.EclipsePackage#getRepositoryRefs_Uri()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='uri'"
	 * @generated
	 */
	String getUri();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.RepositoryRefs#getUri <em>Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uri</em>' attribute.
	 * @see #getUri()
	 * @generated
	 */
	void setUri(String value);

	/**
	 * Returns the value of the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Url</em>' attribute.
	 * @see #setUrl(String)
	 * @see org.gecko.eclipse.EclipsePackage#getRepositoryRefs_Url()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='url'"
	 * @generated
	 */
	String getUrl();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.RepositoryRefs#getUrl <em>Url</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Url</em>' attribute.
	 * @see #getUrl()
	 * @generated
	 */
	void setUrl(String value);

} // RepositoryRefs
