/**
 */
package org.gecko.eclipse.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.gecko.eclipse.EclipsePackage;
import org.gecko.eclipse.LauncherArgs;
import org.gecko.eclipse.Target;
import org.gecko.eclipse.TargetDependencies;
import org.gecko.eclipse.TargetEnvironment;
import org.gecko.eclipse.TargetJRE;
import org.gecko.eclipse.TargetLocations;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Target</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.impl.TargetImpl#getLocations <em>Locations</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.TargetImpl#getEnvironment <em>Environment</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.TargetImpl#getTargetJRE <em>Target JRE</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.TargetImpl#getLauncherArgs <em>Launcher Args</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.TargetImpl#getImplicitDependencies <em>Implicit Dependencies</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.TargetImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.TargetImpl#getSequenceNumber <em>Sequence Number</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TargetImpl extends MinimalEObjectImpl.Container implements Target {
	/**
	 * The cached value of the '{@link #getLocations() <em>Locations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocations()
	 * @generated
	 * @ordered
	 */
	protected TargetLocations locations;

	/**
	 * The cached value of the '{@link #getEnvironment() <em>Environment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnvironment()
	 * @generated
	 * @ordered
	 */
	protected TargetEnvironment environment;

	/**
	 * The cached value of the '{@link #getTargetJRE() <em>Target JRE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetJRE()
	 * @generated
	 * @ordered
	 */
	protected TargetJRE targetJRE;

	/**
	 * The cached value of the '{@link #getLauncherArgs() <em>Launcher Args</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLauncherArgs()
	 * @generated
	 * @ordered
	 */
	protected LauncherArgs launcherArgs;

	/**
	 * The cached value of the '{@link #getImplicitDependencies() <em>Implicit Dependencies</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplicitDependencies()
	 * @generated
	 * @ordered
	 */
	protected TargetDependencies implicitDependencies;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSequenceNumber() <em>Sequence Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSequenceNumber()
	 * @generated
	 * @ordered
	 */
	protected static final Long SEQUENCE_NUMBER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSequenceNumber() <em>Sequence Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSequenceNumber()
	 * @generated
	 * @ordered
	 */
	protected Long sequenceNumber = SEQUENCE_NUMBER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TargetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EclipsePackage.Literals.TARGET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TargetLocations getLocations() {
		return locations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocations(TargetLocations newLocations, NotificationChain msgs) {
		TargetLocations oldLocations = locations;
		locations = newLocations;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET__LOCATIONS, oldLocations, newLocations);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLocations(TargetLocations newLocations) {
		if (newLocations != locations) {
			NotificationChain msgs = null;
			if (locations != null)
				msgs = ((InternalEObject)locations).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.TARGET__LOCATIONS, null, msgs);
			if (newLocations != null)
				msgs = ((InternalEObject)newLocations).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.TARGET__LOCATIONS, null, msgs);
			msgs = basicSetLocations(newLocations, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET__LOCATIONS, newLocations, newLocations));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TargetEnvironment getEnvironment() {
		return environment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnvironment(TargetEnvironment newEnvironment, NotificationChain msgs) {
		TargetEnvironment oldEnvironment = environment;
		environment = newEnvironment;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET__ENVIRONMENT, oldEnvironment, newEnvironment);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEnvironment(TargetEnvironment newEnvironment) {
		if (newEnvironment != environment) {
			NotificationChain msgs = null;
			if (environment != null)
				msgs = ((InternalEObject)environment).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.TARGET__ENVIRONMENT, null, msgs);
			if (newEnvironment != null)
				msgs = ((InternalEObject)newEnvironment).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.TARGET__ENVIRONMENT, null, msgs);
			msgs = basicSetEnvironment(newEnvironment, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET__ENVIRONMENT, newEnvironment, newEnvironment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TargetJRE getTargetJRE() {
		return targetJRE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTargetJRE(TargetJRE newTargetJRE, NotificationChain msgs) {
		TargetJRE oldTargetJRE = targetJRE;
		targetJRE = newTargetJRE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET__TARGET_JRE, oldTargetJRE, newTargetJRE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTargetJRE(TargetJRE newTargetJRE) {
		if (newTargetJRE != targetJRE) {
			NotificationChain msgs = null;
			if (targetJRE != null)
				msgs = ((InternalEObject)targetJRE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.TARGET__TARGET_JRE, null, msgs);
			if (newTargetJRE != null)
				msgs = ((InternalEObject)newTargetJRE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.TARGET__TARGET_JRE, null, msgs);
			msgs = basicSetTargetJRE(newTargetJRE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET__TARGET_JRE, newTargetJRE, newTargetJRE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LauncherArgs getLauncherArgs() {
		return launcherArgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLauncherArgs(LauncherArgs newLauncherArgs, NotificationChain msgs) {
		LauncherArgs oldLauncherArgs = launcherArgs;
		launcherArgs = newLauncherArgs;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET__LAUNCHER_ARGS, oldLauncherArgs, newLauncherArgs);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLauncherArgs(LauncherArgs newLauncherArgs) {
		if (newLauncherArgs != launcherArgs) {
			NotificationChain msgs = null;
			if (launcherArgs != null)
				msgs = ((InternalEObject)launcherArgs).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.TARGET__LAUNCHER_ARGS, null, msgs);
			if (newLauncherArgs != null)
				msgs = ((InternalEObject)newLauncherArgs).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.TARGET__LAUNCHER_ARGS, null, msgs);
			msgs = basicSetLauncherArgs(newLauncherArgs, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET__LAUNCHER_ARGS, newLauncherArgs, newLauncherArgs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TargetDependencies getImplicitDependencies() {
		return implicitDependencies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImplicitDependencies(TargetDependencies newImplicitDependencies, NotificationChain msgs) {
		TargetDependencies oldImplicitDependencies = implicitDependencies;
		implicitDependencies = newImplicitDependencies;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET__IMPLICIT_DEPENDENCIES, oldImplicitDependencies, newImplicitDependencies);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setImplicitDependencies(TargetDependencies newImplicitDependencies) {
		if (newImplicitDependencies != implicitDependencies) {
			NotificationChain msgs = null;
			if (implicitDependencies != null)
				msgs = ((InternalEObject)implicitDependencies).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.TARGET__IMPLICIT_DEPENDENCIES, null, msgs);
			if (newImplicitDependencies != null)
				msgs = ((InternalEObject)newImplicitDependencies).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.TARGET__IMPLICIT_DEPENDENCIES, null, msgs);
			msgs = basicSetImplicitDependencies(newImplicitDependencies, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET__IMPLICIT_DEPENDENCIES, newImplicitDependencies, newImplicitDependencies));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Long getSequenceNumber() {
		return sequenceNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSequenceNumber(Long newSequenceNumber) {
		Long oldSequenceNumber = sequenceNumber;
		sequenceNumber = newSequenceNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET__SEQUENCE_NUMBER, oldSequenceNumber, sequenceNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EclipsePackage.TARGET__LOCATIONS:
				return basicSetLocations(null, msgs);
			case EclipsePackage.TARGET__ENVIRONMENT:
				return basicSetEnvironment(null, msgs);
			case EclipsePackage.TARGET__TARGET_JRE:
				return basicSetTargetJRE(null, msgs);
			case EclipsePackage.TARGET__LAUNCHER_ARGS:
				return basicSetLauncherArgs(null, msgs);
			case EclipsePackage.TARGET__IMPLICIT_DEPENDENCIES:
				return basicSetImplicitDependencies(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EclipsePackage.TARGET__LOCATIONS:
				return getLocations();
			case EclipsePackage.TARGET__ENVIRONMENT:
				return getEnvironment();
			case EclipsePackage.TARGET__TARGET_JRE:
				return getTargetJRE();
			case EclipsePackage.TARGET__LAUNCHER_ARGS:
				return getLauncherArgs();
			case EclipsePackage.TARGET__IMPLICIT_DEPENDENCIES:
				return getImplicitDependencies();
			case EclipsePackage.TARGET__NAME:
				return getName();
			case EclipsePackage.TARGET__SEQUENCE_NUMBER:
				return getSequenceNumber();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EclipsePackage.TARGET__LOCATIONS:
				setLocations((TargetLocations)newValue);
				return;
			case EclipsePackage.TARGET__ENVIRONMENT:
				setEnvironment((TargetEnvironment)newValue);
				return;
			case EclipsePackage.TARGET__TARGET_JRE:
				setTargetJRE((TargetJRE)newValue);
				return;
			case EclipsePackage.TARGET__LAUNCHER_ARGS:
				setLauncherArgs((LauncherArgs)newValue);
				return;
			case EclipsePackage.TARGET__IMPLICIT_DEPENDENCIES:
				setImplicitDependencies((TargetDependencies)newValue);
				return;
			case EclipsePackage.TARGET__NAME:
				setName((String)newValue);
				return;
			case EclipsePackage.TARGET__SEQUENCE_NUMBER:
				setSequenceNumber((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EclipsePackage.TARGET__LOCATIONS:
				setLocations((TargetLocations)null);
				return;
			case EclipsePackage.TARGET__ENVIRONMENT:
				setEnvironment((TargetEnvironment)null);
				return;
			case EclipsePackage.TARGET__TARGET_JRE:
				setTargetJRE((TargetJRE)null);
				return;
			case EclipsePackage.TARGET__LAUNCHER_ARGS:
				setLauncherArgs((LauncherArgs)null);
				return;
			case EclipsePackage.TARGET__IMPLICIT_DEPENDENCIES:
				setImplicitDependencies((TargetDependencies)null);
				return;
			case EclipsePackage.TARGET__NAME:
				setName(NAME_EDEFAULT);
				return;
			case EclipsePackage.TARGET__SEQUENCE_NUMBER:
				setSequenceNumber(SEQUENCE_NUMBER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EclipsePackage.TARGET__LOCATIONS:
				return locations != null;
			case EclipsePackage.TARGET__ENVIRONMENT:
				return environment != null;
			case EclipsePackage.TARGET__TARGET_JRE:
				return targetJRE != null;
			case EclipsePackage.TARGET__LAUNCHER_ARGS:
				return launcherArgs != null;
			case EclipsePackage.TARGET__IMPLICIT_DEPENDENCIES:
				return implicitDependencies != null;
			case EclipsePackage.TARGET__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case EclipsePackage.TARGET__SEQUENCE_NUMBER:
				return SEQUENCE_NUMBER_EDEFAULT == null ? sequenceNumber != null : !SEQUENCE_NUMBER_EDEFAULT.equals(sequenceNumber);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", sequenceNumber: ");
		result.append(sequenceNumber);
		result.append(')');
		return result.toString();
	}

} //TargetImpl
