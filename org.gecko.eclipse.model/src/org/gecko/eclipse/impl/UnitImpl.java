/**
 */
package org.gecko.eclipse.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.gecko.eclipse.Artifacts;
import org.gecko.eclipse.Description;
import org.gecko.eclipse.EclipsePackage;
import org.gecko.eclipse.LicensesType;
import org.gecko.eclipse.Properties;
import org.gecko.eclipse.Provides;
import org.gecko.eclipse.Requires;
import org.gecko.eclipse.Touchpoint;
import org.gecko.eclipse.TouchpointDataType;
import org.gecko.eclipse.Unit;
import org.gecko.eclipse.Update;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unit</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.impl.UnitImpl#getUpdate <em>Update</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.UnitImpl#getArtifacts <em>Artifacts</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.UnitImpl#getHostRequirements <em>Host Requirements</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.UnitImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.UnitImpl#getProvides <em>Provides</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.UnitImpl#getRequires <em>Requires</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.UnitImpl#getFilter <em>Filter</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.UnitImpl#getTouchpoint <em>Touchpoint</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.UnitImpl#getTouchpointData <em>Touchpoint Data</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.UnitImpl#getLicenses <em>Licenses</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.UnitImpl#getCopyright <em>Copyright</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.UnitImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.UnitImpl#isSingleton <em>Singleton</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.UnitImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.UnitImpl#getGeneration <em>Generation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UnitImpl extends MinimalEObjectImpl.Container implements Unit {
	/**
	 * The cached value of the '{@link #getUpdate() <em>Update</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpdate()
	 * @generated
	 * @ordered
	 */
	protected Update update;

	/**
	 * The cached value of the '{@link #getArtifacts() <em>Artifacts</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArtifacts()
	 * @generated
	 * @ordered
	 */
	protected Artifacts artifacts;

	/**
	 * The cached value of the '{@link #getHostRequirements() <em>Host Requirements</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHostRequirements()
	 * @generated
	 * @ordered
	 */
	protected Requires hostRequirements;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected Properties properties;

	/**
	 * The cached value of the '{@link #getProvides() <em>Provides</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvides()
	 * @generated
	 * @ordered
	 */
	protected Provides provides;

	/**
	 * The cached value of the '{@link #getRequires() <em>Requires</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequires()
	 * @generated
	 * @ordered
	 */
	protected Requires requires;

	/**
	 * The default value of the '{@link #getFilter() <em>Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilter()
	 * @generated
	 * @ordered
	 */
	protected static final String FILTER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFilter() <em>Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilter()
	 * @generated
	 * @ordered
	 */
	protected String filter = FILTER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTouchpoint() <em>Touchpoint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTouchpoint()
	 * @generated
	 * @ordered
	 */
	protected Touchpoint touchpoint;

	/**
	 * The cached value of the '{@link #getTouchpointData() <em>Touchpoint Data</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTouchpointData()
	 * @generated
	 * @ordered
	 */
	protected TouchpointDataType touchpointData;

	/**
	 * The cached value of the '{@link #getLicenses() <em>Licenses</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLicenses()
	 * @generated
	 * @ordered
	 */
	protected LicensesType licenses;

	/**
	 * The cached value of the '{@link #getCopyright() <em>Copyright</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCopyright()
	 * @generated
	 * @ordered
	 */
	protected Description copyright;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #isSingleton() <em>Singleton</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSingleton()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SINGLETON_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSingleton() <em>Singleton</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSingleton()
	 * @generated
	 * @ordered
	 */
	protected boolean singleton = SINGLETON_EDEFAULT;

	/**
	 * This is true if the Singleton attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean singletonESet;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getGeneration() <em>Generation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeneration()
	 * @generated
	 * @ordered
	 */
	protected static final Integer GENERATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getGeneration() <em>Generation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeneration()
	 * @generated
	 * @ordered
	 */
	protected Integer generation = GENERATION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EclipsePackage.Literals.UNIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Update getUpdate() {
		return update;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUpdate(Update newUpdate, NotificationChain msgs) {
		Update oldUpdate = update;
		update = newUpdate;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__UPDATE, oldUpdate, newUpdate);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUpdate(Update newUpdate) {
		if (newUpdate != update) {
			NotificationChain msgs = null;
			if (update != null)
				msgs = ((InternalEObject)update).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__UPDATE, null, msgs);
			if (newUpdate != null)
				msgs = ((InternalEObject)newUpdate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__UPDATE, null, msgs);
			msgs = basicSetUpdate(newUpdate, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__UPDATE, newUpdate, newUpdate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Artifacts getArtifacts() {
		return artifacts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetArtifacts(Artifacts newArtifacts, NotificationChain msgs) {
		Artifacts oldArtifacts = artifacts;
		artifacts = newArtifacts;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__ARTIFACTS, oldArtifacts, newArtifacts);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setArtifacts(Artifacts newArtifacts) {
		if (newArtifacts != artifacts) {
			NotificationChain msgs = null;
			if (artifacts != null)
				msgs = ((InternalEObject)artifacts).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__ARTIFACTS, null, msgs);
			if (newArtifacts != null)
				msgs = ((InternalEObject)newArtifacts).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__ARTIFACTS, null, msgs);
			msgs = basicSetArtifacts(newArtifacts, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__ARTIFACTS, newArtifacts, newArtifacts));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Requires getHostRequirements() {
		return hostRequirements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHostRequirements(Requires newHostRequirements, NotificationChain msgs) {
		Requires oldHostRequirements = hostRequirements;
		hostRequirements = newHostRequirements;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__HOST_REQUIREMENTS, oldHostRequirements, newHostRequirements);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setHostRequirements(Requires newHostRequirements) {
		if (newHostRequirements != hostRequirements) {
			NotificationChain msgs = null;
			if (hostRequirements != null)
				msgs = ((InternalEObject)hostRequirements).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__HOST_REQUIREMENTS, null, msgs);
			if (newHostRequirements != null)
				msgs = ((InternalEObject)newHostRequirements).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__HOST_REQUIREMENTS, null, msgs);
			msgs = basicSetHostRequirements(newHostRequirements, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__HOST_REQUIREMENTS, newHostRequirements, newHostRequirements));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Properties getProperties() {
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProperties(Properties newProperties, NotificationChain msgs) {
		Properties oldProperties = properties;
		properties = newProperties;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__PROPERTIES, oldProperties, newProperties);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProperties(Properties newProperties) {
		if (newProperties != properties) {
			NotificationChain msgs = null;
			if (properties != null)
				msgs = ((InternalEObject)properties).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__PROPERTIES, null, msgs);
			if (newProperties != null)
				msgs = ((InternalEObject)newProperties).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__PROPERTIES, null, msgs);
			msgs = basicSetProperties(newProperties, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__PROPERTIES, newProperties, newProperties));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Provides getProvides() {
		return provides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProvides(Provides newProvides, NotificationChain msgs) {
		Provides oldProvides = provides;
		provides = newProvides;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__PROVIDES, oldProvides, newProvides);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProvides(Provides newProvides) {
		if (newProvides != provides) {
			NotificationChain msgs = null;
			if (provides != null)
				msgs = ((InternalEObject)provides).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__PROVIDES, null, msgs);
			if (newProvides != null)
				msgs = ((InternalEObject)newProvides).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__PROVIDES, null, msgs);
			msgs = basicSetProvides(newProvides, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__PROVIDES, newProvides, newProvides));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Requires getRequires() {
		return requires;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRequires(Requires newRequires, NotificationChain msgs) {
		Requires oldRequires = requires;
		requires = newRequires;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__REQUIRES, oldRequires, newRequires);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRequires(Requires newRequires) {
		if (newRequires != requires) {
			NotificationChain msgs = null;
			if (requires != null)
				msgs = ((InternalEObject)requires).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__REQUIRES, null, msgs);
			if (newRequires != null)
				msgs = ((InternalEObject)newRequires).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__REQUIRES, null, msgs);
			msgs = basicSetRequires(newRequires, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__REQUIRES, newRequires, newRequires));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFilter() {
		return filter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFilter(String newFilter) {
		String oldFilter = filter;
		filter = newFilter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__FILTER, oldFilter, filter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Touchpoint getTouchpoint() {
		return touchpoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTouchpoint(Touchpoint newTouchpoint, NotificationChain msgs) {
		Touchpoint oldTouchpoint = touchpoint;
		touchpoint = newTouchpoint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__TOUCHPOINT, oldTouchpoint, newTouchpoint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTouchpoint(Touchpoint newTouchpoint) {
		if (newTouchpoint != touchpoint) {
			NotificationChain msgs = null;
			if (touchpoint != null)
				msgs = ((InternalEObject)touchpoint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__TOUCHPOINT, null, msgs);
			if (newTouchpoint != null)
				msgs = ((InternalEObject)newTouchpoint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__TOUCHPOINT, null, msgs);
			msgs = basicSetTouchpoint(newTouchpoint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__TOUCHPOINT, newTouchpoint, newTouchpoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TouchpointDataType getTouchpointData() {
		return touchpointData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTouchpointData(TouchpointDataType newTouchpointData, NotificationChain msgs) {
		TouchpointDataType oldTouchpointData = touchpointData;
		touchpointData = newTouchpointData;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__TOUCHPOINT_DATA, oldTouchpointData, newTouchpointData);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTouchpointData(TouchpointDataType newTouchpointData) {
		if (newTouchpointData != touchpointData) {
			NotificationChain msgs = null;
			if (touchpointData != null)
				msgs = ((InternalEObject)touchpointData).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__TOUCHPOINT_DATA, null, msgs);
			if (newTouchpointData != null)
				msgs = ((InternalEObject)newTouchpointData).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__TOUCHPOINT_DATA, null, msgs);
			msgs = basicSetTouchpointData(newTouchpointData, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__TOUCHPOINT_DATA, newTouchpointData, newTouchpointData));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LicensesType getLicenses() {
		return licenses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLicenses(LicensesType newLicenses, NotificationChain msgs) {
		LicensesType oldLicenses = licenses;
		licenses = newLicenses;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__LICENSES, oldLicenses, newLicenses);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLicenses(LicensesType newLicenses) {
		if (newLicenses != licenses) {
			NotificationChain msgs = null;
			if (licenses != null)
				msgs = ((InternalEObject)licenses).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__LICENSES, null, msgs);
			if (newLicenses != null)
				msgs = ((InternalEObject)newLicenses).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__LICENSES, null, msgs);
			msgs = basicSetLicenses(newLicenses, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__LICENSES, newLicenses, newLicenses));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Description getCopyright() {
		return copyright;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCopyright(Description newCopyright, NotificationChain msgs) {
		Description oldCopyright = copyright;
		copyright = newCopyright;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__COPYRIGHT, oldCopyright, newCopyright);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCopyright(Description newCopyright) {
		if (newCopyright != copyright) {
			NotificationChain msgs = null;
			if (copyright != null)
				msgs = ((InternalEObject)copyright).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__COPYRIGHT, null, msgs);
			if (newCopyright != null)
				msgs = ((InternalEObject)newCopyright).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.UNIT__COPYRIGHT, null, msgs);
			msgs = basicSetCopyright(newCopyright, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__COPYRIGHT, newCopyright, newCopyright));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSingleton() {
		return singleton;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSingleton(boolean newSingleton) {
		boolean oldSingleton = singleton;
		singleton = newSingleton;
		boolean oldSingletonESet = singletonESet;
		singletonESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__SINGLETON, oldSingleton, singleton, !oldSingletonESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetSingleton() {
		boolean oldSingleton = singleton;
		boolean oldSingletonESet = singletonESet;
		singleton = SINGLETON_EDEFAULT;
		singletonESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EclipsePackage.UNIT__SINGLETON, oldSingleton, SINGLETON_EDEFAULT, oldSingletonESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetSingleton() {
		return singletonESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Integer getGeneration() {
		return generation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGeneration(Integer newGeneration) {
		Integer oldGeneration = generation;
		generation = newGeneration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.UNIT__GENERATION, oldGeneration, generation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EclipsePackage.UNIT__UPDATE:
				return basicSetUpdate(null, msgs);
			case EclipsePackage.UNIT__ARTIFACTS:
				return basicSetArtifacts(null, msgs);
			case EclipsePackage.UNIT__HOST_REQUIREMENTS:
				return basicSetHostRequirements(null, msgs);
			case EclipsePackage.UNIT__PROPERTIES:
				return basicSetProperties(null, msgs);
			case EclipsePackage.UNIT__PROVIDES:
				return basicSetProvides(null, msgs);
			case EclipsePackage.UNIT__REQUIRES:
				return basicSetRequires(null, msgs);
			case EclipsePackage.UNIT__TOUCHPOINT:
				return basicSetTouchpoint(null, msgs);
			case EclipsePackage.UNIT__TOUCHPOINT_DATA:
				return basicSetTouchpointData(null, msgs);
			case EclipsePackage.UNIT__LICENSES:
				return basicSetLicenses(null, msgs);
			case EclipsePackage.UNIT__COPYRIGHT:
				return basicSetCopyright(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EclipsePackage.UNIT__UPDATE:
				return getUpdate();
			case EclipsePackage.UNIT__ARTIFACTS:
				return getArtifacts();
			case EclipsePackage.UNIT__HOST_REQUIREMENTS:
				return getHostRequirements();
			case EclipsePackage.UNIT__PROPERTIES:
				return getProperties();
			case EclipsePackage.UNIT__PROVIDES:
				return getProvides();
			case EclipsePackage.UNIT__REQUIRES:
				return getRequires();
			case EclipsePackage.UNIT__FILTER:
				return getFilter();
			case EclipsePackage.UNIT__TOUCHPOINT:
				return getTouchpoint();
			case EclipsePackage.UNIT__TOUCHPOINT_DATA:
				return getTouchpointData();
			case EclipsePackage.UNIT__LICENSES:
				return getLicenses();
			case EclipsePackage.UNIT__COPYRIGHT:
				return getCopyright();
			case EclipsePackage.UNIT__ID:
				return getId();
			case EclipsePackage.UNIT__SINGLETON:
				return isSingleton();
			case EclipsePackage.UNIT__VERSION:
				return getVersion();
			case EclipsePackage.UNIT__GENERATION:
				return getGeneration();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EclipsePackage.UNIT__UPDATE:
				setUpdate((Update)newValue);
				return;
			case EclipsePackage.UNIT__ARTIFACTS:
				setArtifacts((Artifacts)newValue);
				return;
			case EclipsePackage.UNIT__HOST_REQUIREMENTS:
				setHostRequirements((Requires)newValue);
				return;
			case EclipsePackage.UNIT__PROPERTIES:
				setProperties((Properties)newValue);
				return;
			case EclipsePackage.UNIT__PROVIDES:
				setProvides((Provides)newValue);
				return;
			case EclipsePackage.UNIT__REQUIRES:
				setRequires((Requires)newValue);
				return;
			case EclipsePackage.UNIT__FILTER:
				setFilter((String)newValue);
				return;
			case EclipsePackage.UNIT__TOUCHPOINT:
				setTouchpoint((Touchpoint)newValue);
				return;
			case EclipsePackage.UNIT__TOUCHPOINT_DATA:
				setTouchpointData((TouchpointDataType)newValue);
				return;
			case EclipsePackage.UNIT__LICENSES:
				setLicenses((LicensesType)newValue);
				return;
			case EclipsePackage.UNIT__COPYRIGHT:
				setCopyright((Description)newValue);
				return;
			case EclipsePackage.UNIT__ID:
				setId((String)newValue);
				return;
			case EclipsePackage.UNIT__SINGLETON:
				setSingleton((Boolean)newValue);
				return;
			case EclipsePackage.UNIT__VERSION:
				setVersion((String)newValue);
				return;
			case EclipsePackage.UNIT__GENERATION:
				setGeneration((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EclipsePackage.UNIT__UPDATE:
				setUpdate((Update)null);
				return;
			case EclipsePackage.UNIT__ARTIFACTS:
				setArtifacts((Artifacts)null);
				return;
			case EclipsePackage.UNIT__HOST_REQUIREMENTS:
				setHostRequirements((Requires)null);
				return;
			case EclipsePackage.UNIT__PROPERTIES:
				setProperties((Properties)null);
				return;
			case EclipsePackage.UNIT__PROVIDES:
				setProvides((Provides)null);
				return;
			case EclipsePackage.UNIT__REQUIRES:
				setRequires((Requires)null);
				return;
			case EclipsePackage.UNIT__FILTER:
				setFilter(FILTER_EDEFAULT);
				return;
			case EclipsePackage.UNIT__TOUCHPOINT:
				setTouchpoint((Touchpoint)null);
				return;
			case EclipsePackage.UNIT__TOUCHPOINT_DATA:
				setTouchpointData((TouchpointDataType)null);
				return;
			case EclipsePackage.UNIT__LICENSES:
				setLicenses((LicensesType)null);
				return;
			case EclipsePackage.UNIT__COPYRIGHT:
				setCopyright((Description)null);
				return;
			case EclipsePackage.UNIT__ID:
				setId(ID_EDEFAULT);
				return;
			case EclipsePackage.UNIT__SINGLETON:
				unsetSingleton();
				return;
			case EclipsePackage.UNIT__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
			case EclipsePackage.UNIT__GENERATION:
				setGeneration(GENERATION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EclipsePackage.UNIT__UPDATE:
				return update != null;
			case EclipsePackage.UNIT__ARTIFACTS:
				return artifacts != null;
			case EclipsePackage.UNIT__HOST_REQUIREMENTS:
				return hostRequirements != null;
			case EclipsePackage.UNIT__PROPERTIES:
				return properties != null;
			case EclipsePackage.UNIT__PROVIDES:
				return provides != null;
			case EclipsePackage.UNIT__REQUIRES:
				return requires != null;
			case EclipsePackage.UNIT__FILTER:
				return FILTER_EDEFAULT == null ? filter != null : !FILTER_EDEFAULT.equals(filter);
			case EclipsePackage.UNIT__TOUCHPOINT:
				return touchpoint != null;
			case EclipsePackage.UNIT__TOUCHPOINT_DATA:
				return touchpointData != null;
			case EclipsePackage.UNIT__LICENSES:
				return licenses != null;
			case EclipsePackage.UNIT__COPYRIGHT:
				return copyright != null;
			case EclipsePackage.UNIT__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case EclipsePackage.UNIT__SINGLETON:
				return isSetSingleton();
			case EclipsePackage.UNIT__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
			case EclipsePackage.UNIT__GENERATION:
				return GENERATION_EDEFAULT == null ? generation != null : !GENERATION_EDEFAULT.equals(generation);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (filter: ");
		result.append(filter);
		result.append(", id: ");
		result.append(id);
		result.append(", singleton: ");
		if (singletonESet) result.append(singleton); else result.append("<unset>");
		result.append(", version: ");
		result.append(version);
		result.append(", generation: ");
		result.append(generation);
		result.append(')');
		return result.toString();
	}

} //UnitImpl
