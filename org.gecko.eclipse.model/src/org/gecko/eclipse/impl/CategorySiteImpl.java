/**
 */
package org.gecko.eclipse.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.gecko.eclipse.CategoryDef;
import org.gecko.eclipse.CategoryFeature;
import org.gecko.eclipse.CategorySite;
import org.gecko.eclipse.EclipsePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Category Site</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.impl.CategorySiteImpl#getFeature <em>Feature</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.CategorySiteImpl#getCategoryDef <em>Category Def</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CategorySiteImpl extends MinimalEObjectImpl.Container implements CategorySite {
	/**
	 * The cached value of the '{@link #getFeature() <em>Feature</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeature()
	 * @generated
	 * @ordered
	 */
	protected EList<CategoryFeature> feature;

	/**
	 * The cached value of the '{@link #getCategoryDef() <em>Category Def</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategoryDef()
	 * @generated
	 * @ordered
	 */
	protected EList<CategoryDef> categoryDef;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CategorySiteImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EclipsePackage.Literals.CATEGORY_SITE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<CategoryFeature> getFeature() {
		if (feature == null) {
			feature = new EObjectContainmentEList<CategoryFeature>(CategoryFeature.class, this, EclipsePackage.CATEGORY_SITE__FEATURE);
		}
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<CategoryDef> getCategoryDef() {
		if (categoryDef == null) {
			categoryDef = new EObjectContainmentEList<CategoryDef>(CategoryDef.class, this, EclipsePackage.CATEGORY_SITE__CATEGORY_DEF);
		}
		return categoryDef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EclipsePackage.CATEGORY_SITE__FEATURE:
				return ((InternalEList<?>)getFeature()).basicRemove(otherEnd, msgs);
			case EclipsePackage.CATEGORY_SITE__CATEGORY_DEF:
				return ((InternalEList<?>)getCategoryDef()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EclipsePackage.CATEGORY_SITE__FEATURE:
				return getFeature();
			case EclipsePackage.CATEGORY_SITE__CATEGORY_DEF:
				return getCategoryDef();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EclipsePackage.CATEGORY_SITE__FEATURE:
				getFeature().clear();
				getFeature().addAll((Collection<? extends CategoryFeature>)newValue);
				return;
			case EclipsePackage.CATEGORY_SITE__CATEGORY_DEF:
				getCategoryDef().clear();
				getCategoryDef().addAll((Collection<? extends CategoryDef>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EclipsePackage.CATEGORY_SITE__FEATURE:
				getFeature().clear();
				return;
			case EclipsePackage.CATEGORY_SITE__CATEGORY_DEF:
				getCategoryDef().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EclipsePackage.CATEGORY_SITE__FEATURE:
				return feature != null && !feature.isEmpty();
			case EclipsePackage.CATEGORY_SITE__CATEGORY_DEF:
				return categoryDef != null && !categoryDef.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CategorySiteImpl
