/**
 */
package org.gecko.eclipse.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import org.gecko.eclipse.Artifact;
import org.gecko.eclipse.Artifacts;
import org.gecko.eclipse.CategoryDef;
import org.gecko.eclipse.CategoryFeature;
import org.gecko.eclipse.CategorySite;
import org.gecko.eclipse.CategoryType;
import org.gecko.eclipse.ChildrenType;
import org.gecko.eclipse.Description;
import org.gecko.eclipse.DocumentRoot;
import org.gecko.eclipse.EclipseFactory;
import org.gecko.eclipse.EclipsePackage;
import org.gecko.eclipse.Feature;
import org.gecko.eclipse.Import;
import org.gecko.eclipse.ImportRequires;
import org.gecko.eclipse.Include;
import org.gecko.eclipse.InstallHandler;
import org.gecko.eclipse.Instruction;
import org.gecko.eclipse.InstructionsType;
import org.gecko.eclipse.LauncherArgs;
import org.gecko.eclipse.LicensesType;
import org.gecko.eclipse.LocationIncludeType;
import org.gecko.eclipse.MappingsType;
import org.gecko.eclipse.MatchType;
import org.gecko.eclipse.Plugin;
import org.gecko.eclipse.ProcessStep;
import org.gecko.eclipse.Processing;
import org.gecko.eclipse.Properties;
import org.gecko.eclipse.Property;
import org.gecko.eclipse.Provided;
import org.gecko.eclipse.Provides;
import org.gecko.eclipse.ReferencesType;
import org.gecko.eclipse.Repository;
import org.gecko.eclipse.RepositoryRef;
import org.gecko.eclipse.RepositoryRefs;
import org.gecko.eclipse.Required;
import org.gecko.eclipse.RequiredProperties;
import org.gecko.eclipse.Requires;
import org.gecko.eclipse.Rule;
import org.gecko.eclipse.Site;
import org.gecko.eclipse.Target;
import org.gecko.eclipse.TargetDependencies;
import org.gecko.eclipse.TargetEnvironment;
import org.gecko.eclipse.TargetJRE;
import org.gecko.eclipse.TargetLocation;
import org.gecko.eclipse.TargetLocations;
import org.gecko.eclipse.TargetType;
import org.gecko.eclipse.Touchpoint;
import org.gecko.eclipse.TouchpointData;
import org.gecko.eclipse.TouchpointDataType;
import org.gecko.eclipse.Unit;
import org.gecko.eclipse.UnitsType;
import org.gecko.eclipse.Update;
import org.gecko.eclipse.UrlType;

import org.gecko.eclipse.util.EclipseValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EclipsePackageImpl extends EPackageImpl implements EclipsePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass artifactEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass artifactsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass categoryDefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass categoryFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass categorySiteEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass categoryTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass childrenTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass descriptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass documentRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass importEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass importRequiresEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass includeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass installHandlerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass instructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass instructionsTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass launcherArgsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass licensesTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mappingsTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pluginEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processStepEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertiesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass providedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass providesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referencesTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass repositoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass repositoryRefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass repositoryRefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requiredEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requiresEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass siteEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass targetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass targetDependenciesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass targetEnvironmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass targetJREEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass targetLocationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass targetLocationsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass touchpointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass touchpointDataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass touchpointDataTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unitsTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass updateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass urlTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requiredPropertiesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum locationIncludeTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum matchTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum targetTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType locationIncludeTypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType matchTypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType targetTypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType versionEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.gecko.eclipse.EclipsePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private EclipsePackageImpl() {
		super(eNS_URI, EclipseFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link EclipsePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static EclipsePackage init() {
		if (isInited) return (EclipsePackage)EPackage.Registry.INSTANCE.getEPackage(EclipsePackage.eNS_URI);

		// Obtain or create and register package
		Object registeredEclipsePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		EclipsePackageImpl theEclipsePackage = registeredEclipsePackage instanceof EclipsePackageImpl ? (EclipsePackageImpl)registeredEclipsePackage : new EclipsePackageImpl();

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theEclipsePackage.createPackageContents();

		// Initialize created meta-data
		theEclipsePackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theEclipsePackage,
			 new EValidator.Descriptor() {
				 @Override
				 public EValidator getEValidator() {
					 return EclipseValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theEclipsePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(EclipsePackage.eNS_URI, theEclipsePackage);
		return theEclipsePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getArtifact() {
		return artifactEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArtifact_Processing() {
		return (EReference)artifactEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArtifact_Properties() {
		return (EReference)artifactEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArtifact_RepositoryProperties() {
		return (EReference)artifactEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifact_Classifier() {
		return (EAttribute)artifactEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifact_Id() {
		return (EAttribute)artifactEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifact_Version() {
		return (EAttribute)artifactEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getArtifacts() {
		return artifactsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArtifacts_Artifact() {
		return (EReference)artifactsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getArtifacts_Size() {
		return (EAttribute)artifactsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCategoryDef() {
		return categoryDefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCategoryDef_Description() {
		return (EAttribute)categoryDefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCategoryDef_Label() {
		return (EAttribute)categoryDefEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCategoryDef_Name() {
		return (EAttribute)categoryDefEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCategoryFeature() {
		return categoryFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCategoryFeature_Category() {
		return (EReference)categoryFeatureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCategoryFeature_Id() {
		return (EAttribute)categoryFeatureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCategoryFeature_Version() {
		return (EAttribute)categoryFeatureEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCategorySite() {
		return categorySiteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCategorySite_Feature() {
		return (EReference)categorySiteEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCategorySite_CategoryDef() {
		return (EReference)categorySiteEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCategoryType() {
		return categoryTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCategoryType_Name() {
		return (EAttribute)categoryTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getChildrenType() {
		return childrenTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getChildrenType_Child() {
		return (EReference)childrenTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getChildrenType_Size() {
		return (EAttribute)childrenTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDescription() {
		return descriptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDescription_Value() {
		return (EAttribute)descriptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDescription_Url() {
		return (EAttribute)descriptionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDocumentRoot() {
		return documentRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDocumentRoot_Mixed() {
		return (EAttribute)documentRootEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_XMLNSPrefixMap() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_XSISchemaLocation() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_Feature() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_Repository() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_Site() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_Target() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFeature() {
		return featureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeature_InstallHandler() {
		return (EReference)featureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeature_Description() {
		return (EReference)featureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeature_Copyright() {
		return (EReference)featureEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeature_License() {
		return (EReference)featureEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeature_Url() {
		return (EReference)featureEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeature_Includes() {
		return (EReference)featureEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeature_Requires() {
		return (EReference)featureEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeature_Plugin() {
		return (EReference)featureEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeature_Arch() {
		return (EAttribute)featureEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeature_ColocationAffinity() {
		return (EAttribute)featureEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeature_Exclusive() {
		return (EAttribute)featureEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeature_Id() {
		return (EAttribute)featureEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeature_Image() {
		return (EAttribute)featureEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeature_Label() {
		return (EAttribute)featureEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeature_Nl() {
		return (EAttribute)featureEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeature_Os() {
		return (EAttribute)featureEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeature_Plugin1() {
		return (EAttribute)featureEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeature_ProviderName() {
		return (EAttribute)featureEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeature_Version() {
		return (EAttribute)featureEClass.getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeature_Ws() {
		return (EAttribute)featureEClass.getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getImport() {
		return importEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getImport_Feature() {
		return (EAttribute)importEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getImport_Match() {
		return (EAttribute)importEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getImport_Patch() {
		return (EAttribute)importEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getImport_Plugin() {
		return (EAttribute)importEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getImport_Version() {
		return (EAttribute)importEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getImportRequires() {
		return importRequiresEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getImportRequires_Import() {
		return (EReference)importRequiresEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getInclude() {
		return includeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInclude_Arch() {
		return (EAttribute)includeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInclude_Id() {
		return (EAttribute)includeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInclude_Name() {
		return (EAttribute)includeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInclude_Nl() {
		return (EAttribute)includeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInclude_Optional() {
		return (EAttribute)includeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInclude_Os() {
		return (EAttribute)includeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInclude_SearchLocation() {
		return (EAttribute)includeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInclude_Version() {
		return (EAttribute)includeEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInclude_Ws() {
		return (EAttribute)includeEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getInstallHandler() {
		return installHandlerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInstallHandler_Handler() {
		return (EAttribute)installHandlerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInstallHandler_Library() {
		return (EAttribute)installHandlerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getInstruction() {
		return instructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInstruction_Value() {
		return (EAttribute)instructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInstruction_Key() {
		return (EAttribute)instructionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getInstructionsType() {
		return instructionsTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getInstructionsType_Instruction() {
		return (EReference)instructionsTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInstructionsType_Size() {
		return (EAttribute)instructionsTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLauncherArgs() {
		return launcherArgsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLauncherArgs_ProgramArgs() {
		return (EAttribute)launcherArgsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLauncherArgs_VmArgs() {
		return (EAttribute)launcherArgsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLicensesType() {
		return licensesTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLicensesType_License() {
		return (EReference)licensesTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLicensesType_Size() {
		return (EAttribute)licensesTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMappingsType() {
		return mappingsTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMappingsType_Rule() {
		return (EReference)mappingsTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMappingsType_Size() {
		return (EAttribute)mappingsTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPlugin() {
		return pluginEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPlugin_Arch() {
		return (EAttribute)pluginEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPlugin_DownloadSize() {
		return (EAttribute)pluginEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPlugin_Fragment() {
		return (EAttribute)pluginEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPlugin_Id() {
		return (EAttribute)pluginEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPlugin_InstallSize() {
		return (EAttribute)pluginEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPlugin_Nl() {
		return (EAttribute)pluginEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPlugin_Os() {
		return (EAttribute)pluginEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPlugin_Unpack() {
		return (EAttribute)pluginEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPlugin_Version() {
		return (EAttribute)pluginEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPlugin_Ws() {
		return (EAttribute)pluginEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getProcessing() {
		return processingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getProcessing_Step() {
		return (EReference)processingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getProcessing_Size() {
		return (EAttribute)processingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getProcessStep() {
		return processStepEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getProcessStep_Id() {
		return (EAttribute)processStepEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getProcessStep_Required() {
		return (EAttribute)processStepEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getProperties() {
		return propertiesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getProperties_Property() {
		return (EReference)propertiesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getProperties_Size() {
		return (EAttribute)propertiesEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getProperty() {
		return propertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getProperty_Name() {
		return (EAttribute)propertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getProperty_Value() {
		return (EAttribute)propertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getProvided() {
		return providedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getProvided_Name() {
		return (EAttribute)providedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getProvided_Namespace() {
		return (EAttribute)providedEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getProvided_Version() {
		return (EAttribute)providedEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getProvided_Properties() {
		return (EReference)providedEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getProvides() {
		return providesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getProvides_Provided() {
		return (EReference)providesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getProvides_Size() {
		return (EAttribute)providesEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getReferencesType() {
		return referencesTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getReferencesType_Repository() {
		return (EReference)referencesTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getReferencesType_Size() {
		return (EAttribute)referencesTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRepository() {
		return repositoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRepository_Properties() {
		return (EReference)repositoryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRepository_Mappings() {
		return (EReference)repositoryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRepository_References() {
		return (EReference)repositoryEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRepository_Units() {
		return (EReference)repositoryEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRepository_Children() {
		return (EReference)repositoryEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRepository_Artifacts() {
		return (EReference)repositoryEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRepository_Name() {
		return (EAttribute)repositoryEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRepository_Type() {
		return (EAttribute)repositoryEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRepository_Version() {
		return (EAttribute)repositoryEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRepositoryRef() {
		return repositoryRefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRepositoryRef_Location() {
		return (EAttribute)repositoryRefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRepositoryRefs() {
		return repositoryRefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRepositoryRefs_Options() {
		return (EAttribute)repositoryRefsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRepositoryRefs_Type() {
		return (EAttribute)repositoryRefsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRepositoryRefs_Uri() {
		return (EAttribute)repositoryRefsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRepositoryRefs_Url() {
		return (EAttribute)repositoryRefsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRequired() {
		return requiredEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRequired_Filter() {
		return (EAttribute)requiredEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRequired_Greedy() {
		return (EAttribute)requiredEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRequired_Multiple() {
		return (EAttribute)requiredEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRequired_Name() {
		return (EAttribute)requiredEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRequired_Namespace() {
		return (EAttribute)requiredEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRequired_Optional() {
		return (EAttribute)requiredEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRequired_Range() {
		return (EAttribute)requiredEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRequires() {
		return requiresEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRequires_Required() {
		return (EReference)requiresEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRequires_RequiredProperties() {
		return (EReference)requiresEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRequires_Size() {
		return (EAttribute)requiresEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRule() {
		return ruleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRule_Filter() {
		return (EAttribute)ruleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRule_Output() {
		return (EAttribute)ruleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSite() {
		return siteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSite_Label() {
		return (EAttribute)siteEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSite_Url() {
		return (EAttribute)siteEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTarget() {
		return targetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTarget_Locations() {
		return (EReference)targetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTarget_Environment() {
		return (EReference)targetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTarget_TargetJRE() {
		return (EReference)targetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTarget_LauncherArgs() {
		return (EReference)targetEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTarget_ImplicitDependencies() {
		return (EReference)targetEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTarget_Name() {
		return (EAttribute)targetEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTarget_SequenceNumber() {
		return (EAttribute)targetEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTargetDependencies() {
		return targetDependenciesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTargetDependencies_Plugin() {
		return (EReference)targetDependenciesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTargetEnvironment() {
		return targetEnvironmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTargetEnvironment_Arch() {
		return (EAttribute)targetEnvironmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTargetEnvironment_Os() {
		return (EAttribute)targetEnvironmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTargetEnvironment_Ws() {
		return (EAttribute)targetEnvironmentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTargetEnvironment_Nl() {
		return (EAttribute)targetEnvironmentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTargetJRE() {
		return targetJREEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTargetJRE_Path() {
		return (EAttribute)targetJREEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTargetLocation() {
		return targetLocationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTargetLocation_Repository() {
		return (EReference)targetLocationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTargetLocation_Unit() {
		return (EReference)targetLocationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTargetLocation_Id() {
		return (EAttribute)targetLocationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTargetLocation_IncludeAllPlatforms() {
		return (EAttribute)targetLocationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTargetLocation_IncludeConfigurePhase() {
		return (EAttribute)targetLocationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTargetLocation_IncludeMode() {
		return (EAttribute)targetLocationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTargetLocation_IncludeSource() {
		return (EAttribute)targetLocationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTargetLocation_Path() {
		return (EAttribute)targetLocationEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTargetLocation_Type() {
		return (EAttribute)targetLocationEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTargetLocations() {
		return targetLocationsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTargetLocations_Location() {
		return (EReference)targetLocationsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTouchpoint() {
		return touchpointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTouchpoint_Id() {
		return (EAttribute)touchpointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTouchpoint_Version() {
		return (EAttribute)touchpointEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTouchpointData() {
		return touchpointDataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTouchpointData_Id() {
		return (EAttribute)touchpointDataEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTouchpointData_Version() {
		return (EAttribute)touchpointDataEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTouchpointDataType() {
		return touchpointDataTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTouchpointDataType_Instructions() {
		return (EReference)touchpointDataTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTouchpointDataType_Size() {
		return (EAttribute)touchpointDataTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUnit() {
		return unitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUnit_Update() {
		return (EReference)unitEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUnit_Artifacts() {
		return (EReference)unitEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUnit_HostRequirements() {
		return (EReference)unitEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUnit_Properties() {
		return (EReference)unitEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUnit_Provides() {
		return (EReference)unitEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUnit_Requires() {
		return (EReference)unitEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUnit_Filter() {
		return (EAttribute)unitEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUnit_Touchpoint() {
		return (EReference)unitEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUnit_TouchpointData() {
		return (EReference)unitEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUnit_Licenses() {
		return (EReference)unitEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUnit_Copyright() {
		return (EReference)unitEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUnit_Id() {
		return (EAttribute)unitEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUnit_Singleton() {
		return (EAttribute)unitEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUnit_Version() {
		return (EAttribute)unitEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUnit_Generation() {
		return (EAttribute)unitEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUnitsType() {
		return unitsTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUnitsType_Unit() {
		return (EReference)unitsTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUnitsType_Size() {
		return (EAttribute)unitsTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUpdate() {
		return updateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUpdate_Id() {
		return (EAttribute)updateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUpdate_Range() {
		return (EAttribute)updateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUpdate_Severity() {
		return (EAttribute)updateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUrlType() {
		return urlTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUrlType_Update() {
		return (EReference)urlTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUrlType_Discovery() {
		return (EReference)urlTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRequiredProperties() {
		return requiredPropertiesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRequiredProperties_Match() {
		return (EAttribute)requiredPropertiesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRequiredProperties_Namespace() {
		return (EAttribute)requiredPropertiesEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getLocationIncludeType() {
		return locationIncludeTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getMatchType() {
		return matchTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getTargetType() {
		return targetTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getLocationIncludeTypeObject() {
		return locationIncludeTypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getMatchTypeObject() {
		return matchTypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getTargetTypeObject() {
		return targetTypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getVersion() {
		return versionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EclipseFactory getEclipseFactory() {
		return (EclipseFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		artifactEClass = createEClass(ARTIFACT);
		createEReference(artifactEClass, ARTIFACT__PROCESSING);
		createEReference(artifactEClass, ARTIFACT__PROPERTIES);
		createEReference(artifactEClass, ARTIFACT__REPOSITORY_PROPERTIES);
		createEAttribute(artifactEClass, ARTIFACT__CLASSIFIER);
		createEAttribute(artifactEClass, ARTIFACT__ID);
		createEAttribute(artifactEClass, ARTIFACT__VERSION);

		artifactsEClass = createEClass(ARTIFACTS);
		createEReference(artifactsEClass, ARTIFACTS__ARTIFACT);
		createEAttribute(artifactsEClass, ARTIFACTS__SIZE);

		categoryDefEClass = createEClass(CATEGORY_DEF);
		createEAttribute(categoryDefEClass, CATEGORY_DEF__DESCRIPTION);
		createEAttribute(categoryDefEClass, CATEGORY_DEF__LABEL);
		createEAttribute(categoryDefEClass, CATEGORY_DEF__NAME);

		categoryFeatureEClass = createEClass(CATEGORY_FEATURE);
		createEReference(categoryFeatureEClass, CATEGORY_FEATURE__CATEGORY);
		createEAttribute(categoryFeatureEClass, CATEGORY_FEATURE__ID);
		createEAttribute(categoryFeatureEClass, CATEGORY_FEATURE__VERSION);

		categorySiteEClass = createEClass(CATEGORY_SITE);
		createEReference(categorySiteEClass, CATEGORY_SITE__FEATURE);
		createEReference(categorySiteEClass, CATEGORY_SITE__CATEGORY_DEF);

		categoryTypeEClass = createEClass(CATEGORY_TYPE);
		createEAttribute(categoryTypeEClass, CATEGORY_TYPE__NAME);

		childrenTypeEClass = createEClass(CHILDREN_TYPE);
		createEReference(childrenTypeEClass, CHILDREN_TYPE__CHILD);
		createEAttribute(childrenTypeEClass, CHILDREN_TYPE__SIZE);

		descriptionEClass = createEClass(DESCRIPTION);
		createEAttribute(descriptionEClass, DESCRIPTION__VALUE);
		createEAttribute(descriptionEClass, DESCRIPTION__URL);

		documentRootEClass = createEClass(DOCUMENT_ROOT);
		createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		createEReference(documentRootEClass, DOCUMENT_ROOT__FEATURE);
		createEReference(documentRootEClass, DOCUMENT_ROOT__REPOSITORY);
		createEReference(documentRootEClass, DOCUMENT_ROOT__SITE);
		createEReference(documentRootEClass, DOCUMENT_ROOT__TARGET);

		featureEClass = createEClass(FEATURE);
		createEReference(featureEClass, FEATURE__INSTALL_HANDLER);
		createEReference(featureEClass, FEATURE__DESCRIPTION);
		createEReference(featureEClass, FEATURE__COPYRIGHT);
		createEReference(featureEClass, FEATURE__LICENSE);
		createEReference(featureEClass, FEATURE__URL);
		createEReference(featureEClass, FEATURE__INCLUDES);
		createEReference(featureEClass, FEATURE__REQUIRES);
		createEReference(featureEClass, FEATURE__PLUGIN);
		createEAttribute(featureEClass, FEATURE__ARCH);
		createEAttribute(featureEClass, FEATURE__COLOCATION_AFFINITY);
		createEAttribute(featureEClass, FEATURE__EXCLUSIVE);
		createEAttribute(featureEClass, FEATURE__ID);
		createEAttribute(featureEClass, FEATURE__IMAGE);
		createEAttribute(featureEClass, FEATURE__LABEL);
		createEAttribute(featureEClass, FEATURE__NL);
		createEAttribute(featureEClass, FEATURE__OS);
		createEAttribute(featureEClass, FEATURE__PLUGIN1);
		createEAttribute(featureEClass, FEATURE__PROVIDER_NAME);
		createEAttribute(featureEClass, FEATURE__VERSION);
		createEAttribute(featureEClass, FEATURE__WS);

		importEClass = createEClass(IMPORT);
		createEAttribute(importEClass, IMPORT__FEATURE);
		createEAttribute(importEClass, IMPORT__MATCH);
		createEAttribute(importEClass, IMPORT__PATCH);
		createEAttribute(importEClass, IMPORT__PLUGIN);
		createEAttribute(importEClass, IMPORT__VERSION);

		importRequiresEClass = createEClass(IMPORT_REQUIRES);
		createEReference(importRequiresEClass, IMPORT_REQUIRES__IMPORT);

		includeEClass = createEClass(INCLUDE);
		createEAttribute(includeEClass, INCLUDE__ARCH);
		createEAttribute(includeEClass, INCLUDE__ID);
		createEAttribute(includeEClass, INCLUDE__NAME);
		createEAttribute(includeEClass, INCLUDE__NL);
		createEAttribute(includeEClass, INCLUDE__OPTIONAL);
		createEAttribute(includeEClass, INCLUDE__OS);
		createEAttribute(includeEClass, INCLUDE__SEARCH_LOCATION);
		createEAttribute(includeEClass, INCLUDE__VERSION);
		createEAttribute(includeEClass, INCLUDE__WS);

		installHandlerEClass = createEClass(INSTALL_HANDLER);
		createEAttribute(installHandlerEClass, INSTALL_HANDLER__HANDLER);
		createEAttribute(installHandlerEClass, INSTALL_HANDLER__LIBRARY);

		instructionEClass = createEClass(INSTRUCTION);
		createEAttribute(instructionEClass, INSTRUCTION__VALUE);
		createEAttribute(instructionEClass, INSTRUCTION__KEY);

		instructionsTypeEClass = createEClass(INSTRUCTIONS_TYPE);
		createEReference(instructionsTypeEClass, INSTRUCTIONS_TYPE__INSTRUCTION);
		createEAttribute(instructionsTypeEClass, INSTRUCTIONS_TYPE__SIZE);

		launcherArgsEClass = createEClass(LAUNCHER_ARGS);
		createEAttribute(launcherArgsEClass, LAUNCHER_ARGS__PROGRAM_ARGS);
		createEAttribute(launcherArgsEClass, LAUNCHER_ARGS__VM_ARGS);

		licensesTypeEClass = createEClass(LICENSES_TYPE);
		createEReference(licensesTypeEClass, LICENSES_TYPE__LICENSE);
		createEAttribute(licensesTypeEClass, LICENSES_TYPE__SIZE);

		mappingsTypeEClass = createEClass(MAPPINGS_TYPE);
		createEReference(mappingsTypeEClass, MAPPINGS_TYPE__RULE);
		createEAttribute(mappingsTypeEClass, MAPPINGS_TYPE__SIZE);

		pluginEClass = createEClass(PLUGIN);
		createEAttribute(pluginEClass, PLUGIN__ARCH);
		createEAttribute(pluginEClass, PLUGIN__DOWNLOAD_SIZE);
		createEAttribute(pluginEClass, PLUGIN__FRAGMENT);
		createEAttribute(pluginEClass, PLUGIN__ID);
		createEAttribute(pluginEClass, PLUGIN__INSTALL_SIZE);
		createEAttribute(pluginEClass, PLUGIN__NL);
		createEAttribute(pluginEClass, PLUGIN__OS);
		createEAttribute(pluginEClass, PLUGIN__UNPACK);
		createEAttribute(pluginEClass, PLUGIN__VERSION);
		createEAttribute(pluginEClass, PLUGIN__WS);

		processingEClass = createEClass(PROCESSING);
		createEReference(processingEClass, PROCESSING__STEP);
		createEAttribute(processingEClass, PROCESSING__SIZE);

		processStepEClass = createEClass(PROCESS_STEP);
		createEAttribute(processStepEClass, PROCESS_STEP__ID);
		createEAttribute(processStepEClass, PROCESS_STEP__REQUIRED);

		propertiesEClass = createEClass(PROPERTIES);
		createEReference(propertiesEClass, PROPERTIES__PROPERTY);
		createEAttribute(propertiesEClass, PROPERTIES__SIZE);

		propertyEClass = createEClass(PROPERTY);
		createEAttribute(propertyEClass, PROPERTY__NAME);
		createEAttribute(propertyEClass, PROPERTY__VALUE);

		providedEClass = createEClass(PROVIDED);
		createEAttribute(providedEClass, PROVIDED__NAME);
		createEAttribute(providedEClass, PROVIDED__NAMESPACE);
		createEAttribute(providedEClass, PROVIDED__VERSION);
		createEReference(providedEClass, PROVIDED__PROPERTIES);

		providesEClass = createEClass(PROVIDES);
		createEReference(providesEClass, PROVIDES__PROVIDED);
		createEAttribute(providesEClass, PROVIDES__SIZE);

		referencesTypeEClass = createEClass(REFERENCES_TYPE);
		createEReference(referencesTypeEClass, REFERENCES_TYPE__REPOSITORY);
		createEAttribute(referencesTypeEClass, REFERENCES_TYPE__SIZE);

		repositoryEClass = createEClass(REPOSITORY);
		createEReference(repositoryEClass, REPOSITORY__PROPERTIES);
		createEReference(repositoryEClass, REPOSITORY__MAPPINGS);
		createEReference(repositoryEClass, REPOSITORY__REFERENCES);
		createEReference(repositoryEClass, REPOSITORY__UNITS);
		createEReference(repositoryEClass, REPOSITORY__CHILDREN);
		createEReference(repositoryEClass, REPOSITORY__ARTIFACTS);
		createEAttribute(repositoryEClass, REPOSITORY__NAME);
		createEAttribute(repositoryEClass, REPOSITORY__TYPE);
		createEAttribute(repositoryEClass, REPOSITORY__VERSION);

		repositoryRefEClass = createEClass(REPOSITORY_REF);
		createEAttribute(repositoryRefEClass, REPOSITORY_REF__LOCATION);

		repositoryRefsEClass = createEClass(REPOSITORY_REFS);
		createEAttribute(repositoryRefsEClass, REPOSITORY_REFS__OPTIONS);
		createEAttribute(repositoryRefsEClass, REPOSITORY_REFS__TYPE);
		createEAttribute(repositoryRefsEClass, REPOSITORY_REFS__URI);
		createEAttribute(repositoryRefsEClass, REPOSITORY_REFS__URL);

		requiredEClass = createEClass(REQUIRED);
		createEAttribute(requiredEClass, REQUIRED__FILTER);
		createEAttribute(requiredEClass, REQUIRED__GREEDY);
		createEAttribute(requiredEClass, REQUIRED__MULTIPLE);
		createEAttribute(requiredEClass, REQUIRED__NAME);
		createEAttribute(requiredEClass, REQUIRED__NAMESPACE);
		createEAttribute(requiredEClass, REQUIRED__OPTIONAL);
		createEAttribute(requiredEClass, REQUIRED__RANGE);

		requiredPropertiesEClass = createEClass(REQUIRED_PROPERTIES);
		createEAttribute(requiredPropertiesEClass, REQUIRED_PROPERTIES__MATCH);
		createEAttribute(requiredPropertiesEClass, REQUIRED_PROPERTIES__NAMESPACE);

		requiresEClass = createEClass(REQUIRES);
		createEReference(requiresEClass, REQUIRES__REQUIRED);
		createEReference(requiresEClass, REQUIRES__REQUIRED_PROPERTIES);
		createEAttribute(requiresEClass, REQUIRES__SIZE);

		ruleEClass = createEClass(RULE);
		createEAttribute(ruleEClass, RULE__FILTER);
		createEAttribute(ruleEClass, RULE__OUTPUT);

		siteEClass = createEClass(SITE);
		createEAttribute(siteEClass, SITE__LABEL);
		createEAttribute(siteEClass, SITE__URL);

		targetEClass = createEClass(TARGET);
		createEReference(targetEClass, TARGET__LOCATIONS);
		createEReference(targetEClass, TARGET__ENVIRONMENT);
		createEReference(targetEClass, TARGET__TARGET_JRE);
		createEReference(targetEClass, TARGET__LAUNCHER_ARGS);
		createEReference(targetEClass, TARGET__IMPLICIT_DEPENDENCIES);
		createEAttribute(targetEClass, TARGET__NAME);
		createEAttribute(targetEClass, TARGET__SEQUENCE_NUMBER);

		targetDependenciesEClass = createEClass(TARGET_DEPENDENCIES);
		createEReference(targetDependenciesEClass, TARGET_DEPENDENCIES__PLUGIN);

		targetEnvironmentEClass = createEClass(TARGET_ENVIRONMENT);
		createEAttribute(targetEnvironmentEClass, TARGET_ENVIRONMENT__ARCH);
		createEAttribute(targetEnvironmentEClass, TARGET_ENVIRONMENT__OS);
		createEAttribute(targetEnvironmentEClass, TARGET_ENVIRONMENT__WS);
		createEAttribute(targetEnvironmentEClass, TARGET_ENVIRONMENT__NL);

		targetJREEClass = createEClass(TARGET_JRE);
		createEAttribute(targetJREEClass, TARGET_JRE__PATH);

		targetLocationEClass = createEClass(TARGET_LOCATION);
		createEReference(targetLocationEClass, TARGET_LOCATION__REPOSITORY);
		createEReference(targetLocationEClass, TARGET_LOCATION__UNIT);
		createEAttribute(targetLocationEClass, TARGET_LOCATION__ID);
		createEAttribute(targetLocationEClass, TARGET_LOCATION__INCLUDE_ALL_PLATFORMS);
		createEAttribute(targetLocationEClass, TARGET_LOCATION__INCLUDE_CONFIGURE_PHASE);
		createEAttribute(targetLocationEClass, TARGET_LOCATION__INCLUDE_MODE);
		createEAttribute(targetLocationEClass, TARGET_LOCATION__INCLUDE_SOURCE);
		createEAttribute(targetLocationEClass, TARGET_LOCATION__PATH);
		createEAttribute(targetLocationEClass, TARGET_LOCATION__TYPE);

		targetLocationsEClass = createEClass(TARGET_LOCATIONS);
		createEReference(targetLocationsEClass, TARGET_LOCATIONS__LOCATION);

		touchpointEClass = createEClass(TOUCHPOINT);
		createEAttribute(touchpointEClass, TOUCHPOINT__ID);
		createEAttribute(touchpointEClass, TOUCHPOINT__VERSION);

		touchpointDataEClass = createEClass(TOUCHPOINT_DATA);
		createEAttribute(touchpointDataEClass, TOUCHPOINT_DATA__ID);
		createEAttribute(touchpointDataEClass, TOUCHPOINT_DATA__VERSION);

		unitEClass = createEClass(UNIT);
		createEReference(unitEClass, UNIT__UPDATE);
		createEReference(unitEClass, UNIT__ARTIFACTS);
		createEReference(unitEClass, UNIT__HOST_REQUIREMENTS);
		createEReference(unitEClass, UNIT__PROPERTIES);
		createEReference(unitEClass, UNIT__PROVIDES);
		createEReference(unitEClass, UNIT__REQUIRES);
		createEAttribute(unitEClass, UNIT__FILTER);
		createEReference(unitEClass, UNIT__TOUCHPOINT);
		createEReference(unitEClass, UNIT__TOUCHPOINT_DATA);
		createEReference(unitEClass, UNIT__LICENSES);
		createEReference(unitEClass, UNIT__COPYRIGHT);
		createEAttribute(unitEClass, UNIT__ID);
		createEAttribute(unitEClass, UNIT__SINGLETON);
		createEAttribute(unitEClass, UNIT__VERSION);
		createEAttribute(unitEClass, UNIT__GENERATION);

		unitsTypeEClass = createEClass(UNITS_TYPE);
		createEReference(unitsTypeEClass, UNITS_TYPE__UNIT);
		createEAttribute(unitsTypeEClass, UNITS_TYPE__SIZE);

		touchpointDataTypeEClass = createEClass(TOUCHPOINT_DATA_TYPE);
		createEReference(touchpointDataTypeEClass, TOUCHPOINT_DATA_TYPE__INSTRUCTIONS);
		createEAttribute(touchpointDataTypeEClass, TOUCHPOINT_DATA_TYPE__SIZE);

		urlTypeEClass = createEClass(URL_TYPE);
		createEReference(urlTypeEClass, URL_TYPE__UPDATE);
		createEReference(urlTypeEClass, URL_TYPE__DISCOVERY);

		updateEClass = createEClass(UPDATE);
		createEAttribute(updateEClass, UPDATE__ID);
		createEAttribute(updateEClass, UPDATE__RANGE);
		createEAttribute(updateEClass, UPDATE__SEVERITY);

		// Create enums
		locationIncludeTypeEEnum = createEEnum(LOCATION_INCLUDE_TYPE);
		matchTypeEEnum = createEEnum(MATCH_TYPE);
		targetTypeEEnum = createEEnum(TARGET_TYPE);

		// Create data types
		locationIncludeTypeObjectEDataType = createEDataType(LOCATION_INCLUDE_TYPE_OBJECT);
		matchTypeObjectEDataType = createEDataType(MATCH_TYPE_OBJECT);
		targetTypeObjectEDataType = createEDataType(TARGET_TYPE_OBJECT);
		versionEDataType = createEDataType(VERSION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(artifactEClass, Artifact.class, "Artifact", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getArtifact_Processing(), this.getProcessing(), null, "processing", null, 1, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArtifact_Properties(), this.getProperties(), null, "properties", null, 1, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getArtifact_RepositoryProperties(), this.getProperties(), null, "repositoryProperties", null, 1, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifact_Classifier(), theXMLTypePackage.getString(), "classifier", null, 1, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifact_Id(), theXMLTypePackage.getString(), "id", null, 1, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifact_Version(), this.getVersion(), "version", null, 0, 1, Artifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(artifactsEClass, Artifacts.class, "Artifacts", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getArtifacts_Artifact(), this.getArtifact(), null, "artifact", null, 1, -1, Artifacts.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtifacts_Size(), theXMLTypePackage.getInt(), "size", null, 0, 1, Artifacts.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(categoryDefEClass, CategoryDef.class, "CategoryDef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCategoryDef_Description(), theXMLTypePackage.getString(), "description", null, 1, 1, CategoryDef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCategoryDef_Label(), theXMLTypePackage.getString(), "label", null, 0, 1, CategoryDef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCategoryDef_Name(), theXMLTypePackage.getString(), "name", null, 0, 1, CategoryDef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(categoryFeatureEClass, CategoryFeature.class, "CategoryFeature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCategoryFeature_Category(), this.getCategoryType(), null, "category", null, 1, 1, CategoryFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCategoryFeature_Id(), theXMLTypePackage.getString(), "id", null, 0, 1, CategoryFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCategoryFeature_Version(), this.getVersion(), "version", null, 0, 1, CategoryFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(categorySiteEClass, CategorySite.class, "CategorySite", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCategorySite_Feature(), this.getCategoryFeature(), null, "feature", null, 1, -1, CategorySite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCategorySite_CategoryDef(), this.getCategoryDef(), null, "categoryDef", null, 1, -1, CategorySite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(categoryTypeEClass, CategoryType.class, "CategoryType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCategoryType_Name(), theXMLTypePackage.getString(), "name", null, 0, 1, CategoryType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(childrenTypeEClass, ChildrenType.class, "ChildrenType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getChildrenType_Child(), this.getRepositoryRef(), null, "child", null, 1, -1, ChildrenType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getChildrenType_Size(), theXMLTypePackage.getInt(), "size", null, 0, 1, ChildrenType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(descriptionEClass, Description.class, "Description", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDescription_Value(), theXMLTypePackage.getString(), "value", null, 0, 1, Description.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDescription_Url(), theXMLTypePackage.getString(), "url", null, 0, 1, Description.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_Feature(), this.getFeature(), null, "feature", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_Repository(), this.getRepository(), null, "repository", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_Site(), this.getCategorySite(), null, "site", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_Target(), this.getTarget(), null, "target", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(featureEClass, Feature.class, "Feature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFeature_InstallHandler(), this.getInstallHandler(), null, "installHandler", null, 0, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeature_Description(), this.getDescription(), null, "description", null, 0, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeature_Copyright(), this.getDescription(), null, "copyright", null, 0, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeature_License(), this.getDescription(), null, "license", null, 0, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeature_Url(), this.getUrlType(), null, "url", null, 0, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeature_Includes(), this.getInclude(), null, "includes", null, 0, -1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeature_Requires(), this.getImportRequires(), null, "requires", null, 0, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeature_Plugin(), this.getPlugin(), null, "plugin", null, 0, -1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeature_Arch(), theXMLTypePackage.getString(), "arch", null, 0, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeature_ColocationAffinity(), theXMLTypePackage.getString(), "colocationAffinity", null, 0, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeature_Exclusive(), theXMLTypePackage.getBoolean(), "exclusive", "false", 0, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeature_Id(), theXMLTypePackage.getID(), "id", null, 0, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeature_Image(), theXMLTypePackage.getString(), "image", null, 0, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeature_Label(), theXMLTypePackage.getString(), "label", null, 0, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeature_Nl(), theXMLTypePackage.getString(), "nl", null, 0, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeature_Os(), theXMLTypePackage.getString(), "os", null, 0, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeature_Plugin1(), theXMLTypePackage.getString(), "plugin1", null, 0, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeature_ProviderName(), theXMLTypePackage.getString(), "providerName", null, 0, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeature_Version(), this.getVersion(), "version", null, 0, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeature_Ws(), theXMLTypePackage.getString(), "ws", null, 0, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(importEClass, Import.class, "Import", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImport_Feature(), theXMLTypePackage.getString(), "feature", null, 0, 1, Import.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImport_Match(), this.getMatchType(), "match", null, 0, 1, Import.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImport_Patch(), theXMLTypePackage.getBoolean(), "patch", "false", 0, 1, Import.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImport_Plugin(), theXMLTypePackage.getString(), "plugin", null, 0, 1, Import.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImport_Version(), this.getVersion(), "version", null, 0, 1, Import.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(importRequiresEClass, ImportRequires.class, "ImportRequires", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getImportRequires_Import(), this.getImport(), null, "import", null, 1, -1, ImportRequires.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(includeEClass, Include.class, "Include", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInclude_Arch(), theXMLTypePackage.getString(), "arch", null, 0, 1, Include.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInclude_Id(), theXMLTypePackage.getString(), "id", null, 0, 1, Include.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInclude_Name(), theXMLTypePackage.getString(), "name", null, 0, 1, Include.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInclude_Nl(), theXMLTypePackage.getString(), "nl", null, 0, 1, Include.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInclude_Optional(), theXMLTypePackage.getBoolean(), "optional", null, 0, 1, Include.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInclude_Os(), theXMLTypePackage.getString(), "os", null, 0, 1, Include.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInclude_SearchLocation(), theXMLTypePackage.getString(), "searchLocation", null, 0, 1, Include.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInclude_Version(), this.getVersion(), "version", null, 0, 1, Include.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInclude_Ws(), theXMLTypePackage.getString(), "ws", null, 0, 1, Include.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(installHandlerEClass, InstallHandler.class, "InstallHandler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInstallHandler_Handler(), theXMLTypePackage.getString(), "handler", null, 0, 1, InstallHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInstallHandler_Library(), theXMLTypePackage.getString(), "library", null, 0, 1, InstallHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(instructionEClass, Instruction.class, "Instruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInstruction_Value(), theXMLTypePackage.getString(), "value", null, 0, 1, Instruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInstruction_Key(), theXMLTypePackage.getString(), "key", null, 0, 1, Instruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(instructionsTypeEClass, InstructionsType.class, "InstructionsType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInstructionsType_Instruction(), this.getInstruction(), null, "instruction", null, 1, -1, InstructionsType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInstructionsType_Size(), theXMLTypePackage.getInt(), "size", null, 0, 1, InstructionsType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(launcherArgsEClass, LauncherArgs.class, "LauncherArgs", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLauncherArgs_ProgramArgs(), theXMLTypePackage.getString(), "programArgs", null, 1, 1, LauncherArgs.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLauncherArgs_VmArgs(), theXMLTypePackage.getString(), "vmArgs", null, 1, 1, LauncherArgs.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(licensesTypeEClass, LicensesType.class, "LicensesType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLicensesType_License(), this.getDescription(), null, "license", null, 1, -1, LicensesType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLicensesType_Size(), theXMLTypePackage.getInt(), "size", null, 0, 1, LicensesType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mappingsTypeEClass, MappingsType.class, "MappingsType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMappingsType_Rule(), this.getRule(), null, "rule", null, 1, -1, MappingsType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMappingsType_Size(), theXMLTypePackage.getInt(), "size", null, 0, 1, MappingsType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pluginEClass, Plugin.class, "Plugin", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPlugin_Arch(), theXMLTypePackage.getString(), "arch", null, 0, 1, Plugin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPlugin_DownloadSize(), theXMLTypePackage.getInt(), "downloadSize", null, 0, 1, Plugin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPlugin_Fragment(), theXMLTypePackage.getBoolean(), "fragment", "false", 0, 1, Plugin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPlugin_Id(), theXMLTypePackage.getString(), "id", null, 0, 1, Plugin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPlugin_InstallSize(), theXMLTypePackage.getInt(), "installSize", null, 0, 1, Plugin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPlugin_Nl(), theXMLTypePackage.getString(), "nl", null, 0, 1, Plugin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPlugin_Os(), theXMLTypePackage.getString(), "os", null, 0, 1, Plugin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPlugin_Unpack(), theXMLTypePackage.getBoolean(), "unpack", "false", 0, 1, Plugin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPlugin_Version(), this.getVersion(), "version", null, 0, 1, Plugin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPlugin_Ws(), theXMLTypePackage.getString(), "ws", null, 0, 1, Plugin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(processingEClass, Processing.class, "Processing", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProcessing_Step(), this.getProcessStep(), null, "step", null, 1, -1, Processing.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessing_Size(), theXMLTypePackage.getInt(), "size", null, 0, 1, Processing.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(processStepEClass, ProcessStep.class, "ProcessStep", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getProcessStep_Id(), theXMLTypePackage.getString(), "id", null, 0, 1, ProcessStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessStep_Required(), theXMLTypePackage.getBoolean(), "required", "false", 0, 1, ProcessStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(propertiesEClass, Properties.class, "Properties", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProperties_Property(), this.getProperty(), null, "property", null, 1, -1, Properties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProperties_Size(), theXMLTypePackage.getInt(), "size", null, 0, 1, Properties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(propertyEClass, Property.class, "Property", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getProperty_Name(), theXMLTypePackage.getString(), "name", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProperty_Value(), theXMLTypePackage.getString(), "value", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(providedEClass, Provided.class, "Provided", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getProvided_Name(), theXMLTypePackage.getString(), "name", null, 0, 1, Provided.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProvided_Namespace(), theXMLTypePackage.getString(), "namespace", null, 0, 1, Provided.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProvided_Version(), this.getVersion(), "version", null, 0, 1, Provided.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProvided_Properties(), this.getProperties(), null, "properties", null, 0, 1, Provided.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(providesEClass, Provides.class, "Provides", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProvides_Provided(), this.getProvided(), null, "provided", null, 1, -1, Provides.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProvides_Size(), theXMLTypePackage.getInt(), "size", null, 0, 1, Provides.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(referencesTypeEClass, ReferencesType.class, "ReferencesType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReferencesType_Repository(), this.getRepositoryRefs(), null, "repository", null, 1, -1, ReferencesType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getReferencesType_Size(), theXMLTypePackage.getInt(), "size", null, 0, 1, ReferencesType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(repositoryEClass, Repository.class, "Repository", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRepository_Properties(), this.getProperties(), null, "properties", null, 1, 1, Repository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRepository_Mappings(), this.getMappingsType(), null, "mappings", null, 1, 1, Repository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRepository_References(), this.getReferencesType(), null, "references", null, 1, 1, Repository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRepository_Units(), this.getUnitsType(), null, "units", null, 1, 1, Repository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRepository_Children(), this.getChildrenType(), null, "children", null, 1, 1, Repository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRepository_Artifacts(), this.getArtifacts(), null, "artifacts", null, 1, 1, Repository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRepository_Name(), theXMLTypePackage.getString(), "name", null, 0, 1, Repository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRepository_Type(), theXMLTypePackage.getString(), "type", null, 0, 1, Repository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRepository_Version(), this.getVersion(), "version", null, 0, 1, Repository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(repositoryRefEClass, RepositoryRef.class, "RepositoryRef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRepositoryRef_Location(), theXMLTypePackage.getString(), "location", null, 0, 1, RepositoryRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(repositoryRefsEClass, RepositoryRefs.class, "RepositoryRefs", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRepositoryRefs_Options(), theXMLTypePackage.getInt(), "options", null, 0, 1, RepositoryRefs.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRepositoryRefs_Type(), theXMLTypePackage.getInt(), "type", null, 0, 1, RepositoryRefs.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRepositoryRefs_Uri(), theXMLTypePackage.getString(), "uri", null, 0, 1, RepositoryRefs.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRepositoryRefs_Url(), theXMLTypePackage.getString(), "url", null, 0, 1, RepositoryRefs.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(requiredEClass, Required.class, "Required", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRequired_Filter(), theXMLTypePackage.getString(), "filter", null, 1, 1, Required.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequired_Greedy(), theXMLTypePackage.getBoolean(), "greedy", "true", 0, 1, Required.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequired_Multiple(), theXMLTypePackage.getBoolean(), "multiple", "false", 0, 1, Required.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequired_Name(), theXMLTypePackage.getString(), "name", null, 0, 1, Required.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequired_Namespace(), theXMLTypePackage.getString(), "namespace", null, 0, 1, Required.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequired_Optional(), theXMLTypePackage.getBoolean(), "optional", "false", 0, 1, Required.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequired_Range(), theXMLTypePackage.getString(), "range", null, 0, 1, Required.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(requiredPropertiesEClass, RequiredProperties.class, "RequiredProperties", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRequiredProperties_Match(), theXMLTypePackage.getString(), "match", null, 1, 1, RequiredProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequiredProperties_Namespace(), theXMLTypePackage.getString(), "namespace", null, 0, 1, RequiredProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(requiresEClass, Requires.class, "Requires", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRequires_Required(), this.getRequired(), null, "required", null, 1, -1, Requires.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRequires_RequiredProperties(), this.getRequiredProperties(), null, "requiredProperties", null, 0, -1, Requires.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequires_Size(), theXMLTypePackage.getInt(), "size", null, 0, 1, Requires.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ruleEClass, Rule.class, "Rule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRule_Filter(), theXMLTypePackage.getString(), "filter", null, 0, 1, Rule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRule_Output(), theXMLTypePackage.getString(), "output", null, 0, 1, Rule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(siteEClass, Site.class, "Site", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSite_Label(), theXMLTypePackage.getString(), "label", null, 0, 1, Site.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSite_Url(), theXMLTypePackage.getString(), "url", null, 0, 1, Site.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(targetEClass, Target.class, "Target", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTarget_Locations(), this.getTargetLocations(), null, "locations", null, 1, 1, Target.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTarget_Environment(), this.getTargetEnvironment(), null, "environment", null, 1, 1, Target.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTarget_TargetJRE(), this.getTargetJRE(), null, "targetJRE", null, 1, 1, Target.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTarget_LauncherArgs(), this.getLauncherArgs(), null, "launcherArgs", null, 1, 1, Target.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTarget_ImplicitDependencies(), this.getTargetDependencies(), null, "implicitDependencies", null, 1, 1, Target.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTarget_Name(), theXMLTypePackage.getString(), "name", null, 0, 1, Target.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTarget_SequenceNumber(), ecorePackage.getELongObject(), "sequenceNumber", null, 0, 1, Target.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(targetDependenciesEClass, TargetDependencies.class, "TargetDependencies", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTargetDependencies_Plugin(), this.getArtifact(), null, "plugin", null, 1, -1, TargetDependencies.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(targetEnvironmentEClass, TargetEnvironment.class, "TargetEnvironment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTargetEnvironment_Arch(), theXMLTypePackage.getString(), "arch", null, 1, 1, TargetEnvironment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTargetEnvironment_Os(), theXMLTypePackage.getString(), "os", null, 1, 1, TargetEnvironment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTargetEnvironment_Ws(), theXMLTypePackage.getString(), "ws", null, 1, 1, TargetEnvironment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTargetEnvironment_Nl(), theXMLTypePackage.getString(), "nl", null, 1, 1, TargetEnvironment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(targetJREEClass, TargetJRE.class, "TargetJRE", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTargetJRE_Path(), theXMLTypePackage.getString(), "path", null, 0, 1, TargetJRE.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(targetLocationEClass, TargetLocation.class, "TargetLocation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTargetLocation_Repository(), this.getRepositoryRef(), null, "repository", null, 1, 1, TargetLocation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTargetLocation_Unit(), this.getUnit(), null, "unit", null, 1, -1, TargetLocation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTargetLocation_Id(), theXMLTypePackage.getString(), "id", null, 0, 1, TargetLocation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTargetLocation_IncludeAllPlatforms(), theXMLTypePackage.getBoolean(), "includeAllPlatforms", "true", 0, 1, TargetLocation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTargetLocation_IncludeConfigurePhase(), theXMLTypePackage.getBoolean(), "includeConfigurePhase", "false", 0, 1, TargetLocation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTargetLocation_IncludeMode(), this.getLocationIncludeType(), "includeMode", null, 0, 1, TargetLocation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTargetLocation_IncludeSource(), theXMLTypePackage.getBoolean(), "includeSource", "true", 0, 1, TargetLocation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTargetLocation_Path(), theXMLTypePackage.getString(), "path", null, 0, 1, TargetLocation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTargetLocation_Type(), this.getTargetType(), "type", null, 0, 1, TargetLocation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(targetLocationsEClass, TargetLocations.class, "TargetLocations", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTargetLocations_Location(), this.getTargetLocation(), null, "location", null, 1, -1, TargetLocations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(touchpointEClass, Touchpoint.class, "Touchpoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTouchpoint_Id(), theXMLTypePackage.getString(), "id", null, 0, 1, Touchpoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTouchpoint_Version(), this.getVersion(), "version", null, 0, 1, Touchpoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(touchpointDataEClass, TouchpointData.class, "TouchpointData", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTouchpointData_Id(), theXMLTypePackage.getString(), "id", null, 0, 1, TouchpointData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTouchpointData_Version(), this.getVersion(), "version", null, 0, 1, TouchpointData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unitEClass, Unit.class, "Unit", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUnit_Update(), this.getUpdate(), null, "update", null, 1, 1, Unit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUnit_Artifacts(), this.getArtifacts(), null, "artifacts", null, 1, 1, Unit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUnit_HostRequirements(), this.getRequires(), null, "hostRequirements", null, 1, 1, Unit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUnit_Properties(), this.getProperties(), null, "properties", null, 1, 1, Unit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUnit_Provides(), this.getProvides(), null, "provides", null, 1, 1, Unit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUnit_Requires(), this.getRequires(), null, "requires", null, 1, 1, Unit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUnit_Filter(), theXMLTypePackage.getString(), "filter", null, 1, 1, Unit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUnit_Touchpoint(), this.getTouchpoint(), null, "touchpoint", null, 1, 1, Unit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUnit_TouchpointData(), this.getTouchpointDataType(), null, "touchpointData", null, 1, 1, Unit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUnit_Licenses(), this.getLicensesType(), null, "licenses", null, 1, 1, Unit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUnit_Copyright(), this.getDescription(), null, "copyright", null, 1, 1, Unit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUnit_Id(), theXMLTypePackage.getString(), "id", null, 0, 1, Unit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUnit_Singleton(), theXMLTypePackage.getBoolean(), "singleton", null, 0, 1, Unit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUnit_Version(), this.getVersion(), "version", null, 0, 1, Unit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUnit_Generation(), ecorePackage.getEIntegerObject(), "generation", null, 0, 1, Unit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unitsTypeEClass, UnitsType.class, "UnitsType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUnitsType_Unit(), this.getUnit(), null, "unit", null, 1, -1, UnitsType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUnitsType_Size(), theXMLTypePackage.getInt(), "size", null, 0, 1, UnitsType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(touchpointDataTypeEClass, TouchpointDataType.class, "TouchpointDataType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTouchpointDataType_Instructions(), this.getInstructionsType(), null, "instructions", null, 1, -1, TouchpointDataType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTouchpointDataType_Size(), theXMLTypePackage.getInt(), "size", null, 0, 1, TouchpointDataType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(urlTypeEClass, UrlType.class, "UrlType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUrlType_Update(), this.getSite(), null, "update", null, 1, 1, UrlType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUrlType_Discovery(), this.getSite(), null, "discovery", null, 1, -1, UrlType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(updateEClass, Update.class, "Update", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUpdate_Id(), theXMLTypePackage.getString(), "id", null, 0, 1, Update.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUpdate_Range(), theXMLTypePackage.getString(), "range", null, 0, 1, Update.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUpdate_Severity(), theXMLTypePackage.getInt(), "severity", null, 0, 1, Update.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(locationIncludeTypeEEnum, LocationIncludeType.class, "LocationIncludeType");
		addEEnumLiteral(locationIncludeTypeEEnum, LocationIncludeType.NONE);
		addEEnumLiteral(locationIncludeTypeEEnum, LocationIncludeType.PLANNER);
		addEEnumLiteral(locationIncludeTypeEEnum, LocationIncludeType.SLICER);

		initEEnum(matchTypeEEnum, MatchType.class, "MatchType");
		addEEnumLiteral(matchTypeEEnum, MatchType.NONE);
		addEEnumLiteral(matchTypeEEnum, MatchType.GREATER_OR_EQUAL);
		addEEnumLiteral(matchTypeEEnum, MatchType.EQUIVALENT);
		addEEnumLiteral(matchTypeEEnum, MatchType.COMPATIBLE);
		addEEnumLiteral(matchTypeEEnum, MatchType.PERFECT);

		initEEnum(targetTypeEEnum, TargetType.class, "TargetType");
		addEEnumLiteral(targetTypeEEnum, TargetType.NONE);
		addEEnumLiteral(targetTypeEEnum, TargetType.DIRECTORY);
		addEEnumLiteral(targetTypeEEnum, TargetType.FEATURE);
		addEEnumLiteral(targetTypeEEnum, TargetType.INSTALLABLE_UNIT);
		addEEnumLiteral(targetTypeEEnum, TargetType.PROFILE);

		// Initialize data types
		initEDataType(locationIncludeTypeObjectEDataType, LocationIncludeType.class, "LocationIncludeTypeObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(matchTypeObjectEDataType, MatchType.class, "MatchTypeObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(targetTypeObjectEDataType, TargetType.class, "TargetTypeObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(versionEDataType, String.class, "Version", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";
		addAnnotation
		  (artifactEClass,
		   source,
		   new String[] {
			   "name", "Artifact",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getArtifact_Processing(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "processing",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getArtifact_Properties(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "properties",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getArtifact_RepositoryProperties(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "repositoryProperties",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getArtifact_Classifier(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "classifier"
		   });
		addAnnotation
		  (getArtifact_Id(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "id"
		   });
		addAnnotation
		  (getArtifact_Version(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "version"
		   });
		addAnnotation
		  (artifactsEClass,
		   source,
		   new String[] {
			   "name", "Artifacts",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getArtifacts_Artifact(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "artifact",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getArtifacts_Size(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "size"
		   });
		addAnnotation
		  (categoryDefEClass,
		   source,
		   new String[] {
			   "name", "CategoryDef",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getCategoryDef_Description(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "description",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getCategoryDef_Label(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "label"
		   });
		addAnnotation
		  (getCategoryDef_Name(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "name"
		   });
		addAnnotation
		  (categoryFeatureEClass,
		   source,
		   new String[] {
			   "name", "CategoryFeature",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getCategoryFeature_Category(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "category",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getCategoryFeature_Id(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "id"
		   });
		addAnnotation
		  (getCategoryFeature_Version(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "version"
		   });
		addAnnotation
		  (categorySiteEClass,
		   source,
		   new String[] {
			   "name", "CategorySite",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getCategorySite_Feature(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "feature",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getCategorySite_CategoryDef(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "category-def",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (categoryTypeEClass,
		   source,
		   new String[] {
			   "name", "category_._type",
			   "kind", "empty"
		   });
		addAnnotation
		  (getCategoryType_Name(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "name"
		   });
		addAnnotation
		  (childrenTypeEClass,
		   source,
		   new String[] {
			   "name", "children_._type",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getChildrenType_Child(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "child",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getChildrenType_Size(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "size"
		   });
		addAnnotation
		  (descriptionEClass,
		   source,
		   new String[] {
			   "name", "Description",
			   "kind", "simple"
		   });
		addAnnotation
		  (getDescription_Value(),
		   source,
		   new String[] {
			   "name", ":0",
			   "kind", "simple"
		   });
		addAnnotation
		  (getDescription_Url(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "url"
		   });
		addAnnotation
		  (documentRootEClass,
		   source,
		   new String[] {
			   "name", "",
			   "kind", "mixed"
		   });
		addAnnotation
		  (getDocumentRoot_Mixed(),
		   source,
		   new String[] {
			   "kind", "elementWildcard",
			   "name", ":mixed"
		   });
		addAnnotation
		  (getDocumentRoot_XMLNSPrefixMap(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "xmlns:prefix"
		   });
		addAnnotation
		  (getDocumentRoot_XSISchemaLocation(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "xsi:schemaLocation"
		   });
		addAnnotation
		  (getDocumentRoot_Feature(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "feature",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDocumentRoot_Repository(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "repository",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDocumentRoot_Site(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "site",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDocumentRoot_Target(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "target",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (featureEClass,
		   source,
		   new String[] {
			   "name", "Feature",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getFeature_InstallHandler(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "install-handler",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFeature_Description(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "description",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFeature_Copyright(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "copyright",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFeature_License(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "license",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFeature_Url(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "url",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFeature_Includes(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "includes",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFeature_Requires(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "requires",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFeature_Plugin(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "plugin",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFeature_Arch(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "arch"
		   });
		addAnnotation
		  (getFeature_ColocationAffinity(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "colocation-affinity"
		   });
		addAnnotation
		  (getFeature_Exclusive(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "exclusive"
		   });
		addAnnotation
		  (getFeature_Id(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "id"
		   });
		addAnnotation
		  (getFeature_Image(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "image"
		   });
		addAnnotation
		  (getFeature_Label(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "label"
		   });
		addAnnotation
		  (getFeature_Nl(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "nl"
		   });
		addAnnotation
		  (getFeature_Os(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "os"
		   });
		addAnnotation
		  (getFeature_Plugin1(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "plugin"
		   });
		addAnnotation
		  (getFeature_ProviderName(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "provider-name"
		   });
		addAnnotation
		  (getFeature_Version(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "version"
		   });
		addAnnotation
		  (getFeature_Ws(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "ws"
		   });
		addAnnotation
		  (importEClass,
		   source,
		   new String[] {
			   "name", "Import",
			   "kind", "empty"
		   });
		addAnnotation
		  (getImport_Feature(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "feature"
		   });
		addAnnotation
		  (getImport_Match(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "match"
		   });
		addAnnotation
		  (getImport_Patch(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "patch"
		   });
		addAnnotation
		  (getImport_Plugin(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "plugin"
		   });
		addAnnotation
		  (getImport_Version(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "version"
		   });
		addAnnotation
		  (importRequiresEClass,
		   source,
		   new String[] {
			   "name", "ImportRequires",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getImportRequires_Import(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "import",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (includeEClass,
		   source,
		   new String[] {
			   "name", "Include",
			   "kind", "empty"
		   });
		addAnnotation
		  (getInclude_Arch(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "arch"
		   });
		addAnnotation
		  (getInclude_Id(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "id"
		   });
		addAnnotation
		  (getInclude_Name(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "name"
		   });
		addAnnotation
		  (getInclude_Nl(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "nl"
		   });
		addAnnotation
		  (getInclude_Optional(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "optional"
		   });
		addAnnotation
		  (getInclude_Os(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "os"
		   });
		addAnnotation
		  (getInclude_SearchLocation(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "search-location"
		   });
		addAnnotation
		  (getInclude_Version(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "version"
		   });
		addAnnotation
		  (getInclude_Ws(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "ws"
		   });
		addAnnotation
		  (installHandlerEClass,
		   source,
		   new String[] {
			   "name", "InstallHandler",
			   "kind", "empty"
		   });
		addAnnotation
		  (getInstallHandler_Handler(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "handler"
		   });
		addAnnotation
		  (getInstallHandler_Library(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "library"
		   });
		addAnnotation
		  (instructionEClass,
		   source,
		   new String[] {
			   "name", "Instruction",
			   "kind", "simple"
		   });
		addAnnotation
		  (getInstruction_Value(),
		   source,
		   new String[] {
			   "name", ":0",
			   "kind", "simple"
		   });
		addAnnotation
		  (getInstruction_Key(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "key"
		   });
		addAnnotation
		  (instructionsTypeEClass,
		   source,
		   new String[] {
			   "name", "instructions_._type",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getInstructionsType_Instruction(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "instruction",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getInstructionsType_Size(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "size"
		   });
		addAnnotation
		  (launcherArgsEClass,
		   source,
		   new String[] {
			   "name", "LauncherArgs",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getLauncherArgs_ProgramArgs(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "programArgs",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getLauncherArgs_VmArgs(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "vmArgs",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (licensesTypeEClass,
		   source,
		   new String[] {
			   "name", "licenses_._type",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getLicensesType_License(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "license",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getLicensesType_Size(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "size"
		   });
		addAnnotation
		  (locationIncludeTypeEEnum,
		   source,
		   new String[] {
			   "name", "LocationIncludeType"
		   });
		addAnnotation
		  (locationIncludeTypeObjectEDataType,
		   source,
		   new String[] {
			   "name", "LocationIncludeType:Object",
			   "baseType", "LocationIncludeType"
		   });
		addAnnotation
		  (mappingsTypeEClass,
		   source,
		   new String[] {
			   "name", "mappings_._type",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getMappingsType_Rule(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "rule",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getMappingsType_Size(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "size"
		   });
		addAnnotation
		  (matchTypeEEnum,
		   source,
		   new String[] {
			   "name", "MatchType"
		   });
		addAnnotation
		  (matchTypeObjectEDataType,
		   source,
		   new String[] {
			   "name", "MatchType:Object",
			   "baseType", "MatchType"
		   });
		addAnnotation
		  (pluginEClass,
		   source,
		   new String[] {
			   "name", "Plugin",
			   "kind", "empty"
		   });
		addAnnotation
		  (getPlugin_Arch(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "arch"
		   });
		addAnnotation
		  (getPlugin_DownloadSize(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "download-size"
		   });
		addAnnotation
		  (getPlugin_Fragment(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "fragment"
		   });
		addAnnotation
		  (getPlugin_Id(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "id"
		   });
		addAnnotation
		  (getPlugin_InstallSize(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "install-size"
		   });
		addAnnotation
		  (getPlugin_Nl(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "nl"
		   });
		addAnnotation
		  (getPlugin_Os(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "os"
		   });
		addAnnotation
		  (getPlugin_Unpack(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "unpack"
		   });
		addAnnotation
		  (getPlugin_Version(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "version"
		   });
		addAnnotation
		  (getPlugin_Ws(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "ws"
		   });
		addAnnotation
		  (processingEClass,
		   source,
		   new String[] {
			   "name", "Processing",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getProcessing_Step(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "step",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getProcessing_Size(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "size"
		   });
		addAnnotation
		  (processStepEClass,
		   source,
		   new String[] {
			   "name", "ProcessStep",
			   "kind", "empty"
		   });
		addAnnotation
		  (getProcessStep_Id(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "id"
		   });
		addAnnotation
		  (getProcessStep_Required(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "required"
		   });
		addAnnotation
		  (propertiesEClass,
		   source,
		   new String[] {
			   "name", "Properties",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getProperties_Property(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "property",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getProperties_Size(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "size"
		   });
		addAnnotation
		  (propertyEClass,
		   source,
		   new String[] {
			   "name", "Property",
			   "kind", "empty"
		   });
		addAnnotation
		  (getProperty_Name(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "name"
		   });
		addAnnotation
		  (getProperty_Value(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "value"
		   });
		addAnnotation
		  (providedEClass,
		   source,
		   new String[] {
			   "name", "Provided",
			   "kind", "empty"
		   });
		addAnnotation
		  (getProvided_Name(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "name"
		   });
		addAnnotation
		  (getProvided_Namespace(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "namespace"
		   });
		addAnnotation
		  (getProvided_Version(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "version"
		   });
		addAnnotation
		  (providesEClass,
		   source,
		   new String[] {
			   "name", "Provides",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getProvides_Provided(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "provided",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getProvides_Size(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "size"
		   });
		addAnnotation
		  (referencesTypeEClass,
		   source,
		   new String[] {
			   "name", "references_._type",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getReferencesType_Repository(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "repository",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getReferencesType_Size(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "size"
		   });
		addAnnotation
		  (repositoryEClass,
		   source,
		   new String[] {
			   "name", "Repository",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getRepository_Properties(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "properties",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getRepository_Mappings(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "mappings",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getRepository_References(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "references",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getRepository_Units(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "units",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getRepository_Children(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "children",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getRepository_Artifacts(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "artifacts",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getRepository_Name(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "name"
		   });
		addAnnotation
		  (getRepository_Type(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "type"
		   });
		addAnnotation
		  (getRepository_Version(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "version"
		   });
		addAnnotation
		  (repositoryRefEClass,
		   source,
		   new String[] {
			   "name", "RepositoryRef",
			   "kind", "empty"
		   });
		addAnnotation
		  (getRepositoryRef_Location(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "location"
		   });
		addAnnotation
		  (repositoryRefsEClass,
		   source,
		   new String[] {
			   "name", "RepositoryRefs",
			   "kind", "empty"
		   });
		addAnnotation
		  (getRepositoryRefs_Options(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "options"
		   });
		addAnnotation
		  (getRepositoryRefs_Type(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "type"
		   });
		addAnnotation
		  (getRepositoryRefs_Uri(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "uri"
		   });
		addAnnotation
		  (getRepositoryRefs_Url(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "url"
		   });
		addAnnotation
		  (requiredEClass,
		   source,
		   new String[] {
			   "name", "Required",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getRequired_Filter(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "filter",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getRequired_Greedy(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "greedy"
		   });
		addAnnotation
		  (getRequired_Multiple(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "multiple"
		   });
		addAnnotation
		  (getRequired_Name(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "name"
		   });
		addAnnotation
		  (getRequired_Namespace(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "namespace"
		   });
		addAnnotation
		  (getRequired_Optional(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "optional"
		   });
		addAnnotation
		  (getRequired_Range(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "range"
		   });
		addAnnotation
		  (requiredPropertiesEClass,
		   source,
		   new String[] {
			   "name", "RequiredProperties",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getRequiredProperties_Match(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "match"
		   });
		addAnnotation
		  (getRequiredProperties_Namespace(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "namespace"
		   });
		addAnnotation
		  (requiresEClass,
		   source,
		   new String[] {
			   "name", "Requires",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getRequires_Required(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "required",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getRequires_RequiredProperties(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "requiredProperties",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getRequires_Size(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "size"
		   });
		addAnnotation
		  (ruleEClass,
		   source,
		   new String[] {
			   "name", "Rule",
			   "kind", "empty"
		   });
		addAnnotation
		  (getRule_Filter(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "filter"
		   });
		addAnnotation
		  (getRule_Output(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "output"
		   });
		addAnnotation
		  (siteEClass,
		   source,
		   new String[] {
			   "name", "Site",
			   "kind", "empty"
		   });
		addAnnotation
		  (getSite_Label(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "label"
		   });
		addAnnotation
		  (getSite_Url(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "url"
		   });
		addAnnotation
		  (targetEClass,
		   source,
		   new String[] {
			   "name", "Target",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getTarget_Locations(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "locations",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getTarget_Environment(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "environment",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getTarget_TargetJRE(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "targetJRE",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getTarget_LauncherArgs(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "launcherArgs",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getTarget_ImplicitDependencies(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "implicitDependencies",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getTarget_Name(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "name"
		   });
		addAnnotation
		  (getTarget_SequenceNumber(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "sequenceNumber"
		   });
		addAnnotation
		  (targetDependenciesEClass,
		   source,
		   new String[] {
			   "name", "TargetDependencies",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getTargetDependencies_Plugin(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "plugin",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (targetEnvironmentEClass,
		   source,
		   new String[] {
			   "name", "TargetEnvironment",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getTargetEnvironment_Arch(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "arch",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getTargetEnvironment_Os(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "os",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getTargetEnvironment_Ws(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "ws",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getTargetEnvironment_Nl(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "nl",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (targetJREEClass,
		   source,
		   new String[] {
			   "name", "TargetJRE",
			   "kind", "empty"
		   });
		addAnnotation
		  (getTargetJRE_Path(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "path"
		   });
		addAnnotation
		  (targetLocationEClass,
		   source,
		   new String[] {
			   "name", "TargetLocation",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getTargetLocation_Repository(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "repository",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getTargetLocation_Unit(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "unit",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getTargetLocation_Id(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "id"
		   });
		addAnnotation
		  (getTargetLocation_IncludeAllPlatforms(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "includeAllPlatforms"
		   });
		addAnnotation
		  (getTargetLocation_IncludeConfigurePhase(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "includeConfigurePhase"
		   });
		addAnnotation
		  (getTargetLocation_IncludeMode(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "includeMode"
		   });
		addAnnotation
		  (getTargetLocation_IncludeSource(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "includeSource"
		   });
		addAnnotation
		  (getTargetLocation_Path(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "path"
		   });
		addAnnotation
		  (getTargetLocation_Type(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "type"
		   });
		addAnnotation
		  (targetLocationsEClass,
		   source,
		   new String[] {
			   "name", "TargetLocations",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getTargetLocations_Location(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "location",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (targetTypeEEnum,
		   source,
		   new String[] {
			   "name", "TargetType"
		   });
		addAnnotation
		  (targetTypeObjectEDataType,
		   source,
		   new String[] {
			   "name", "TargetType:Object",
			   "baseType", "TargetType"
		   });
		addAnnotation
		  (touchpointEClass,
		   source,
		   new String[] {
			   "name", "Touchpoint",
			   "kind", "empty"
		   });
		addAnnotation
		  (getTouchpoint_Id(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "id"
		   });
		addAnnotation
		  (getTouchpoint_Version(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "version"
		   });
		addAnnotation
		  (touchpointDataEClass,
		   source,
		   new String[] {
			   "name", "TouchpointData",
			   "kind", "empty"
		   });
		addAnnotation
		  (getTouchpointData_Id(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "id"
		   });
		addAnnotation
		  (getTouchpointData_Version(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "version"
		   });
		addAnnotation
		  (unitEClass,
		   source,
		   new String[] {
			   "name", "Unit",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getUnit_Update(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "update",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getUnit_Artifacts(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "artifacts",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getUnit_HostRequirements(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "hostRequirements",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getUnit_Properties(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "properties",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getUnit_Provides(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "provides",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getUnit_Requires(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "requires",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getUnit_Filter(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "filter",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getUnit_Touchpoint(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "touchpoint",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getUnit_TouchpointData(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "touchpointData",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getUnit_Licenses(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "licenses",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getUnit_Copyright(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "copyright",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getUnit_Id(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "id"
		   });
		addAnnotation
		  (getUnit_Singleton(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "singleton"
		   });
		addAnnotation
		  (getUnit_Version(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "version"
		   });
		addAnnotation
		  (getUnit_Generation(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "generation"
		   });
		addAnnotation
		  (unitsTypeEClass,
		   source,
		   new String[] {
			   "name", "units_._type",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getUnitsType_Unit(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "unit",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getUnitsType_Size(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "size"
		   });
		addAnnotation
		  (touchpointDataTypeEClass,
		   source,
		   new String[] {
			   "name", "touchpointData_._type",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getTouchpointDataType_Instructions(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "instructions",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getTouchpointDataType_Size(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "size"
		   });
		addAnnotation
		  (urlTypeEClass,
		   source,
		   new String[] {
			   "name", "url_._type",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getUrlType_Update(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "update",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getUrlType_Discovery(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "discovery",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (updateEClass,
		   source,
		   new String[] {
			   "name", "Update",
			   "kind", "empty"
		   });
		addAnnotation
		  (getUpdate_Id(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "id"
		   });
		addAnnotation
		  (getUpdate_Range(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "range"
		   });
		addAnnotation
		  (getUpdate_Severity(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "severity"
		   });
		addAnnotation
		  (versionEDataType,
		   source,
		   new String[] {
			   "name", "Version",
			   "baseType", "http://www.eclipse.org/emf/2003/XMLType#string",
			   "pattern", "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}(\\..*)?"
		   });
	}

} //EclipsePackageImpl
