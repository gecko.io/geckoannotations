/**
 */
package org.gecko.eclipse.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.gecko.eclipse.EclipsePackage;
import org.gecko.eclipse.LauncherArgs;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Launcher Args</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.impl.LauncherArgsImpl#getProgramArgs <em>Program Args</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.LauncherArgsImpl#getVmArgs <em>Vm Args</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LauncherArgsImpl extends MinimalEObjectImpl.Container implements LauncherArgs {
	/**
	 * The default value of the '{@link #getProgramArgs() <em>Program Args</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProgramArgs()
	 * @generated
	 * @ordered
	 */
	protected static final String PROGRAM_ARGS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProgramArgs() <em>Program Args</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProgramArgs()
	 * @generated
	 * @ordered
	 */
	protected String programArgs = PROGRAM_ARGS_EDEFAULT;

	/**
	 * The default value of the '{@link #getVmArgs() <em>Vm Args</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVmArgs()
	 * @generated
	 * @ordered
	 */
	protected static final String VM_ARGS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVmArgs() <em>Vm Args</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVmArgs()
	 * @generated
	 * @ordered
	 */
	protected String vmArgs = VM_ARGS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LauncherArgsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EclipsePackage.Literals.LAUNCHER_ARGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getProgramArgs() {
		return programArgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProgramArgs(String newProgramArgs) {
		String oldProgramArgs = programArgs;
		programArgs = newProgramArgs;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.LAUNCHER_ARGS__PROGRAM_ARGS, oldProgramArgs, programArgs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVmArgs() {
		return vmArgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVmArgs(String newVmArgs) {
		String oldVmArgs = vmArgs;
		vmArgs = newVmArgs;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.LAUNCHER_ARGS__VM_ARGS, oldVmArgs, vmArgs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EclipsePackage.LAUNCHER_ARGS__PROGRAM_ARGS:
				return getProgramArgs();
			case EclipsePackage.LAUNCHER_ARGS__VM_ARGS:
				return getVmArgs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EclipsePackage.LAUNCHER_ARGS__PROGRAM_ARGS:
				setProgramArgs((String)newValue);
				return;
			case EclipsePackage.LAUNCHER_ARGS__VM_ARGS:
				setVmArgs((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EclipsePackage.LAUNCHER_ARGS__PROGRAM_ARGS:
				setProgramArgs(PROGRAM_ARGS_EDEFAULT);
				return;
			case EclipsePackage.LAUNCHER_ARGS__VM_ARGS:
				setVmArgs(VM_ARGS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EclipsePackage.LAUNCHER_ARGS__PROGRAM_ARGS:
				return PROGRAM_ARGS_EDEFAULT == null ? programArgs != null : !PROGRAM_ARGS_EDEFAULT.equals(programArgs);
			case EclipsePackage.LAUNCHER_ARGS__VM_ARGS:
				return VM_ARGS_EDEFAULT == null ? vmArgs != null : !VM_ARGS_EDEFAULT.equals(vmArgs);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (programArgs: ");
		result.append(programArgs);
		result.append(", vmArgs: ");
		result.append(vmArgs);
		result.append(')');
		return result.toString();
	}

} //LauncherArgsImpl
