/**
 */
package org.gecko.eclipse.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.gecko.eclipse.Description;
import org.gecko.eclipse.EclipsePackage;
import org.gecko.eclipse.Feature;
import org.gecko.eclipse.ImportRequires;
import org.gecko.eclipse.Include;
import org.gecko.eclipse.InstallHandler;
import org.gecko.eclipse.Plugin;
import org.gecko.eclipse.UrlType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getInstallHandler <em>Install Handler</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getCopyright <em>Copyright</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getLicense <em>License</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getUrl <em>Url</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getIncludes <em>Includes</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getRequires <em>Requires</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getPlugin <em>Plugin</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getArch <em>Arch</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getColocationAffinity <em>Colocation Affinity</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#isExclusive <em>Exclusive</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getImage <em>Image</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getNl <em>Nl</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getOs <em>Os</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getPlugin1 <em>Plugin1</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getProviderName <em>Provider Name</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.FeatureImpl#getWs <em>Ws</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FeatureImpl extends MinimalEObjectImpl.Container implements Feature {
	/**
	 * The cached value of the '{@link #getInstallHandler() <em>Install Handler</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstallHandler()
	 * @generated
	 * @ordered
	 */
	protected InstallHandler installHandler;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected Description description;

	/**
	 * The cached value of the '{@link #getCopyright() <em>Copyright</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCopyright()
	 * @generated
	 * @ordered
	 */
	protected Description copyright;

	/**
	 * The cached value of the '{@link #getLicense() <em>License</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLicense()
	 * @generated
	 * @ordered
	 */
	protected Description license;

	/**
	 * The cached value of the '{@link #getUrl() <em>Url</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUrl()
	 * @generated
	 * @ordered
	 */
	protected UrlType url;

	/**
	 * The cached value of the '{@link #getIncludes() <em>Includes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncludes()
	 * @generated
	 * @ordered
	 */
	protected EList<Include> includes;

	/**
	 * The cached value of the '{@link #getRequires() <em>Requires</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequires()
	 * @generated
	 * @ordered
	 */
	protected ImportRequires requires;

	/**
	 * The cached value of the '{@link #getPlugin() <em>Plugin</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlugin()
	 * @generated
	 * @ordered
	 */
	protected EList<Plugin> plugin;

	/**
	 * The default value of the '{@link #getArch() <em>Arch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArch()
	 * @generated
	 * @ordered
	 */
	protected static final String ARCH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getArch() <em>Arch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArch()
	 * @generated
	 * @ordered
	 */
	protected String arch = ARCH_EDEFAULT;

	/**
	 * The default value of the '{@link #getColocationAffinity() <em>Colocation Affinity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColocationAffinity()
	 * @generated
	 * @ordered
	 */
	protected static final String COLOCATION_AFFINITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getColocationAffinity() <em>Colocation Affinity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColocationAffinity()
	 * @generated
	 * @ordered
	 */
	protected String colocationAffinity = COLOCATION_AFFINITY_EDEFAULT;

	/**
	 * The default value of the '{@link #isExclusive() <em>Exclusive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExclusive()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EXCLUSIVE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isExclusive() <em>Exclusive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExclusive()
	 * @generated
	 * @ordered
	 */
	protected boolean exclusive = EXCLUSIVE_EDEFAULT;

	/**
	 * This is true if the Exclusive attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean exclusiveESet;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getImage() <em>Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImage()
	 * @generated
	 * @ordered
	 */
	protected static final String IMAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getImage() <em>Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImage()
	 * @generated
	 * @ordered
	 */
	protected String image = IMAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getNl() <em>Nl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNl()
	 * @generated
	 * @ordered
	 */
	protected static final String NL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNl() <em>Nl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNl()
	 * @generated
	 * @ordered
	 */
	protected String nl = NL_EDEFAULT;

	/**
	 * The default value of the '{@link #getOs() <em>Os</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOs()
	 * @generated
	 * @ordered
	 */
	protected static final String OS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOs() <em>Os</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOs()
	 * @generated
	 * @ordered
	 */
	protected String os = OS_EDEFAULT;

	/**
	 * The default value of the '{@link #getPlugin1() <em>Plugin1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlugin1()
	 * @generated
	 * @ordered
	 */
	protected static final String PLUGIN1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPlugin1() <em>Plugin1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlugin1()
	 * @generated
	 * @ordered
	 */
	protected String plugin1 = PLUGIN1_EDEFAULT;

	/**
	 * The default value of the '{@link #getProviderName() <em>Provider Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProviderName()
	 * @generated
	 * @ordered
	 */
	protected static final String PROVIDER_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProviderName() <em>Provider Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProviderName()
	 * @generated
	 * @ordered
	 */
	protected String providerName = PROVIDER_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getWs() <em>Ws</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWs()
	 * @generated
	 * @ordered
	 */
	protected static final String WS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWs() <em>Ws</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWs()
	 * @generated
	 * @ordered
	 */
	protected String ws = WS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EclipsePackage.Literals.FEATURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InstallHandler getInstallHandler() {
		return installHandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInstallHandler(InstallHandler newInstallHandler, NotificationChain msgs) {
		InstallHandler oldInstallHandler = installHandler;
		installHandler = newInstallHandler;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__INSTALL_HANDLER, oldInstallHandler, newInstallHandler);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInstallHandler(InstallHandler newInstallHandler) {
		if (newInstallHandler != installHandler) {
			NotificationChain msgs = null;
			if (installHandler != null)
				msgs = ((InternalEObject)installHandler).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.FEATURE__INSTALL_HANDLER, null, msgs);
			if (newInstallHandler != null)
				msgs = ((InternalEObject)newInstallHandler).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.FEATURE__INSTALL_HANDLER, null, msgs);
			msgs = basicSetInstallHandler(newInstallHandler, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__INSTALL_HANDLER, newInstallHandler, newInstallHandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Description getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDescription(Description newDescription, NotificationChain msgs) {
		Description oldDescription = description;
		description = newDescription;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__DESCRIPTION, oldDescription, newDescription);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescription(Description newDescription) {
		if (newDescription != description) {
			NotificationChain msgs = null;
			if (description != null)
				msgs = ((InternalEObject)description).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.FEATURE__DESCRIPTION, null, msgs);
			if (newDescription != null)
				msgs = ((InternalEObject)newDescription).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.FEATURE__DESCRIPTION, null, msgs);
			msgs = basicSetDescription(newDescription, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__DESCRIPTION, newDescription, newDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Description getCopyright() {
		return copyright;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCopyright(Description newCopyright, NotificationChain msgs) {
		Description oldCopyright = copyright;
		copyright = newCopyright;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__COPYRIGHT, oldCopyright, newCopyright);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCopyright(Description newCopyright) {
		if (newCopyright != copyright) {
			NotificationChain msgs = null;
			if (copyright != null)
				msgs = ((InternalEObject)copyright).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.FEATURE__COPYRIGHT, null, msgs);
			if (newCopyright != null)
				msgs = ((InternalEObject)newCopyright).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.FEATURE__COPYRIGHT, null, msgs);
			msgs = basicSetCopyright(newCopyright, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__COPYRIGHT, newCopyright, newCopyright));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Description getLicense() {
		return license;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLicense(Description newLicense, NotificationChain msgs) {
		Description oldLicense = license;
		license = newLicense;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__LICENSE, oldLicense, newLicense);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLicense(Description newLicense) {
		if (newLicense != license) {
			NotificationChain msgs = null;
			if (license != null)
				msgs = ((InternalEObject)license).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.FEATURE__LICENSE, null, msgs);
			if (newLicense != null)
				msgs = ((InternalEObject)newLicense).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.FEATURE__LICENSE, null, msgs);
			msgs = basicSetLicense(newLicense, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__LICENSE, newLicense, newLicense));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UrlType getUrl() {
		return url;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUrl(UrlType newUrl, NotificationChain msgs) {
		UrlType oldUrl = url;
		url = newUrl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__URL, oldUrl, newUrl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUrl(UrlType newUrl) {
		if (newUrl != url) {
			NotificationChain msgs = null;
			if (url != null)
				msgs = ((InternalEObject)url).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.FEATURE__URL, null, msgs);
			if (newUrl != null)
				msgs = ((InternalEObject)newUrl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.FEATURE__URL, null, msgs);
			msgs = basicSetUrl(newUrl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__URL, newUrl, newUrl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Include> getIncludes() {
		if (includes == null) {
			includes = new EObjectContainmentEList<Include>(Include.class, this, EclipsePackage.FEATURE__INCLUDES);
		}
		return includes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ImportRequires getRequires() {
		return requires;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRequires(ImportRequires newRequires, NotificationChain msgs) {
		ImportRequires oldRequires = requires;
		requires = newRequires;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__REQUIRES, oldRequires, newRequires);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRequires(ImportRequires newRequires) {
		if (newRequires != requires) {
			NotificationChain msgs = null;
			if (requires != null)
				msgs = ((InternalEObject)requires).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.FEATURE__REQUIRES, null, msgs);
			if (newRequires != null)
				msgs = ((InternalEObject)newRequires).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.FEATURE__REQUIRES, null, msgs);
			msgs = basicSetRequires(newRequires, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__REQUIRES, newRequires, newRequires));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Plugin> getPlugin() {
		if (plugin == null) {
			plugin = new EObjectContainmentEList<Plugin>(Plugin.class, this, EclipsePackage.FEATURE__PLUGIN);
		}
		return plugin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getArch() {
		return arch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setArch(String newArch) {
		String oldArch = arch;
		arch = newArch;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__ARCH, oldArch, arch));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getColocationAffinity() {
		return colocationAffinity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setColocationAffinity(String newColocationAffinity) {
		String oldColocationAffinity = colocationAffinity;
		colocationAffinity = newColocationAffinity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__COLOCATION_AFFINITY, oldColocationAffinity, colocationAffinity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isExclusive() {
		return exclusive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setExclusive(boolean newExclusive) {
		boolean oldExclusive = exclusive;
		exclusive = newExclusive;
		boolean oldExclusiveESet = exclusiveESet;
		exclusiveESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__EXCLUSIVE, oldExclusive, exclusive, !oldExclusiveESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetExclusive() {
		boolean oldExclusive = exclusive;
		boolean oldExclusiveESet = exclusiveESet;
		exclusive = EXCLUSIVE_EDEFAULT;
		exclusiveESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EclipsePackage.FEATURE__EXCLUSIVE, oldExclusive, EXCLUSIVE_EDEFAULT, oldExclusiveESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetExclusive() {
		return exclusiveESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getImage() {
		return image;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setImage(String newImage) {
		String oldImage = image;
		image = newImage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__IMAGE, oldImage, image));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__LABEL, oldLabel, label));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getNl() {
		return nl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNl(String newNl) {
		String oldNl = nl;
		nl = newNl;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__NL, oldNl, nl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOs() {
		return os;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOs(String newOs) {
		String oldOs = os;
		os = newOs;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__OS, oldOs, os));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPlugin1() {
		return plugin1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPlugin1(String newPlugin1) {
		String oldPlugin1 = plugin1;
		plugin1 = newPlugin1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__PLUGIN1, oldPlugin1, plugin1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getProviderName() {
		return providerName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProviderName(String newProviderName) {
		String oldProviderName = providerName;
		providerName = newProviderName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__PROVIDER_NAME, oldProviderName, providerName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getWs() {
		return ws;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setWs(String newWs) {
		String oldWs = ws;
		ws = newWs;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.FEATURE__WS, oldWs, ws));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EclipsePackage.FEATURE__INSTALL_HANDLER:
				return basicSetInstallHandler(null, msgs);
			case EclipsePackage.FEATURE__DESCRIPTION:
				return basicSetDescription(null, msgs);
			case EclipsePackage.FEATURE__COPYRIGHT:
				return basicSetCopyright(null, msgs);
			case EclipsePackage.FEATURE__LICENSE:
				return basicSetLicense(null, msgs);
			case EclipsePackage.FEATURE__URL:
				return basicSetUrl(null, msgs);
			case EclipsePackage.FEATURE__INCLUDES:
				return ((InternalEList<?>)getIncludes()).basicRemove(otherEnd, msgs);
			case EclipsePackage.FEATURE__REQUIRES:
				return basicSetRequires(null, msgs);
			case EclipsePackage.FEATURE__PLUGIN:
				return ((InternalEList<?>)getPlugin()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EclipsePackage.FEATURE__INSTALL_HANDLER:
				return getInstallHandler();
			case EclipsePackage.FEATURE__DESCRIPTION:
				return getDescription();
			case EclipsePackage.FEATURE__COPYRIGHT:
				return getCopyright();
			case EclipsePackage.FEATURE__LICENSE:
				return getLicense();
			case EclipsePackage.FEATURE__URL:
				return getUrl();
			case EclipsePackage.FEATURE__INCLUDES:
				return getIncludes();
			case EclipsePackage.FEATURE__REQUIRES:
				return getRequires();
			case EclipsePackage.FEATURE__PLUGIN:
				return getPlugin();
			case EclipsePackage.FEATURE__ARCH:
				return getArch();
			case EclipsePackage.FEATURE__COLOCATION_AFFINITY:
				return getColocationAffinity();
			case EclipsePackage.FEATURE__EXCLUSIVE:
				return isExclusive();
			case EclipsePackage.FEATURE__ID:
				return getId();
			case EclipsePackage.FEATURE__IMAGE:
				return getImage();
			case EclipsePackage.FEATURE__LABEL:
				return getLabel();
			case EclipsePackage.FEATURE__NL:
				return getNl();
			case EclipsePackage.FEATURE__OS:
				return getOs();
			case EclipsePackage.FEATURE__PLUGIN1:
				return getPlugin1();
			case EclipsePackage.FEATURE__PROVIDER_NAME:
				return getProviderName();
			case EclipsePackage.FEATURE__VERSION:
				return getVersion();
			case EclipsePackage.FEATURE__WS:
				return getWs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EclipsePackage.FEATURE__INSTALL_HANDLER:
				setInstallHandler((InstallHandler)newValue);
				return;
			case EclipsePackage.FEATURE__DESCRIPTION:
				setDescription((Description)newValue);
				return;
			case EclipsePackage.FEATURE__COPYRIGHT:
				setCopyright((Description)newValue);
				return;
			case EclipsePackage.FEATURE__LICENSE:
				setLicense((Description)newValue);
				return;
			case EclipsePackage.FEATURE__URL:
				setUrl((UrlType)newValue);
				return;
			case EclipsePackage.FEATURE__INCLUDES:
				getIncludes().clear();
				getIncludes().addAll((Collection<? extends Include>)newValue);
				return;
			case EclipsePackage.FEATURE__REQUIRES:
				setRequires((ImportRequires)newValue);
				return;
			case EclipsePackage.FEATURE__PLUGIN:
				getPlugin().clear();
				getPlugin().addAll((Collection<? extends Plugin>)newValue);
				return;
			case EclipsePackage.FEATURE__ARCH:
				setArch((String)newValue);
				return;
			case EclipsePackage.FEATURE__COLOCATION_AFFINITY:
				setColocationAffinity((String)newValue);
				return;
			case EclipsePackage.FEATURE__EXCLUSIVE:
				setExclusive((Boolean)newValue);
				return;
			case EclipsePackage.FEATURE__ID:
				setId((String)newValue);
				return;
			case EclipsePackage.FEATURE__IMAGE:
				setImage((String)newValue);
				return;
			case EclipsePackage.FEATURE__LABEL:
				setLabel((String)newValue);
				return;
			case EclipsePackage.FEATURE__NL:
				setNl((String)newValue);
				return;
			case EclipsePackage.FEATURE__OS:
				setOs((String)newValue);
				return;
			case EclipsePackage.FEATURE__PLUGIN1:
				setPlugin1((String)newValue);
				return;
			case EclipsePackage.FEATURE__PROVIDER_NAME:
				setProviderName((String)newValue);
				return;
			case EclipsePackage.FEATURE__VERSION:
				setVersion((String)newValue);
				return;
			case EclipsePackage.FEATURE__WS:
				setWs((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EclipsePackage.FEATURE__INSTALL_HANDLER:
				setInstallHandler((InstallHandler)null);
				return;
			case EclipsePackage.FEATURE__DESCRIPTION:
				setDescription((Description)null);
				return;
			case EclipsePackage.FEATURE__COPYRIGHT:
				setCopyright((Description)null);
				return;
			case EclipsePackage.FEATURE__LICENSE:
				setLicense((Description)null);
				return;
			case EclipsePackage.FEATURE__URL:
				setUrl((UrlType)null);
				return;
			case EclipsePackage.FEATURE__INCLUDES:
				getIncludes().clear();
				return;
			case EclipsePackage.FEATURE__REQUIRES:
				setRequires((ImportRequires)null);
				return;
			case EclipsePackage.FEATURE__PLUGIN:
				getPlugin().clear();
				return;
			case EclipsePackage.FEATURE__ARCH:
				setArch(ARCH_EDEFAULT);
				return;
			case EclipsePackage.FEATURE__COLOCATION_AFFINITY:
				setColocationAffinity(COLOCATION_AFFINITY_EDEFAULT);
				return;
			case EclipsePackage.FEATURE__EXCLUSIVE:
				unsetExclusive();
				return;
			case EclipsePackage.FEATURE__ID:
				setId(ID_EDEFAULT);
				return;
			case EclipsePackage.FEATURE__IMAGE:
				setImage(IMAGE_EDEFAULT);
				return;
			case EclipsePackage.FEATURE__LABEL:
				setLabel(LABEL_EDEFAULT);
				return;
			case EclipsePackage.FEATURE__NL:
				setNl(NL_EDEFAULT);
				return;
			case EclipsePackage.FEATURE__OS:
				setOs(OS_EDEFAULT);
				return;
			case EclipsePackage.FEATURE__PLUGIN1:
				setPlugin1(PLUGIN1_EDEFAULT);
				return;
			case EclipsePackage.FEATURE__PROVIDER_NAME:
				setProviderName(PROVIDER_NAME_EDEFAULT);
				return;
			case EclipsePackage.FEATURE__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
			case EclipsePackage.FEATURE__WS:
				setWs(WS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EclipsePackage.FEATURE__INSTALL_HANDLER:
				return installHandler != null;
			case EclipsePackage.FEATURE__DESCRIPTION:
				return description != null;
			case EclipsePackage.FEATURE__COPYRIGHT:
				return copyright != null;
			case EclipsePackage.FEATURE__LICENSE:
				return license != null;
			case EclipsePackage.FEATURE__URL:
				return url != null;
			case EclipsePackage.FEATURE__INCLUDES:
				return includes != null && !includes.isEmpty();
			case EclipsePackage.FEATURE__REQUIRES:
				return requires != null;
			case EclipsePackage.FEATURE__PLUGIN:
				return plugin != null && !plugin.isEmpty();
			case EclipsePackage.FEATURE__ARCH:
				return ARCH_EDEFAULT == null ? arch != null : !ARCH_EDEFAULT.equals(arch);
			case EclipsePackage.FEATURE__COLOCATION_AFFINITY:
				return COLOCATION_AFFINITY_EDEFAULT == null ? colocationAffinity != null : !COLOCATION_AFFINITY_EDEFAULT.equals(colocationAffinity);
			case EclipsePackage.FEATURE__EXCLUSIVE:
				return isSetExclusive();
			case EclipsePackage.FEATURE__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case EclipsePackage.FEATURE__IMAGE:
				return IMAGE_EDEFAULT == null ? image != null : !IMAGE_EDEFAULT.equals(image);
			case EclipsePackage.FEATURE__LABEL:
				return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT.equals(label);
			case EclipsePackage.FEATURE__NL:
				return NL_EDEFAULT == null ? nl != null : !NL_EDEFAULT.equals(nl);
			case EclipsePackage.FEATURE__OS:
				return OS_EDEFAULT == null ? os != null : !OS_EDEFAULT.equals(os);
			case EclipsePackage.FEATURE__PLUGIN1:
				return PLUGIN1_EDEFAULT == null ? plugin1 != null : !PLUGIN1_EDEFAULT.equals(plugin1);
			case EclipsePackage.FEATURE__PROVIDER_NAME:
				return PROVIDER_NAME_EDEFAULT == null ? providerName != null : !PROVIDER_NAME_EDEFAULT.equals(providerName);
			case EclipsePackage.FEATURE__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
			case EclipsePackage.FEATURE__WS:
				return WS_EDEFAULT == null ? ws != null : !WS_EDEFAULT.equals(ws);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (arch: ");
		result.append(arch);
		result.append(", colocationAffinity: ");
		result.append(colocationAffinity);
		result.append(", exclusive: ");
		if (exclusiveESet) result.append(exclusive); else result.append("<unset>");
		result.append(", id: ");
		result.append(id);
		result.append(", image: ");
		result.append(image);
		result.append(", label: ");
		result.append(label);
		result.append(", nl: ");
		result.append(nl);
		result.append(", os: ");
		result.append(os);
		result.append(", plugin1: ");
		result.append(plugin1);
		result.append(", providerName: ");
		result.append(providerName);
		result.append(", version: ");
		result.append(version);
		result.append(", ws: ");
		result.append(ws);
		result.append(')');
		return result.toString();
	}

} //FeatureImpl
