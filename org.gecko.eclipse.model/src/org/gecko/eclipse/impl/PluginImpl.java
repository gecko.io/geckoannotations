/**
 */
package org.gecko.eclipse.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.gecko.eclipse.EclipsePackage;
import org.gecko.eclipse.Plugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Plugin</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.impl.PluginImpl#getArch <em>Arch</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.PluginImpl#getDownloadSize <em>Download Size</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.PluginImpl#isFragment <em>Fragment</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.PluginImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.PluginImpl#getInstallSize <em>Install Size</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.PluginImpl#getNl <em>Nl</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.PluginImpl#getOs <em>Os</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.PluginImpl#isUnpack <em>Unpack</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.PluginImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.PluginImpl#getWs <em>Ws</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PluginImpl extends MinimalEObjectImpl.Container implements Plugin {
	/**
	 * The default value of the '{@link #getArch() <em>Arch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArch()
	 * @generated
	 * @ordered
	 */
	protected static final String ARCH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getArch() <em>Arch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArch()
	 * @generated
	 * @ordered
	 */
	protected String arch = ARCH_EDEFAULT;

	/**
	 * The default value of the '{@link #getDownloadSize() <em>Download Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDownloadSize()
	 * @generated
	 * @ordered
	 */
	protected static final int DOWNLOAD_SIZE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDownloadSize() <em>Download Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDownloadSize()
	 * @generated
	 * @ordered
	 */
	protected int downloadSize = DOWNLOAD_SIZE_EDEFAULT;

	/**
	 * This is true if the Download Size attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean downloadSizeESet;

	/**
	 * The default value of the '{@link #isFragment() <em>Fragment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFragment()
	 * @generated
	 * @ordered
	 */
	protected static final boolean FRAGMENT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isFragment() <em>Fragment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFragment()
	 * @generated
	 * @ordered
	 */
	protected boolean fragment = FRAGMENT_EDEFAULT;

	/**
	 * This is true if the Fragment attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean fragmentESet;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getInstallSize() <em>Install Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstallSize()
	 * @generated
	 * @ordered
	 */
	protected static final int INSTALL_SIZE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getInstallSize() <em>Install Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstallSize()
	 * @generated
	 * @ordered
	 */
	protected int installSize = INSTALL_SIZE_EDEFAULT;

	/**
	 * This is true if the Install Size attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean installSizeESet;

	/**
	 * The default value of the '{@link #getNl() <em>Nl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNl()
	 * @generated
	 * @ordered
	 */
	protected static final String NL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNl() <em>Nl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNl()
	 * @generated
	 * @ordered
	 */
	protected String nl = NL_EDEFAULT;

	/**
	 * The default value of the '{@link #getOs() <em>Os</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOs()
	 * @generated
	 * @ordered
	 */
	protected static final String OS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOs() <em>Os</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOs()
	 * @generated
	 * @ordered
	 */
	protected String os = OS_EDEFAULT;

	/**
	 * The default value of the '{@link #isUnpack() <em>Unpack</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUnpack()
	 * @generated
	 * @ordered
	 */
	protected static final boolean UNPACK_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isUnpack() <em>Unpack</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUnpack()
	 * @generated
	 * @ordered
	 */
	protected boolean unpack = UNPACK_EDEFAULT;

	/**
	 * This is true if the Unpack attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean unpackESet;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getWs() <em>Ws</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWs()
	 * @generated
	 * @ordered
	 */
	protected static final String WS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWs() <em>Ws</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWs()
	 * @generated
	 * @ordered
	 */
	protected String ws = WS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PluginImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EclipsePackage.Literals.PLUGIN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getArch() {
		return arch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setArch(String newArch) {
		String oldArch = arch;
		arch = newArch;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.PLUGIN__ARCH, oldArch, arch));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getDownloadSize() {
		return downloadSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDownloadSize(int newDownloadSize) {
		int oldDownloadSize = downloadSize;
		downloadSize = newDownloadSize;
		boolean oldDownloadSizeESet = downloadSizeESet;
		downloadSizeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.PLUGIN__DOWNLOAD_SIZE, oldDownloadSize, downloadSize, !oldDownloadSizeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetDownloadSize() {
		int oldDownloadSize = downloadSize;
		boolean oldDownloadSizeESet = downloadSizeESet;
		downloadSize = DOWNLOAD_SIZE_EDEFAULT;
		downloadSizeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EclipsePackage.PLUGIN__DOWNLOAD_SIZE, oldDownloadSize, DOWNLOAD_SIZE_EDEFAULT, oldDownloadSizeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetDownloadSize() {
		return downloadSizeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isFragment() {
		return fragment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFragment(boolean newFragment) {
		boolean oldFragment = fragment;
		fragment = newFragment;
		boolean oldFragmentESet = fragmentESet;
		fragmentESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.PLUGIN__FRAGMENT, oldFragment, fragment, !oldFragmentESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetFragment() {
		boolean oldFragment = fragment;
		boolean oldFragmentESet = fragmentESet;
		fragment = FRAGMENT_EDEFAULT;
		fragmentESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EclipsePackage.PLUGIN__FRAGMENT, oldFragment, FRAGMENT_EDEFAULT, oldFragmentESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetFragment() {
		return fragmentESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.PLUGIN__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getInstallSize() {
		return installSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInstallSize(int newInstallSize) {
		int oldInstallSize = installSize;
		installSize = newInstallSize;
		boolean oldInstallSizeESet = installSizeESet;
		installSizeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.PLUGIN__INSTALL_SIZE, oldInstallSize, installSize, !oldInstallSizeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetInstallSize() {
		int oldInstallSize = installSize;
		boolean oldInstallSizeESet = installSizeESet;
		installSize = INSTALL_SIZE_EDEFAULT;
		installSizeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EclipsePackage.PLUGIN__INSTALL_SIZE, oldInstallSize, INSTALL_SIZE_EDEFAULT, oldInstallSizeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetInstallSize() {
		return installSizeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getNl() {
		return nl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNl(String newNl) {
		String oldNl = nl;
		nl = newNl;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.PLUGIN__NL, oldNl, nl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOs() {
		return os;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOs(String newOs) {
		String oldOs = os;
		os = newOs;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.PLUGIN__OS, oldOs, os));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isUnpack() {
		return unpack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUnpack(boolean newUnpack) {
		boolean oldUnpack = unpack;
		unpack = newUnpack;
		boolean oldUnpackESet = unpackESet;
		unpackESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.PLUGIN__UNPACK, oldUnpack, unpack, !oldUnpackESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetUnpack() {
		boolean oldUnpack = unpack;
		boolean oldUnpackESet = unpackESet;
		unpack = UNPACK_EDEFAULT;
		unpackESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EclipsePackage.PLUGIN__UNPACK, oldUnpack, UNPACK_EDEFAULT, oldUnpackESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetUnpack() {
		return unpackESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.PLUGIN__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getWs() {
		return ws;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setWs(String newWs) {
		String oldWs = ws;
		ws = newWs;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.PLUGIN__WS, oldWs, ws));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EclipsePackage.PLUGIN__ARCH:
				return getArch();
			case EclipsePackage.PLUGIN__DOWNLOAD_SIZE:
				return getDownloadSize();
			case EclipsePackage.PLUGIN__FRAGMENT:
				return isFragment();
			case EclipsePackage.PLUGIN__ID:
				return getId();
			case EclipsePackage.PLUGIN__INSTALL_SIZE:
				return getInstallSize();
			case EclipsePackage.PLUGIN__NL:
				return getNl();
			case EclipsePackage.PLUGIN__OS:
				return getOs();
			case EclipsePackage.PLUGIN__UNPACK:
				return isUnpack();
			case EclipsePackage.PLUGIN__VERSION:
				return getVersion();
			case EclipsePackage.PLUGIN__WS:
				return getWs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EclipsePackage.PLUGIN__ARCH:
				setArch((String)newValue);
				return;
			case EclipsePackage.PLUGIN__DOWNLOAD_SIZE:
				setDownloadSize((Integer)newValue);
				return;
			case EclipsePackage.PLUGIN__FRAGMENT:
				setFragment((Boolean)newValue);
				return;
			case EclipsePackage.PLUGIN__ID:
				setId((String)newValue);
				return;
			case EclipsePackage.PLUGIN__INSTALL_SIZE:
				setInstallSize((Integer)newValue);
				return;
			case EclipsePackage.PLUGIN__NL:
				setNl((String)newValue);
				return;
			case EclipsePackage.PLUGIN__OS:
				setOs((String)newValue);
				return;
			case EclipsePackage.PLUGIN__UNPACK:
				setUnpack((Boolean)newValue);
				return;
			case EclipsePackage.PLUGIN__VERSION:
				setVersion((String)newValue);
				return;
			case EclipsePackage.PLUGIN__WS:
				setWs((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EclipsePackage.PLUGIN__ARCH:
				setArch(ARCH_EDEFAULT);
				return;
			case EclipsePackage.PLUGIN__DOWNLOAD_SIZE:
				unsetDownloadSize();
				return;
			case EclipsePackage.PLUGIN__FRAGMENT:
				unsetFragment();
				return;
			case EclipsePackage.PLUGIN__ID:
				setId(ID_EDEFAULT);
				return;
			case EclipsePackage.PLUGIN__INSTALL_SIZE:
				unsetInstallSize();
				return;
			case EclipsePackage.PLUGIN__NL:
				setNl(NL_EDEFAULT);
				return;
			case EclipsePackage.PLUGIN__OS:
				setOs(OS_EDEFAULT);
				return;
			case EclipsePackage.PLUGIN__UNPACK:
				unsetUnpack();
				return;
			case EclipsePackage.PLUGIN__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
			case EclipsePackage.PLUGIN__WS:
				setWs(WS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EclipsePackage.PLUGIN__ARCH:
				return ARCH_EDEFAULT == null ? arch != null : !ARCH_EDEFAULT.equals(arch);
			case EclipsePackage.PLUGIN__DOWNLOAD_SIZE:
				return isSetDownloadSize();
			case EclipsePackage.PLUGIN__FRAGMENT:
				return isSetFragment();
			case EclipsePackage.PLUGIN__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case EclipsePackage.PLUGIN__INSTALL_SIZE:
				return isSetInstallSize();
			case EclipsePackage.PLUGIN__NL:
				return NL_EDEFAULT == null ? nl != null : !NL_EDEFAULT.equals(nl);
			case EclipsePackage.PLUGIN__OS:
				return OS_EDEFAULT == null ? os != null : !OS_EDEFAULT.equals(os);
			case EclipsePackage.PLUGIN__UNPACK:
				return isSetUnpack();
			case EclipsePackage.PLUGIN__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
			case EclipsePackage.PLUGIN__WS:
				return WS_EDEFAULT == null ? ws != null : !WS_EDEFAULT.equals(ws);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (arch: ");
		result.append(arch);
		result.append(", downloadSize: ");
		if (downloadSizeESet) result.append(downloadSize); else result.append("<unset>");
		result.append(", fragment: ");
		if (fragmentESet) result.append(fragment); else result.append("<unset>");
		result.append(", id: ");
		result.append(id);
		result.append(", installSize: ");
		if (installSizeESet) result.append(installSize); else result.append("<unset>");
		result.append(", nl: ");
		result.append(nl);
		result.append(", os: ");
		result.append(os);
		result.append(", unpack: ");
		if (unpackESet) result.append(unpack); else result.append("<unset>");
		result.append(", version: ");
		result.append(version);
		result.append(", ws: ");
		result.append(ws);
		result.append(')');
		return result.toString();
	}

} //PluginImpl
