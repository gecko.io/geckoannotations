/**
 */
package org.gecko.eclipse.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.xml.type.XMLTypeFactory;
import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import org.gecko.eclipse.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EclipseFactoryImpl extends EFactoryImpl implements EclipseFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static EclipseFactory init() {
		try {
			EclipseFactory theEclipseFactory = (EclipseFactory)EPackage.Registry.INSTANCE.getEFactory(EclipsePackage.eNS_URI);
			if (theEclipseFactory != null) {
				return theEclipseFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new EclipseFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EclipseFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case EclipsePackage.ARTIFACT: return createArtifact();
			case EclipsePackage.ARTIFACTS: return createArtifacts();
			case EclipsePackage.CATEGORY_DEF: return createCategoryDef();
			case EclipsePackage.CATEGORY_FEATURE: return createCategoryFeature();
			case EclipsePackage.CATEGORY_SITE: return createCategorySite();
			case EclipsePackage.CATEGORY_TYPE: return createCategoryType();
			case EclipsePackage.CHILDREN_TYPE: return createChildrenType();
			case EclipsePackage.DESCRIPTION: return createDescription();
			case EclipsePackage.DOCUMENT_ROOT: return createDocumentRoot();
			case EclipsePackage.FEATURE: return createFeature();
			case EclipsePackage.IMPORT: return createImport();
			case EclipsePackage.IMPORT_REQUIRES: return createImportRequires();
			case EclipsePackage.INCLUDE: return createInclude();
			case EclipsePackage.INSTALL_HANDLER: return createInstallHandler();
			case EclipsePackage.INSTRUCTION: return createInstruction();
			case EclipsePackage.INSTRUCTIONS_TYPE: return createInstructionsType();
			case EclipsePackage.LAUNCHER_ARGS: return createLauncherArgs();
			case EclipsePackage.LICENSES_TYPE: return createLicensesType();
			case EclipsePackage.MAPPINGS_TYPE: return createMappingsType();
			case EclipsePackage.PLUGIN: return createPlugin();
			case EclipsePackage.PROCESSING: return createProcessing();
			case EclipsePackage.PROCESS_STEP: return createProcessStep();
			case EclipsePackage.PROPERTIES: return createProperties();
			case EclipsePackage.PROPERTY: return createProperty();
			case EclipsePackage.PROVIDED: return createProvided();
			case EclipsePackage.PROVIDES: return createProvides();
			case EclipsePackage.REFERENCES_TYPE: return createReferencesType();
			case EclipsePackage.REPOSITORY: return createRepository();
			case EclipsePackage.REPOSITORY_REF: return createRepositoryRef();
			case EclipsePackage.REPOSITORY_REFS: return createRepositoryRefs();
			case EclipsePackage.REQUIRED: return createRequired();
			case EclipsePackage.REQUIRED_PROPERTIES: return createRequiredProperties();
			case EclipsePackage.REQUIRES: return createRequires();
			case EclipsePackage.RULE: return createRule();
			case EclipsePackage.SITE: return createSite();
			case EclipsePackage.TARGET: return createTarget();
			case EclipsePackage.TARGET_DEPENDENCIES: return createTargetDependencies();
			case EclipsePackage.TARGET_ENVIRONMENT: return createTargetEnvironment();
			case EclipsePackage.TARGET_JRE: return createTargetJRE();
			case EclipsePackage.TARGET_LOCATION: return createTargetLocation();
			case EclipsePackage.TARGET_LOCATIONS: return createTargetLocations();
			case EclipsePackage.TOUCHPOINT: return createTouchpoint();
			case EclipsePackage.TOUCHPOINT_DATA: return createTouchpointData();
			case EclipsePackage.UNIT: return createUnit();
			case EclipsePackage.UNITS_TYPE: return createUnitsType();
			case EclipsePackage.TOUCHPOINT_DATA_TYPE: return createTouchpointDataType();
			case EclipsePackage.URL_TYPE: return createUrlType();
			case EclipsePackage.UPDATE: return createUpdate();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case EclipsePackage.LOCATION_INCLUDE_TYPE:
				return createLocationIncludeTypeFromString(eDataType, initialValue);
			case EclipsePackage.MATCH_TYPE:
				return createMatchTypeFromString(eDataType, initialValue);
			case EclipsePackage.TARGET_TYPE:
				return createTargetTypeFromString(eDataType, initialValue);
			case EclipsePackage.LOCATION_INCLUDE_TYPE_OBJECT:
				return createLocationIncludeTypeObjectFromString(eDataType, initialValue);
			case EclipsePackage.MATCH_TYPE_OBJECT:
				return createMatchTypeObjectFromString(eDataType, initialValue);
			case EclipsePackage.TARGET_TYPE_OBJECT:
				return createTargetTypeObjectFromString(eDataType, initialValue);
			case EclipsePackage.VERSION:
				return createVersionFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case EclipsePackage.LOCATION_INCLUDE_TYPE:
				return convertLocationIncludeTypeToString(eDataType, instanceValue);
			case EclipsePackage.MATCH_TYPE:
				return convertMatchTypeToString(eDataType, instanceValue);
			case EclipsePackage.TARGET_TYPE:
				return convertTargetTypeToString(eDataType, instanceValue);
			case EclipsePackage.LOCATION_INCLUDE_TYPE_OBJECT:
				return convertLocationIncludeTypeObjectToString(eDataType, instanceValue);
			case EclipsePackage.MATCH_TYPE_OBJECT:
				return convertMatchTypeObjectToString(eDataType, instanceValue);
			case EclipsePackage.TARGET_TYPE_OBJECT:
				return convertTargetTypeObjectToString(eDataType, instanceValue);
			case EclipsePackage.VERSION:
				return convertVersionToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Artifact createArtifact() {
		ArtifactImpl artifact = new ArtifactImpl();
		return artifact;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Artifacts createArtifacts() {
		ArtifactsImpl artifacts = new ArtifactsImpl();
		return artifacts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CategoryDef createCategoryDef() {
		CategoryDefImpl categoryDef = new CategoryDefImpl();
		return categoryDef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CategoryFeature createCategoryFeature() {
		CategoryFeatureImpl categoryFeature = new CategoryFeatureImpl();
		return categoryFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CategorySite createCategorySite() {
		CategorySiteImpl categorySite = new CategorySiteImpl();
		return categorySite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CategoryType createCategoryType() {
		CategoryTypeImpl categoryType = new CategoryTypeImpl();
		return categoryType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ChildrenType createChildrenType() {
		ChildrenTypeImpl childrenType = new ChildrenTypeImpl();
		return childrenType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Description createDescription() {
		DescriptionImpl description = new DescriptionImpl();
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DocumentRoot createDocumentRoot() {
		DocumentRootImpl documentRoot = new DocumentRootImpl();
		return documentRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Feature createFeature() {
		FeatureImpl feature = new FeatureImpl();
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Import createImport() {
		ImportImpl import_ = new ImportImpl();
		return import_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ImportRequires createImportRequires() {
		ImportRequiresImpl importRequires = new ImportRequiresImpl();
		return importRequires;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Include createInclude() {
		IncludeImpl include = new IncludeImpl();
		return include;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InstallHandler createInstallHandler() {
		InstallHandlerImpl installHandler = new InstallHandlerImpl();
		return installHandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Instruction createInstruction() {
		InstructionImpl instruction = new InstructionImpl();
		return instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InstructionsType createInstructionsType() {
		InstructionsTypeImpl instructionsType = new InstructionsTypeImpl();
		return instructionsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LauncherArgs createLauncherArgs() {
		LauncherArgsImpl launcherArgs = new LauncherArgsImpl();
		return launcherArgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LicensesType createLicensesType() {
		LicensesTypeImpl licensesType = new LicensesTypeImpl();
		return licensesType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MappingsType createMappingsType() {
		MappingsTypeImpl mappingsType = new MappingsTypeImpl();
		return mappingsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Plugin createPlugin() {
		PluginImpl plugin = new PluginImpl();
		return plugin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Processing createProcessing() {
		ProcessingImpl processing = new ProcessingImpl();
		return processing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ProcessStep createProcessStep() {
		ProcessStepImpl processStep = new ProcessStepImpl();
		return processStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Properties createProperties() {
		PropertiesImpl properties = new PropertiesImpl();
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Property createProperty() {
		PropertyImpl property = new PropertyImpl();
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Provided createProvided() {
		ProvidedImpl provided = new ProvidedImpl();
		return provided;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Provides createProvides() {
		ProvidesImpl provides = new ProvidesImpl();
		return provides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ReferencesType createReferencesType() {
		ReferencesTypeImpl referencesType = new ReferencesTypeImpl();
		return referencesType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Repository createRepository() {
		RepositoryImpl repository = new RepositoryImpl();
		return repository;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RepositoryRef createRepositoryRef() {
		RepositoryRefImpl repositoryRef = new RepositoryRefImpl();
		return repositoryRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RepositoryRefs createRepositoryRefs() {
		RepositoryRefsImpl repositoryRefs = new RepositoryRefsImpl();
		return repositoryRefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Required createRequired() {
		RequiredImpl required = new RequiredImpl();
		return required;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Requires createRequires() {
		RequiresImpl requires = new RequiresImpl();
		return requires;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Rule createRule() {
		RuleImpl rule = new RuleImpl();
		return rule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Site createSite() {
		SiteImpl site = new SiteImpl();
		return site;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Target createTarget() {
		TargetImpl target = new TargetImpl();
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TargetDependencies createTargetDependencies() {
		TargetDependenciesImpl targetDependencies = new TargetDependenciesImpl();
		return targetDependencies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TargetEnvironment createTargetEnvironment() {
		TargetEnvironmentImpl targetEnvironment = new TargetEnvironmentImpl();
		return targetEnvironment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TargetJRE createTargetJRE() {
		TargetJREImpl targetJRE = new TargetJREImpl();
		return targetJRE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TargetLocation createTargetLocation() {
		TargetLocationImpl targetLocation = new TargetLocationImpl();
		return targetLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TargetLocations createTargetLocations() {
		TargetLocationsImpl targetLocations = new TargetLocationsImpl();
		return targetLocations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Touchpoint createTouchpoint() {
		TouchpointImpl touchpoint = new TouchpointImpl();
		return touchpoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TouchpointData createTouchpointData() {
		TouchpointDataImpl touchpointData = new TouchpointDataImpl();
		return touchpointData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TouchpointDataType createTouchpointDataType() {
		TouchpointDataTypeImpl touchpointDataType = new TouchpointDataTypeImpl();
		return touchpointDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Unit createUnit() {
		UnitImpl unit = new UnitImpl();
		return unit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UnitsType createUnitsType() {
		UnitsTypeImpl unitsType = new UnitsTypeImpl();
		return unitsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Update createUpdate() {
		UpdateImpl update = new UpdateImpl();
		return update;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UrlType createUrlType() {
		UrlTypeImpl urlType = new UrlTypeImpl();
		return urlType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RequiredProperties createRequiredProperties() {
		RequiredPropertiesImpl requiredProperties = new RequiredPropertiesImpl();
		return requiredProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocationIncludeType createLocationIncludeTypeFromString(EDataType eDataType, String initialValue) {
		LocationIncludeType result = LocationIncludeType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLocationIncludeTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MatchType createMatchTypeFromString(EDataType eDataType, String initialValue) {
		MatchType result = MatchType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMatchTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TargetType createTargetTypeFromString(EDataType eDataType, String initialValue) {
		TargetType result = TargetType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTargetTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocationIncludeType createLocationIncludeTypeObjectFromString(EDataType eDataType, String initialValue) {
		return createLocationIncludeTypeFromString(EclipsePackage.Literals.LOCATION_INCLUDE_TYPE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLocationIncludeTypeObjectToString(EDataType eDataType, Object instanceValue) {
		return convertLocationIncludeTypeToString(EclipsePackage.Literals.LOCATION_INCLUDE_TYPE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MatchType createMatchTypeObjectFromString(EDataType eDataType, String initialValue) {
		return createMatchTypeFromString(EclipsePackage.Literals.MATCH_TYPE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMatchTypeObjectToString(EDataType eDataType, Object instanceValue) {
		return convertMatchTypeToString(EclipsePackage.Literals.MATCH_TYPE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TargetType createTargetTypeObjectFromString(EDataType eDataType, String initialValue) {
		return createTargetTypeFromString(EclipsePackage.Literals.TARGET_TYPE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTargetTypeObjectToString(EDataType eDataType, Object instanceValue) {
		return convertTargetTypeToString(EclipsePackage.Literals.TARGET_TYPE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createVersionFromString(EDataType eDataType, String initialValue) {
		return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.STRING, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertVersionToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.STRING, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EclipsePackage getEclipsePackage() {
		return (EclipsePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static EclipsePackage getPackage() {
		return EclipsePackage.eINSTANCE;
	}

} //EclipseFactoryImpl
