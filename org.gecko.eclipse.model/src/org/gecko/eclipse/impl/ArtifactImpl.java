/**
 */
package org.gecko.eclipse.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.gecko.eclipse.Artifact;
import org.gecko.eclipse.EclipsePackage;
import org.gecko.eclipse.Processing;
import org.gecko.eclipse.Properties;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Artifact</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.impl.ArtifactImpl#getProcessing <em>Processing</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.ArtifactImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.ArtifactImpl#getRepositoryProperties <em>Repository Properties</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.ArtifactImpl#getClassifier <em>Classifier</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.ArtifactImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.ArtifactImpl#getVersion <em>Version</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ArtifactImpl extends MinimalEObjectImpl.Container implements Artifact {
	/**
	 * The cached value of the '{@link #getProcessing() <em>Processing</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessing()
	 * @generated
	 * @ordered
	 */
	protected Processing processing;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected Properties properties;

	/**
	 * The cached value of the '{@link #getRepositoryProperties() <em>Repository Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepositoryProperties()
	 * @generated
	 * @ordered
	 */
	protected Properties repositoryProperties;

	/**
	 * The default value of the '{@link #getClassifier() <em>Classifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassifier()
	 * @generated
	 * @ordered
	 */
	protected static final String CLASSIFIER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getClassifier() <em>Classifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassifier()
	 * @generated
	 * @ordered
	 */
	protected String classifier = CLASSIFIER_EDEFAULT;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArtifactImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EclipsePackage.Literals.ARTIFACT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Processing getProcessing() {
		return processing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcessing(Processing newProcessing, NotificationChain msgs) {
		Processing oldProcessing = processing;
		processing = newProcessing;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.ARTIFACT__PROCESSING, oldProcessing, newProcessing);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProcessing(Processing newProcessing) {
		if (newProcessing != processing) {
			NotificationChain msgs = null;
			if (processing != null)
				msgs = ((InternalEObject)processing).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.ARTIFACT__PROCESSING, null, msgs);
			if (newProcessing != null)
				msgs = ((InternalEObject)newProcessing).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.ARTIFACT__PROCESSING, null, msgs);
			msgs = basicSetProcessing(newProcessing, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.ARTIFACT__PROCESSING, newProcessing, newProcessing));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Properties getProperties() {
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProperties(Properties newProperties, NotificationChain msgs) {
		Properties oldProperties = properties;
		properties = newProperties;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.ARTIFACT__PROPERTIES, oldProperties, newProperties);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProperties(Properties newProperties) {
		if (newProperties != properties) {
			NotificationChain msgs = null;
			if (properties != null)
				msgs = ((InternalEObject)properties).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.ARTIFACT__PROPERTIES, null, msgs);
			if (newProperties != null)
				msgs = ((InternalEObject)newProperties).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.ARTIFACT__PROPERTIES, null, msgs);
			msgs = basicSetProperties(newProperties, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.ARTIFACT__PROPERTIES, newProperties, newProperties));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Properties getRepositoryProperties() {
		return repositoryProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRepositoryProperties(Properties newRepositoryProperties, NotificationChain msgs) {
		Properties oldRepositoryProperties = repositoryProperties;
		repositoryProperties = newRepositoryProperties;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.ARTIFACT__REPOSITORY_PROPERTIES, oldRepositoryProperties, newRepositoryProperties);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRepositoryProperties(Properties newRepositoryProperties) {
		if (newRepositoryProperties != repositoryProperties) {
			NotificationChain msgs = null;
			if (repositoryProperties != null)
				msgs = ((InternalEObject)repositoryProperties).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.ARTIFACT__REPOSITORY_PROPERTIES, null, msgs);
			if (newRepositoryProperties != null)
				msgs = ((InternalEObject)newRepositoryProperties).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.ARTIFACT__REPOSITORY_PROPERTIES, null, msgs);
			msgs = basicSetRepositoryProperties(newRepositoryProperties, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.ARTIFACT__REPOSITORY_PROPERTIES, newRepositoryProperties, newRepositoryProperties));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getClassifier() {
		return classifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setClassifier(String newClassifier) {
		String oldClassifier = classifier;
		classifier = newClassifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.ARTIFACT__CLASSIFIER, oldClassifier, classifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.ARTIFACT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.ARTIFACT__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EclipsePackage.ARTIFACT__PROCESSING:
				return basicSetProcessing(null, msgs);
			case EclipsePackage.ARTIFACT__PROPERTIES:
				return basicSetProperties(null, msgs);
			case EclipsePackage.ARTIFACT__REPOSITORY_PROPERTIES:
				return basicSetRepositoryProperties(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EclipsePackage.ARTIFACT__PROCESSING:
				return getProcessing();
			case EclipsePackage.ARTIFACT__PROPERTIES:
				return getProperties();
			case EclipsePackage.ARTIFACT__REPOSITORY_PROPERTIES:
				return getRepositoryProperties();
			case EclipsePackage.ARTIFACT__CLASSIFIER:
				return getClassifier();
			case EclipsePackage.ARTIFACT__ID:
				return getId();
			case EclipsePackage.ARTIFACT__VERSION:
				return getVersion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EclipsePackage.ARTIFACT__PROCESSING:
				setProcessing((Processing)newValue);
				return;
			case EclipsePackage.ARTIFACT__PROPERTIES:
				setProperties((Properties)newValue);
				return;
			case EclipsePackage.ARTIFACT__REPOSITORY_PROPERTIES:
				setRepositoryProperties((Properties)newValue);
				return;
			case EclipsePackage.ARTIFACT__CLASSIFIER:
				setClassifier((String)newValue);
				return;
			case EclipsePackage.ARTIFACT__ID:
				setId((String)newValue);
				return;
			case EclipsePackage.ARTIFACT__VERSION:
				setVersion((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EclipsePackage.ARTIFACT__PROCESSING:
				setProcessing((Processing)null);
				return;
			case EclipsePackage.ARTIFACT__PROPERTIES:
				setProperties((Properties)null);
				return;
			case EclipsePackage.ARTIFACT__REPOSITORY_PROPERTIES:
				setRepositoryProperties((Properties)null);
				return;
			case EclipsePackage.ARTIFACT__CLASSIFIER:
				setClassifier(CLASSIFIER_EDEFAULT);
				return;
			case EclipsePackage.ARTIFACT__ID:
				setId(ID_EDEFAULT);
				return;
			case EclipsePackage.ARTIFACT__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EclipsePackage.ARTIFACT__PROCESSING:
				return processing != null;
			case EclipsePackage.ARTIFACT__PROPERTIES:
				return properties != null;
			case EclipsePackage.ARTIFACT__REPOSITORY_PROPERTIES:
				return repositoryProperties != null;
			case EclipsePackage.ARTIFACT__CLASSIFIER:
				return CLASSIFIER_EDEFAULT == null ? classifier != null : !CLASSIFIER_EDEFAULT.equals(classifier);
			case EclipsePackage.ARTIFACT__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case EclipsePackage.ARTIFACT__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (classifier: ");
		result.append(classifier);
		result.append(", id: ");
		result.append(id);
		result.append(", version: ");
		result.append(version);
		result.append(')');
		return result.toString();
	}

} //ArtifactImpl
