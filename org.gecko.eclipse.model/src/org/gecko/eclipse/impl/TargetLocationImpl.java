/**
 */
package org.gecko.eclipse.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.gecko.eclipse.EclipsePackage;
import org.gecko.eclipse.LocationIncludeType;
import org.gecko.eclipse.RepositoryRef;
import org.gecko.eclipse.TargetLocation;
import org.gecko.eclipse.TargetType;
import org.gecko.eclipse.Unit;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Target Location</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.impl.TargetLocationImpl#getRepository <em>Repository</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.TargetLocationImpl#getUnit <em>Unit</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.TargetLocationImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.TargetLocationImpl#isIncludeAllPlatforms <em>Include All Platforms</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.TargetLocationImpl#isIncludeConfigurePhase <em>Include Configure Phase</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.TargetLocationImpl#getIncludeMode <em>Include Mode</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.TargetLocationImpl#isIncludeSource <em>Include Source</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.TargetLocationImpl#getPath <em>Path</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.TargetLocationImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TargetLocationImpl extends MinimalEObjectImpl.Container implements TargetLocation {
	/**
	 * The cached value of the '{@link #getRepository() <em>Repository</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepository()
	 * @generated
	 * @ordered
	 */
	protected RepositoryRef repository;

	/**
	 * The cached value of the '{@link #getUnit() <em>Unit</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnit()
	 * @generated
	 * @ordered
	 */
	protected EList<Unit> unit;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #isIncludeAllPlatforms() <em>Include All Platforms</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIncludeAllPlatforms()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INCLUDE_ALL_PLATFORMS_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isIncludeAllPlatforms() <em>Include All Platforms</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIncludeAllPlatforms()
	 * @generated
	 * @ordered
	 */
	protected boolean includeAllPlatforms = INCLUDE_ALL_PLATFORMS_EDEFAULT;

	/**
	 * This is true if the Include All Platforms attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean includeAllPlatformsESet;

	/**
	 * The default value of the '{@link #isIncludeConfigurePhase() <em>Include Configure Phase</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIncludeConfigurePhase()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INCLUDE_CONFIGURE_PHASE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIncludeConfigurePhase() <em>Include Configure Phase</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIncludeConfigurePhase()
	 * @generated
	 * @ordered
	 */
	protected boolean includeConfigurePhase = INCLUDE_CONFIGURE_PHASE_EDEFAULT;

	/**
	 * This is true if the Include Configure Phase attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean includeConfigurePhaseESet;

	/**
	 * The default value of the '{@link #getIncludeMode() <em>Include Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncludeMode()
	 * @generated
	 * @ordered
	 */
	protected static final LocationIncludeType INCLUDE_MODE_EDEFAULT = LocationIncludeType.NONE;

	/**
	 * The cached value of the '{@link #getIncludeMode() <em>Include Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncludeMode()
	 * @generated
	 * @ordered
	 */
	protected LocationIncludeType includeMode = INCLUDE_MODE_EDEFAULT;

	/**
	 * This is true if the Include Mode attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean includeModeESet;

	/**
	 * The default value of the '{@link #isIncludeSource() <em>Include Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIncludeSource()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INCLUDE_SOURCE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isIncludeSource() <em>Include Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIncludeSource()
	 * @generated
	 * @ordered
	 */
	protected boolean includeSource = INCLUDE_SOURCE_EDEFAULT;

	/**
	 * This is true if the Include Source attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean includeSourceESet;

	/**
	 * The default value of the '{@link #getPath() <em>Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPath()
	 * @generated
	 * @ordered
	 */
	protected static final String PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPath() <em>Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPath()
	 * @generated
	 * @ordered
	 */
	protected String path = PATH_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final TargetType TYPE_EDEFAULT = TargetType.NONE;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected TargetType type = TYPE_EDEFAULT;

	/**
	 * This is true if the Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean typeESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TargetLocationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EclipsePackage.Literals.TARGET_LOCATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RepositoryRef getRepository() {
		return repository;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRepository(RepositoryRef newRepository, NotificationChain msgs) {
		RepositoryRef oldRepository = repository;
		repository = newRepository;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET_LOCATION__REPOSITORY, oldRepository, newRepository);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRepository(RepositoryRef newRepository) {
		if (newRepository != repository) {
			NotificationChain msgs = null;
			if (repository != null)
				msgs = ((InternalEObject)repository).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.TARGET_LOCATION__REPOSITORY, null, msgs);
			if (newRepository != null)
				msgs = ((InternalEObject)newRepository).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclipsePackage.TARGET_LOCATION__REPOSITORY, null, msgs);
			msgs = basicSetRepository(newRepository, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET_LOCATION__REPOSITORY, newRepository, newRepository));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Unit> getUnit() {
		if (unit == null) {
			unit = new EObjectContainmentEList<Unit>(Unit.class, this, EclipsePackage.TARGET_LOCATION__UNIT);
		}
		return unit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET_LOCATION__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isIncludeAllPlatforms() {
		return includeAllPlatforms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIncludeAllPlatforms(boolean newIncludeAllPlatforms) {
		boolean oldIncludeAllPlatforms = includeAllPlatforms;
		includeAllPlatforms = newIncludeAllPlatforms;
		boolean oldIncludeAllPlatformsESet = includeAllPlatformsESet;
		includeAllPlatformsESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET_LOCATION__INCLUDE_ALL_PLATFORMS, oldIncludeAllPlatforms, includeAllPlatforms, !oldIncludeAllPlatformsESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetIncludeAllPlatforms() {
		boolean oldIncludeAllPlatforms = includeAllPlatforms;
		boolean oldIncludeAllPlatformsESet = includeAllPlatformsESet;
		includeAllPlatforms = INCLUDE_ALL_PLATFORMS_EDEFAULT;
		includeAllPlatformsESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EclipsePackage.TARGET_LOCATION__INCLUDE_ALL_PLATFORMS, oldIncludeAllPlatforms, INCLUDE_ALL_PLATFORMS_EDEFAULT, oldIncludeAllPlatformsESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetIncludeAllPlatforms() {
		return includeAllPlatformsESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isIncludeConfigurePhase() {
		return includeConfigurePhase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIncludeConfigurePhase(boolean newIncludeConfigurePhase) {
		boolean oldIncludeConfigurePhase = includeConfigurePhase;
		includeConfigurePhase = newIncludeConfigurePhase;
		boolean oldIncludeConfigurePhaseESet = includeConfigurePhaseESet;
		includeConfigurePhaseESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET_LOCATION__INCLUDE_CONFIGURE_PHASE, oldIncludeConfigurePhase, includeConfigurePhase, !oldIncludeConfigurePhaseESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetIncludeConfigurePhase() {
		boolean oldIncludeConfigurePhase = includeConfigurePhase;
		boolean oldIncludeConfigurePhaseESet = includeConfigurePhaseESet;
		includeConfigurePhase = INCLUDE_CONFIGURE_PHASE_EDEFAULT;
		includeConfigurePhaseESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EclipsePackage.TARGET_LOCATION__INCLUDE_CONFIGURE_PHASE, oldIncludeConfigurePhase, INCLUDE_CONFIGURE_PHASE_EDEFAULT, oldIncludeConfigurePhaseESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetIncludeConfigurePhase() {
		return includeConfigurePhaseESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LocationIncludeType getIncludeMode() {
		return includeMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIncludeMode(LocationIncludeType newIncludeMode) {
		LocationIncludeType oldIncludeMode = includeMode;
		includeMode = newIncludeMode == null ? INCLUDE_MODE_EDEFAULT : newIncludeMode;
		boolean oldIncludeModeESet = includeModeESet;
		includeModeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET_LOCATION__INCLUDE_MODE, oldIncludeMode, includeMode, !oldIncludeModeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetIncludeMode() {
		LocationIncludeType oldIncludeMode = includeMode;
		boolean oldIncludeModeESet = includeModeESet;
		includeMode = INCLUDE_MODE_EDEFAULT;
		includeModeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EclipsePackage.TARGET_LOCATION__INCLUDE_MODE, oldIncludeMode, INCLUDE_MODE_EDEFAULT, oldIncludeModeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetIncludeMode() {
		return includeModeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isIncludeSource() {
		return includeSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIncludeSource(boolean newIncludeSource) {
		boolean oldIncludeSource = includeSource;
		includeSource = newIncludeSource;
		boolean oldIncludeSourceESet = includeSourceESet;
		includeSourceESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET_LOCATION__INCLUDE_SOURCE, oldIncludeSource, includeSource, !oldIncludeSourceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetIncludeSource() {
		boolean oldIncludeSource = includeSource;
		boolean oldIncludeSourceESet = includeSourceESet;
		includeSource = INCLUDE_SOURCE_EDEFAULT;
		includeSourceESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EclipsePackage.TARGET_LOCATION__INCLUDE_SOURCE, oldIncludeSource, INCLUDE_SOURCE_EDEFAULT, oldIncludeSourceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetIncludeSource() {
		return includeSourceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPath() {
		return path;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPath(String newPath) {
		String oldPath = path;
		path = newPath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET_LOCATION__PATH, oldPath, path));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TargetType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(TargetType newType) {
		TargetType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		boolean oldTypeESet = typeESet;
		typeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET_LOCATION__TYPE, oldType, type, !oldTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetType() {
		TargetType oldType = type;
		boolean oldTypeESet = typeESet;
		type = TYPE_EDEFAULT;
		typeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EclipsePackage.TARGET_LOCATION__TYPE, oldType, TYPE_EDEFAULT, oldTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetType() {
		return typeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EclipsePackage.TARGET_LOCATION__REPOSITORY:
				return basicSetRepository(null, msgs);
			case EclipsePackage.TARGET_LOCATION__UNIT:
				return ((InternalEList<?>)getUnit()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EclipsePackage.TARGET_LOCATION__REPOSITORY:
				return getRepository();
			case EclipsePackage.TARGET_LOCATION__UNIT:
				return getUnit();
			case EclipsePackage.TARGET_LOCATION__ID:
				return getId();
			case EclipsePackage.TARGET_LOCATION__INCLUDE_ALL_PLATFORMS:
				return isIncludeAllPlatforms();
			case EclipsePackage.TARGET_LOCATION__INCLUDE_CONFIGURE_PHASE:
				return isIncludeConfigurePhase();
			case EclipsePackage.TARGET_LOCATION__INCLUDE_MODE:
				return getIncludeMode();
			case EclipsePackage.TARGET_LOCATION__INCLUDE_SOURCE:
				return isIncludeSource();
			case EclipsePackage.TARGET_LOCATION__PATH:
				return getPath();
			case EclipsePackage.TARGET_LOCATION__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EclipsePackage.TARGET_LOCATION__REPOSITORY:
				setRepository((RepositoryRef)newValue);
				return;
			case EclipsePackage.TARGET_LOCATION__UNIT:
				getUnit().clear();
				getUnit().addAll((Collection<? extends Unit>)newValue);
				return;
			case EclipsePackage.TARGET_LOCATION__ID:
				setId((String)newValue);
				return;
			case EclipsePackage.TARGET_LOCATION__INCLUDE_ALL_PLATFORMS:
				setIncludeAllPlatforms((Boolean)newValue);
				return;
			case EclipsePackage.TARGET_LOCATION__INCLUDE_CONFIGURE_PHASE:
				setIncludeConfigurePhase((Boolean)newValue);
				return;
			case EclipsePackage.TARGET_LOCATION__INCLUDE_MODE:
				setIncludeMode((LocationIncludeType)newValue);
				return;
			case EclipsePackage.TARGET_LOCATION__INCLUDE_SOURCE:
				setIncludeSource((Boolean)newValue);
				return;
			case EclipsePackage.TARGET_LOCATION__PATH:
				setPath((String)newValue);
				return;
			case EclipsePackage.TARGET_LOCATION__TYPE:
				setType((TargetType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EclipsePackage.TARGET_LOCATION__REPOSITORY:
				setRepository((RepositoryRef)null);
				return;
			case EclipsePackage.TARGET_LOCATION__UNIT:
				getUnit().clear();
				return;
			case EclipsePackage.TARGET_LOCATION__ID:
				setId(ID_EDEFAULT);
				return;
			case EclipsePackage.TARGET_LOCATION__INCLUDE_ALL_PLATFORMS:
				unsetIncludeAllPlatforms();
				return;
			case EclipsePackage.TARGET_LOCATION__INCLUDE_CONFIGURE_PHASE:
				unsetIncludeConfigurePhase();
				return;
			case EclipsePackage.TARGET_LOCATION__INCLUDE_MODE:
				unsetIncludeMode();
				return;
			case EclipsePackage.TARGET_LOCATION__INCLUDE_SOURCE:
				unsetIncludeSource();
				return;
			case EclipsePackage.TARGET_LOCATION__PATH:
				setPath(PATH_EDEFAULT);
				return;
			case EclipsePackage.TARGET_LOCATION__TYPE:
				unsetType();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EclipsePackage.TARGET_LOCATION__REPOSITORY:
				return repository != null;
			case EclipsePackage.TARGET_LOCATION__UNIT:
				return unit != null && !unit.isEmpty();
			case EclipsePackage.TARGET_LOCATION__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case EclipsePackage.TARGET_LOCATION__INCLUDE_ALL_PLATFORMS:
				return isSetIncludeAllPlatforms();
			case EclipsePackage.TARGET_LOCATION__INCLUDE_CONFIGURE_PHASE:
				return isSetIncludeConfigurePhase();
			case EclipsePackage.TARGET_LOCATION__INCLUDE_MODE:
				return isSetIncludeMode();
			case EclipsePackage.TARGET_LOCATION__INCLUDE_SOURCE:
				return isSetIncludeSource();
			case EclipsePackage.TARGET_LOCATION__PATH:
				return PATH_EDEFAULT == null ? path != null : !PATH_EDEFAULT.equals(path);
			case EclipsePackage.TARGET_LOCATION__TYPE:
				return isSetType();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", includeAllPlatforms: ");
		if (includeAllPlatformsESet) result.append(includeAllPlatforms); else result.append("<unset>");
		result.append(", includeConfigurePhase: ");
		if (includeConfigurePhaseESet) result.append(includeConfigurePhase); else result.append("<unset>");
		result.append(", includeMode: ");
		if (includeModeESet) result.append(includeMode); else result.append("<unset>");
		result.append(", includeSource: ");
		if (includeSourceESet) result.append(includeSource); else result.append("<unset>");
		result.append(", path: ");
		result.append(path);
		result.append(", type: ");
		if (typeESet) result.append(type); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //TargetLocationImpl
