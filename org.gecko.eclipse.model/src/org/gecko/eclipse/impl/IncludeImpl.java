/**
 */
package org.gecko.eclipse.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.gecko.eclipse.EclipsePackage;
import org.gecko.eclipse.Include;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Include</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.impl.IncludeImpl#getArch <em>Arch</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.IncludeImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.IncludeImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.IncludeImpl#getNl <em>Nl</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.IncludeImpl#isOptional <em>Optional</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.IncludeImpl#getOs <em>Os</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.IncludeImpl#getSearchLocation <em>Search Location</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.IncludeImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.IncludeImpl#getWs <em>Ws</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IncludeImpl extends MinimalEObjectImpl.Container implements Include {
	/**
	 * The default value of the '{@link #getArch() <em>Arch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArch()
	 * @generated
	 * @ordered
	 */
	protected static final String ARCH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getArch() <em>Arch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArch()
	 * @generated
	 * @ordered
	 */
	protected String arch = ARCH_EDEFAULT;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getNl() <em>Nl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNl()
	 * @generated
	 * @ordered
	 */
	protected static final String NL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNl() <em>Nl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNl()
	 * @generated
	 * @ordered
	 */
	protected String nl = NL_EDEFAULT;

	/**
	 * The default value of the '{@link #isOptional() <em>Optional</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOptional()
	 * @generated
	 * @ordered
	 */
	protected static final boolean OPTIONAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isOptional() <em>Optional</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOptional()
	 * @generated
	 * @ordered
	 */
	protected boolean optional = OPTIONAL_EDEFAULT;

	/**
	 * This is true if the Optional attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean optionalESet;

	/**
	 * The default value of the '{@link #getOs() <em>Os</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOs()
	 * @generated
	 * @ordered
	 */
	protected static final String OS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOs() <em>Os</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOs()
	 * @generated
	 * @ordered
	 */
	protected String os = OS_EDEFAULT;

	/**
	 * The default value of the '{@link #getSearchLocation() <em>Search Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSearchLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String SEARCH_LOCATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSearchLocation() <em>Search Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSearchLocation()
	 * @generated
	 * @ordered
	 */
	protected String searchLocation = SEARCH_LOCATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getWs() <em>Ws</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWs()
	 * @generated
	 * @ordered
	 */
	protected static final String WS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWs() <em>Ws</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWs()
	 * @generated
	 * @ordered
	 */
	protected String ws = WS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IncludeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EclipsePackage.Literals.INCLUDE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getArch() {
		return arch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setArch(String newArch) {
		String oldArch = arch;
		arch = newArch;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.INCLUDE__ARCH, oldArch, arch));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.INCLUDE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.INCLUDE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getNl() {
		return nl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNl(String newNl) {
		String oldNl = nl;
		nl = newNl;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.INCLUDE__NL, oldNl, nl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isOptional() {
		return optional;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOptional(boolean newOptional) {
		boolean oldOptional = optional;
		optional = newOptional;
		boolean oldOptionalESet = optionalESet;
		optionalESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.INCLUDE__OPTIONAL, oldOptional, optional, !oldOptionalESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetOptional() {
		boolean oldOptional = optional;
		boolean oldOptionalESet = optionalESet;
		optional = OPTIONAL_EDEFAULT;
		optionalESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EclipsePackage.INCLUDE__OPTIONAL, oldOptional, OPTIONAL_EDEFAULT, oldOptionalESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetOptional() {
		return optionalESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOs() {
		return os;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOs(String newOs) {
		String oldOs = os;
		os = newOs;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.INCLUDE__OS, oldOs, os));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSearchLocation() {
		return searchLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSearchLocation(String newSearchLocation) {
		String oldSearchLocation = searchLocation;
		searchLocation = newSearchLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.INCLUDE__SEARCH_LOCATION, oldSearchLocation, searchLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.INCLUDE__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getWs() {
		return ws;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setWs(String newWs) {
		String oldWs = ws;
		ws = newWs;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.INCLUDE__WS, oldWs, ws));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EclipsePackage.INCLUDE__ARCH:
				return getArch();
			case EclipsePackage.INCLUDE__ID:
				return getId();
			case EclipsePackage.INCLUDE__NAME:
				return getName();
			case EclipsePackage.INCLUDE__NL:
				return getNl();
			case EclipsePackage.INCLUDE__OPTIONAL:
				return isOptional();
			case EclipsePackage.INCLUDE__OS:
				return getOs();
			case EclipsePackage.INCLUDE__SEARCH_LOCATION:
				return getSearchLocation();
			case EclipsePackage.INCLUDE__VERSION:
				return getVersion();
			case EclipsePackage.INCLUDE__WS:
				return getWs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EclipsePackage.INCLUDE__ARCH:
				setArch((String)newValue);
				return;
			case EclipsePackage.INCLUDE__ID:
				setId((String)newValue);
				return;
			case EclipsePackage.INCLUDE__NAME:
				setName((String)newValue);
				return;
			case EclipsePackage.INCLUDE__NL:
				setNl((String)newValue);
				return;
			case EclipsePackage.INCLUDE__OPTIONAL:
				setOptional((Boolean)newValue);
				return;
			case EclipsePackage.INCLUDE__OS:
				setOs((String)newValue);
				return;
			case EclipsePackage.INCLUDE__SEARCH_LOCATION:
				setSearchLocation((String)newValue);
				return;
			case EclipsePackage.INCLUDE__VERSION:
				setVersion((String)newValue);
				return;
			case EclipsePackage.INCLUDE__WS:
				setWs((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EclipsePackage.INCLUDE__ARCH:
				setArch(ARCH_EDEFAULT);
				return;
			case EclipsePackage.INCLUDE__ID:
				setId(ID_EDEFAULT);
				return;
			case EclipsePackage.INCLUDE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case EclipsePackage.INCLUDE__NL:
				setNl(NL_EDEFAULT);
				return;
			case EclipsePackage.INCLUDE__OPTIONAL:
				unsetOptional();
				return;
			case EclipsePackage.INCLUDE__OS:
				setOs(OS_EDEFAULT);
				return;
			case EclipsePackage.INCLUDE__SEARCH_LOCATION:
				setSearchLocation(SEARCH_LOCATION_EDEFAULT);
				return;
			case EclipsePackage.INCLUDE__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
			case EclipsePackage.INCLUDE__WS:
				setWs(WS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EclipsePackage.INCLUDE__ARCH:
				return ARCH_EDEFAULT == null ? arch != null : !ARCH_EDEFAULT.equals(arch);
			case EclipsePackage.INCLUDE__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case EclipsePackage.INCLUDE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case EclipsePackage.INCLUDE__NL:
				return NL_EDEFAULT == null ? nl != null : !NL_EDEFAULT.equals(nl);
			case EclipsePackage.INCLUDE__OPTIONAL:
				return isSetOptional();
			case EclipsePackage.INCLUDE__OS:
				return OS_EDEFAULT == null ? os != null : !OS_EDEFAULT.equals(os);
			case EclipsePackage.INCLUDE__SEARCH_LOCATION:
				return SEARCH_LOCATION_EDEFAULT == null ? searchLocation != null : !SEARCH_LOCATION_EDEFAULT.equals(searchLocation);
			case EclipsePackage.INCLUDE__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
			case EclipsePackage.INCLUDE__WS:
				return WS_EDEFAULT == null ? ws != null : !WS_EDEFAULT.equals(ws);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (arch: ");
		result.append(arch);
		result.append(", id: ");
		result.append(id);
		result.append(", name: ");
		result.append(name);
		result.append(", nl: ");
		result.append(nl);
		result.append(", optional: ");
		if (optionalESet) result.append(optional); else result.append("<unset>");
		result.append(", os: ");
		result.append(os);
		result.append(", searchLocation: ");
		result.append(searchLocation);
		result.append(", version: ");
		result.append(version);
		result.append(", ws: ");
		result.append(ws);
		result.append(')');
		return result.toString();
	}

} //IncludeImpl
