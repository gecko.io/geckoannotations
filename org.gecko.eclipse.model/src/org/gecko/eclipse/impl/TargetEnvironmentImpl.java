/**
 */
package org.gecko.eclipse.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.gecko.eclipse.EclipsePackage;
import org.gecko.eclipse.TargetEnvironment;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Target Environment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.impl.TargetEnvironmentImpl#getArch <em>Arch</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.TargetEnvironmentImpl#getOs <em>Os</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.TargetEnvironmentImpl#getWs <em>Ws</em>}</li>
 *   <li>{@link org.gecko.eclipse.impl.TargetEnvironmentImpl#getNl <em>Nl</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TargetEnvironmentImpl extends MinimalEObjectImpl.Container implements TargetEnvironment {
	/**
	 * The default value of the '{@link #getArch() <em>Arch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArch()
	 * @generated
	 * @ordered
	 */
	protected static final String ARCH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getArch() <em>Arch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArch()
	 * @generated
	 * @ordered
	 */
	protected String arch = ARCH_EDEFAULT;

	/**
	 * The default value of the '{@link #getOs() <em>Os</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOs()
	 * @generated
	 * @ordered
	 */
	protected static final String OS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOs() <em>Os</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOs()
	 * @generated
	 * @ordered
	 */
	protected String os = OS_EDEFAULT;

	/**
	 * The default value of the '{@link #getWs() <em>Ws</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWs()
	 * @generated
	 * @ordered
	 */
	protected static final String WS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWs() <em>Ws</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWs()
	 * @generated
	 * @ordered
	 */
	protected String ws = WS_EDEFAULT;

	/**
	 * The default value of the '{@link #getNl() <em>Nl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNl()
	 * @generated
	 * @ordered
	 */
	protected static final String NL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNl() <em>Nl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNl()
	 * @generated
	 * @ordered
	 */
	protected String nl = NL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TargetEnvironmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EclipsePackage.Literals.TARGET_ENVIRONMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getArch() {
		return arch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setArch(String newArch) {
		String oldArch = arch;
		arch = newArch;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET_ENVIRONMENT__ARCH, oldArch, arch));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOs() {
		return os;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOs(String newOs) {
		String oldOs = os;
		os = newOs;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET_ENVIRONMENT__OS, oldOs, os));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getWs() {
		return ws;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setWs(String newWs) {
		String oldWs = ws;
		ws = newWs;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET_ENVIRONMENT__WS, oldWs, ws));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getNl() {
		return nl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNl(String newNl) {
		String oldNl = nl;
		nl = newNl;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclipsePackage.TARGET_ENVIRONMENT__NL, oldNl, nl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EclipsePackage.TARGET_ENVIRONMENT__ARCH:
				return getArch();
			case EclipsePackage.TARGET_ENVIRONMENT__OS:
				return getOs();
			case EclipsePackage.TARGET_ENVIRONMENT__WS:
				return getWs();
			case EclipsePackage.TARGET_ENVIRONMENT__NL:
				return getNl();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EclipsePackage.TARGET_ENVIRONMENT__ARCH:
				setArch((String)newValue);
				return;
			case EclipsePackage.TARGET_ENVIRONMENT__OS:
				setOs((String)newValue);
				return;
			case EclipsePackage.TARGET_ENVIRONMENT__WS:
				setWs((String)newValue);
				return;
			case EclipsePackage.TARGET_ENVIRONMENT__NL:
				setNl((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EclipsePackage.TARGET_ENVIRONMENT__ARCH:
				setArch(ARCH_EDEFAULT);
				return;
			case EclipsePackage.TARGET_ENVIRONMENT__OS:
				setOs(OS_EDEFAULT);
				return;
			case EclipsePackage.TARGET_ENVIRONMENT__WS:
				setWs(WS_EDEFAULT);
				return;
			case EclipsePackage.TARGET_ENVIRONMENT__NL:
				setNl(NL_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EclipsePackage.TARGET_ENVIRONMENT__ARCH:
				return ARCH_EDEFAULT == null ? arch != null : !ARCH_EDEFAULT.equals(arch);
			case EclipsePackage.TARGET_ENVIRONMENT__OS:
				return OS_EDEFAULT == null ? os != null : !OS_EDEFAULT.equals(os);
			case EclipsePackage.TARGET_ENVIRONMENT__WS:
				return WS_EDEFAULT == null ? ws != null : !WS_EDEFAULT.equals(ws);
			case EclipsePackage.TARGET_ENVIRONMENT__NL:
				return NL_EDEFAULT == null ? nl != null : !NL_EDEFAULT.equals(nl);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (arch: ");
		result.append(arch);
		result.append(", os: ");
		result.append(os);
		result.append(", ws: ");
		result.append(ws);
		result.append(", nl: ");
		result.append(nl);
		result.append(')');
		return result.toString();
	}

} //TargetEnvironmentImpl
