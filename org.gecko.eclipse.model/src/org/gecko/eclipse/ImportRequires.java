/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Import Requires</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.ImportRequires#getImport <em>Import</em>}</li>
 * </ul>
 *
 * @see org.gecko.eclipse.EclipsePackage#getImportRequires()
 * @model extendedMetaData="name='ImportRequires' kind='elementOnly'"
 * @generated
 */
public interface ImportRequires extends EObject {
	/**
	 * Returns the value of the '<em><b>Import</b></em>' containment reference list.
	 * The list contents are of type {@link org.gecko.eclipse.Import}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import</em>' containment reference list.
	 * @see org.gecko.eclipse.EclipsePackage#getImportRequires_Import()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='import' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Import> getImport();

} // ImportRequires
