/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Launcher Args</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.LauncherArgs#getProgramArgs <em>Program Args</em>}</li>
 *   <li>{@link org.gecko.eclipse.LauncherArgs#getVmArgs <em>Vm Args</em>}</li>
 * </ul>
 *
 * @see org.gecko.eclipse.EclipsePackage#getLauncherArgs()
 * @model extendedMetaData="name='LauncherArgs' kind='elementOnly'"
 * @generated
 */
public interface LauncherArgs extends EObject {
	/**
	 * Returns the value of the '<em><b>Program Args</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Program Args</em>' attribute.
	 * @see #setProgramArgs(String)
	 * @see org.gecko.eclipse.EclipsePackage#getLauncherArgs_ProgramArgs()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='programArgs' namespace='##targetNamespace'"
	 * @generated
	 */
	String getProgramArgs();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.LauncherArgs#getProgramArgs <em>Program Args</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Program Args</em>' attribute.
	 * @see #getProgramArgs()
	 * @generated
	 */
	void setProgramArgs(String value);

	/**
	 * Returns the value of the '<em><b>Vm Args</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vm Args</em>' attribute.
	 * @see #setVmArgs(String)
	 * @see org.gecko.eclipse.EclipsePackage#getLauncherArgs_VmArgs()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='vmArgs' namespace='##targetNamespace'"
	 * @generated
	 */
	String getVmArgs();

	/**
	 * Sets the value of the '{@link org.gecko.eclipse.LauncherArgs#getVmArgs <em>Vm Args</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vm Args</em>' attribute.
	 * @see #getVmArgs()
	 * @generated
	 */
	void setVmArgs(String value);

} // LauncherArgs
