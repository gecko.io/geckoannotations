/**
 */
package org.gecko.eclipse;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Target Locations</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.gecko.eclipse.TargetLocations#getLocation <em>Location</em>}</li>
 * </ul>
 *
 * @see org.gecko.eclipse.EclipsePackage#getTargetLocations()
 * @model extendedMetaData="name='TargetLocations' kind='elementOnly'"
 * @generated
 */
public interface TargetLocations extends EObject {
	/**
	 * Returns the value of the '<em><b>Location</b></em>' containment reference list.
	 * The list contents are of type {@link org.gecko.eclipse.TargetLocation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' containment reference list.
	 * @see org.gecko.eclipse.EclipsePackage#getTargetLocations_Location()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='location' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<TargetLocation> getLocation();

} // TargetLocations
