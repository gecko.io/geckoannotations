/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.eclipse.converter.tests;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.Collections;
import java.util.List;

import org.gecko.artifact.converter.FeatureLoader;
import org.gecko.eclipse.Artifact;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * <p>
 * This is a Demo Resource for a Jaxrs Whiteboard 
 * </p>
 * 
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
public class ArtifactLoadIntegrationTest {
	
	/**
	 * Creates a new instance.
	 * @param bundleContext
	 */
	public ArtifactLoadIntegrationTest() {
	}

	/**
	 * Here you can put everything you want to be exectued before every test
	 */
	@Before
	public void doBefore() {
	}

	/**
	 * Here you can put everything you want to be exectued after every test
	 */
	@After
	public void doAfter() {

	}
	
	public List<Artifact> getArtifacts(URI uri) {
		if (uri == null) {
			return Collections.emptyList();
		}
		try {
			FeatureLoader loader = new FeatureLoader(uri);
			return loader.getArtifacts();
		} catch (Exception e) {
			throw new IllegalStateException(String.format("Error getting artifacts for %s", uri ), e);
		}
	}
	
	public List<Artifact> getFeatureArtifacts(URI uri, boolean excludeSources) {
		if (uri == null) {
			return Collections.emptyList();
		}
		try {
			FeatureLoader loader = new FeatureLoader(uri);
			return loader.getFeatureArtifacts(excludeSources);
		} catch (Exception e) {
			throw new IllegalStateException(String.format("Error getting feature artifacts for %s", uri ), e);
		}
	}

	@Test
	public void testLoadEMFTooling() throws IOException, InvocationTargetException, InterruptedException {
		System.out.println("########## Gecko.io EMF Tooling ##########");
		long start = System.currentTimeMillis();
		try {
			List<Artifact> artifacts = getArtifacts(URI.create("https://devel.data-in-motion.biz/public/repository/gecko/release/geckoEMF.eclipse.tooling"));
			artifacts.stream().filter((a)->"org.eclipse.update.feature".equals(a.getClassifier())).map(Artifact::getId).forEach(System.out::println);
			System.out.println("Getting artifacts took " + (System.currentTimeMillis()-start) + " ms");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testLoadEclipseDIM() throws IOException, InvocationTargetException, InterruptedException {
		System.out.println("########## Eclipse 2019-09 ##########");
		long start = System.currentTimeMillis();
		try {
			URI uri = URI.create("https://devel.data-in-motion.biz/repository/eclipse-2019-09");
			List<Artifact> artifacts = getArtifacts(uri);
			long features = artifacts.stream()
					.filter((a)->"org.eclipse.update.feature".equals(a.getClassifier()))
					.filter(a->!a.getId().endsWith("source"))
					.count();
			assertEquals(210, features);
			assertEquals(210, getFeatureArtifacts(uri, true).size());
			System.out.println("Features " + features + ", " + (System.currentTimeMillis() - start) + " ms");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testLoadEclipseOrg() throws IOException, InvocationTargetException, InterruptedException {
		System.out.println("########## Eclipse 2019-09 ##########");
		long start = System.currentTimeMillis();
		try {
			URI uri = URI.create("http://download.eclipse.org/releases/2019-12");
			List<Artifact> artifacts = getArtifacts(uri);
			long features = artifacts.stream().filter((a)->"org.eclipse.update.feature".equals(a.getClassifier())).filter(a->!a.getId().endsWith("source")).count();
			assertEquals(837, features);
			assertEquals(837, getFeatureArtifacts(uri, true).size());
			System.out.println("Features " + features + ", " + (System.currentTimeMillis() - start) + " ms");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
