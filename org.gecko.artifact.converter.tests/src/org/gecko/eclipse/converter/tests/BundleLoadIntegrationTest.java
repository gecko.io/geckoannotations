/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.eclipse.converter.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.Collections;
import java.util.List;

import org.gecko.artifact.converter.FeatureLoader;
import org.gecko.core.tests.AbstractOSGiTest;
import org.gecko.eclipse.Artifact;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.FrameworkUtil;


/**
 * <p>
 * This is a Demo Resource for a Jaxrs Whiteboard 
 * </p>
 * 
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
public class BundleLoadIntegrationTest extends AbstractOSGiTest {
	
	/**
	 * Creates a new instance.
	 * @param bundleContext
	 */
	public BundleLoadIntegrationTest() {
		super(FrameworkUtil.getBundle(BundleLoadIntegrationTest.class).getBundleContext());
	}

	/**
	 * Here you can put everything you want to be exectued before every test
	 */
	public void doBefore() {
		System.out.println("test");
	}

	/**
	 * Here you can put everything you want to be exectued after every test
	 */
	public void doAfter() {

	}
	
	public List<Artifact> getFeatureArtifacts(URI uri, boolean excludeSources) {
		if (uri == null) {
			return Collections.emptyList();
		}
		try {
			FeatureLoader loader = new FeatureLoader(uri);
			return loader.getFeatureArtifacts(excludeSources);
		} catch (Exception e) {
			throw new IllegalStateException(String.format("Error getting feature artifacts for %s", uri ), e);
		}
	}
	
	@Test
	public void testLoadEMFToolingBundles() throws IOException, InvocationTargetException, InterruptedException {
		System.out.println("########## Gecko.io EMF Tooling ##########");
		long start = System.currentTimeMillis();
		try {
			FeatureLoader loader = new FeatureLoader(URI.create("https://devel.data-in-motion.biz/public/repository/gecko/release/geckoEMF.eclipse.tooling"));
			List<Artifact> artifacts = loader.getBundleArtifacts(true);
			assertEquals(8, artifacts.size());
			System.out.println("Getting bundle artifacts took " + (System.currentTimeMillis()-start) + " ms");
			artifacts.stream().map(Artifact::getId).forEach(System.out::println);
			start = System.currentTimeMillis();
			artifacts.stream().map(loader::getBundle).filter(f->f!=null).forEach(System.out::println);
			System.out.println(String.format("Loading %s bundles took %s ms", artifacts.size(), (System.currentTimeMillis()-start)));
		} catch (Exception e) {
			fail("Failed with exception " + e.getMessage());
		}
	}
	
//	@Test
	public void testLoadEclipseDIMBundles() throws IOException, InvocationTargetException, InterruptedException {
		System.out.println("########## Gecko.io Eclipse 2019-09 ##########");
		long start = System.currentTimeMillis();
		try {
			FeatureLoader loader = new FeatureLoader(URI.create("https://devel.data-in-motion.biz/repository/eclipse-2019-09"));
			List<Artifact> artifacts = loader.getBundleArtifacts(true);
			assertEquals(1095, artifacts.size());
			System.out.println("Getting bundle artifacts took " + (System.currentTimeMillis()-start) + " ms");
			artifacts.stream().map(Artifact::getId).forEach(System.out::println);
			start = System.currentTimeMillis();
			artifacts.stream().map(loader::getBundle).filter(f->f!=null).forEach(System.out::println);
			System.out.println(String.format("Loading %s bundles took %s ms", artifacts.size(), (System.currentTimeMillis()-start)));
			System.out.println("Loading bundles took " + (System.currentTimeMillis()-start) + " ms");
		} catch (Exception e) {
			e.printStackTrace();
			fail("Failed with exception " + e.getMessage());
		}
	}
	
//	@Test
	public void testLoadEclipseORGBundles() throws IOException, InvocationTargetException, InterruptedException {
		System.out.println("########## Eclipse.org Eclipse 2019-12 ##########");
		long start = System.currentTimeMillis();
		try {
			FeatureLoader loader = new FeatureLoader(URI.create("https://download.eclipse.org/releases/2019-12"));
			List<Artifact> artifacts = loader.getBundleArtifacts(true);
			assertEquals(6442, artifacts.size());
			System.out.println("Getting artifacts took " + (System.currentTimeMillis()-start) + " ms");
			start = System.currentTimeMillis();
			artifacts.stream().map(loader::getBundle).filter(f->f!=null).forEach(System.out::println);
			System.out.println(String.format("Loading %s bundle took %s ms", artifacts.size(), (System.currentTimeMillis()-start)));
		} catch (Exception e) {
			e.printStackTrace();
			fail("Failed with exception " + e.getMessage());
		}
	}

}
