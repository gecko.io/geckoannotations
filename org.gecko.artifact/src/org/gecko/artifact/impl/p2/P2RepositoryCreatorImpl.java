/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.impl.p2;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.gecko.artifact.api.RepositoryCreator;
import org.gecko.artifact.impl.p2.P2RepositoryCreator.CategoryData;
import org.osgi.framework.Version;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.log.Logger;
import org.osgi.service.log.LoggerFactory;

import aQute.bnd.osgi.Jar;

/**
 * 
 * @author mark
 * @since 13.10.2020
 */
@Component(immediate = true, property = {"artifactRepositoryType=P2"})
public class P2RepositoryCreatorImpl implements RepositoryCreator {
	
	private static final String targetPath = "/home/mark/tmp/test";
	private static final String path = "/home/mark/tmp/test";
	private static final String pluginFolder = path + "/orig";
	private Logger logger;

	/**
	 * Creates a new instance.
	 */
	@Activate
	public P2RepositoryCreatorImpl(@Reference(service = LoggerFactory.class)Logger logger) {
		this.logger = logger;
	}
	
	@Activate
	public void activate() {
		File plugins = new File(pluginFolder);
		if (plugins.isDirectory()) {
			File[] files = plugins.listFiles();
			List<Jar> pluginJars = new ArrayList<Jar>();
			for (File file : files) {
				try {
					Jar bundle = new Jar(file);
					pluginJars.add(bundle);
				} catch (IOException e) {
					logger.error("Cannot create Jar instance for file '{}'", file.getName(), e);
				}
			}
			File target = new File(targetPath);
			if (!target.exists()) {
				target.mkdirs();
			}
			try {
				P2RepositoryCreator creator = createRepository(targetPath, "my-test-repo", Version.parseVersion("14.0.0"), false);
				for (Jar jar : pluginJars) {
					creator.addJar(jar);
				}
				CategoryData category = new CategoryData();
				category.id = "org.gecko.p2";
				category.name = "Gecko bnd P2 creator plugin";
				category.version = "1.0.0";
				creator.withCategory(category).create();
			} catch (Exception e) {
				logger.error("Cannot create P2 creator for repository 'my-test-repo'", e);
			}
		} else {
			logger.warn("File path '{}' is no folder path");
		}
	}
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.artifact.api.P2RepositoryCreator#createRepository(java.lang.String, java.lang.String, org.osgi.framework.Version)
	 */
	@Override
	public void createRepository(String targetUri, String name, Version javaVersion) {
		try {
			createRepository(targetUri, name, javaVersion, false);
		} catch (Exception e) {
			logger.error("Error creating P2 repository with name {} at location {}", name, targetUri, e);
		}
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.artifact.api.P2RepositoryCreator#createRepositoryCompressed(java.lang.String, java.lang.String, org.osgi.framework.Version)
	 */
	@Override
	public void createRepositoryCompressed(String targetUri, String name, Version javaVersion) {
		try {
			createRepository(targetUri, name, javaVersion, true);
		} catch (Exception e) {
			logger.error("Error creating P2 repository with name {} at location {}", name, targetUri, e);
		}
	}
	
	private P2RepositoryCreator createRepository(String targetUri, String name, Version javaVersion, boolean compress) throws Exception {
		URI location = URI.create(targetUri);
		try {
			P2RepositoryCreator creator = new P2RepositoryCreator(location);
			return creator.javaVersion(javaVersion).name(name).compress(compress);
		} catch (Exception e) {
			throw e;
		}
	}

}
