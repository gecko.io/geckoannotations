/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.impl.p2;

import static org.gecko.artifact.api.ArtifactConstants.P2_ECLIPSE_TYPE_BUNDLE;
import static org.gecko.artifact.api.ArtifactConstants.P2_INSTRUCTION_INSTALL;
import static org.gecko.artifact.api.ArtifactConstants.P2_INSTRUCTION_MANIFEST;
import static org.gecko.artifact.api.ArtifactConstants.P2_LOCALIZATION_DFLT;
import static org.gecko.artifact.api.ArtifactConstants.P2_NAMESPACE_EE;
import static org.gecko.artifact.api.ArtifactConstants.P2_NAMESPACE_IU;
import static org.gecko.artifact.api.ArtifactConstants.P2_NAMESPACE_PACKAGE;
import static org.gecko.artifact.api.ArtifactConstants.P2_PROP_ARTIFACT_SIZE;
import static org.gecko.artifact.api.ArtifactConstants.P2_PROP_DOWNLOAD_CHECKSUM_MD5;
import static org.gecko.artifact.api.ArtifactConstants.P2_PROP_DOWNLOAD_CHECKSUM_SHA256;
import static org.gecko.artifact.api.ArtifactConstants.P2_PROP_DOWNLOAD_MD5;
import static org.gecko.artifact.api.ArtifactConstants.P2_PROP_DOWNLOAD_SIZE;
import static org.gecko.artifact.api.ArtifactConstants.P2_PROP_LOCALIZATION;
import static org.gecko.artifact.api.ArtifactConstants.P2_PROP_MAVEN_ARTIFACT;
import static org.gecko.artifact.api.ArtifactConstants.P2_PROP_MAVEN_GROUP;
import static org.gecko.artifact.api.ArtifactConstants.P2_PROP_MAVEN_VERSION;
import static org.gecko.artifact.api.ArtifactConstants.P2_PROP_NAME;
import static org.gecko.artifact.api.ArtifactConstants.P2_PROP_PROVIDER;
import static org.gecko.artifact.api.ArtifactConstants.P2_PROP_TYPE;
import static org.gecko.artifact.api.ArtifactConstants.P2_TOUCHPOINT_NATIVE;
import static org.gecko.artifact.api.ArtifactConstants.P2_TOUCHPOINT_OSGI;
import static org.gecko.artifact.api.ArtifactConstants.P2_TOUCHPOINT_VERSION;
import static org.gecko.artifact.api.ArtifactConstants.P2_TYPE_ECLIPSE;
import static org.gecko.artifact.api.ArtifactConstants.P2_TYPE_FRAGMENT;
import static org.gecko.artifact.api.ArtifactConstants.P2_TYPE_LOCALIZATION;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.stream.Collectors;

import org.gecko.eclipse.Artifact;
import org.gecko.eclipse.Artifacts;
import org.gecko.eclipse.EclipseFactory;
import org.gecko.eclipse.Instruction;
import org.gecko.eclipse.InstructionsType;
import org.gecko.eclipse.Properties;
import org.gecko.eclipse.Property;
import org.gecko.eclipse.Provided;
import org.gecko.eclipse.Provides;
import org.gecko.eclipse.Required;
import org.gecko.eclipse.RequiredProperties;
import org.gecko.eclipse.Requires;
import org.gecko.eclipse.Rule;
import org.gecko.eclipse.Touchpoint;
import org.gecko.eclipse.TouchpointDataType;
import org.gecko.eclipse.Unit;
import org.osgi.framework.Constants;
import org.osgi.framework.namespace.IdentityNamespace;

import aQute.bnd.header.Attrs;
import aQute.bnd.header.OSGiHeader;
import aQute.bnd.header.Parameters;
import aQute.bnd.osgi.Jar;
import aQute.bnd.osgi.Resource;
import aQute.libg.cryptography.MD5;
import aQute.libg.cryptography.SHA256;

/**
 * Helper for the creation of a artifacts.xml and content.xml of a P2 update site
 * @author Mark Hoffmann
 * @since 12.10.2020
 */
public class P2CreationHelper {

	private static final String JAVA_PACKAGES = "java.applet,java.awt,java.awt.color,java.awt.datatransfer,"
			+ "java.awt.desktop,java.awt.dnd,java.awt.event,java.awt.font,java.awt.geom,java.awt.im,java.awt.im.spi,"
			+ "java.awt.image,java.awt.image.renderable,java.awt.print,java.beans,java.beans.beancontext,java.io,"
			+ "java.lang,java.lang.annotation,java.lang.instrument,java.lang.invoke,java.lang.management,java.lang.module,"
			+ "java.lang.ref,java.lang.reflect,java.math,java.net,java.net.spi,java.nio,java.nio.channels,"
			+ "java.nio.channels.spi,java.nio.charset,java.nio.charset.spi,java.nio.file,java.nio.file.attribute,"
			+ "java.nio.file.spi,java.rmi,java.rmi.activation,java.rmi.dgc,java.rmi.registry,java.rmi.server,java.security,"
			+ "java.security.acl,java.security.cert,java.security.interfaces,java.security.spec,java.sql,java.text,"
			+ "java.text.spi,java.time,java.time.chrono,java.time.format,java.time.temporal,java.time.zone,java.util,"
			+ "java.util.concurrent,java.util.concurrent.atomic,java.util.concurrent.locks,java.util.function,java.util.jar,"
			+ "java.util.logging,java.util.prefs,java.util.regex,java.util.spi,java.util.stream,java.util.zip,"
			+ "javax.accessibility,javax.annotation.processing,javax.crypto,javax.crypto.interfaces,javax.crypto.spec,"
			+ "javax.imageio,javax.imageio.event,javax.imageio.metadata,javax.imageio.plugins.bmp,javax.imageio.plugins.jpeg,"
			+ "javax.imageio.spi,javax.imageio.stream,javax.lang.model,javax.lang.model.element,javax.lang.model.type,"
			+ "javax.lang.model.util,javax.management,javax.management.loading,javax.management.modelmbean,"
			+ "javax.management.monitor,javax.management.openmbean,javax.management.relation,javax.management.remote,"
			+ "javax.management.remote.rmi,javax.management.timer,javax.naming,javax.naming.directory,javax.naming.event,"
			+ "javax.naming.ldap,javax.naming.spi,javax.net,javax.net.ssl,javax.print,javax.print.attribute,"
			+ "javax.print.attribute.standard,javax.print.event,javax.rmi.ssl,javax.script,javax.security.auth,"
			+ "javax.security.auth.callback,javax.security.auth.kerberos,javax.security.auth.login,javax.security.auth.spi,"
			+ "javax.security.auth.x500,javax.security.cert,javax.security.sasl,javax.sound.midi,javax.sound.midi.spi,"
			+ "javax.sound.sampled,javax.sound.sampled.spi,javax.sql,javax.sql.rowset,javax.sql.rowset.serial,"
			+ "javax.sql.rowset.spi,javax.swing,javax.swing.border,javax.swing.colorchooser,javax.swing.event,"
			+ "javax.swing.filechooser,javax.swing.plaf,javax.swing.plaf.basic,javax.swing.plaf.metal,javax.swing.plaf.multi,"
			+ "javax.swing.plaf.nimbus,javax.swing.plaf.synth,javax.swing.table,javax.swing.text,javax.swing.text.html,"
			+ "javax.swing.text.html.parser,javax.swing.text.rtf,javax.swing.tree,javax.swing.undo,javax.tools,"
			+ "javax.transaction.xa,javax.xml,javax.xml.crypto,javax.xml.crypto.dom,javax.xml.crypto.dsig,"
			+ "javax.xml.crypto.dsig.dom,javax.xml.crypto.dsig.keyinfo,javax.xml.crypto.dsig.spec,javax.xml.datatype,"
			+ "javax.xml.namespace,javax.xml.parsers,javax.xml.stream,javax.xml.stream.events,javax.xml.stream.util,"
			+ "javax.xml.transform,javax.xml.transform.dom,javax.xml.transform.sax,javax.xml.transform.stax,"
			+ "javax.xml.transform.stream,javax.xml.validation,javax.xml.xpath,org.ietf.jgss,org.w3c.dom,org.w3c.dom.bootstrap,"
			+ "org.w3c.dom.css,org.w3c.dom.events,org.w3c.dom.html,org.w3c.dom.ls,org.w3c.dom.ranges,org.w3c.dom.stylesheets,"
			+ "org.w3c.dom.traversal,org.w3c.dom.views,org.w3c.dom.xpath,org.xml.sax,org.xml.sax.ext,org.xml.sax.helpers";
	private static final String JAVA_VERSION_9 = "9.0.0";
	private static final String JAVA_VERSION_10 = "10.0.0";
	private static final String JAVA_VERSION_11 = "11.0.0";
	private static final String JAVA_VERSION_12 = "12.0.0";
	private static final String JAVA_VERSION_13 = "13.0.0";
	private static final String JAVA_VERSION_14 = "14.0.0";
	private static final String EE_OSGI_MIN = "OSGi/Minimum";
	private static final String EE_JRE = "JRE";
	private static final String EE_JAVASE = "JavaSE";
	private static final String EE_COMPACT = "JavaSE/compact%s";
	private static final String JRE_UNIT_ID = "a.jre.javase";
	private static final String JRE_CONFIG_UNIT_ID = "config." + JRE_UNIT_ID;
	private static final String TP_MANIFEST_DATA = "Bundle-SmbolicName: %s&#xA;Bundle-Version: %s";
	private static final List<String> POST_JAVA8_VERSION = new ArrayList<String>(4);
	private static final List<String> ALL_POST_JAVA8_VERSION = new ArrayList<String>(6);

	static {
		POST_JAVA8_VERSION.add(JAVA_VERSION_9);
		POST_JAVA8_VERSION.add(JAVA_VERSION_11);
		POST_JAVA8_VERSION.add(JAVA_VERSION_13);
		POST_JAVA8_VERSION.add(JAVA_VERSION_14);

		ALL_POST_JAVA8_VERSION.add(JAVA_VERSION_9);
		ALL_POST_JAVA8_VERSION.add(JAVA_VERSION_10);
		ALL_POST_JAVA8_VERSION.add(JAVA_VERSION_11);
		ALL_POST_JAVA8_VERSION.add(JAVA_VERSION_12);
		ALL_POST_JAVA8_VERSION.add(JAVA_VERSION_13);
		ALL_POST_JAVA8_VERSION.add(JAVA_VERSION_14);
	}

	/**
	 * If we run on Java9 or larger, we need additional units. This method creates this units and the corresponding
	 * configuration units
	 * @return the set of unit and configuration units for the JRE's
	 */
	public static final Set<Unit> createPostJava8Units(String maxVersion) {
		int idx = POST_JAVA8_VERSION.indexOf(maxVersion);
		if (idx > -1) {
			return POST_JAVA8_VERSION.subList(0, idx + 1).stream()
					.map(P2CreationHelper::createJREUnits)
					.flatMap(Set::stream)
					.collect(Collectors.toSet());
		} else {
			return POST_JAVA8_VERSION.stream()
					.map(P2CreationHelper::createJREUnits)
					.flatMap(Set::stream)
					.collect(Collectors.toSet());
		}
	}

	/**
	 * Creates a JRE unit and its configuration unit, for the given version
	 * @param version the version to create the units for
	 * @return the jre unit and the configuration unit
	 */
	public static Set<Unit> createJREUnits(String version) {
		Unit jreUnit = createJREUnit(version);
		Unit jreConfigUnit = createJREConfigUnit(version);
		Set<Unit> units = new HashSet<Unit>(2);
		units.add(jreUnit);
		units.add(jreConfigUnit);
		return units;
	}

	/**
	 * Create the {@link Properties} section in an {@link Artifact}
	 * @param artifact the artifact to create the properties for
	 * @param bundleJar the bundle that represents the artifact
	 * @throws Exception thrown on errors
	 */
	public static void createArtifactProperties(Artifact artifact, Jar bundleJar) throws Exception {
		Properties properties = EclipseFactory.eINSTANCE.createProperties();
		artifact.setProperties(properties);
		// create file size properties
		long size = bundleJar.getSource().length();
		Property property = P2CreationHelper.createProperty(P2_PROP_ARTIFACT_SIZE, Long.toString(size));
		properties.getProperty().add(property);
		property = P2CreationHelper.createProperty(P2_PROP_DOWNLOAD_SIZE, Long.toString(size));
		properties.getProperty().add(property);
		// create file hashes
		File file = bundleJar.getSource();
		String md5 = MD5.digest(file).asHex().toLowerCase();
		property = P2CreationHelper.createProperty(P2_PROP_DOWNLOAD_MD5, md5);
		properties.getProperty().add(property);
		property = P2CreationHelper.createProperty(P2_PROP_DOWNLOAD_CHECKSUM_MD5, md5);
		properties.getProperty().add(property);
		String sha256 = SHA256.digest(file).asHex().toLowerCase();
		property = P2CreationHelper.createProperty(P2_PROP_DOWNLOAD_CHECKSUM_SHA256, sha256);
		properties.getProperty().add(property);
		// create Maven related properties
		Set<Property> mavenProps = P2CreationHelper.createContentMavenProperties(properties, bundleJar);
		properties.getProperty().addAll(mavenProps);
		properties.setSize(properties.getProperty().size());

	}

	/**
	 * Creates the properties section for a {@link Unit} in the content.xml
	 * @param unit the {@link Unit} to create the properties for
	 * @param bundleJar the corresponding bundle
	 * @throws Exception thrown on errors
	 */
	public static void createContentProperties(Unit unit, Jar bundleJar) throws Exception {
		Properties properties = EclipseFactory.eINSTANCE.createProperties();
		unit.setProperties(properties);
		Property property;
		Manifest manifest = bundleJar.getManifest();
		// bundle name 
		Optional<String> bundleName = P2CreationHelper.getManifestValue(manifest, Constants.BUNDLE_NAME);
		if (bundleName.isPresent()) {
			property = createProperty(P2_PROP_NAME, bundleName.get());
			properties.getProperty().add(property);
		}
		// bundle vendor 
		Optional<String> bundleVendor = P2CreationHelper.getManifestValue(manifest, Constants.BUNDLE_VENDOR);
		if (bundleVendor.isPresent()) {
			property = createProperty(P2_PROP_PROVIDER, bundleVendor.get());
			properties.getProperty().add(property);
		}
		// bundle localization 
		Optional<String> localization = P2CreationHelper.getManifestValue(manifest, Constants.BUNDLE_LOCALIZATION);
		if (localization.isPresent()) {
			property = createProperty(P2_PROP_LOCALIZATION, localization.get());
			properties.getProperty().add(property);
			// add properties for the substituted values from the localization property file
			Set<Property> replacements = createBundlePropertyReplacement(localization.get(), bundleName.orElse(null), bundleVendor.orElse(null), bundleJar);
			properties.getProperty().addAll(replacements);
		}
		// Maven related properties
		Set<Property> mavenProps = P2CreationHelper.createContentMavenProperties(properties, bundleJar);
		properties.getProperty().addAll(mavenProps);
		properties.setSize(properties.getProperty().size());
	}
	
	/**
	 * Create a property instance
	 * @param key the property key
	 * @param value the property value
	 * @return the property instance
	 */
	public static Property createProperty(String key, String value) {
		Property property = EclipseFactory.eINSTANCE.createProperty();
		property.setName(key);
		property.setValue(value);
		return property;
	}

	/**
	 * Create the {@link Requires} for a {@link Unit} in the content.xml
	 * @param unit the unit the requirements are for
	 * @param bundleJar the bundle that represents the {@link Unit}
	 * @throws Exception thrown on errors
	 */
	public static void createContentRequires(Unit unit, Jar bundleJar) throws Exception {
		Requires requires = EclipseFactory.eINSTANCE.createRequires();
		unit.setRequires(requires);
		// create import package requires
		createContentImportPackage(requires, bundleJar);
		// create requirement requires
		createContentCapabilitiesRequires(requires, bundleJar);

		requires.setSize(requires.getRequired().size() + requires.getRequiredProperties().size());
	}

	/**
	 * Creates the capabilities for a given {@link Unit}
	 * @param unit the {@link Unit} instance
	 * @param bundleJar the bundle that represents the {@link Unit}
	 * @throws Exception thrown on errors
	 */
	public static void createContentProvides(Unit unit, Jar bundleJar) throws Exception {
		Provides provides = EclipseFactory.eINSTANCE.createProvides();
		unit.setProvides(provides);
		// create installable unit default provides
		Provided provided = createProvided(P2_NAMESPACE_IU, bundleJar.getBsn(), bundleJar.getVersion());
		provides.getProvided().add(provided);
		provided = createProvided(IdentityNamespace.TYPE_BUNDLE, bundleJar.getBsn(), bundleJar.getVersion());
		provides.getProvided().add(provided);
		// identity namespace contains properties
		provided = createProvided(IdentityNamespace.IDENTITY_NAMESPACE, bundleJar.getBsn(), bundleJar.getVersion());
		Properties properties = EclipseFactory.eINSTANCE.createProperties();
		properties.setSize(1);
		Property property = P2CreationHelper.createProperty(P2_PROP_TYPE, IdentityNamespace.TYPE_BUNDLE);
		properties.getProperty().add(property);
		provided.setProperties(properties);
		provides.getProvided().add(provided);

		// create export package capabilities
		createContentExportPackage(provides, bundleJar);

		provided = createProvided(P2_TYPE_ECLIPSE, P2_ECLIPSE_TYPE_BUNDLE, "1.0.0");
		provides.getProvided().add(provided);
		// create bundle localization provided
		Optional<String> localization = getManifestValue(bundleJar.getManifest(), Constants.BUNDLE_LOCALIZATION);
		if (localization.isPresent()) {
			provided = createProvided(P2_TYPE_LOCALIZATION, P2_LOCALIZATION_DFLT, "1.0.0");
			provides.getProvided().add(provided);
		}
		// create capability provides
		createContentCapabilitiesProvides(provides, bundleJar);

		provides.setSize(provides.getProvided().size());
	}

	/**
	 * Create a capability for a certain namespace, name and version
	 * @param namespace the capability namespace
	 * @param name the capability name
	 * @param version the capability version
	 * @return return the {@link Provided} instance
	 */
	public static Provided createProvided(String namespace, String name, String version) {
		Provided provided = EclipseFactory.eINSTANCE.createProvided();
		provided.setNamespace(namespace);
		provided.setName(name);
		provided.setVersion(version);
		return provided;
	}

	/**
	 * Creates a requirement for a certain namespace, name and version range
	 * @param namespace the requirement namespace
	 * @param name the name of the requirement
	 * @param range the version range
	 * @param optional the resolution optional
	 * @param greedy
	 * @return the {@link Required} instance
	 */
	public static Required createRequired(String namespace, String name, String range, boolean optional, boolean greedy) {
		Required required = createRequired(namespace, name, range);
		required.setOptional(optional);
		required.setGreedy(greedy);
		return required;
	}

	/**
	 * Creates a requirement for a certain namespace, name and version range
	 * @param namespace the requirement namespace
	 * @param name the name of the requirement
	 * @param range the version range
	 * @return the {@link Required} instance
	 */
	public static Required createRequired(String namespace, String name, String range) {
		Required required = EclipseFactory.eINSTANCE.createRequired();
		required.setNamespace(namespace);
		required.setName(name);
		required.setRange(range);
		return required;
	}

	/**
	 * Create an instruction rule for a given filter and path
	 * @param filter the rule filter
	 * @param path the rule path
	 * @return the {@link Rule} instance
	 */
	public static Rule createRule(String filter, String path) {
		Rule rule = EclipseFactory.eINSTANCE.createRule();
		rule.setFilter(filter);
		rule.setOutput(path);
		return rule;
	}

	/**
	 * Creates a {@link Artifacts} section in a {@link Unit} of content.xml
	 * @param unit the {@link Unit}
	 * @param bundleJar the bundle, that represent this {@link Unit}
	 * @throws Exception thrown, when an error occurs
	 */
	public static void createContentUnitArtifacts(Unit unit, Jar bundleJar) throws Exception {
		Artifacts artifacts = EclipseFactory.eINSTANCE.createArtifacts();
		artifacts.setSize(1);
		unit.setArtifacts(artifacts);
		Artifact artifact = EclipseFactory.eINSTANCE.createArtifact();
		artifact.setClassifier(IdentityNamespace.TYPE_BUNDLE);
		artifact.setId(bundleJar.getBsn());
		artifact.setVersion(bundleJar.getVersion());
		artifacts.getArtifact().add(artifact);
	}

	/**
	 * Create properties with the substituted bundle name and bundle vendor from the properties file,
	 * defined in the bundle localization header
	 * @param localization the localization base name
	 * @param nameKey the key for the bundle name {@link String}
	 * @param vendorKey the key for the bundle vendor {@link String}
	 * @param bundleJar the bundle jar
	 * @return the set of properties that contain the replaced values
	 */
	public static Set<Property> createBundlePropertyReplacement(String localization, String nameKey, String vendorKey, Jar bundleJar) {
		Set<Property> result = new HashSet<Property>();
		// get the properties file from the JAR
		String resourceName = localization + ".properties";
		Resource resource = bundleJar.getResource(resourceName);
		if (resource == null) {
			return result;
		}
		// load properties file
		java.util.Properties p = new java.util.Properties();
		try (InputStream is = resource.openInputStream()) {
			p.load(is);
			// read value for bundle name key
			Optional<Property> property = getBundleProperty(p, nameKey);
			property.map(result::add);
			// read value for bundle vendor key
			property = getBundleProperty(p, vendorKey);
			property.map(result::add);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Returns the value for a manifest entry
	 * @param manifest the manifest instance
	 * @param key the manifest entry key
	 * @return {@link Optional} for the value
	 */
	public static Optional<String> getManifestValue(Manifest manifest, String key) {
		Attributes mainAttributes = manifest.getMainAttributes();
		String value = mainAttributes.getValue(key);
		return Optional.ofNullable(value);
	}

	/**
	 * Creates the Maven-Properties, if there are any. We check for pom.properties file
	 * and read the corresponding data out of it.
	 * @param properties
	 * @param bundleJar
	 */
	public static Set<Property> createContentMavenProperties(Properties properties, Jar bundleJar) {
		Set<Property> result = new HashSet<Property>();
		// find the pom.properties in the JAR
		Optional<Resource> pomData = bundleJar.getResources((rs)->rs.endsWith("pom.properties")).findFirst();
		if (pomData.isPresent()) {
			Property property;
			Resource pomResource = pomData.get();
			// load pom.properties from JAR
			java.util.Properties pomProperties = new java.util.Properties();
			try (InputStream is = pomResource.openInputStream()) {
				pomProperties.load(is);
				String value = pomProperties.getProperty("groupId");
				if (value != null) {
					property = createProperty(P2_PROP_MAVEN_GROUP, value);
					result.add(property);
				}
				value = pomProperties.getProperty("artifactId");
				if (value != null) {
					property = createProperty(P2_PROP_MAVEN_ARTIFACT, value);
					result.add(property);
				}
				value = pomProperties.getProperty("version");
				if (value != null) {
					property = createProperty(P2_PROP_MAVEN_VERSION, value);
					result.add(property);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * Create an OSGi touchpoint
	 * @param unit the unit this touchpoint is for
	 * @param bundleJar the bundle 
	 * @throws Exception throw non errors
	 */
	public static void createOSGiTouchpoint(Unit unit, Jar bundleJar) throws Exception {
		Touchpoint tp = EclipseFactory.eINSTANCE.createTouchpoint();
		unit.setTouchpoint(tp);
		tp.setId(P2_TOUCHPOINT_OSGI);
		tp.setVersion(P2_TOUCHPOINT_VERSION);
		unit.setTouchpointData(createManifestTouchpointData(bundleJar));
	}

	/**
	 * Create a jre unit
	 * @param version the java version this unit is for
	 * @return the {@link Unit} instance
	 */
	private static Unit createJREUnit(String version) {
		Unit jreUnit = EclipseFactory.eINSTANCE.createUnit();
		jreUnit.setId(JRE_UNIT_ID);
		jreUnit.setVersion(version);
		jreUnit.setSingleton(false);
		// touchpoints
		Touchpoint tp = EclipseFactory.eINSTANCE.createTouchpoint();
		jreUnit.setTouchpoint(tp);
		tp.setId(P2_TOUCHPOINT_NATIVE);
		tp.setVersion(P2_TOUCHPOINT_VERSION);
		if (JAVA_VERSION_9.equals(version)) {
			jreUnit.setId(JRE_UNIT_ID.replace(".javase", ""));
			return jreUnit;
		}
		// provides
		Provides provides = EclipseFactory.eINSTANCE.createProvides();
		jreUnit.setProvides(provides);
		Provided prov = createProvided(P2_NAMESPACE_IU, JRE_UNIT_ID, version);
		provides.getProvided().add(prov);
		String[] packages = JAVA_PACKAGES.split(",");
		for (String pckg : packages) {
			prov = createProvided(P2_NAMESPACE_PACKAGE, pckg, "0.0.0");
			provides.getProvided().add(prov);
		}
		provides.getProvided().addAll(createAllEEProvided(version));
		provides.setSize(provides.getProvided().size());
		return jreUnit;
	}

	/**
	 * Create the configuration {@link Unit} for a JRE with the given version
	 * @param version the JRE version
	 * @return the {@link Unit} instance
	 */
	private static Unit createJREConfigUnit(String version) {
		Unit jreUnit = EclipseFactory.eINSTANCE.createUnit();
		if (JAVA_VERSION_9.equals(version)) {
			jreUnit.setId(JRE_CONFIG_UNIT_ID.replace(".javase", ""));
		} else {
			jreUnit.setId(JRE_CONFIG_UNIT_ID);
		}
		jreUnit.setVersion(version);
		jreUnit.setSingleton(false);
		// host requirements
		Requires hostRequires = EclipseFactory.eINSTANCE.createRequires();
		hostRequires.setSize(1);
		Required required = createRequired(P2_NAMESPACE_IU, JRE_UNIT_ID, version);
		hostRequires.getRequired().add(required);
		jreUnit.setHostRequirements(hostRequires);
		// properties
		Properties properties = EclipseFactory.eINSTANCE.createProperties();
		jreUnit.setProperties(properties);
		properties.setSize(1);
		Property prop = createProperty(P2_TYPE_FRAGMENT, Boolean.TRUE.toString());
		properties.getProperty().add(prop);
		// provides
		Provides provides = EclipseFactory.eINSTANCE.createProvides();
		jreUnit.setProvides(provides);
		provides.setSize(1);
		Provided prov = createProvided(P2_NAMESPACE_IU, JRE_CONFIG_UNIT_ID, version);
		provides.getProvided().add(prov);
		// requires
		Requires requires = EclipseFactory.eINSTANCE.createRequires();
		jreUnit.setRequires(requires);
		requires.setSize(1);
		Required req = createRequired(P2_NAMESPACE_IU, JRE_UNIT_ID, version);
		requires.getRequired().add(req);
		// touchpoints
		Touchpoint tp = EclipseFactory.eINSTANCE.createTouchpoint();
		jreUnit.setTouchpoint(tp);
		tp.setId(P2_TOUCHPOINT_NATIVE);
		tp.setVersion(P2_TOUCHPOINT_VERSION);
		// touchpoint data
		TouchpointDataType tpd = EclipseFactory.eINSTANCE.createTouchpointDataType();
		jreUnit.setTouchpointData(tpd);
		tpd.setSize(1);
		InstructionsType instructions = EclipseFactory.eINSTANCE.createInstructionsType();
		tpd.getInstructions().add(instructions);
		instructions.setSize(1);
		Instruction i = EclipseFactory.eINSTANCE.createInstruction();
		instructions.getInstruction().add(i);
		i.setKey(P2_INSTRUCTION_INSTALL);
		return jreUnit;
	}

	/**
	 * Creates all {@link Provided} for the execution environment of the given version
	 * @param version the EE version
	 * @return the {@link Set} of {@link Provided} instances that are calculated for a certain version
	 */
	private static Set<Provided> createAllEEProvided(String version) {
		String[] osgiMin = {"1.0.0", "1.1.0", "1.2.0"};
		String[] jre = {"1.0.0", "1.1.0"};
		String[] javase = {"1.0.0", "1.1.0", "1.2.0", "1.3.0", "1.4.0", "1.5.0", "1.6.0", "1.7.0", "1.8.0"};
		String[] compact = {"1.8.0", "9.0.0", "10.0.0", "11.0.0"};
		Set<Provided> provided = new HashSet<Provided>();
		provided.addAll(createEEProvided(EE_OSGI_MIN, Arrays.asList(osgiMin)));
		provided.addAll(createEEProvided(EE_JRE, Arrays.asList(jre)));
		provided.addAll(createEEProvided(EE_JAVASE, Arrays.asList(javase)));
		int idx = ALL_POST_JAVA8_VERSION.indexOf(version);
		if (idx > -1) {
			provided.addAll(createEEProvided(EE_JAVASE, ALL_POST_JAVA8_VERSION.subList(0, idx + 1)));
		}
		for (int i = 1; i < 4; i++) {
			String eeName = String.format(EE_COMPACT, i);
			provided.addAll(createEEProvided(eeName, Arrays.asList(compact)));
		}
		return provided;
	}

	/**
	 * Create the {@link Provided} instance for a set of versions and provided name like JRE, JavaSE
	 * @param name the provided name
	 * @param versions the version
	 * @return the {@link Set} of {@link Provided} instances
	 */
	private static Set<Provided> createEEProvided(String name, List<String> versions) {
		return versions.stream()
				.map(v->P2CreationHelper.createEEProvided(name, v))
				.collect(Collectors.toSet());
	}

	/**
	 * Create capability for the Execution Environment
	 * @param name the name
	 * @param version the EE version
	 * @return the {@link Provided} instance
	 */
	private static Provided createEEProvided(String name, String version) {
		return createProvided(P2_NAMESPACE_EE, name, version);
	}

	/**
	 * Create {@link Provides} for export package declaration
	 * @param provides the holder for all exports
	 * @param bundleJar the bundle
	 * @throws Exception thrown on errors
	 */
	private static void createContentExportPackage(Provides provides, Jar bundleJar) throws Exception {
		Parameters exports = getManifestParameters(bundleJar, Constants.EXPORT_PACKAGE);
		for (String exp : exports.keySet()) {
			String name = exp;
			Attrs attributes = exports.get(name);
			String version = attributes.get("version");
			if (name != null) {
				version = version == null ? "0.0.0" : version;
				Provided provided = createProvided(P2_NAMESPACE_PACKAGE, name, version);
				provides.getProvided().add(provided);
			}
		}
	}

	/**
	 * Create import package declaration
	 * @param requires the holder for {@link Required}
	 * @param bundleJar the bundle 
	 * @throws Exception thrown on errors
	 */
	private static void createContentImportPackage(Requires requires, Jar bundleJar) throws Exception {
		Parameters imports = getManifestParameters(bundleJar, Constants.IMPORT_PACKAGE);
		for (String imp : imports.keySet()) {
			String name = imp;
			Attrs attributes = imports.get(name);
			String range = attributes.get("version");
			String resolution = attributes.get("resolution:");
			if (name != null) {
				range = range == null ? "0.0.0" : range;
				Required required;
				if (resolution != null) {
					required = createRequired(P2_NAMESPACE_PACKAGE, name, range, "optional".equals(resolution), "greedy".equals(resolution));
				} else {
					required = createRequired(P2_NAMESPACE_PACKAGE, name, range);
				}
				requires.getRequired().add(required);
			}
		}
	}

	/**
	 * Create the capability provided for the content.xml
	 * @param provides the holder for the {@link Provided}
	 * @param bundleJar the bundle
	 * @throws Exception thrown on errorss
	 */
	private static void createContentCapabilitiesProvides(Provides provides, Jar bundleJar) throws Exception {
		Parameters capabilities = getManifestParameters(bundleJar, Constants.PROVIDE_CAPABILITY);
		for (String namespace : capabilities.keySet()) {
			Attrs attributes = capabilities.get(namespace);
			String name = attributes.get(namespace);
			String version = attributes.get("version");
			if (name != null) {
				version = version == null ? "0.0.0" : version;
				Provided provided = createProvided(namespace, name, version);
				provides.getProvided().add(provided);
			}
		}
	}

	/**
	 * Create requirements for the content.xml
	 * @param requires the holder for the {@link Required}
	 * @param bundleJar the bundle
	 * @throws Exception thrown on errors
	 */
	private static void createContentCapabilitiesRequires(Requires requires, Jar bundleJar) throws Exception {
		Parameters capabilities = getManifestParameters(bundleJar, Constants.REQUIRE_CAPABILITY);
		for (String namespace : capabilities.keySet()) {
			Attrs attributes = capabilities.get(namespace);
			String match = attributes.get("filter:");
			String ns = namespace.replaceAll("~", "");
			if (match != null) {
				RequiredProperties required = EclipseFactory.eINSTANCE.createRequiredProperties();
				required.setMatch(match);
				required.setNamespace(ns);
				requires.getRequiredProperties().add(required);
			}
		}
	}

	/**
	 * Creates 'manifest' touchpoint data
	 * @param bundleJar the bundle
	 * @return the touchpoint data instance
	 * @throws Exception thrown on errors
	 */
	private static TouchpointDataType createManifestTouchpointData(Jar bundleJar) throws Exception {
		TouchpointDataType tpd = EclipseFactory.eINSTANCE.createTouchpointDataType();
		tpd.setSize(1);
		InstructionsType instructions = EclipseFactory.eINSTANCE.createInstructionsType();
		tpd.getInstructions().add(instructions);
		instructions.setSize(1);
		Instruction i = EclipseFactory.eINSTANCE.createInstruction();
		instructions.getInstruction().add(i);
		i.setKey(P2_INSTRUCTION_MANIFEST);
		i.setValue(String.format(TP_MANIFEST_DATA, bundleJar.getBsn(), bundleJar.getVersion()));
		return tpd;
	}

	/**
	 * Creates a property for a bundle-property subsitution
	 * @param properties the content of the bundle localization property file
	 * @param key the property key
	 * @return the value {@link Optional}
	 */
	private static Optional<Property> getBundleProperty(java.util.Properties properties, String key) {
		if (properties == null) {
			throw new IllegalArgumentException("Properties fiel must not be null");
		}
		if (key != null) {
			String v = properties.getProperty(key.replace("%", ""));
			if (v != null) {
				String k = key.replace("%", "df_LT.");
				return Optional.of(createProperty(k, v));
			}
		}
		return Optional.empty();
	}

	/**
	 * Returns the {@link Parameters} for a certain manifest header
	 * @param jar the bundle of the manifest
	 * @param header the manifest header
	 * @return the {@link Parameters} instance
	 * @throws Exception thrown on errors
	 */
	private static Parameters getManifestParameters(Jar jar, String header) throws Exception {
		Manifest m = jar.getManifest();
		if (m == null) {
			return new Parameters();
		}
		return OSGiHeader.parseHeader(m.getMainAttributes()
				.getValue(header));
	}

}
