/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.impl;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;

import org.gecko.artifact.api.ArtifactLoader;
import org.gecko.artifact.api.ResolvedArtifactLoader;
import org.gecko.artifacts.ArtifactRepository;
import org.gecko.artifacts.EclipseFeature;
import org.gecko.artifacts.P2Repository;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.util.promise.Promise;
import org.osgi.util.promise.PromiseFactory;

/**
 * Loader that chaches the artifacts
 * @author Mark Hoffmann
 * @since 04.03.2020
 */
@Component(property = {"artifactRepositoryType=cached"})
public class CachedArtifactsLoader implements ArtifactLoader {
	
	@Reference(target = "(artifactRepositoryType=P2)")
	private ResolvedArtifactLoader loader;
	private static Map<String, ArtifactRepository> repositoryMap = new ConcurrentHashMap<String, ArtifactRepository>();

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.artifact.api.ArtifactLoader#loadRepository(java.lang.String)
	 */
	@Override
	public ArtifactRepository loadRepository(String baseUri) {
		ArtifactRepository repository = repositoryMap.get(baseUri);
		if (repository == null) {
			repository = loader.loadResolvedRepository(baseUri);
			if (repository != null) {
				repositoryMap.put(baseUri, repository);
			}
		}
		return repository;
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.artifact.api.ArtifactLoader#loadRepositoryAsync(java.lang.String)
	 */
	@Override
	public Promise<ArtifactRepository> loadRepositoryAsync(String baseUri) {
		PromiseFactory pf = new PromiseFactory(Executors.newCachedThreadPool());
		try {
			ArtifactRepository repository = loadRepository(baseUri);
			return pf.resolved(repository);
		} catch (Exception e) {
			return pf.failed(e);
		}
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.artifact.api.ArtifactLoader#loadFeatures(java.lang.String)
	 */
	@Override
	public List<EclipseFeature> loadFeatures(String baseUri) {
		P2Repository repository = (P2Repository) loadRepository(baseUri);
		if (repository != null) {
			return repository.getEclipseFeatures();
		} else {
			return Collections.emptyList();
		}
	}

}
