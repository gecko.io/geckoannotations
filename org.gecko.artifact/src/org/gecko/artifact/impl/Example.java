/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.impl;

import org.gecko.artifact.api.ArtifactLoader;
import org.gecko.artifacts.P2Repository;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Reference;

import aQute.bnd.osgi.Processor;

/**
 * 
 * @author mark
 * @since 02.03.2020
 */
//@Component(immediate = true)
public class Example {
	
	@Reference(target = "(artifactRepositoryType=cached)")
	private ArtifactLoader loader;
//	@Reference
//	private MessagingService messaging;
	
	@Activate
	public void activate() {
		try {
			Processor.getPromiseFactory().resolved(null).delay(3000).onResolve(()->{
				try {
					P2Repository repository = (P2Repository) loader.loadRepository("https://devel.data-in-motion.biz/repository/eclipse-2019-09");
//					ArtifactRepository repository = loader.loadRepository("http://download.eclipse.org/releases/2019-12");
					System.out.println("Repos: " + repository);
					System.out.println("Repo parent: " + repository.getParent());
//					System.out.println("Repo parent-parent: " + repository.getParent().getParent());
					System.out.println("Repo size: " + repository.getRepositories().size());
					System.out.println("Root Artifacts size: " + repository.getArtifacts().size());
					if (repository.getRepositories().size() == 2) {
						System.out.println("Artifacts size: " + repository.getRepositories().get(0).getArtifacts().size());
						System.out.println("Artifacts size: " + repository.getRepositories().get(1).getArtifacts().size());
					}
					System.out.println("Root All Artifacts size: " + repository.getAllArtifacts().size());
					System.out.println("Features size: " + repository.getEclipseFeatures().size());
//					List<EclipseFeature> features = loader.loadFeatures("https://devel.data-in-motion.biz/repository/eclipse-2019-09");
//					List<EclipseFeature> features = loader.loadFeatures("https://devel.data-in-motion.biz/public/repository/gecko/release/geckoEMF.eclipse.tooling");
//					List<EclipseFeature> features = loader.loadFeatures("http://download.eclipse.org/releases/2019-12");
					
//					repository.getAllArtifacts().stream()
//						.filter(a->a instanceof EclipseFeature)
//						.map(a->(EclipseFeature)a)
//						.filter(f->!f.getName().endsWith("source"))
//						.forEach(loader.);
//					messaging.subscribe("dim/test").forEach(m->{
//						String s = new String(m.payload().array());
//						System.out.println("Received: " + s);
//					});
//					messaging.publish("dim/test", ByteBuffer.wrap("Hello".getBytes()));
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
