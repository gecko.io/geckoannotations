/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.api;

import org.gecko.artifacts.ArtifactReference;
import org.gecko.artifacts.EclipseFeature;

/**
 * constants interface
 * @author Mark Hoffmann
 * @since 02.03.2020
 */
public interface ArtifactConstants {
	
	public static final String P2_ARTIFACTS_TYPE = "org.eclipse.equinox.p2.artifact.repository.simpleRepository";
	public static final String P2_CONTENT_TYPE = "org.eclipse.equinox.internal.p2.metadata.repository.LocalMetadataRepository";
	public static final String P2_NAMESPACE_IU = "org.eclipse.equinox.p2.iu";
	public static final String P2_NAMESPACE_PACKAGE = "java.package";
	public static final String P2_NAMESPACE_EE = "osgi.ee";
	public static final String P2_TYPE_FRAGMENT = "org.eclipse.equinox.p2.type.fragment";
	public static final String P2_TYPE_CATEGORY = "org.eclipse.equinox.p2.type.category";
	public static final String P2_TYPE_ECLIPSE = "org.eclipse.equinox.p2.eclipse.type";
	public static final String P2_TYPE_LOCALIZATION = "org.eclipse.equinox.p2.localization";
	public static final String P2_ECLIPSE_TYPE_BUNDLE = "bundle";
	public static final String P2_TOUCHPOINT_NATIVE = "org.eclipse.equinox.p2.native";
	public static final String P2_TOUCHPOINT_OSGI = "org.eclipse.equinox.p2.osgi";
	public static final String P2_TOUCHPOINT_VERSION = "1.0.0";
	public static final String P2_INSTRUCTION_INSTALL = "install";
	public static final String P2_INSTRUCTION_MANIFEST = "manifest";
	public static final String P2_LOCALIZATION_DFLT = "df_LT";

	public static final String P2_INDEX_FILE = "p2.index";
	public static final String P2_TIMESTAMP = "p2.timestamp";
	public static final String P2_COMPRESSED = "p2.compressed";
	
	public static final String P2_ARTIFACTS_COMPOSITES_FILE = "compositeArtifacts.xml";
	public static final String P2_ARTIFACTS_FILE = "artifacts.xml";
	public static final String P2_ARTIFACTS_XZ_FILE = P2_ARTIFACTS_FILE + ".xz";
	public static final String P2_CONTENT_COMPOSITES_FILE = "compositeContents.xml";
	public static final String P2_CONTENT_FILE = "content.xml";
	public static final String P2_CONTENT_XZ_FILE = P2_CONTENT_FILE + ".xz";
	public static final String P2_FEATURE_FILE = "feature.xml";
	public static final String P2_FEATURE_PROP_FILE = "feature.properties";
	
	public static final String P2_ECLIPSE_FEATURE = "org.eclipse.update.feature";
	public static final String P2_OSGI_BUNDLE = "osgi.bundle";
	public static final String P2_BINARY = "binary";
	
	public static final String P2_PROP_ARTIFACT_CONTENT_TYPE = "download.contentType";
	public static final String P2_PROP_ARTIFACT_REFERENCE = "artifact.reference";
	public static final String P2_PROP_ARTIFACT_SIZE = "artifact.size";
	public static final String P2_PROP_DOWNLOAD_CHECKSUM_MD5 = "download.checksum.md5";
	public static final String P2_PROP_DOWNLOAD_CHECKSUM_SHA256 = "download.checksum.sha-256";
	public static final String P2_PROP_DOWNLOAD_MD5 = "download.md5";
	public static final String P2_PROP_DOWNLOAD_SIZE = "download.size";
	public static final String P2_PROP_NAME = "org.eclipse.equinox.p2.name";
	public static final String P2_PROP_PROVIDER = "org.eclipse.equinox.p2.provider";
	public static final String P2_PROP_LOCALIZATION = "org.eclipse.equinox.p2.bundle.localization";
	public static final String P2_PROP_MAVEN_GROUP = "maven-groupId";
	public static final String P2_PROP_MAVEN_ARTIFACT = "maven-artifactId";
	public static final String P2_PROP_MAVEN_VERSION = "maven-version";
	public static final String P2_PROP_TYPE = "type";
	
	public static final String APPLICATION_ZIP = "application/zip";
	
	/** 
	 * Property that is usually set, if the original source location  that was determined was wrong and substituted
	 * by a correct one. In that case the original location can be stored using this key  
	 */
	public static final String PROP_ARTIFACTS_ORIG_SOURCE = "artifacts.orig.source.location";
	/**
	 * Property for {@link ArtifactReference} that mark a version validity range hint.
	 * For {@link EclipseFeature} it could be equivalent, perfect, greaterOrEqual, compatible
	 */
	public static final String PROP_TARGET_VERSION_HINT = "artifacts.target.version.hint";
	

}
