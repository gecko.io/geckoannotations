/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.api;

import org.gecko.artifacts.ArtifactRepository;
import org.osgi.annotation.versioning.ProviderType;
import org.osgi.util.promise.Promise;

/**
 * Artifact loader interface
 * @author Mark Hoffmann
 * @since 02.03.2020
 */
@ProviderType
public interface ResolvedArtifactLoader {

	public ArtifactRepository loadResolvedRepository(String baseUri);
	
	public Promise<ArtifactRepository> loadResolvedRepositoryAsync(String baseUri);
	
}
