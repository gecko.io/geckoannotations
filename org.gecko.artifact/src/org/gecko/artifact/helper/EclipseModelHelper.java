/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.BasicExtendedMetaData;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.gecko.eclipse.DocumentRoot;
import org.gecko.eclipse.EclipseFactory;
import org.gecko.eclipse.EclipsePackage;
import org.gecko.eclipse.util.EclipseResourceFactoryImpl;

/**
 * Model helper class that helps to load Eclipse XML data
 * @author Mark Hoffmann
 * @since 27.02.2020
 */
public class EclipseModelHelper {
	
	private final static Logger logger = Logger.getLogger(EclipseModelHelper.class.getName());
	
	public static ResourceSet createResourceSet() {
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put
		(Resource.Factory.Registry.DEFAULT_EXTENSION, 
				new EclipseResourceFactoryImpl());
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put
		("ecore", 
				new EcoreResourceFactoryImpl());

		// Register the package to ensure it is available during loading.
		//
		resourceSet.getPackageRegistry().put
		(EclipsePackage.eNS_URI, 
				EclipsePackage.eINSTANCE);
		resourceSet.getPackageRegistry().put
		(EcorePackage.eNS_URI, 
				EcorePackage.eINSTANCE);
		BasicExtendedMetaData bemd = new BasicExtendedMetaData() {
			@Override
			protected boolean isFeatureNamespaceMatchingLax() {
				return true;
			}
			@Override
			public EPackage getPackage(String namespace) {
				return EclipsePackage.eINSTANCE;
			}
			
		};
		resourceSet.getLoadOptions().put(XMLResource.OPTION_EXTENDED_META_DATA, bemd);
		return resourceSet;
	}
	
	public static DocumentRoot createDocumentRoot(ResourceSet resourceSet, File file, String resourceName) throws IOException {
		if (resourceSet == null) {
			logger.log(Level.WARNING, String.format("Cannot get XML document root for %s with a null resource set", resourceName));
			return null;
		}
		if (file == null) {
			logger.log(Level.WARNING, String.format("Cannot get XML document root for %s with a file", resourceName));
			return null;
		}
		if (!file.exists() || !file.canRead()) {
			logger.log(Level.WARNING, String.format("Cannot get XML document root for %s with a file that does not exist or is not accessible ", resourceName));
			return null;
		}
		return createDocumentRoot(resourceSet, file.getAbsoluteFile(), resourceName);
	}
	
	public static DocumentRoot getDocumentRoot(ResourceSet resourceSet, File file, String resourceName) throws IOException {
		if (resourceSet == null) {
			logger.log(Level.WARNING, String.format("Cannot get XML document root for %s with a null resource set", resourceName));
			return null;
		}
		if (file == null) {
			logger.log(Level.WARNING, String.format("Cannot get XML document root for %s with a file", resourceName));
			return null;
		}
		if (!file.exists() || !file.canRead()) {
			logger.log(Level.WARNING, String.format("Cannot get XML document root for %s with a file that does not exist or is not accessible ", resourceName));
			return null;
		}
		try (FileInputStream fis = new FileInputStream(file)) {
			return getDocumentRoot(resourceSet, fis, resourceName);
		}
	}
	
	public static DocumentRoot getDocumentRoot(ResourceSet resourceSet, URL url, String resourceName) throws IOException {
		if (resourceSet == null) {
			logger.log(Level.WARNING, String.format("Cannot get XML document root for %s with a null resource set", resourceName));
			return null;
		}
		if (url == null) {
			logger.log(Level.WARNING, String.format("Cannot get XML document root for %s with a null url", resourceName));
			return null;
		}
		return getDocumentRoot(resourceSet, url.openStream(), resourceName);
	}
	
	public static DocumentRoot getDocumentRoot(ResourceSet resourceSet, InputStream inputStream, String resourceName) throws IOException {
		if (resourceSet == null) {
			logger.log(Level.WARNING, String.format("Cannot get XML document root for %s with a null resource set", resourceName));
			return null;
		}
		if (inputStream == null) {
			logger.log(Level.WARNING, String.format("Cannot get XML document root for %s with a null input stream", resourceName));
			return null;
		}
		Resource resource = null;
		try {
			resource = resourceSet.createResource(URI.createFileURI(resourceName));
			// Use the load options to load XML without schema definition
			resource.load(inputStream, resourceSet.getLoadOptions());
			return resource.getContents().isEmpty() ? null : (DocumentRoot) resource.getContents().get(0);
		} finally {
			// Just for memory optimization, detach loaded objects from cache
			if (resource != null) {
				resource.getContents().clear();
				resourceSet.getResources().remove(resource);
			}
		}
	}
	
	public static DocumentRoot createDocumentRoot(ResourceSet resourceSet, String filepath, String resourceName) throws IOException {
		if (resourceSet == null) {
			logger.log(Level.WARNING, String.format("Cannot get XML document root for %s with a null resource set", resourceName));
			return null;
		}
		String uri = (filepath.endsWith("/") ? filepath : filepath + "/") + resourceName;
		Resource resource = null;
		resource = resourceSet.createResource(URI.createFileURI(uri));
		DocumentRoot root = EclipseFactory.eINSTANCE.createDocumentRoot();
		root.getXMLNSPrefixMap().put("", EclipsePackage.eNS_URI);
		resource.getContents().add(root);
		return root;
	}

}
