/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact.helper;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.gecko.artifact.api.ArtifactConstants;
import org.gecko.artifacts.Artifact;
import org.gecko.artifacts.ArtifactsPackage;
import org.osgi.framework.Version;
import org.osgi.framework.VersionRange;

/**
 * Helper class for artifacts
 * @author Mark Hoffmann
 * @since 12.03.2020
 */
public class ArtifactsHelper implements ArtifactConstants {
	
	private Version baseVersion;
	private String match;
	private String filter;
	private static final Map<String, EClass> artifactTypeMap = new HashMap<String, EClass>();
	
	static {
		artifactTypeMap.put(P2_ECLIPSE_FEATURE, ArtifactsPackage.Literals.ECLIPSE_FEATURE);
		artifactTypeMap.put(P2_OSGI_BUNDLE, ArtifactsPackage.Literals.BUNDLE_ARTIFACT);
		artifactTypeMap.put(P2_BINARY, ArtifactsPackage.Literals.BINARY_ARTIFACT);
	}
	
	private ArtifactsHelper(Version base, String match, String filter) {
		this.baseVersion = base;
		this.match = match;
		this.filter = filter;
	}
	
	public static ArtifactsHelper createArtifactsHelper(String version, String match, String filter) {
		Version v = Version.parseVersion(version);
		return new ArtifactsHelper(v, match, filter);
	}
	
	public static VersionRange createVersionRange(String rangeString) {
		if (rangeString == null) {
			return null;
		}
		return VersionRange.valueOf(rangeString);
	}
	
	public static boolean isArtifactType(Artifact artifact, String type) {
		if (type == null || artifact == null) {
			return false;
		}
		EClass eclass = artifactTypeMap.get(type);
		if (eclass == null) {
			return false;
		} else {
			return eclass.equals(artifact.eClass());
		}
	}
	
	/**
	 * Compares the version to the base version depending on the match rule
	 * @param version the version to compare
	 * @param baseVersion the version to compare against
	 * @param match the match rule
	 * @return <code>true</code>, if the version range matches
	 */
	public static boolean isVersionRange(String version, String baseVersion, String match) {
		if (match == null || baseVersion == null || (baseVersion == null && version == null)) {
			return true;
		}
		Version v = Version.parseVersion(version);
		int vMaj = v.getMajor();
		int vMin = v.getMinor();
		Version vc = Version.parseVersion(baseVersion);
		int vcMaj = vc.getMajor();
		int vcMin = vc.getMinor();
		switch (match) {
		case "equivalent":
			return vcMaj == vMaj && vcMin == vMin;
		case "compatible":
			return vcMaj == vMaj;
		case "perfect":
			return vc.compareTo(v) == 0;
		case "greaterOrEqual":
			return vMaj >= vcMaj && vMin >= vcMin;
		case "none":
			return true;
		default:
			return false;
		}
	}
	
	public boolean filterVersion(Artifact artifact) {
		if (artifact == null) {
			return true;
		}
		return isVersionRange(artifact.getVersion(), baseVersion.toString(), match);
	}
	
	public boolean filterVersion(String version) {
		return isVersionRange(version, baseVersion.toString(), match);
	}
	
	public boolean isArtifactType(Artifact artifact) {
		return isArtifactType(artifact, filter);
	}
	
	public static String getPackageSegments(String name) {
		String[] segs = name.split("\\.");
		if (segs.length == 0) {
			return null;
		}
		String last = segs[segs.length - 1];
		return name.replace("." + last, "");
	}
	
	public static String getImportSegments(String name) {
		String[] segs = name.split("\\.");
		if (segs.length == 0) {
			return null;
		}
		String last = segs[segs.length - 1];
		return name.replace("." + last, "." + toFirstUpper(last));
	}
	
	public static String getFeatureName(String name) {
		String[] segs = name.split("\\.");
		if (segs.length == 0) {
			return null;
		}
		String last = segs[segs.length - 1];
		return toFirstUpper(last);
	}
	
	private static String toFirstUpper(String string) {
		if (string == null || string.isEmpty()) {
			return string;
		}
		return string.substring(0, 1).toUpperCase() + string.substring(1);
	}
	
}
