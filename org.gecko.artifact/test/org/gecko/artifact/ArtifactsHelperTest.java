/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.artifact;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.gecko.artifact.helper.ArtifactsHelper;
import org.junit.Test;

public class ArtifactsHelperTest {

	@Test(expected = IllegalArgumentException.class)
	public void testEclipseVersion_WrongVersion() {
		String version1 = "a.b.c.";
		ArtifactsHelper.isVersionRange(version1, version1, "exact");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testEclipseVersion_NullVersionMatch() {
		String version1 = "a.b.c.";
		assertFalse(ArtifactsHelper.isVersionRange(null, version1, "exact"));
		assertFalse(ArtifactsHelper.isVersionRange(version1, null, "exact"));
	}
	
	@Test
	public void testEclipseVersion_NullMatch() {
		String version1 = "a.b.c.";
		assertTrue(ArtifactsHelper.isVersionRange(version1, version1, null));
		assertTrue(ArtifactsHelper.isVersionRange(version1, null, null));
		assertTrue(ArtifactsHelper.isVersionRange(null, version1, null));
		assertTrue(ArtifactsHelper.isVersionRange(null, null, null));
	}
	
	@Test
	public void testEclipseVersion_WrongMatch() {
		String version1 = "1.2";
		assertFalse(ArtifactsHelper.isVersionRange(version1, version1, "exact"));
	}
	
	@Test
	public void testEclipseVersion_Perfect() {
		String version1 = "1.2.3.test";
		String version2 = "1.2.3.test4";
		assertTrue(ArtifactsHelper.isVersionRange(version1, version1, "perfect"));
		assertFalse(ArtifactsHelper.isVersionRange(version2, version1, "perfect"));
	}
	
	@Test
	public void testEclipseVersion_Equivalent() {
		String version1 = "1.2.3.test";
		String version2 = "1.2.3.test4";
		String version3 = "1.3.3.test4";
		String version4 = "1.2.12";
		assertTrue(ArtifactsHelper.isVersionRange(version1, version1, "equivalent"));
		assertTrue(ArtifactsHelper.isVersionRange(version2, version1, "equivalent"));
		assertTrue(ArtifactsHelper.isVersionRange(version4, version1, "equivalent"));
		assertFalse(ArtifactsHelper.isVersionRange(version3, version1, "equivalent"));
	}
	
	@Test
	public void testEclipseVersion_Compatible() {
		String version1 = "1.2.3.test";
		String version2 = "1.2.3.test4";
		String version3 = "1.3.3.test4";
		String version4 = "1.2.12";
		String version5 = "4.2.12";
		assertTrue(ArtifactsHelper.isVersionRange(version1, version1, "compatible"));
		assertTrue(ArtifactsHelper.isVersionRange(version2, version1, "compatible"));
		assertTrue(ArtifactsHelper.isVersionRange(version4, version1, "compatible"));
		assertTrue(ArtifactsHelper.isVersionRange(version3, version1, "compatible"));
		assertFalse(ArtifactsHelper.isVersionRange(version5, version1, "compatible"));
	}
	
	@Test
	public void testEclipseVersion_GOE() {
		String version1 = "2.2.3.test";
		String version2 = "2.2.3.test";
		String version3 = "2.3.3.test4";
		String version4 = "2.2.12";
		String version5 = "4.2.12";
		String version6 = "1.2.12";
		assertTrue(ArtifactsHelper.isVersionRange(version1, version1, "greaterOrEqual"));
		assertTrue(ArtifactsHelper.isVersionRange(version2, version1, "greaterOrEqual"));
		assertTrue(ArtifactsHelper.isVersionRange(version4, version1, "greaterOrEqual"));
		assertTrue(ArtifactsHelper.isVersionRange(version3, version1, "greaterOrEqual"));
		assertTrue(ArtifactsHelper.isVersionRange(version5, version1, "greaterOrEqual"));
		assertFalse(ArtifactsHelper.isVersionRange(version6, version1, "greaterOrEqual"));
	}
	
}
