/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.bnd.target.converter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.gecko.artifact.helper.EclipseModelHelper;
import org.gecko.eclipse.DocumentRoot;
import org.gecko.eclipse.Target;

/**
 * 
 * @author mark
 * @since 12.05.2020
 */
public class TargetToBndConverter {
	
	private static String TARGET_FILE_EXTENSION = "target";
//	private static String BND_FILE_EXTENSION = "bnd";
//	private static final Logger logger = Logger.getLogger(TargetToBndConverter.class.getName());
	private final ResourceSet resourceSet;
	
	public TargetToBndConverter() {
		resourceSet = EclipseModelHelper.createResourceSet();
	}
	
	public void convertTarget(String targetPath, String bndPath) {
		File targetFile = new File(targetPath);
		File bndFile = new File(bndPath);
		convertTarget(targetFile, bndFile);
	}
	
	public void convertTarget(File targetDefinition, File bndFile) {
		if (targetDefinition == null) {
			throw new IllegalStateException("Target definition file is null");
		}
		if (bndFile == null) {
			throw new IllegalStateException("Bnd file path is null");
		}
		if (!targetDefinition.exists()) {
			throw new IllegalStateException(String.format("Cannot target file does not exist '%s'", targetDefinition.toString()));
		}
		if (!targetDefinition.canRead()) {
			throw new IllegalStateException(String.format("Cannot read target file '%s'", targetDefinition.toString()));
		}
		if (bndFile.exists() && !bndFile.canWrite()) {
			throw new IllegalStateException(String.format("Cannot write bnd file '%s'", bndFile.toString()));
		}
		if (!targetDefinition.getPath().endsWith(TARGET_FILE_EXTENSION)) {
			throw new IllegalStateException(String.format("Target file path doesn't end with '%s'", TARGET_FILE_EXTENSION));
		}
		String targetPath = targetDefinition.getPath();
//		Resource resource = resourceSet.createResource(URI.createURI(targetPath));
//		if (resource == null) {
//			throw new IllegalStateException(String.format("Cannot create resource for file path '%s'", targetPath));
//		}
		try (FileOutputStream fos = new FileOutputStream(bndFile)){
//			resource.load(null);
//			if (resource.getContents().isEmpty()) {
//				logger.log(Level.SEVERE, String.format("Loaded target resource seems empty for file '%s'", targetPath));
//				return;
//			}
			DocumentRoot root = EclipseModelHelper.getDocumentRoot(resourceSet, targetDefinition, "target.target");
			Target target = (Target) root.getTarget();
			TargetToBnd t2b  = new TargetToBnd();
			CharSequence resultSeq = t2b.convertTarget(target);
			fos.write(resultSeq.toString().getBytes("UTF-8"));
			fos.flush();
		} catch (IOException e) {
			throw new IllegalStateException(String.format("Error loading resource for file path '%s'. Message: '%s'", targetPath, e.getLocalizedMessage()), e);
		} catch (Exception e) {
			throw new IllegalStateException(String.format("Error converting target '%s'. Message: '%s'", targetPath, e.getLocalizedMessage()), e);
		}
	}

}
