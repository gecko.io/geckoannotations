/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.bnd.target.converter

import org.gecko.eclipse.Target

/**
 * Creates a bnd file with P2 repositories out of an Eclipse target definition
 * @author Mark Hoffmann
 * @since 12.05.2020
 */
class TargetToBnd {
	
	def CharSequence convertTarget(Target target) {
		target.toBndFile
	}
	
	def toBndFile(Target target) {
		var cnt = 0;
'''
 «FOR location : target.locations.location»
 -plugin.«cnt»: \
 	aQute.bnd.repository.p2.provider.P2Repository; \
 		name = "Location-«cnt++»";\
 		url=«location.repository.location»
 «ENDFOR»

'''
	}
	
}