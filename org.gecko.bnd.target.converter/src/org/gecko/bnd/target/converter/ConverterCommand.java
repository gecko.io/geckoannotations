/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.bnd.target.converter;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.osgi.service.component.annotations.*;

@Component(property= {"osgi.command.scope=TargetConverter", "osgi.command.function=convertTarget"}, service=Object.class)
public class ConverterCommand {
	
	private static final Logger logger = Logger.getLogger(ConverterCommand.class.getName());

	public void convertTarget(String targetPath, String bndPath) {
		try {
			TargetToBndConverter converter = new TargetToBndConverter();
			converter.convertTarget(targetPath, bndPath);
			logger.log(Level.INFO, String.format("Converted target '%s' into bnd repository definition '%s'", targetPath, bndPath));
		} catch (Exception e) {
			logger.log(Level.SEVERE, String.format("Error converting target '%s' into bnd repository definition '%s'; Message: %s", targetPath, bndPath, e.getMessage()));
		}
	}

}
