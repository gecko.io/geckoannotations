/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved.
 * 
 * This program and the accompanying materials are made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.bnd.target.converter;

import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.gecko.eclipse.Target;
import org.gecko.eclipse.TargetLocation;

/**
 * Creates a bnd file with P2 repositories out of an Eclipse target definition
 * @author Mark Hoffmann
 * @since 12.05.2020
 */
@SuppressWarnings("all")
public class TargetToBnd {
  public CharSequence convertTarget(final Target target) {
    return this.toBndFile(target);
  }
  
  public CharSequence toBndFile(final Target target) {
    CharSequence _xblockexpression = null;
    {
      int cnt = 0;
      StringConcatenation _builder = new StringConcatenation();
      {
        EList<TargetLocation> _location = target.getLocations().getLocation();
        for(final TargetLocation location : _location) {
          _builder.append("-plugin.");
          _builder.append(cnt);
          _builder.append(": \\");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("aQute.bnd.repository.p2.provider.P2Repository; \\");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("name = \"Location-");
          int _plusPlus = cnt++;
          _builder.append(_plusPlus, "\t\t");
          _builder.append("\";\\");
          _builder.newLineIfNotEmpty();
          _builder.append("\t\t");
          _builder.append("url=");
          String _location_1 = location.getRepository().getLocation();
          _builder.append(_location_1, "\t\t");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.newLine();
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
}
