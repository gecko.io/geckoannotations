/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.bnd.target.converter;

import static org.junit.Assert.*;

import org.gecko.eclipse.EclipseFactory;
import org.gecko.eclipse.RepositoryRef;
import org.gecko.eclipse.Target;
import org.gecko.eclipse.TargetLocation;
import org.gecko.eclipse.TargetLocations;
import org.junit.Test;

public class TargetToBndTest {

	@Test
	public void testTarget() {
		Target target = EclipseFactory.eINSTANCE.createTarget();
		TargetLocations locations = EclipseFactory.eINSTANCE.createTargetLocations();
		target.setLocations(locations);
		TargetLocation loc01 = EclipseFactory.eINSTANCE.createTargetLocation();
		RepositoryRef repo01 = EclipseFactory.eINSTANCE.createRepositoryRef();
		repo01.setLocation("http://test.de");
		loc01.setRepository(repo01);
		TargetLocation loc02 = EclipseFactory.eINSTANCE.createTargetLocation();
		RepositoryRef repo02 = EclipseFactory.eINSTANCE.createRepositoryRef();
		repo02.setLocation("http://google.de");
		loc02.setRepository(repo02);
		locations.getLocation().add(loc01);
		locations.getLocation().add(loc02);
		TargetToBnd t2b = new TargetToBnd();
		CharSequence result = t2b.convertTarget(target);
		assertNotNull(result);
		String resultString = result.toString();
		assertTrue(resultString.contains("-plugin.0"));
		assertTrue(resultString.contains("-plugin.1"));
		assertTrue(resultString.contains("name = \"Location-0\";\\"));
		assertTrue(resultString.contains("name = \"Location-1\";\\"));
		assertTrue(resultString.contains("url=http://test.de"));
		assertTrue(resultString.contains("url=http://google.de"));
	}

}
